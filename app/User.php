<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract,
                                    Auditable
{
    use Authenticatable, Authorizable, CanResetPassword;
    use \OwenIt\Auditing\Auditable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user', 'name', 'email', 'password', 'Dependencia_idDependencia'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $auditInclude = [
        'user', 
        'name', 
        'email', 
        'Dependencia_idDependencia'
    ];

    public function setPasswordAttribute($value)
    {
        if( is_null(request()->password) or request()->password == '' )
            $this->attributes['password'] = request()->claveOriginal;
        else
            $this->attributes['password'] = ($value);
    }
}
