<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class SubSerie extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'subserie';
    protected $primaryKey = 'idSubSerie';
    protected $fillable = ['codigoSubSerie', 'nombreSubSerie', 'directorioSubSerie' , 'descripcionSubSerie', 'Compania_idCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'codigoSubSerie', 
        'nombreSubSerie', 
        'directorioSubSerie', 
        'descripcionSubSerie'
    ];
}
