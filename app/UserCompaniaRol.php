<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class UserCompaniaRol extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $table = 'userscompaniarol';
    protected $primaryKey = 'idUsersCompaniaRol';

    protected $fillable = ['Users_id', 'Compania_idCompania', 'Rol_idRol'];

    public $timestamps = false;

    protected $auditInclude = [
        'Compania_idCompania', 
        'Rol_idRol'
    ];
}
