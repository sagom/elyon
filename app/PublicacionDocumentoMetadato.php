<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PublicacionDocumentoMetadato extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'publicaciondocumentometadato';
    protected $primaryKey = 'idPublicacionDocumentoMetadato';

    protected $fillable = ['Publicacion_idPublicacion', 'DocumentoMetadato_idDocumentoMetadato', 'valorPublicacionDocumentoMetadato', 'PublicacionVersion_idPublicacionVersion'];
    public $timestamps = false;

    protected $auditInclude = [
        'valorPublicacionDocumentoMetadato', 
    ];
}
