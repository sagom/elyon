<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Opcion extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'opcion';
    protected $primaryKey = 'idOpcion';

    protected $fillable = ['ordenOpcion', 'nombreOpcion', 'rutaOpcion', 'Paquete_idPaquete'];

    public $timestamps = false;

    protected $auditInclude = [
        'ordenOpcion',
        'nombreOpcion',
        'rutaOpcion',
        'Paquete_idPaquete'
    ];
}
