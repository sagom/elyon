<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Retencion extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'retencion';
    protected $primaryKey = 'idRetencion';

    protected $fillable = ['Dependencia_idDependencia', 'Serie_idSerie', 'SubSerie_idSubSerie', 'Documento_idDocumento', 'tiempoGestionRetencion', 'tiempoCentralRetencion', 'soporteRetencion', 'disposicionFinalRetencion', 'procedimientoRetencion', 'estadoRetencion', 'Compania_idCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'Dependencia_idDependencia', 
        'Serie_idSerie', 
        'SubSerie_idSubSerie', 
        'Documento_idDocumento', 
        'tiempoGestionRetencion', 
        'tiempoCentralRetencion', 
        'soporteRetencion', 
        'disposicionFinalRetencion', 
        'procedimientoRetencion', 
        'estadoRetencion'
    ];
}
