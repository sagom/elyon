<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class CentroCosto extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'centrocosto';
    protected $primaryKey = 'idCentroCosto';

    protected $fillable = ['codigoCentroCosto', 'nombreCentroCosto', 'Compania_idCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'codigoCentroCosto', 
        'nombreCentroCosto', 
    ];
}
