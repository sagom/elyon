<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Estructura extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'estructura';
    protected $primaryKey = 'idEstructura';

    protected $fillable = ['Dependencia_idDependencia', 'Serie_idSerie', 'SubSerie_idSubSerie', 'Documento_idDocumento', 'Compania_idCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'Dependencia_idDependencia', 
        'Serie_idSerie', 
        'SubSerie_idSubSerie', 
        'Documento_idDocumento'
    ];
}
