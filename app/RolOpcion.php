<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class RolOpcion extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'rolopcion';
    protected $primaryKey = 'idRolOpcion';

    protected $fillable = ['Rol_idRol', 'Opcion_idOpcion', 'adicionarRolOpcion', 'modificarRolOpcion', 'eliminarRolOpcion', 'imprimirRolOpcion', 'aprobarRolOpcion'];

    public $timestamps = false;

    protected $auditInclude = [
        'Opcion_idOpcion', 
        'adicionarRolOpcion', 
        'modificarRolOpcion', 
        'eliminarRolOpcion', 
        'imprimirRolOpcion', 
        'aprobarRolOpcion'];
}
