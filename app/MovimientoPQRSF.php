<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class MovimientoPQRSF extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'movimientopqrsf';
    protected $primaryKey = 'idMovimientoPQRSF';

    protected $fillable = ['fechaSolicitudMovimientoPQRSF', 'nombreSolicitanteMovimientoPQRSF', 'telefonoSolicitanteMovimientoPQRSF', 'movilSolicitanteMovimientoPQRSF', 'correoSolicitanteMovimientoPQRSF', 
    'tipoMovimientoPQRSF', 'mensajeMovimientoPQRSF', 'autorizacionMovimientoPQRSF', 'respuestaMovimientoPQRSF', 'fechaRespuestaMovimientoPQRSF', 'correoRespuestaMovimientoPQRSF', 'rutaAdjuntoRespuestaMovimientoPQRSF', 'Users_idMovimientoPQRSF', 'Compania_idCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'fechaSolicitudMovimientoPQRSF', 
        'nombreSolicitanteMovimientoPQRSF', 
        'correoSolicitanteMovimientoPQRSF', 
        'tipoMovimientoPQRSF', 
        'mensajeMovimientoPQRSF',  
        'respuestaMovimientoPQRSF', 
        'fechaRespuestaMovimientoPQRSF', 
        'correoRespuestaMovimientoPQRSF', 
        'Users_idMovimientoPQRSF'];
}
