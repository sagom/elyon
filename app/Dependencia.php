<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Dependencia extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'dependencia';
    protected $primaryKey = 'idDependencia';

    protected $fillable = ['codigoDependencia', 'nombreDependencia', 'directorioDependencia', 'Compania_idCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'codigoDependencia',
        'nombreDependencia',
        'directorioDependencia'
    ];
}
