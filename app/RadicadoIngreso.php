<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class RadicadoIngreso extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $table = 'radicadoingreso';
    protected $primaryKey = 'idRadicadoIngreso';

    protected $fillable = ['numeroRadicadoIngreso', 'fechaRadicadoIngreso', 'tipoRadicadoIngreso', 'nombreRemitenteRadicadoIngreso', 'identificacionRemitenteRadicadoIngreso', 'asuntoRadicadoIngreso', 'fechaDocumentoRadicadoIngreso', 'Users_idRadicadoIngreso', 'Dependencia_idRadicadoIngreso', 'impresionRadicadoIngreso', 'correoRadicadoIngreso', 'observacionRadicadoIngreso', 'Compania_idCompania', 'Users_idRadica'];

    public $timestamps = false;

    protected $auditInclude = [
        'numeroRadicadoIngreso', 
        'fechaRadicadoIngreso', 
        'tipoRadicadoIngreso', 
        'nombreRemitenteRadicadoIngreso', 
        'identificacionRemitenteRadicadoIngreso', 
        'asuntoRadicadoIngreso',
        'Users_idRadicadoIngreso', 
        'Dependencia_idRadicadoIngreso',
        'correoRadicadoIngreso', 
        'observacionRadicadoIngreso',
        'Users_idRadica'
    ];
}
