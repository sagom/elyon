<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Publicacion extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'publicacion';
    protected $primaryKey = 'idPublicacion';

    protected $fillable = ['codigoPublicacion', 'Dependencia_idDependencia', 'Serie_idSerie', 'SubSerie_idSubSerie', 'Documento_idDocumento', 'paginasPublicacion', 'Radicado_idRadicadoIngreso', 'Radicado_idRadicadoSalida', 'Radicado_idRadicadoInterno', 'FlujoManual_idFlujoManual', 'Compania_idCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'codigoPublicacion',
        'Dependencia_idDependencia',
        'Serie_idSerie',
        'SubSerie_idSubSerie',
        'Documento_idDocumento',
        'paginasPublicacion',
        'Radicado_idRadicadoIngreso',
        'Radicado_idRadicadoSalida',
        'Radicado_idRadicadoInterno',
        'FlujoManual_idFlujoManual'
    ];
}
