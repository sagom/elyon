<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Metadato extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'metadato';
    protected $primaryKey = 'idMetadato';

    protected $fillable = ['codigoMetadato', 'nombreMetadato', 'tipoMetadato', 'opcionMetadato', 'longitudMetadato', 'Compania_idCompania', 'informeFlujoMetadato'];

    public $timestamps = false;

    protected $auditInclude = [
        'codigoMetadato',
        'nombreMetadato',
        'tipoMetadato',
        'opcionMetadato',
        'longitudMetadato',
        'informeFlujoMetadato'
    ];
}
