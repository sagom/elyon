<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Compania extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'compania';
    protected $primaryKey = 'idCompania';

    protected $fillable = ['codigoCompania', 'nombreCompania', 'rutaImagenCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'codigoCompania',
        'nombreCompania'
    ];
}
