<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Paquete extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'paquete';
    protected $primaryKey = 'idPaquete';

    protected $fillable = ['ordenPaquete', 'iconoPaquete', 'nombrePaquete'];
    public $timestamps = false;

    protected $auditInclude = [
        'ordenPaquete', 
        'nombrePaquete'];
}
