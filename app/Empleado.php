<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Empleado extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'empleado';
    protected $primaryKey = 'idEmpleado';

    protected $fillable = ['documentoEmpleado' , 'nombreCompletoEmpleado' , 'estadoEmpleado', 'CentroCosto_idCentroCosto', 'Compania_idCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'documentoEmpleado', 
        'nombreCompletoEmpleado', 
        'estadoEmpleado',
        'Compania_idCompania' 
    ];
}