<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class RadicadoSalida extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'radicadosalida';
    protected $primaryKey = 'idRadicadoSalida';

    protected $fillable = ['numeroRadicadoSalida', 'fechaRadicadoSalida','Users_idRadicadoSalida', 'Dependencia_idRadicadoSalida', 'fechaDocumentoRadicadoSalida', 'asuntoRadicadoSalida', 'observacionRadicadoSalida','nombreDestinatarioRadicadoSalida', 'identificacionDestinatarioRadicadoSalida', 'impresionRadicadoSalida', 'Compania_idCompania', 'Users_idRadica'];

    public $timestamps = false;

    protected $auditInclude = [
        'numeroRadicadoSalida', 
        'fechaRadicadoSalida',
        'Users_idRadicadoSalida', 
        'Dependencia_idRadicadoSalida', 
        'asuntoRadicadoSalida', 
        'observacionRadicadoSalida',
        'nombreDestinatarioRadicadoSalida', 
        'identificacionDestinatarioRadicadoSalida',
        'Users_idRadica'
    ];
}
