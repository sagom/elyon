<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DocumentoRol extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'documentorol';
    protected $primaryKey = 'idDocumentoRol';

    protected $fillable = ['Documento_idDocumento', 'Rol_idRol'];

    public $timestamps = false;

    protected $auditInclude = [
        'Rol_idRol'
    ];
}
