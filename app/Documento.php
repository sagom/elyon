<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Documento extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'documento';
    protected $primaryKey = 'idDocumento';

    protected $fillable = ['codigoDocumento', 'nombreDocumento', 'directorioDocumento', 'versionDocumento', 'radicadoDocumento', 'tablaDocumento', 'Compania_idCompania', 'generaMetadatoFlujoDocumento'];

    public $timestamps = false;

    protected $auditInclude = [
        'codigoDocumento',
        'nombreDocumento',
        'directorioDocumento',
        'radicadoDocumento',
        'tablaDocumento',
        'versionDocumento',
        'generaMetadatoFlujoDocumento'
    ];
}
