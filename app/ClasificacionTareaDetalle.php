<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ClasificacionTareaDetalle extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'clasificaciontareadetalle';
    protected $primaryKey = 'idClasificacionTareaDetalle';

    protected $fillable = ['ClasificacionTarea_idClasificacionTarea', 'codigoClasificacionTareaDetalle', 'nombreClasificacionTareaDetalle'];

    public $timestamps = false;

    protected $auditInclude = [
        'codigoClasificacionTareaDetalle', 
        'nombreClasificacionTareaDetalle'
    ];
}
