<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class FlujoTarea extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'flujotarea';
    protected $primaryKey = 'idFlujoTarea';

    protected $fillable = ['Flujo_idFlujo', 'nombreFlujoTarea', 'Users_idFlujoTarea', 'permiteAnularFlujoTarea', 'encuestaFlujoTarea'];

    public $timestamps = false;

    protected $casts = [
        'encuestaFlujoTarea' => 'array',
    ];

    protected $auditInclude = [
        'nombreFlujoTarea',
        'Users_idFlujoTarea',
        'estadoFlujoTarea',
        'permiteAnularFlujoTarea',
        'encuestaFlujoTarea'
    ];
}
