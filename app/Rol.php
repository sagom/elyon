<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Rol extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'rol';
    protected $primaryKey = 'idRol';

    protected $fillable = ['codigoRol', 'nombreRol', 'Compania_idCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'codigoRol',
        'nombreRol'
    ];
}
