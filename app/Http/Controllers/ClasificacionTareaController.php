<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ClasificacionTareaRequest;
use App\Http\Controllers\Controller;
use DB;

class ClasificacionTareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $datos = $this->consultarPermisos("clasificaciontarea");
        if(!empty($datos))
            return view('clasificaciontareagrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clasificaciontareaform');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClasificacionTareaRequest $request)
    {
        if($request->ajax()) 
            return;
            
        \App\ClasificacionTarea::create([
            'codigoClasificacionTarea' => $request['codigoClasificacionTarea'],
            'nombreClasificacionTarea' => $request['nombreClasificacionTarea'],
            'Compania_idCompania' => \Session::get("idCompania")
        ]);

        $clasificaciontarea = \App\ClasificacionTarea::All()->last();

        $this->grabarDetalle($request, $clasificaciontarea->idClasificacionTarea);
        
        return redirect('clasificaciontarea');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clasificaciontarea = \App\ClasificacionTarea::find($id);

        $clasificaciontareadetalle = DB::table('clasificaciontareadetalle')
                    ->where('ClasificacionTarea_idClasificacionTarea', '=', $id)
                    ->get();

        return view('clasificaciontareaform', compact('clasificaciontarea', 'clasificaciontareadetalle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClasificacionTareaRequest $request, $id)
    {
        if($request->ajax()) 
            return;

        $clasificaciontarea = \App\ClasificacionTarea::find($id);
        $clasificaciontarea->update([
                'codigoClasificacionTarea' => $request['codigoClasificacionTarea'],
                'nombreClasificacionTarea' => $request['nombreClasificacionTarea'],
                'Compania_idCompania' => \Session::get("idCompania")
        ]);

        $this->grabarDetalle($request, $id);

        return redirect('clasificaciontarea');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\ClasificacionTarea::destroy($id);
        return redirect('clasificaciontarea');
    }

    public function grabarDetalle($request, $id)
    {
        $idsEliminar = explode(',', $request['eliminarClasificacionTareaDet']);
        \App\ClasificacionTareaDetalle::whereIn('idClasificacionTareaDetalle',$idsEliminar)->delete();
        for($i = 0; $i < count($request['codigoClasificacionTareaDetalle']); $i++)
        {
            $indice = array(
                'idClasificacionTareaDetalle' => $request['idClasificacionTareaDetalle'][$i]);

            $datos= array(
                'ClasificacionTarea_idClasificacionTarea' => $id,
                'codigoClasificacionTareaDetalle' => $request['codigoClasificacionTareaDetalle'][$i],
                'nombreClasificacionTareaDetalle' => $request['nombreClasificacionTareaDetalle'][$i]);

            $guardar = \App\ClasificacionTareaDetalle::updateOrCreate($indice, $datos);
        }
    }
}
