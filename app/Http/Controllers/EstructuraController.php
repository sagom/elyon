<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\EstructuraRequest;
use DB;
use File;

class EstructuraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estructura = DB::table('estructura')
        ->select('idEstructura', 'Dependencia_idDependencia', 'Serie_idSerie', 'SubSerie_idSubSerie', 'Documento_idDocumento')
        ->where('Compania_idCompania', '=', \Session::get('idCompania'))
        ->get();
        $idDependencia = \App\Dependencia::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('idDependencia');
        $nombreDependencia = \App\Dependencia::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreDependencia');
        $idSerie = \App\Serie::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('idSerie');
        $nombreSerie = \App\Serie::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreSerie');
        $idSubSerie = \App\SubSerie::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('idSubSerie');
        $nombreSubSerie = \App\SubSerie::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreSubSerie');
        $idDocumento = \App\Documento::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('idDocumento');
        $nombreDocumento = \App\Documento::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreDocumento');

        return view('estructuraform', compact('estructura', 'idDependencia', 'nombreDependencia', 'idSerie', 'nombreSerie', 'idSubSerie', 'nombreSubSerie', 'idDocumento', 'nombreDocumento'));
    }

    public function store(EstructuraRequest $request)
    {
        if($request->ajax()) 
            return;

        $idsEliminar = explode(',', $request['eliminarEstructura']);
        \App\Estructura::whereIn('idEstructura',$idsEliminar)->delete();
        for($i = 0; $i < count($request['Dependencia_idDependencia']); $i++)
        {
            $indice = array(
                'idEstructura' => $request['idEstructura'][$i]);

            $datos= array(
                'Dependencia_idDependencia' => $request['Dependencia_idDependencia'][$i],
                'Serie_idSerie' => $request['Serie_idSerie'][$i],
                'SubSerie_idSubSerie' => $request['SubSerie_idSubSerie'][$i],
                'Documento_idDocumento' => $request['Documento_idDocumento'][$i],
                'Compania_idCompania' => \Session::get('idCompania'));

            $guardar = \App\Estructura::updateOrCreate($indice, $datos);

            
            $directorioDependencia = \App\Dependencia::where('idDependencia', '=', $request['Dependencia_idDependencia'][$i])->lists('directorioDependencia');
            $directorioSerie = \App\Serie::where('idSerie', '=', $request['Serie_idSerie'][$i])->lists('directorioSerie');
            $directorioSubSerie = \App\SubSerie::where('idSubserie', '=', $request['SubSerie_idSubSerie'][$i])->lists('directorioSubSerie');
            $directorioDocumento = \App\Documento::where('idDocumento', '=', $request['Documento_idDocumento'][$i])->lists('directorioDocumento');

            $directorioDependencia = str_replace('["', '', str_replace('"]', '', $directorioDependencia));
            $directorioSerie = str_replace('["', '', str_replace('"]', '', $directorioSerie));
            $directorioSubSerie = str_replace('["', '', str_replace('"]', '', $directorioSubSerie));
            $directorioDocumento = str_replace('["', '', str_replace('"]', '', $directorioDocumento));

            $directorioDep = public_path() . '/storage/estructura/' . $directorioDependencia;
            if (!File::exists($directorioDep)) 
            {
                $resultado = File::makeDirectory($directorioDep , 0777, true);
            }

            $directorioSer = $directorioDep.'/'.$directorioSerie;
            if (!File::exists($directorioSer))
            {
                $resultado = File::makeDirectory($directorioSer , 0777, true);
            }

            $directorioSubSer = $directorioSer.'/'.$directorioSubSerie;
            if (!File::exists($directorioSubSer))
            {
                $resultado = File::makeDirectory($directorioSubSer , 0777, true);
            }

            $directorioDoc = $directorioSubSer.'/'.$directorioDocumento;
            if (!File::exists($directorioDoc))
            {
                $resultado = File::makeDirectory($directorioDoc , 0777, true);
            }
        }

        return redirect('estructura');
    }
}
