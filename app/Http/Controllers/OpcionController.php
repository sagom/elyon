<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\OpcionRequest;


class OpcionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("opcion");
        if(!empty($datos))
            return view('opciongrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paquete = \App\Paquete::All()->lists('nombrePaquete', 'idPaquete');

        return view('opcionform', compact('paquete'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OpcionRequest $request)
    {
        if($request->ajax()) 
            return;
            
        \App\Opcion::create([
            'ordenOpcion' => $request['ordenOpcion'],
            'nombreOpcion' => $request['nombreOpcion'],
            'rutaOpcion' => $request['rutaOpcion'],
            'Paquete_idPaquete' => $request['Paquete_idPaquete']
        ]);
        
        return redirect('opcion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $opcion = \App\Opcion::find($id);
        $paquete = \App\Paquete::All()->lists('nombrePaquete', 'idPaquete');

        return view('opcionform', compact('opcion', 'paquete'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OpcionRequest $request, $id)
    {
        if($request->ajax()) 
            return;

        $opcion = \App\Opcion::find($id);
        $opcion->update([
            'ordenOpcion' => $request['ordenOpcion'],
            'nombreOpcion' => $request['nombreOpcion'],
            'rutaOpcion' => $request['rutaOpcion'],
            'Paquete_idPaquete' => $request['Paquete_idPaquete']
        ]);

        return redirect('opcion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Opcion::destroy($id);
        return redirect('opcion');
    }
}
