<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\FlujoRequest;
use Illuminate\Support\Facades\DB;
use Mail;

class FlujoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("flujo");
        if (!empty($datos))
            return view('flujogrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    public function indexFlujoSelect()
    {
        return view('flujogridselect');
    }

    public function create()
    {
        $idUsuario = \App\User::All()->lists('id');
        $nombreUsuario = \App\User::All()->lists('name');

        return view('flujoform', compact('idUsuario', 'nombreUsuario'));
    }

    public function store(FlujoRequest $request)
    {
        if ($request->ajax())
            return;

        \App\Flujo::create([
            'codigoFlujo' => $request['codigoFlujo'],
            'nombreFlujo' => $request['nombreFlujo'],
            'Compania_idCompania' => \Session::get("idCompania")
        ]);

        $flujo = \App\Flujo::All()->last();
        $this->grabarDetalle($request, $flujo->idFlujo);

        return redirect('flujo');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $flujo = \App\Flujo::find($id);
        $idUsuario = \App\User::All()->lists('id');
        $nombreUsuario = \App\User::All()->lists('name');

        $flujotarea = \App\FlujoTarea::where('Flujo_idFlujo', '=', $id)
            ->select('idFlujoTarea', 'nombreFlujoTarea', 'Users_idFlujoTarea', 'encuestaFlujoTarea', 'permiteAnularFlujoTarea')
            ->get();

        return view('flujoform', compact('flujo', 'idUsuario', 'nombreUsuario', 'flujotarea'));
    }

    public function update(FlujoRequest $request, $id)
    {
        if ($request->ajax())
            return;

        $flujo = \App\Flujo::find($id);
        $flujo->update([
            'codigoFlujo' => $request['codigoFlujo'],
            'nombreFlujo' => $request['nombreFlujo'],
            'Compania_idCompania' => \Session::get("idCompania")
        ]);

        $this->grabarDetalle($request, $id);

        return redirect('flujo');
    }

    public function destroy($id)
    {
        \App\Flujo::destroy($id);
        return redirect('flujo');
    }

    public function grabarDetalle($request, $id)
    {
        $idsEliminar = explode(',', $request['eliminarFlujoTarea']);
        \App\FlujoTarea::whereIn('idFlujoTarea', $idsEliminar)->delete();
        for ($i = 0; $i < count($request['nombreFlujoTarea']); $i++) {
            $indice = array(
                'idFlujoTarea' => $request['idFlujoTarea'][$i]
            );

            $datos = array(
                'Flujo_idFlujo' => $id,
                'nombreFlujoTarea' => $request['nombreFlujoTarea'][$i],
                'Users_idFlujoTarea' => $request['Users_idFlujoTarea'][$i],
                'permiteAnularFlujoTarea' => $request['permiteAnularFlujoTarea'][$i],
                'encuestaFlujoTarea' => json_decode($request['encuestaFlujoTarea'][$i], true),
            );

            $guardar = \App\FlujoTarea::updateOrCreate($indice, $datos);
        }
    }

    public function listarTareas()
    {
        $metadatos = DB::select("SELECT idMetadato, nombreMetadato FROM metadato WHERE informeFlujoMetadato = 1 AND Compania_idCompania = " . \Session::get("idCompania") . " ORDER BY tipoMetadato DESC");

        $field = '';
        if (count($metadatos) > 0) {
            // $fields = [];
            foreach ($metadatos as $metadato) {
                $field .= "MAX(IF(documentometadato.Metadato_idMetadato = $metadato->idMetadato, publicaciondocumentometadato.valorPublicacionDocumentoMetadato, null)),";
            }
            // $field = implode(',', $fields);
        }

        $pendientes = DB::table('flujotarea')
            ->select(
                DB::raw("idFlujoTareaRadicado,
                CONCAT_WS(' - ', $field CONCAT(IF(idRadicadoIngreso IS NOT NULL, CONCAT('R', numeroRadicadoIngreso),IF(idRadicadoSalida IS NOT NULL, CONCAT('R', numeroRadicadoIngreso), CONCAT('F', nombreFlujoManual))), ': ', nombreFlujo, ' - ', nombreFlujoTarea)) as nombreFlujoTarea,
                Flujo_idFlujo,
                estadoFlujoTareaRadicado,
                IF(idRadicadoIngreso IS NOT NULL, 'ingreso', IF(idRadicadoSalida IS NOT NULL, 'salida', 'flujomanual')) as tipoRadicado,
                IF(idRadicadoIngreso IS NOT NULL, idRadicadoIngreso, IF(idRadicadoSalida IS NOT NULL, idRadicadoSalida, idFlujoManual)) as idRadicado")
            )
            ->leftJoin('flujo', 'flujotarea.Flujo_idFlujo', '=', 'flujo.idFlujo')
            ->leftJoin('flujotarearadicado', 'flujotarea.idFlujoTarea', '=', 'flujotarearadicado.FlujoTarea_idFlujoTarea')
            ->leftJoin('publicacion', 'flujotarearadicado.FlujoManual_idFlujoManual', '=', 'publicacion.FlujoManual_idFlujoManual')
            ->leftJoin('publicaciondocumentometadato', 'publicacion.idPublicacion', '=', 'publicaciondocumentometadato.Publicacion_idPublicacion')
            ->leftJoin('documentometadato', 'publicaciondocumentometadato.DocumentoMetadato_idDocumentoMetadato', '=', 'documentometadato.idDocumentoMetadato')
            ->leftJoin('metadato', 'documentometadato.Metadato_idMetadato', '=', 'metadato.idMetadato')
            ->leftJoin('radicadoingreso', 'flujotarearadicado.Radicado_idRadicadoIngreso', '=', 'radicadoingreso.idRadicadoIngreso')
            ->leftJoin('radicadosalida', 'flujotarearadicado.Radicado_idRadicadoSalida', '=', 'radicadosalida.idRadicadoSalida')
            ->leftJoin('flujomanual', 'flujotarearadicado.FlujoManual_idFlujoManual', '=', 'flujomanual.idFlujoManual')
            ->where('Users_idFlujoTarea', '=', \Session::get('idUsuario'))
            ->where('flujo.Compania_idCompania', '=',\Session::get('idCompania'))
            ->where('estadoFlujoTareaRadicado', '=', 'EnProceso')
            ->groupBy('idFlujoTareaRadicado')
            ->orderBy('idFlujoTareaRadicado', 'desc')
            ->get();

        $anuladas = DB::table('flujotarea')
            ->select(
                DB::raw("idFlujoTareaRadicado,
            CONCAT_WS(' - ', $field CONCAT(IF(idRadicadoIngreso IS NOT NULL, CONCAT('R', numeroRadicadoIngreso),IF(idRadicadoSalida IS NOT NULL, CONCAT('R', numeroRadicadoIngreso), CONCAT('F', nombreFlujoManual))), ': ', nombreFlujo, ' - ', nombreFlujoTarea)) as nombreFlujoTarea,
            Flujo_idFlujo,
            estadoFlujoTareaRadicado,
            IF(idRadicadoIngreso IS NOT NULL, 'ingreso', IF(idRadicadoSalida IS NOT NULL, 'salida', 'flujomanual')) as tipoRadicado,
            IF(idRadicadoIngreso IS NOT NULL, idRadicadoIngreso, IF(idRadicadoSalida IS NOT NULL, idRadicadoSalida, idFlujoManual)) as idRadicado")
            )
            ->leftJoin('flujo', 'flujotarea.Flujo_idFlujo', '=', 'flujo.idFlujo')
            ->leftJoin('flujotarearadicado', 'flujotarea.idFlujoTarea', '=', 'flujotarearadicado.FlujoTarea_idFlujoTarea')
            ->leftJoin('publicacion', 'flujotarearadicado.FlujoManual_idFlujoManual', '=', 'publicacion.FlujoManual_idFlujoManual')
            ->leftJoin('publicaciondocumentometadato', 'publicacion.idPublicacion', '=', 'publicaciondocumentometadato.Publicacion_idPublicacion')
            ->leftJoin('documentometadato', 'publicaciondocumentometadato.DocumentoMetadato_idDocumentoMetadato', '=', 'documentometadato.idDocumentoMetadato')
            ->leftJoin('metadato', 'documentometadato.Metadato_idMetadato', '=', 'metadato.idMetadato')
            ->leftJoin('radicadoingreso', 'flujotarearadicado.Radicado_idRadicadoIngreso', '=', 'radicadoingreso.idRadicadoIngreso')
            ->leftJoin('radicadosalida', 'flujotarearadicado.Radicado_idRadicadoSalida', '=', 'radicadosalida.idRadicadoSalida')
            ->leftJoin('flujomanual', 'flujotarearadicado.FlujoManual_idFlujoManual', '=', 'flujomanual.idFlujoManual')
            ->where('Users_idFlujoTarea', '=', \Session::get('idUsuario'))
            ->where('flujo.Compania_idCompania', '=',\Session::get('idCompania'))
            ->where('estadoFlujoTareaRadicado', '=', 'Anulado')
            ->groupBy('idFlujoTareaRadicado')
            ->orderBy('idFlujoTareaRadicado', 'desc')
            ->get();

        $terminadas = DB::table('flujotarea')
            ->select(
                DB::raw("idFlujoTareaRadicado,
            CONCAT_WS(' - ', $field CONCAT(IF(idRadicadoIngreso IS NOT NULL, CONCAT('R', numeroRadicadoIngreso),IF(idRadicadoSalida IS NOT NULL, CONCAT('R', numeroRadicadoIngreso), CONCAT('F', nombreFlujoManual))), ': ', nombreFlujo, ' - ', nombreFlujoTarea)) as nombreFlujoTarea,
            Flujo_idFlujo,
            estadoFlujoTareaRadicado,
            IF(idRadicadoIngreso IS NOT NULL, 'ingreso', IF(idRadicadoSalida IS NOT NULL, 'salida', 'flujomanual')) as tipoRadicado,
            IF(idRadicadoIngreso IS NOT NULL, idRadicadoIngreso, IF(idRadicadoSalida IS NOT NULL, idRadicadoSalida, idFlujoManual)) as idRadicado")
            )
            ->leftJoin('flujo', 'flujotarea.Flujo_idFlujo', '=', 'flujo.idFlujo')
            ->leftJoin('flujotarearadicado', 'flujotarea.idFlujoTarea', '=', 'flujotarearadicado.FlujoTarea_idFlujoTarea')
            ->leftJoin('publicacion', 'flujotarearadicado.FlujoManual_idFlujoManual', '=', 'publicacion.FlujoManual_idFlujoManual')
            ->leftJoin('publicaciondocumentometadato', 'publicacion.idPublicacion', '=', 'publicaciondocumentometadato.Publicacion_idPublicacion')
            ->leftJoin('documentometadato', 'publicaciondocumentometadato.DocumentoMetadato_idDocumentoMetadato', '=', 'documentometadato.idDocumentoMetadato')
            ->leftJoin('metadato', 'documentometadato.Metadato_idMetadato', '=', 'metadato.idMetadato')
            ->leftJoin('radicadoingreso', 'flujotarearadicado.Radicado_idRadicadoIngreso', '=', 'radicadoingreso.idRadicadoIngreso')
            ->leftJoin('radicadosalida', 'flujotarearadicado.Radicado_idRadicadoSalida', '=', 'radicadosalida.idRadicadoSalida')
            ->leftJoin('flujomanual', 'flujotarearadicado.FlujoManual_idFlujoManual', '=', 'flujomanual.idFlujoManual')
            ->where('Users_idFlujoTarea', '=', \Session::get('idUsuario'))
            ->where('flujo.Compania_idCompania', '=',\Session::get('idCompania'))
            ->where('estadoFlujoTareaRadicado', '=', 'Terminada')
            ->groupBy('idFlujoTareaRadicado')
            ->orderBy('idFlujoTareaRadicado', 'desc')
            ->get();

        $datos = $this->consultarPermisos("tareas");
        if (!empty($datos))
            return view('flujotareaform', compact('pendientes', 'anuladas', 'terminadas'));
        else
            return view("accesodenegado");
    }

    public function ejecutarTarea($idFlujoTareaRadicado, $tipoRadicado, $idFlujo, $tipoEjecucion, $idRadicado)
    {
        if ($tipoRadicado == 'ingreso') {
            $tablaradicado = 'radicadoingreso';
            $relacionradicado = 'Radicado_idRadicadoIngreso';
            $idradicado = 'idRadicadoIngreso';
            $whereRadicado = "Radicado_idRadicadoIngreso = $idRadicado";
        } elseif ($tipoRadicado == 'salida') {
            $tablaradicado = 'radicadosalida';
            $relacionradicado = 'Radicado_idRadicadoSalida';
            $idradicado = 'idRadicadoSalida';
            $whereRadicado = "Radicado_idRadicadoSalida = $idRadicado";
        } else {
            $tablaradicado = 'flujomanual';
            $relacionradicado = 'FlujoManual_idFlujoManual';
            $idradicado = 'idFlujoManual';
            $whereRadicado = "FlujoManual_idFlujoManual = $idRadicado";
        }

        if ($idFlujoTareaRadicado == 0 || $tipoRadicado == '') {
            echo 'Parámetros enviados incorrectamente';
            return;
        }

        $flujotarearadicado = \App\FlujoTareaRadicado::selectRaw("
                nombreFlujo,
                nombreDocumento,
                rutaImagenPublicacionVersion,
                idFlujoTareaRadicado,
                nombreFlujoTarea, name, idFlujo,
                idPublicacion,
                encuestaFlujoTarea,
                IF(flujotarearadicado.Radicado_idRadicadoIngreso IS NOT NULL, 'ingreso', IF(flujotarearadicado.Radicado_idRadicadoSalida IS NOT NULL, 'salida', 'flujomanual')) as tipoRadicado,
                $idRadicado as idRadicado")
            ->leftJoin('flujotarea', 'flujotarearadicado.FlujoTarea_idFlujoTarea', '=', 'flujotarea.idFlujoTarea')
            ->leftJoin('flujo', 'flujotarea.Flujo_idFlujo', '=', 'flujo.idFlujo')
            ->leftJoin("$tablaradicado", "flujotarearadicado.$relacionradicado", '=', "$tablaradicado.$idradicado")
            ->leftJoin('publicacion', "$tablaradicado.$idradicado", '=', "publicacion.$relacionradicado")
            ->leftJoin('publicacionversion', 'publicacion.idPublicacion', '=', 'publicacionversion.Publicacion_idPublicacion')
            ->leftJoin('documento', 'publicacion.Documento_idDocumento', '=', 'documento.idDocumento')
            ->leftJoin('users', 'flujotarea.Users_idFlujoTarea', '=', 'users.id')
            ->where('idFlujoTareaRadicado', '=', $idFlujoTareaRadicado)
            ->firstOrFail();

        $flujotarearadicadocomentario = DB::table('flujotarearadicadocomentario')
            ->leftJoin('flujotarearadicado', 'flujotarearadicadocomentario.FlujoTareaRadicado_idFlujoTareaRadicado', '=', 'flujotarearadicado.idFlujoTareaRadicado')
            ->leftJoin('flujotarea', 'flujotarearadicado.FlujoTarea_idFlujoTarea', '=', 'flujotarea.idFlujoTarea')
            ->leftJoin('users', 'flujotarea.Users_idFlujoTarea', '=', 'users.id')
            ->select(DB::raw('nombreFlujoTarea, encuestaFlujoTarea, name, fechaFlujoTareaRadicadoComentario, estadoFlujoTareaRadicadoComentario, observacionFlujoTareaRadicadoComentario'))
            ->where('Flujo_idFlujo', '=', $idFlujo)
            ->whereNotNull($relacionradicado)
            ->whereRaw($whereRadicado)
            ->orderBy('fechaFlujoTareaRadicadoComentario')
            ->get();

        return view('flujotarearadicadoform', compact('flujotarearadicado', 'flujotarearadicadocomentario', 'tipoEjecucion'));
    }

    public function aceptarTarea()
    {
        $idFlujoTareaRadicado = isset($_POST['idFlujoTareaRadicado']) ? $_POST['idFlujoTareaRadicado'] : '';
        $observacion = isset($_POST['observacion']) ? $_POST['observacion'] : '';
        $respuesta = isset($_POST['respuesta']) ? $_POST['respuesta'] : '';
        $idFlujo = isset($_POST['idFlujo']) ? $_POST['idFlujo'] : '';
        $tipoRadicado = isset($_POST['tipoRadicado']) ? $_POST['tipoRadicado'] : '';
        $idRadicado = isset($_POST['idRadicado']) ? $_POST['idRadicado'] : '';

        if ($tipoRadicado == 'ingreso') {
            $relacionradicado = 'Radicado_idRadicadoIngreso';
            $whereRadicado = "Radicado_idRadicadoIngreso = $idRadicado";
        } else if ($tipoRadicado == 'salida') {
            $relacionradicado = 'Radicado_idRadicadoSalida';
            $whereRadicado = "Radicado_idRadicadoSalida = $idRadicado";
        } else {
            $relacionradicado = 'FlujoManual_idFlujoManual';
            $whereRadicado = "FlujoManual_idFlujoManual = $idRadicado";
        }

        \App\FlujoTareaRadicadoComentario::create([
            'FlujoTareaRadicado_idFlujoTareaRadicado' => $idFlujoTareaRadicado,
            'estadoFlujoTareaRadicadoComentario' => 'Aceptado',
            'fechaFlujoTareaRadicadoComentario' => date('Y-m-d H:i:s'),
            'observacionFlujoTareaRadicadoComentario' => $observacion,
            'respuestaFlujoTareaRadicadoComentario' => $respuesta
        ]);

        \App\FlujoTareaRadicado::where('idFlujoTareaRadicado', '=', $idFlujoTareaRadicado)
            ->whereRaw($whereRadicado)
            ->update(['estadoFlujoTareaRadicado' => 'Terminada']);

        $tareasiguiente = DB::table('flujotarearadicado')
            ->leftJoin('flujotarea', 'flujotarearadicado.FlujoTarea_idFlujoTarea', '=', 'flujotarea.idFlujoTarea')
            ->leftJoin('flujo', 'flujotarea.Flujo_idFlujo', '=', 'flujo.idFlujo')
            ->leftJoin('users', 'flujotarea.Users_idFlujoTarea', '=', 'users.id')
            ->select(DB::raw('idFlujoTareaRadicado, nombreFlujoTarea, nombreFlujo, users.email'))
            ->where('idFlujo', '=', $idFlujo)
            ->where('estadoFlujoTareaRadicado', '=', 'SinAsignar')
            ->whereNotNull($relacionradicado)
            ->whereRaw($whereRadicado)
            ->orderBy('idFlujoTarea', 'asc')
            ->first();

        if (!empty($tareasiguiente)) {

            \App\FlujoTareaRadicado::where('idFlujoTareaRadicado', $tareasiguiente->idFlujoTareaRadicado)
                ->update(['estadoFlujoTareaRadicado' => 'EnProceso']);

            $datos = [
                'direccion' => $tareasiguiente->email,
                'asunto' => "Asignación tarea del flujo: $tareasiguiente->nombreFlujo",
                'mensaje' => "Se le ha asignado la tarea <b>$tareasiguiente->nombreFlujoTarea</b> del flujo <b>$tareasiguiente->nombreFlujo</b>. Por favor revisela y realice las respectivas acciones sobre la misma en el módulo de Tareas"
            ];

            // Mail::send('email.correo', $datos, function ($msj) use ($datos) {
            //     $msj->to(explode(';', $datos['direccion']));
            //     $msj->subject($datos['asunto']);
            // });
        }

        echo json_encode('Tarea aceptada correctamente');
    }

    public function rechazarTarea()
    {
        $idFlujoTareaRadicado = isset($_POST['idFlujoTareaRadicado']) ? $_POST['idFlujoTareaRadicado'] : '';
        $observacion = isset($_POST['observacion']) ? $_POST['observacion'] : '';
        $respuesta = isset($_POST['respuesta']) ? $_POST['respuesta'] : false;
        $rechazar = isset($_POST['rechazar']) ? $_POST['rechazar'] : '';
        $idFlujo = isset($_POST['idFlujo']) ? $_POST['idFlujo'] : '';
        $tipoRadicado = isset($_POST['tipoRadicado']) ? $_POST['tipoRadicado'] : '';
        $idRadicado = isset($_POST['idRadicado']) ? $_POST['idRadicado'] : '';

        $flujoTarea = \App\FlujoTarea::join('flujotarearadicado as ftr', 'flujotarea.idFlujoTarea', '=', 'ftr.FlujoTarea_idFlujoTarea')
            ->where('ftr.idFlujoTareaRadicado', $idFlujoTareaRadicado)
            ->select('flujotarea.*')
            ->first();

        if ($tipoRadicado == 'ingreso') {
            $relacionradicado = 'Radicado_idRadicadoIngreso';
            $whereRadicado = "Radicado_idRadicadoIngreso = $idRadicado";
        } else if ($tipoRadicado == 'salida') {
            $relacionradicado = 'Radicado_idRadicadoSalida';
            $whereRadicado = "Radicado_idRadicadoSalida = $idRadicado";
        } else {
            $relacionradicado = 'FlujoManual_idFlujoManual';
            $whereRadicado = "FlujoManual_idFlujoManual = $idRadicado";
        }

        \App\FlujoTareaRadicadoComentario::create([
            'FlujoTareaRadicado_idFlujoTareaRadicado' => $idFlujoTareaRadicado,
            'estadoFlujoTareaRadicadoComentario' => 'Rechazado',
            'fechaFlujoTareaRadicadoComentario' => date('Y-m-d H:i:s'),
            'observacionFlujoTareaRadicadoComentario' => $observacion,
            'respuestaFlujoTareaRadicadoComentario' => $respuesta
        ]);


        if ($flujoTarea->permiteAnularFlujoTarea && self::is_true($rechazar)) {
            \App\FlujoTareaRadicado::whereRaw($whereRadicado)
                ->update(['estadoFlujoTareaRadicado' => 'Anulado']);

            echo json_encode('Tarea anulada correctamente');
            return;
        }

        \App\FlujoTareaRadicado::where('idFlujoTareaRadicado', '=', $idFlujoTareaRadicado)
            ->whereRaw($whereRadicado)
            ->update(['estadoFlujoTareaRadicado' => 'SinAsignar']);

        $tareasiguiente = DB::table('flujotarearadicado')
            ->leftJoin('flujotarea', 'flujotarearadicado.FlujoTarea_idFlujoTarea', '=', 'flujotarea.idFlujoTarea')
            ->leftJoin('flujo', 'flujotarea.Flujo_idFlujo', '=', 'flujo.idFlujo')
            ->leftJoin('users', 'flujotarea.Users_idFlujoTarea', '=', 'users.id')
            ->select(DB::raw('idFlujoTareaRadicado, nombreFlujoTarea, nombreFlujo, users.email'))
            ->where('idFlujo', '=', $idFlujo)
            ->where('estadoFlujoTareaRadicado', '=', 'Terminada')
            ->whereNotNull($relacionradicado)
            ->whereRaw($whereRadicado)
            ->orderBy('idFlujoTarea', 'desc')
            ->first();

        if (!empty($tareasiguiente)) {
            \App\FlujoTareaRadicado::where('idFlujoTareaRadicado', $tareasiguiente->idFlujoTareaRadicado)
                ->update(['estadoFlujoTareaRadicado' => 'EnProceso']);

            $datos = [
                'direccion' => $tareasiguiente->email,
                'asunto' => "Rechazo tarea del flujo $tareasiguiente->nombreFlujo",
                'mensaje' => "Se ha rechazado la tarea <b>$tareasiguiente->nombreFlujoTarea</b> del flujo <b>$tareasiguiente->nombreFlujo</b>. Por favor revisela y realice las respectivas acciones sobre la misma en el módulo de tareas."
            ];

            // Mail::send('email.correo', $datos, function ($msj) use ($datos) {
            //     $msj->to(explode(';', $datos['direccion']));
            //     $msj->subject($datos['asunto']);
            // });
        } else {
            $tareasiguiente = DB::table('flujotarearadicado')
                ->leftJoin('flujotarea', 'flujotarearadicado.FlujoTarea_idFlujoTarea', '=', 'flujotarea.idFlujoTarea')
                ->leftJoin('flujo', 'flujotarea.Flujo_idFlujo', '=', 'flujo.idFlujo')
                ->leftJoin('users', 'flujotarea.Users_idFlujoTarea', '=', 'users.id')
                ->select(DB::raw('idFlujoTareaRadicado, nombreFlujoTarea, nombreFlujo, users.email'))
                ->where('idFlujo', '=', $idFlujo)
                ->where('estadoFlujoTareaRadicado', '=', 'SinAsignar')
                ->whereNotNull($relacionradicado)
                ->orderBy('idFlujoTarea', 'asc')
                ->first();

            if (!empty($tareasiguiente)) {
                \App\FlujoTareaRadicado::where('idFlujoTareaRadicado', $tareasiguiente->idFlujoTareaRadicado)
                    ->update(['estadoFlujoTareaRadicado' => 'EnProceso']);

                $datos = [
                    'direccion' => $tareasiguiente->email,
                    'asunto' => "Rechazo tarea del flujo $tareasiguiente->nombreFlujo",
                    'mensaje' => "Se ha rechazado la tarea <b>$tareasiguiente->nombreFlujoTarea</b> del flujo <b>$tareasiguiente->nombreFlujo</b>. Por favor revisela y realice las respectivas acciones sobre la misma en el módulo de tareas."
                ];

                // Mail::send('email.correo', $datos, function ($msj) use ($datos) {
                //     $msj->to(explode(';', $datos['direccion']));
                //     $msj->subject($datos['asunto']);
                // });
            }
        }

        echo json_encode('Tarea rechazada correctamente');
    }

    private function is_true($val, $return_null = false)
    {
        $boolval = (is_string($val) ? filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : (bool) $val);
        return ($boolval === null && !$return_null ? false : $boolval);
    }

    public function flujoInforme()
    {
        $flujo = \App\Flujo::All()->lists('nombreFlujo', 'idFlujo');

        return view('flujoinformeform', compact('flujo'));
    }

    public function generarFlujoInforme()
    {
        $condicion = (isset($_GET['condicion']) && $_GET['condicion'] != '') ? "WHERE " . $_GET['condicion'] : '';
        $tipoInforme = $_GET['tipo'];

        $metadatos = DB::select("SELECT idMetadato, nombreMetadato FROM metadato WHERE informeFlujoMetadato = 1 AND Compania_idCompania = " . \Session::get("idCompania") . " ORDER BY tipoMetadato DESC");

        $field = null;
        if (count($metadatos) > 0) {
            $fields = [];
            foreach ($metadatos as $metadato) {
                $fields[] = "MAX(IF(dm.Metadato_idMetadato = $metadato->idMetadato, pdm.valorPublicacionDocumentoMetadato, null)) as '$metadato->nombreMetadato'";
            }
            $field = implode(',', $fields);
        }


        $consulta = DB::select(
            "SELECT
                f.nombreFlujo ,
                ft.nombreFlujoTarea,
                u.name ,
                ftr.estadoFlujoTareaRadicado,
                IFNULL(CONCAT('R', ri.numeroRadicadoIngreso), '') AS numeroRadicadoIngreso,
                IFNULL(CONCAT('R', rs.numeroRadicadoSalida), '') AS numeroRadicadoSalida,
                IFNULL(CONCAT('F', fm.nombreFlujoManual), '') AS nombreFlujoManual,
                ftrc.fechaFlujoTareaRadicadoComentario,
                ftrc.estadoFlujoTareaRadicadoComentario,
                ftrc.observacionFlujoTareaRadicadoComentario,
                $field
            from
                flujo f
            left join flujotarea ft on
                f.idFlujo = ft.Flujo_idFlujo
            join users u on
                ft.Users_idFlujoTarea = u.id
            left join flujotarearadicado ftr on
                ft.idFlujoTarea = ftr.FlujoTarea_idFlujoTarea
            left join publicacion p on
                ftr.FlujoManual_idFlujoManual = p.FlujoManual_idFlujoManual
            left join publicaciondocumentometadato pdm on
                p.idPublicacion = pdm.Publicacion_idPublicacion
            left join documentometadato dm on
                pdm.DocumentoMetadato_idDocumentoMetadato = dm.idDocumentoMetadato
            left join metadato m on
                dm.Metadato_idMetadato = m.idMetadato
            left join radicadoingreso ri on ftr.Radicado_idRadicadoIngreso = ri.idRadicadoIngreso
            left join radicadosalida rs on ftr.Radicado_idRadicadoSalida = rs.idRadicadoSalida
            left join flujomanual fm on ftr.FlujoManual_idFlujoManual = fm.idFlujoManual
            left join flujotarearadicadocomentario ftrc on
                ftr.idFlujoTareaRadicado = ftrc.FlujoTareaRadicado_idFlujoTareaRadicado
            $condicion
            group by ftr.idFlujoTareaRadicado
            ORDER BY ftr.idFlujoTareaRadicado DESC"
        );

        foreach ($consulta as $data) {
        }


        if (empty($consulta)) {
            return 'No hay resultados para los filtros indicados';
        }

        return view('formatos.flujoinformeimpresion', compact('consulta', 'tipoInforme', 'metadatos'));
    }
}
