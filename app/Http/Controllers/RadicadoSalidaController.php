<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\RadicadoSalidaRequest;
use DB;


class RadicadoSalidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("radicadosalida");
        $dependencia = \App\Dependencia::All()->lists('nombreDependencia', 'idDependencia');
        if (!empty($datos))
            return view('radicadosalidagrid', compact('datos', 'dependencia'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = \App\User::All()->lists('name', 'id');
        $dependencia = \App\Dependencia::All()->lists('nombreDependencia', 'idDependencia');

        return view('radicadosalidaform', compact('user', 'dependencia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RadicadoSalidaRequest $request)
    {
        if ($request->ajax())
            return;

        $numeroR = DB::table('radicadosalida')
            ->select(DB::raw('IF(
                                MAX(SUBSTRING(numeroRadicadoSalida, 1, 4)) = SUBSTRING(CURDATE(), 1, 4),
                                CONCAT(MAX(SUBSTRING(numeroRadicadoSalida, 1, 4)), "-", LPAD(IF(numeroRadicadoSalida IS NULL, "1",
                                SUBSTRING(MAX(REPLACE(numeroRadicadoSalida, "-", "")) + 1, 5, 4)), 3, "0")),
                                CONCAT(YEAR(CURDATE()), "-", LPAD("1", 3, "0"))
                            ) AS numeroRadicadoSalida'))
            ->where('Compania_idCompania', '=', \Session::get('idCompania'))
            ->get();

        \App\RadicadoSalida::create([
            'numeroRadicadoSalida' => get_object_vars($numeroR[0])['numeroRadicadoSalida'],
            'fechaRadicadoSalida' => $request['fechaRadicadoSalida'],
            'nombreDestinatarioRadicadoSalida' => $request['nombreDestinatarioRadicadoSalida'],
            'identificacionDestinatarioRadicadoSalida' => $request['identificacionDestinatarioRadicadoSalida'],
            'asuntoRadicadoSalida' => $request['asuntoRadicadoSalida'],
            'fechaDocumentoRadicadoSalida' => $request['fechaDocumentoRadicadoSalida'],
            'Users_idRadicadoSalida' => $request['Users_idRadicadoSalida'],
            'Dependencia_idRadicadoSalida' => $request['Dependencia_idRadicadoSalida'],
            'observacionRadicadoSalida' => $request['observacionRadicadoSalida'],
            'Compania_idCompania' => \Session::get('idCompania'),
            'Users_idRadica' => \Session::get('idUsuario'),
        ]);

        return redirect('radicadosalida');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $radicadosalida = \App\RadicadoSalida::find($id);
        $user = \App\User::All()->lists('name', 'id');
        $dependencia = \App\Dependencia::All()->lists('nombreDependencia', 'idDependencia');

        return view('radicadosalidaform', compact('radicadosalida', 'user', 'dependencia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RadicadoSalidaRequest $request, $id)
    {
        if ($request->ajax())
            return;

        $radicadosalida = \App\RadicadoSalida::find($id);
        $radicadosalida->update([
            'numeroRadicadoSalida' => $request['numeroRadicadoSalida'],
            'fechaRadicadoSalida' => $request['fechaRadicadoSalida'],
            'Users_idRadicadoSalida' => $request['Users_idRadicadoSalida'],
            'Dependencia_idRadicadoSalida' => $request['Dependencia_idRadicadoSalida'],
            'fechaDocumentoRadicadoSalida' => $request['fechaDocumentoRadicadoSalida'],
            'asuntoRadicadoSalida' => $request['asuntoRadicadoSalida'],
            'observacionRadicadoSalida' => $request['observacionRadicadoSalida'],
            'nombreDestinatarioRadicadoSalida' => $request['nombreDestinatarioRadicadoSalida'],
            'identificacionDestinatarioRadicadoSalida' => $request['identificacionDestinatarioRadicadoSalida'],
            'Compania_idCompania' => \Session::get('idCompania'),
            'Users_idRadica' => \Session::get('idUsuario'),
        ]);

        return redirect('radicadosalida');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\RadicadoSalida::destroy($id);
        return redirect('radicadosalida');
    }

    public function guardarRadicadoSalida()
    {
        $dependencia = $_POST['dependencia'];
        $asunto = $_POST['asunto'];
        $nombre = $_POST['nombre'];
        $identificacion = $_POST['identificacion'];

        $numeroR = DB::table('radicadosalida')
            ->select(DB::raw('IF(
                                MAX(SUBSTRING(numeroRadicadoSalida, 1, 4)) = SUBSTRING(CURDATE(), 1, 4),
                                CONCAT(MAX(SUBSTRING(numeroRadicadoSalida, 1, 4)), "-", LPAD(IF(numeroRadicadoSalida IS NULL, "1",
                                SUBSTRING(MAX(REPLACE(numeroRadicadoSalida, "-", "")) + 1, 5, 4)), 3, "0")),
                                CONCAT(YEAR(CURDATE()), "-", LPAD("1", 3, "0"))
                            ) AS numeroRadicadoSalida'))
            ->where('Compania_idCompania', '=', \Session::get('idCompania'))
            ->get();

        \App\RadicadoSalida::create([
            'numeroRadicadoSalida' => get_object_vars($numeroR[0])['numeroRadicadoSalida'],
            'nombreDestinatarioRadicadoSalida' => $nombre,
            'fechaRadicadoSalida' => date("Y-m-d H:i:s"),
            'Dependencia_idRadicadoSalida' => $dependencia,
            'identificacionDestinatarioRadicadoSalida' => $identificacion,
            'asuntoRadicadoSalida' => $asunto,
            'impresionRadicadoSalida' => 0,
            'Compania_idCompania' => \Session::get('idCompania'),
            'Users_idRadica' => \Session::get('idUsuario'),
        ]);

        echo json_encode('Radicado generado correctamente');
    }

    public function imprimirRadicadoSalida($idRadicadoSalida)
    {
        $codigoCompania = \Session::get('codigoCompania');
        $radicado = DB::table('radicadosalida')
            ->leftJoin('users', 'radicadosalida.Users_idRadica', '=', 'users.id')
            ->leftJoin('compania', 'radicadosalida.Compania_idCompania', '=', 'compania.idCompania')
            ->leftJoin('dependencia', 'radicadosalida.Dependencia_idRadicadoSalida', '=', 'dependencia.idDependencia')
            ->select(DB::raw('"Correspondencia enviada" AS tipoRadicado, numeroRadicadoSalida AS numeroRadicado, fechaRadicadoSalida AS fechaRadicado,
        asuntoRadicadoSalida AS asuntoRadicado, nombreDestinatarioRadicadoSalida AS terceroRadicado, name, nombreCompania, nombreDependencia'))
            ->where('idRadicadoSalida', '=', $idRadicadoSalida)
            ->get();

        \App\RadicadoSalida::where('idRadicadoSalida', $idRadicadoSalida)
            ->update(['impresionRadicadoSalida' => 1]);

        return view("formatos.radicadosalida.radicadoimpresionSalida$codigoCompania", compact('radicado'));
    }

    public function cargarImagenRadicadoSalida()
    {
        $idRadicadoSalida = (isset($_POST['idRadicadoSalida']) && $_POST['idRadicadoSalida'] != '') ? $_POST['idRadicadoSalida'] : 0;

        $imagen = DB::table('publicacion')
            ->leftJoin('publicacionversion', 'publicacion.idPublicacion', '=', 'publicacionversion.Publicacion_idPublicacion')
            ->select(DB::raw('rutaImagenPublicacionVersion'))
            ->where('Radicado_idRadicadoSalida', '=', $idRadicadoSalida)
            ->get();

        echo json_encode($imagen);
    }

    public function guardarFlujoRadicadoSalida()
    {
        $idFlujo = (isset($_POST['idFlujo']) ? $_POST['idFlujo'] : '');
        $idRadicado = (isset($_POST['idRadicado']) ? $_POST['idRadicado'] : '');

        if ($idFlujo != '' && $idRadicado != '') {
            $tarearadicado = DB::table('flujotarearadicado')
                ->select(DB::raw('Radicado_idRadicadoSalida'))
                ->where('Radicado_idRadicadoSalida', '=', $idRadicado)
                ->get();

            if (empty($tarearadicado)) {
                $tareas = \App\FlujoTarea::where('Flujo_idFlujo', '=', $idFlujo)->lists('idFlujoTarea');

                foreach ($tareas as $key => $tarea) {
                    \App\FlujoTareaRadicado::create([
                        'FlujoTarea_idFlujoTarea' => $tarea,
                        'Radicado_idRadicadoIngreso' => null,
                        'Radicado_idRadicadoSalida' => $idRadicado,
                        'estadoFlujoTareaRadicado' => 'SinAsignar'
                    ]);
                }

                $tareainicial = \App\FlujoTarea::where('Flujo_idFlujo', $idFlujo)->first();

                \App\FlujoTareaRadicado::where('FlujoTarea_idFlujoTarea', $tareainicial->idFlujoTarea)
                    ->where('Radicado_idRadicadoSalida', $idRadicado)
                    ->update(['estadoFlujoTareaRadicado' => 'EnProceso']);

                echo json_encode(array(true, 'Flujo iniciado correctamente. Diríjase al panel de tareas para ejecutarlo.'));
            } else {
                echo json_encode(array(false, 'Este radicado ya está asociado a un flujo'));
            }
        } else {
            echo json_encode(array(false, 'Debe seleccionar un flujo válido'));
        }
    }
}
