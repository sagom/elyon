<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\SerieRequest;


class SerieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("serie");
        if(!empty($datos))
            return view('seriegrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('serieform');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SerieRequest $request)
    {
        if($request->ajax()) 
            return;
            
        \App\Serie::create([
            'codigoSerie' => $request['codigoSerie'],
            'nombreSerie' => $request['nombreSerie'],
            'directorioSerie' => $request['directorioSerie'],
            'descripcionSerie' => $request['descripcionSerie'],
            'Compania_idCompania' => \Session::get("idCompania") 
        ]);
        
        return redirect('serie');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $serie = \App\Serie::selectRaw(
            'idSerie,
            codigoSerie,
            nombreSerie,
            directorioSerie,
            descripcionSerie,
            IF(Serie_idSerie IS NULL, 0, 1) AS Serie_idSerie')
            ->leftJoin('estructura', 'serie.idSerie', '=', 'estructura.Serie_idSerie')
            ->where('idSerie', '=', $id)
            ->firstOrFail();

        return view('serieform', compact('serie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SerieRequest $request, $id)
    {
        if($request->ajax()) 
            return;

        $serie = \App\Serie::find($id);
        $serie->update([
                'codigoSerie' => $request['codigoSerie'],
                'nombreSerie' => $request['nombreSerie'],
                'directorioSerie' => $request['directorioSerie'],
                'descripcionSerie' => $request['descripcionSerie'],
                'Compania_idCompania' => \Session::get("idCompania")
        ]);

        return redirect('serie');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Serie::destroy($id);
        return redirect('serie');
    }
}
