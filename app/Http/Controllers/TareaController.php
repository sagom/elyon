<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\TareaRequest;
use DB;
use Mail;

class TareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("tarea");
        if (!empty($datos))
            return view('tareagrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $depsolicitante = DB::table('users')
            ->leftJoin('dependencia', 'users.Dependencia_idDependencia', '=', 'dependencia.idDependencia')
            ->where('id', '=', \Session::get('idUsuario'))
            ->select('nombreDependencia')
            ->get();
        $depsolicitante = get_object_vars($depsolicitante[0])['nombreDependencia'];

        $clasificacion = \App\ClasificacionTarea::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreClasificacionTarea', 'idClasificacionTarea');
        $clasificaciondet = array();
        $dependenciaresp = \App\Dependencia::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreDependencia', 'idDependencia');
        $userresp = array();

        return view('tareaform', compact('depsolicitante', 'clasificacion', 'clasificaciondet', 'dependenciaresp', 'userresp'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TareaRequest $request)
    {
        if ($request->ajax())
            return;

        $numeroT = DB::table('tarea')
            ->select(DB::raw('LPAD(if(numeroTarea is null, "1", MAX(numeroTarea) +1), 10,"0") as numeroTarea'))
            ->where('Compania_idCompania', '=', \Session::get('idCompania'))
            ->get();

        $numero = get_object_vars($numeroT[0])['numeroTarea'];

        \App\Tarea::create([
            'numeroTarea' => $numero,
            'fechaElaboracionTarea' => $request['fechaElaboracionTarea'],
            'asuntoTarea' => $request['asuntoTarea'],
            'prioridadTarea' => $request['prioridadTarea'],
            'Users_idSolicitante' => $request['Users_idSolicitante'],
            'ClasificacionTarea_idClasificacionTarea' => $request['ClasificacionTarea_idClasificacionTarea'],
            'ClasificacionTareaDetalle_idClasificacionTareaDetalle' => $request['ClasificacionTareaDetalle_idClasificacionTareaDetalle'],
            'estadoTarea' => $request['estadoTarea'],
            'Dependencia_idResponsable' => $request['Dependencia_idResponsable'],
            'Users_idResponsable' => ($request['Users_idResponsable'] == '' ? null : $request['Users_idResponsable']),
            'fechaVencimientoTarea' => $request['fechaVencimientoTarea'],
            'descripcionTarea' => $request['descripcionTarea'],
            'solucionTarea' => $request['solucionTarea'],
            'fechaSolucionTarea' => $request['fechaSolucionTarea'],
            'Compania_idCompania' => \Session::get("idCompania")
        ]);

        $tarea = \App\Tarea::All()->last();

        $this->enviarNotificacionTarea($request, $tarea->idTarea, 'crear');

        return redirect('tarea');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tarea = \App\Tarea::selectRaw(
            '*, us.name as nombreUsuarioSolicitante, nombreDependencia as nombreDependenciaSolicitante'
        )
            ->leftJoin('users AS us', 'tarea.Users_idSolicitante', '=', 'us.id')
            ->leftJoin('dependencia', 'us.Dependencia_idDependencia', '=', 'dependencia.idDependencia')
            ->where('idTarea', '=', $id)
            ->firstOrFail();

        $clasificacion = \App\ClasificacionTarea::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreClasificacionTarea', 'idClasificacionTarea');
        $clasificaciondet = array();
        $dependenciaresp = \App\Dependencia::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreDependencia', 'idDependencia');
        $userresp = array();

        return view('tareaform', compact('tarea', 'clasificacion', 'clasificaciondet', 'dependenciaresp', 'userresp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TareaRequest $request, $id)
    {
        if ($request->ajax())
            return;

        $tarea = \App\Tarea::find($id);
        $tarea->update([
            'numeroTarea' => $request['numeroTarea'],
            'fechaElaboracionTarea' => $request['fechaElaboracionTarea'],
            'asuntoTarea' => $request['asuntoTarea'],
            'prioridadTarea' => $request['prioridadTarea'],
            'Users_idSolicitante' => $request['Users_idSolicitante'],
            'ClasificacionTarea_idClasificacionTarea' => $request['ClasificacionTarea_idClasificacionTarea'],
            'ClasificacionTareaDetalle_idClasificacionTareaDetalle' => $request['ClasificacionTareaDetalle_idClasificacionTareaDetalle'],
            'estadoTarea' => $request['estadoTarea'],
            'Dependencia_idResponsable' => $request['Dependencia_idResponsable'],
            'Users_idResponsable' => ($request['Users_idResponsable'] == '' ? null : $request['Users_idResponsable']),
            'fechaVencimientoTarea' => $request['fechaVencimientoTarea'],
            'descripcionTarea' => $request['descripcionTarea'],
            'solucionTarea' => $request['solucionTarea'],
            'fechaSolucionTarea' => $request['fechaSolucionTarea'],
            'Compania_idCompania' => \Session::get("idCompania")
        ]);

        if ($request['estadoTarea'] == 'Terminado' || $request['estadoTarea'] == 'Rechazado') {
            $tarea->update([
                'fechaSolucionTarea' => date('Y-m-d H:i:s')
            ]);
        }

        $this->enviarNotificacionTarea($request, $id, 'modificar');

        return redirect('tarea');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Tarea::destroy($id);
        return redirect('tarea');
    }

    public function buscarClasificacionDetalle()
    {
        $idClasificacion = (isset($_POST['idClasificacion']) && $_POST['idClasificacion'] != '') ? $_POST['idClasificacion'] : '';

        $campos = DB::table('clasificaciontareadetalle')
            ->select('idClasificacionTareaDetalle', 'nombreClasificacionTareaDetalle')
            ->where('ClasificacionTarea_idClasificacionTarea', '=', $idClasificacion)
            ->get();

        echo json_encode($campos);
    }

    public function buscarUsuarioResponsable()
    {
        $idDependenciaResponsable = (isset($_POST['idDependenciaResponsable']) && $_POST['idDependenciaResponsable'] != '') ? $_POST['idDependenciaResponsable'] : '';

        $campos = DB::table('users')
            ->select('id', 'name')
            ->where('Dependencia_idDependencia', '=', $idDependenciaResponsable)
            ->get();

        echo json_encode($campos);
    }

    public function enviarNotificacionTarea($request, $id, $tipo)
    {
        if ($tipo == 'crear')
            $asunto = "Creación de la tarea: " . $request['asuntoTarea'];
        else
            $asunto = "Modificación de la tarea: " . $request['asuntoTarea'];

        $mail = array();

        $solicitante = DB::table('users')
            ->where('id', '=', \Session::get('idUsuario'))
            ->select('email')
            ->get();

        $responsable = DB::table('users')
            ->where('id', '=', $request['Users_idResponsable'])
            ->select('email')
            ->get();

        $correoSolicitante = get_object_vars($solicitante[0])['email'];
        $correoResponsable = get_object_vars($responsable[0])['email'];

        $mail['direccion'] = "$correoSolicitante;$correoResponsable";
        $mail['asunto'] = $asunto;
        $mensaje = 'Adjunto encontrará más detalles sobre la tarea';

        $mensaje .= $this->generarHTMLTarea($id);

        $mail['mensaje'] = $mensaje;

        // Mail::send('email.correo',$mail,function($msj) use ($mail)
        // {
        //     $msj->to(explode(';',$mail['direccion']));
        //     $msj->subject($mail['asunto']);
        // });
    }

    public function generarHTMLTarea($id)
    {
        $tarea = DB::table('tarea')
            ->leftJoin('users AS us', 'tarea.Users_idSolicitante', '=', 'us.id')
            ->leftJoin('users AS ur', 'tarea.Users_idResponsable', '=', 'ur.id')
            ->leftJoin('clasificaciontarea', 'tarea.ClasificacionTarea_idClasificacionTarea', '=', 'clasificaciontarea.idClasificacionTarea')
            ->leftJoin('clasificaciontareadetalle', 'clasificaciontarea.idClasificacionTarea', '=', 'clasificaciontareadetalle.ClasificacionTarea_idClasificacionTarea')
            ->leftJoin('dependencia AS ds', 'us.Dependencia_idDependencia', '=', 'ds.idDependencia')
            ->leftJoin('dependencia AS dr', 'ur.Dependencia_idDependencia', '=', 'dr.idDependencia')
            ->select(DB::raw('tarea.*, us.name as nombreSolicitanteTarea, ur.name as nombreResponsableTarea, nombreClasificacionTarea, nombreClasificacionTareaDetalle, ds.nombreDependencia as dependenciaSolicitanteTarea, dr.nombreDependencia as dependenciaResponsableTarea'))
            ->where('idTarea', '=', $id)
            ->get();

        $datosTarea = get_object_vars($tarea[0]);

        $mensaje = "<link rel='stylesheet' href='//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css'>
        <table class='table table-striped table-bordered' style='font-size:10pt'>
            <tr>
                <th colspan='2'>TAREA</th>
            </tr>
            <tr>
                <th colspan='1'>Número: " . $datosTarea['numeroTarea'] . "</th>
                <th colspan='1'>Fecha de elaboración: " . $datosTarea['fechaElaboracionTarea'] . "</th>
            </tr>
            <tr>
                <th colspan='1'>Asunto: " . $datosTarea['asuntoTarea'] . "</th>
                <th colspan='1'>Prioridad: " . $datosTarea['prioridadTarea'] . "</th>
            </tr>
            <tr>
                <th colspan='1'>Solicitante: " . $datosTarea['nombreSolicitanteTarea'] . "</th>
                <th colspan='1'>Dependencia del solicitante: " . $datosTarea['dependenciaSolicitanteTarea'] . "</th>
            </tr>
            <tr>
                <th colspan='1'>Clasificación: " . $datosTarea['nombreClasificacionTarea'] . "</th>
                <th colspan='1'>Sub clasificación: " . $datosTarea['nombreClasificacionTareaDetalle'] . "</th>
            </tr>
            <tr>
                <th colspan='1'>Dependencia del responsable: " . $datosTarea['dependenciaResponsableTarea'] . "</th>
                <th colspan='1'>Responsable: " . $datosTarea['nombreResponsableTarea'] . "</th>
            </tr>
            <tr>
                <th colspan='1'>Estado: " . $datosTarea['estadoTarea'] . "</th>
                <th colspan='1'>Fecha de vencimiento: " . $datosTarea['fechaVencimientoTarea'] . "</th>
            </tr>
            <tr>
                <th colspan='1'>Fecha de solución: " . $datosTarea['fechaSolucionTarea'] . "</th>
            </tr>
            <tr>
                <th colspan='2'>Descripción: " . $datosTarea['descripcionTarea'] . "</th>
            </tr>
            <tr>
                <th colspan='2'>Solución: " . $datosTarea['solucionTarea'] . "</th>
            </tr>
        </table>";

        return $mensaje;
    }

    public function mostrarDashboardTarea()
    {
        $totalSolicitante = DB::table('tarea')
            ->leftJoin('users AS us', 'tarea.Users_idSolicitante', '=', 'us.id')
            ->select(DB::raw('COUNT(Users_idSolicitante) as totalSolicitudes, us.name as nombreSolicitanteTarea'))
            ->groupBy('Users_idSolicitante')
            ->get();

        $totalResponsable = DB::table('tarea')
            ->leftJoin('users AS ur', 'tarea.Users_idResponsable', '=', 'ur.id')
            ->select(DB::raw('COUNT(Users_idResponsable) as totalResponsable, ur.name as nombreResponsableTarea'))
            ->groupBy('Users_idResponsable')
            ->get();

        $totalEstado = DB::table('tarea')
            ->select(DB::raw('COUNT(estadoTarea) as totalEstado, estadoTarea'))
            ->groupBy('estadoTarea')
            ->get();

        $totalDepSolicitante = DB::table('tarea')
            ->leftJoin('users AS us', 'tarea.Users_idSolicitante', '=', 'us.id')
            ->leftJoin('dependencia AS dp', 'us.Dependencia_idDependencia', '=', 'dp.idDependencia')
            ->select(DB::raw('COUNT(Users_idSolicitante) as totalDepSolicitante, nombreDependencia as nombreDependenciaSolicitante'))
            ->groupBy('Users_idSolicitante')
            ->get();

        $totalDepResponsable = DB::table('tarea')
            ->leftJoin('users AS ur', 'tarea.Users_idResponsable', '=', 'ur.id')
            ->leftJoin('dependencia AS dp', 'ur.Dependencia_idDependencia', '=', 'dp.idDependencia')
            ->select(DB::raw('COUNT(Users_idResponsable) as totalDepResponsable, nombreDependencia as nombreDependenciaResponsable'))
            ->groupBy('Users_idResponsable')
            ->get();

        $totalClasificacion = DB::table('tarea')
            ->leftJoin('clasificaciontarea', 'tarea.ClasificacionTarea_idClasificacionTarea', '=', 'clasificaciontarea.idClasificacionTarea')
            ->select(DB::raw('COUNT(ClasificacionTarea_idClasificacionTarea) as totalClasificacion, nombreClasificacionTarea'))
            ->groupBy('ClasificacionTarea_idClasificacionTarea')
            ->get();

        return view('formatos.tareadashboardimpresion', compact('totalSolicitante', 'totalResponsable', 'totalEstado', 'totalDepSolicitante', 'totalDepResponsable', 'totalClasificacion'));
    }
}
