<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\MovimientoPQRSFRequest;
use Input;
use File;
use Validator;
use Response;
use Mail;
use Intervention\Image\ImageManager;

class MovimientoPQRSFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = array();
        return view('movimientopqrsfgrid', compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {     
        return view('movimientopqrsfform');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MovimientoPQRSFRequest $request)
    {
        if($request->ajax()) 
            return;
            
        \App\MovimientoPQRSF::create([
            'fechaSolicitudMovimientoPQRSF' => $request['fechaSolicitudMovimientoPQRSF'],
            'nombreSolicitanteMovimientoPQRSF' => $request['nombreSolicitanteMovimientoPQRSF'],
            'telefonoSolicitanteMovimientoPQRSF' => $request['telefonoSolicitanteMovimientoPQRSF'],
            'movilSolicitanteMovimientoPQRSF' => $request['movilSolicitanteMovimientoPQRSF'],
            'correoSolicitanteMovimientoPQRSF' => $request['correoSolicitanteMovimientoPQRSF'],
            'tipoMovimientoPQRSF' => $request['tipoMovimientoPQRSF'],
            'mensajeMovimientoPQRSF' => $request['mensajeMovimientoPQRSF'],
            'autorizacionMovimientoPQRSF' => $request['autorizacionMovimientoPQRSF'],
            'respuestaMovimientoPQRSF' => $request['respuestaMovimientoPQRSF'],
            'correoRespuestaMovimientoPQRSF' => 0,
            'rutaAdjuntoRespuestaMovimientoPQRSF' => null,
            'Compania_idCompania' => \Session::get('idCompania'),
            'Users_idMovimientoPQRSF' => null

        ]);
        
        return redirect('movimientopqrsf/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $movimientopqrsf = \App\MovimientoPQRSF::find($id);

        return view('movimientopqrsfform', compact('movimientopqrsf'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MovimientoPQRSFRequest $request, $id)
    {
        if($request->ajax()) 
            return;

        if(null !== Input::file('rutaAdjuntoRespuestaMovimientoPQRSF'))
        {
            $image = Input::file('rutaAdjuntoRespuestaMovimientoPQRSF');
            $imageName = 'storage/images/pqrsf/'.$request->file('rutaAdjuntoRespuestaMovimientoPQRSF')->getClientOriginalName();
            $manager = new ImageManager();
            $manager->make($image->getRealPath())->heighten(500)->save($imageName);
        }
        else
        {
            $imageName = null;
        }
        
        $movimientopqrsf = \App\MovimientoPQRSF::find($id);
        $movimientopqrsf->update([
            'respuestaMovimientoPQRSF' => $request['respuestaMovimientoPQRSF'],
            'fechaRespuestaMovimientoPQRSF' => $request['fechaRespuestaMovimientoPQRSF'],
            'rutaAdjuntoRespuestaMovimientoPQRSF' => $imageName,
            'Users_idMovimientoPQRSF' => \Session::get("idUsuario")
        ]);   

        $this->enviarCorreoPQRSF($request, $id);

        return redirect("movimientopqrsf");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\MovimientoPQRSF::destroy($id);
        return redirect('movimientopqrsf');
    }

    public function enviarCorreoPQRSF($request, $id)
    {
        $movimientopqrsf = \App\MovimientoPQRSF::find($id);
        $user = \App\User::find($movimientopqrsf->Users_idMovimientoPQRSF);

        if($movimientopqrsf->correoRespuestaMovimientoPQRSF == 0)
        {
            $datos = ['rutaAdjunto' => $movimientopqrsf->rutaAdjuntoRespuestaMovimientoPQRSF, 
            'direccion' => $movimientopqrsf->correoSolicitanteMovimientoPQRSF, 
            'asunto' => 'Respuesta de PQRSF', 
            'mensaje' => '
            <b>Solicitado por: </b>'.$movimientopqrsf->nombreSolicitanteMovimientoPQRSF.' </br>
            <b>Enviado: </b>'.$movimientopqrsf->fechaSolicitudMovimientoPQRSF.' </br>
            <b>Mensaje: </b>'.$movimientopqrsf->mensajeMovimientoPQRSF.'<br><br> 
            <b>Resuelto por: </b>'.$user->name.'<br> 
            <b>En la fecha: </b>'.$movimientopqrsf->fechaRespuestaMovimientoPQRSF.'<br>
            <b>Respuesta</b>: '.$movimientopqrsf->respuestaMovimientoPQRSF];

            Mail::send('email.correo',$datos,function($msj) use ($datos)
            {
                $msj->to(explode(';',$datos['direccion']));
                $msj->subject($datos['asunto']);
                
                if (($datos['rutaAdjunto'] != '' || $datos['rutaAdjunto'] != null) && file_exists(public_path().'/'.$datos['rutaAdjunto'])) 
				{
                    $msj->attach(public_path().'/'.$datos['rutaAdjunto']);
                }
            }); 

            $movimientopqrsf->update([
                'correoRespuestaMovimientoPQRSF' => 1
            ]);
        }
    }
}
