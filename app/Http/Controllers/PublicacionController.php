<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tercero;
use DB;
use Input;
use Validator;
use Response;
use Storage;
use PDFMerger;
use stdClass;

class PublicacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $radicado = (isset($_GET['idRadicadoIngreso']) || isset($_GET['idRadicadoSalida']) || isset($_GET['idRadicadoInterno'])) ? 'radicadoDocumento = 1' : '';

        $estructura = DB::table('estructura')
            ->leftJoin('dependencia', 'estructura.Dependencia_idDependencia', '=', 'dependencia.idDependencia')
            ->leftJoin('serie', 'estructura.Serie_idSerie', '=', 'serie.idSerie')
            ->leftJoin('subserie', 'estructura.SubSerie_idSubSerie', '=', 'subserie.idSubSerie')
            ->leftJoin('documento', 'estructura.Documento_idDocumento', '=', 'documento.idDocumento')
            ->leftJoin('dependenciarol', 'dependencia.idDependencia', '=', 'dependenciarol.Dependencia_idDependencia')
            ->leftJoin('userscompaniarol', 'dependenciarol.Rol_idRol', '=', 'userscompaniarol.Rol_idRol')
            ->leftJoin('documentorol', 'documento.idDocumento', '=', 'documentorol.Documento_idDocumento')
            ->leftJoin('userscompaniarol as ucrdoc', 'documentorol.Rol_idRol', '=', 'ucrdoc.Rol_idRol')
            ->select(DB::raw('idDependencia, idSerie, idSubSerie, idDocumento, nombreDependencia, nombreSerie, nombreSubSerie, nombreDocumento'))
            ->where('userscompaniarol.Users_id', '=', \Session::get('idUsuario'))
            ->where('ucrdoc.Users_id', '=', \Session::get('idUsuario'))
            ->where('estructura.Compania_idCompania', '=', \Session::get('idCompania'))
            ->where('cargarDependenciaRol', '=', '1')
            ->groupBy('idDependencia', 'idSerie', 'idSubSerie', 'idDocumento')
            ->orderBy('nombreDependencia', 'nombreSerie', 'nombreSubSerie', 'nombreDocumento');

        if ($radicado != '') {
            $estructura->whereRaw($radicado);
        }

        $estructura = $estructura->get();

        $flujo = \App\Flujo::All()->pluck('nombreFlujo', 'idFlujo');

        return view('publicacionform', compact('estructura', 'flujo'));
    }

    public function cargarArchivoPublicacion()
    {
        $input = Input::all();

        $rules = array();

        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return Response::make($validation->errors->first(), 400);
        }

        $destinationPath = public_path() . '/storage/temporal'; //Guardo en la carpeta  temporal

        $extension = Input::file('file')->getClientOriginalExtension();
        $fileName = Input::file('file')->getClientOriginalName(); // nombre de archivo
        $nombreArchivoReplace = str_replace(" ", "_", $fileName);
        $nombreArchivoReplace = str_replace("(", "_", $nombreArchivoReplace);
        $nombreArchivoReplace = str_replace(")", "_", $nombreArchivoReplace);
        $upload_success = Input::file('file')->move($destinationPath, "$nombreArchivoReplace");

        if ($upload_success) {
            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $codigo = DB::table('publicacion')
            ->select(DB::raw("LPAD(IF(codigoPublicacion IS NULL, '1', MAX(codigoPublicacion) +1), 9,'0') AS codigoPublicacion"))
            ->get();

        $codigoPublicacion = get_object_vars($codigo[0])['codigoPublicacion'];

        DB::beginTransaction();
        try {
            $flujoManualId = null;
            if ($request['Flujo_idFlujo'] != '') {
                $flujo =  \App\Flujo::find($request['Flujo_idFlujo']);
                \App\FlujoManual::create([
                    'nombreFlujoManual' => "{$flujo->nombreFlujo} - $codigoPublicacion",
                    'Users_idFlujoManual' => \Session::get('idUsuario')
                ]);
                $flujoManual = \App\FlujoManual::All()->last();
                $flujoManualId = $flujoManual->idFlujoManual;
            }

            \App\Publicacion::create([
                'codigoPublicacion' => $codigoPublicacion,
                'Dependencia_idDependencia' => $request['Dependencia_idDependencia'],
                'Serie_idSerie' => $request['Serie_idSerie'],
                'SubSerie_idSubSerie' => $request['SubSerie_idSubSerie'],
                'Documento_idDocumento' => $request['Documento_idDocumento'],
                'paginasPublicacion' => $request['paginasPublicacion'],
                'Radicado_idRadicadoIngreso' => ($request['Radicado_idRadicadoIngreso'] == '' ? null : $request['Radicado_idRadicadoIngreso']),
                'Radicado_idRadicadoSalida' => ($request['Radicado_idRadicadoSalida'] == '' ? null : $request['Radicado_idRadicadoSalida']),
                'Radicado_idRadicadoInterno' => ($request['Radicado_idRadicadoInterno'] == '' ? null : $request['Radicado_idRadicadoInterno']),
                'FlujoManual_idFlujoManual' => $flujoManualId,
                'Compania_idCompania' => \Session::get("idCompania")
            ]);

            $publicacion = \App\Publicacion::All()->last();

            $directorio = DB::table('estructura')
                ->leftJoin('dependencia', 'estructura.Dependencia_idDependencia', '=', 'dependencia.idDependencia')
                ->leftJoin('serie', 'estructura.Serie_idSerie', '=', 'serie.idSerie')
                ->leftJoin('subserie', 'estructura.SubSerie_idSubSerie', '=', 'subserie.idSubSerie')
                ->leftJoin('documento', 'estructura.Documento_idDocumento', '=', 'documento.idDocumento')
                ->where('estructura.Dependencia_idDependencia', '=', $request['Dependencia_idDependencia'])
                ->where('estructura.Serie_idSerie', '=', $request['Serie_idSerie'])
                ->where('estructura.SubSerie_idSubSerie', '=', $request['SubSerie_idSubSerie'])
                ->where('estructura.Documento_idDocumento', '=', $request['Documento_idDocumento'])
                ->select(DB::raw('CONCAT(directorioDependencia,"/",directorioSerie,"/",directorioSubSerie,"/",directorioDocumento) AS directorio'))
                ->get();

            $nombreDir = 'storage/estructura/' . get_object_vars($directorio[0])['directorio'];
            $rutaReplace = str_replace(" ", "_", $request['rutaImagenPublicacionVersion']);
            $nombreDoc = 'storage/estructura/' . get_object_vars($directorio[0])['directorio'] . '/' . $codigoPublicacion . '_' . $rutaReplace;

            $origen = public_path() . '/storage/temporal/' . $request['rutaImagenPublicacionVersion'];
            if ($request['Radicado_idRadicadoInterno'] != '') {
                $pdf = new PDFMerger();
                $radicadointerno = \App\RadicadoInterno::find($request['Radicado_idRadicadoInterno']);
                $rutaRadicadoInterno = public_path() . '/storage/radicadointerno/' . $radicadointerno['numeroRadicadoInterno'] . '.pdf';

                $pdf->addPDF($rutaRadicadoInterno, 'all');
                $pdf->addPDF($origen, 'all');

                $origen = public_path() . '/storage/temporal/' . $request['rutaImagenPublicacionVersion'];
                $pdf->merge('file', $origen);
            }

            $destino = public_path() . '/' . $nombreDir . '/' . $codigoPublicacion . '_' . $rutaReplace;

            \App\PublicacionVersion::create([
                'Publicacion_idPublicacion' => $publicacion->idPublicacion,
                'fechaPublicacionVersion' => $request['fechaPublicacionVersion'],
                'numeroPublicacionVersion' => $request['numeroPublicacionVersion'],
                'rutaImagenPublicacionVersion' => $nombreDoc,
                'Users_idPublicacionVersion' => \Session::get('idUsuario')
            ]);

            $publicacionversion = \App\PublicacionVersion::All()->last();

            $metadatos = DB::table('documentometadato')
                ->leftjoin('metadato', 'documentometadato.Metadato_idMetadato', "=", 'metadato.idMetadato')
                ->select(DB::raw('documentometadato.*,metadato.*'))
                ->where('Documento_idDocumento', "=", $request['Documento_idDocumento'])
                ->get();

            $documento = \App\Documento::where('tablaDocumento', 'tercero')
                ->where('generaMetadatoFlujoDocumento', true)
                ->where('idDocumento', $request['Documento_idDocumento'])
                ->first();

            $crear = false;
            $tercero = new stdClass();
            if ($documento) {
                $crear = true;
            }

            for ($i = 0; $i < count($metadatos); $i++) {
                $metadato = get_object_vars($metadatos[$i]);
                $valor = $request[$metadato["idDocumentoMetadato"]];

                if ($metadato["campoTablaDocumentoMetadato"]) {
                    $campo = $metadato["campoTablaDocumentoMetadato"];
                    $tercero->$campo = $valor;
                }

                \App\PublicacionDocumentoMetadato::create([
                    'Publicacion_idPublicacion' => $publicacion->idPublicacion,
                    'DocumentoMetadato_idDocumentoMetadato' => $metadato["idDocumentoMetadato"],
                    'valorPublicacionDocumentoMetadato' => $valor,
                    'PublicacionVersion_idPublicacionVersion' => $publicacionversion->idPublicacionVersion
                ]);
            }

            if ($crear) {
                $tercero->tipoTercero = 'Proveedor';
                $tercero->estadoTercero = 'Activo';
                $tercero->codigoTercero = $tercero->documentoTercero;

                \App\Tercero::updateOrCreate(
                    [
                        'documentoTercero' => $tercero->documentoTercero,
                        'nombreCompletoTercero' => $tercero->nombreCompletoTercero,
                        'codigoTercero' => $tercero->codigoTercero,
                        'tipoTercero' => $tercero->tipoTercero,
                        'Compania_idCompania' => \Session::get("idCompania")
                    ],
                    [
                        'documentoTercero' => $tercero->documentoTercero,
                        'nombreCompletoTercero' => $tercero->nombreCompletoTercero,
                        'codigoTercero' => $tercero->codigoTercero,
                        'tipoTercero' => $tercero->tipoTercero,
                        'estadoTercero' => $tercero->estadoTercero,
                        'Compania_idCompania' => \Session::get("idCompania")
                    ]
                );
            }

            if (file_exists($origen)) {
                copy($origen, $destino);
                unlink($origen);
                if ($request['Flujo_idFlujo'] != '') {
                    self::iniciarFlujo($request['Flujo_idFlujo'], $flujoManualId);
                }
                $informacion = "Publicacion realizada correctamente";
                DB::commit();
            }
        } catch (\Exception $e) {
            $informacion = $e->getMessage();
            DB::rollback();
        }

        echo json_encode($informacion);
    }

    private function iniciarFlujo($idFlujo, $idFlujoManual)
    {
        $tareas = \App\FlujoTarea::where('Flujo_idFlujo', '=', $idFlujo)->lists('idFlujoTarea');

        foreach ($tareas as $key => $tarea) {
            \App\FlujoTareaRadicado::create([
                'FlujoTarea_idFlujoTarea' => $tarea,
                'Radicado_idRadicadoIngreso' => null,
                'Radicado_idRadicadoSalida' => null,
                'FlujoManual_idFlujoManual' => $idFlujoManual,
                'estadoFlujoTareaRadicado' => 'SinAsignar'
            ]);
        }

        $tareainicial = \App\FlujoTarea::where('Flujo_idFlujo', $idFlujo)->first();

        \App\FlujoTareaRadicado::where('FlujoTarea_idFlujoTarea', $tareainicial->idFlujoTarea)
            ->where('FlujoManual_idFlujoManual', $idFlujoManual)
            ->update(['estadoFlujoTareaRadicado' => 'EnProceso']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $imagen = DB::table('publicacionversion')
            ->select(DB::raw('rutaImagenPublicacionVersion'))
            ->where('Publicacion_idPublicacion', '=', $id)
            ->get();

        foreach ($imagen as $key => $value) {
            if (file_exists(public_path() . '/' . $value->rutaImagenPublicacionVersion)) {
                unlink(public_path() . '/' . $value->rutaImagenPublicacionVersion);
            }
        }

        \App\Publicacion::destroy($id);

        return response()->json(['Publicacion eliminada']);
    }

    public function armarMetadatos()
    {
        $idDocumento = $_POST['idDocumento'];

        $metadatos = DB::table('documentometadato')
            ->leftjoin('metadato', 'documentometadato.Metadato_idMetadato', "=", 'metadato.idMetadato')
            ->leftJoin('documento', 'documentometadato.Documento_idDocumento', "=", 'documento.idDocumento')
            ->select(DB::raw('documentometadato.*,metadato.*, documento.*'))
            ->where('idDocumento', "=", $idDocumento)
            ->orderBy('ordenDocumentoMetadato')
            ->get();

        $estructura = "";
        $condicion = "";
        $campos = "";
        $tabla = "";

        foreach ($metadatos as $key => $value) {
            $colorIndice = '';
            $eventoIndice = '';

            if ($value->indiceDocumentoMetadato == 1) {
                $colorIndice = 'style="background-color: #5cb85c"';
                $eventoIndice = 'onchange="consultarDatosMetadatos(this.value);"';
                $condicion .= "$value->campoTablaDocumentoMetadato = ";
            }

            if ($value->campoTablaDocumentoMetadato) {
                $campos .= "$value->campoTablaDocumentoMetadato,";
            }
            $tabla = $value->tablaDocumento;

            switch ($value->tipoMetadato) {
                case 'Texto':
                    $estructura .= "
                    <div id='form-group' class='div-responsive-doce div-agrupador'>
                        <label for='$value->idDocumentoMetadato' class='label-responsive-dos'>$value->nombreMetadato</label>
                        <div class='div-input-responsive'>
                            <div class='input-group'>
                                <span  $colorIndice class='input-group-addon'>
                                    <i class='fa fa-pen-square'></i>
                                </span>
                                <input type='text' $eventoIndice class='form-control' name='$value->idDocumentoMetadato' id='$value->idDocumentoMetadato' placeholder='Digite $value->nombreMetadato' required max='$value->longitudMetadato'>
                            </div>
                        </div>
                    </div>";
                    break;

                case 'Numero':
                    $estructura .= "
                    <div id='form-group' class='div-responsive-doce div-agrupador'>
                        <label for='$value->idDocumentoMetadato' class='label-responsive-dos'>$value->nombreMetadato</label>
                        <div class='div-input-responsive'>
                            <div class='input-group'>
                                <span $colorIndice class='input-group-addon'>
                                    <i class='fa fa-pen-square'></i>
                                </span>
                                <input type='number' $eventoIndice class='form-control' name='$value->idDocumentoMetadato' id='$value->idDocumentoMetadato' placeholder='Digite $value->nombreMetadato' required max='$value->longitudMetadato'>
                            </div>
                        </div>
                    </div>";
                    break;

                case 'Fecha':
                    $estructura .= "
                    <div id='form-group' class='div-responsive-doce div-agrupador'>
                        <label for='$value->idDocumentoMetadato' class='label-responsive-dos'>$value->nombreMetadato</label>
                        <div class='div-input-responsive'>
                            <div class='input-group'>
                                <span $colorIndice class='input-group-addon'>
                                    <i class='fa fa-calendar'></i>
                                </span>
                                <input type='date' class='form-control' name='$value->idDocumentoMetadato' id='$value->idDocumentoMetadato' placeholder='Seleccione $value->nombreMetadato' required max='$value->longitudMetadato'>
                            </div>
                        </div>
                    </div>";
                    break;

                case 'Hora':
                    $estructura .= "
                    <div id='form-group' class='div-responsive-doce div-agrupador'>
                        <label for='$value->idDocumentoMetadato' class='label-responsive-dos'>$value->nombreMetadato</label>
                        <div class='div-input-responsive'>
                            <div class='input-group'>
                                <span $colorIndice class='input-group-addon'>
                                    <i class='fa fa-calendar'></i>
                                </span>
                                <input type='text' class='form-control' name='$value->idDocumentoMetadato' id='$value->idDocumentoMetadato' placeholder='Seleccione $value->nombreMetadato' required max='$value->longitudMetadato'>
                            </div>
                        </div>
                    </div>
                    <script type='text/javascript'>
                        $('#$value->idDocumentoMetadato').datetimepicker(({
                            format: 'HH:mm:ss'
                        }));
                    </script>";
                    break;

                case 'Lista':
                    $estructura .= "
                    <div id='form-group' class='div-responsive-doce div-agrupador'>
                        <label for='$value->idDocumentoMetadato' class='label-responsive-dos'>$value->nombreMetadato</label>
                        <div class='div-input-responsive'>
                            <div class='input-group'>
                                <span $colorIndice class='input-group-addon'>
                                    <i class='fa fa-list-ul'></i>
                                </span>
                                <select $eventoIndice class='form-control' name='$value->idDocumentoMetadato' id='$value->idDocumentoMetadato' required>";

                    $opcion = explode(',', $value->opcionMetadato);

                    $estructura .= "<option value=''>- Seleccione -</option>";

                    foreach ($opcion as $key => $option) {
                        $estructura .= "
                                    <option value='$option'>$option</option>";
                    }

                    $estructura .= "
                                </select>
                            </div>
                        </div>
                    </div>";
                    break;

                case 'Editor':
                    $estructura .= "
                    <div id='form-group' class='div-responsive-doce div-agrupador'>
                        <label for='$value->idDocumentoMetadato' class='label-responsive-dos'>$value->nombreMetadato</label>
                        <div class='div-input-responsive'>
                            <div class='input-group'>
                                <span $colorIndice class='input-group-addon'>
                                    <i class='fa fa-pen-square'></i>
                                </span>
                                <textarea id='$value->idDocumentoMetadato' name='$value->idDocumentoMetadato' class='form-control' placeholder='Digite $value->nombreMetadato' required max='$value->longitudMetadato'></textarea>
                            </div>
                        </div>
                    </div>";
                    break;

                default:
                    $estructura .= "
                    <div id='form-group' class='div-responsive-doce div-agrupador'>
                        <label for='$value->idDocumentoMetadato' class='label-responsive-dos'>$value->nombreMetadato</label>
                        <div class='div-input-responsive'>
                            <div class='input-group'>
                                <span $colorIndice class='input-group-addon'>
                                    <i class='fa fa-pen-square'></i>
                                </span>
                                <input type='text' class='form-control' name='$value->idDocumentoMetadato' id='$value->idDocumentoMetadato' placeholder='Digite $value->nombreMetadato' required max='$value->longitudMetadato'>
                            </div>
                        </div>
                    </div>";
                    break;
            }
        }


        $campos = substr($campos, 0, -1);
        $condicion = substr($condicion, 0, -3);

        $estructura .= "
        <input type='hidden' id='camposDocumentoMetadato' value='$campos'>
        <input type='hidden' id='condicionDocumentoMetadato' value='$condicion'>
        <input type='hidden' id='tablaDocumento' value='$tabla'>";

        echo json_encode($estructura);
    }

    public function consultarDatosMetadatos()
    {
        $valorCondicion = (isset($_POST['value']) && $_POST['value'] != '') ? $_POST['value'] : '';
        $camposDocumentoMetadato = (isset($_POST['campos']) && $_POST['campos'] != '') ? $_POST['campos'] : '';
        $idDocumento = (isset($_POST['idDocumento']) && $_POST['idDocumento'] != '') ? $_POST['idDocumento'] : '';
        $tablaDocumento = (isset($_POST['tablaDocumento']) && $_POST['tablaDocumento'] != '') ? $_POST['tablaDocumento'] : '';
        $condicionDocumentoMetadato = (isset($_POST['condicion']) && $_POST['condicion'] != '') ? $_POST['condicion'] : '';

        $informationSchema = DB::table("information_schema.COLUMNS")
            ->leftJoin('information_schema.TABLES', function ($join) {
                $join->on('COLUMNS.TABLE_NAME', '=', 'TABLES.TABLE_NAME');
                $join->on('COLUMNS.TABLE_SCHEMA', '=', 'TABLES.TABLE_SCHEMA');
            })
            ->select(DB::raw('COLUMN_NAME'))
            ->where('COLUMNS.TABLE_SCHEMA', '=', 'elyon')
            ->where('COLUMNS.TABLE_NAME', '=', $tablaDocumento)
            ->get();

        $consultaMetadatos = DB::table("$tablaDocumento")
            ->select(DB::raw($camposDocumentoMetadato))
            ->where($condicionDocumentoMetadato, "=", $valorCondicion);

        if (!empty($informationSchema)) {
            for ($i = 0; $i < count($informationSchema); $i++) {
                if (strpos(get_object_vars($informationSchema[$i])['COLUMN_NAME'], 'idCompania') !== false) {
                    $consultaMetadatos->whereRaw(get_object_vars($informationSchema[$i])['COLUMN_NAME'] . " = " . \Session::get('idCompania'));
                }
            }
        }

        $consultaMetadatos = $consultaMetadatos->get();

        $valores = array();
        if (!empty($consultaMetadatos)) {
            $datosMetadatos = get_object_vars($consultaMetadatos[0]);

            $propiedades = DB::table('documentometadato')
                ->select('idDocumentoMetadato', 'campoTablaDocumentoMetadato')
                ->where('Documento_idDocumento', '=', $idDocumento)
                ->where('campoTablaDocumentoMetadato', '!=', 'null')
                ->get();

            for ($i = 0; $i < count($propiedades); $i++) {
                $datosPropiedades = get_object_vars($propiedades[$i]);

                $valores[$i]["campo"] = $datosPropiedades["idDocumentoMetadato"];
                $valores[$i]["valor"] = $datosMetadatos[$datosPropiedades["campoTablaDocumentoMetadato"]];
            }
        }

        echo json_encode($valores);
    }
}
