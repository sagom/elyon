<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\TerceroRequest;
use Session;

class TerceroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("tercero");
        if(!empty($datos))
            return view('tercerogrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('terceroform');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TerceroRequest $request)
    {
        if($request->ajax()) 
            return;
        
        \App\Tercero::create([
            'documentoTercero' => $request['documentoTercero'],
            'nombreCompletoTercero' => $request['nombreCompletoTercero'],
            'codigoTercero' => $request['codigoTercero'],
            'tipoTercero' => $request['tipoTercero'],
            'estadoTercero' => $request['estadoTercero'],
            'Compania_idCompania' => \Session::get('idCompania')
        ]);
    
        return redirect('tercero');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tercero = \App\Tercero::find($id);

        return view('terceroform', compact('tercero'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TerceroRequest $request, $id)
    {
        if($request->ajax()) 
            return;

        $tercero = \App\Tercero::find($id);
        $tercero->update([
            'documentoTercero' => $request['documentoTercero'],
            'nombreCompletoTercero' => $request['nombreCompletoTercero'],
            'codigoTercero' => $request['codigoTercero'],
            'tipoTercero' => $request['tipoTercero'],
            'estadoTercero' => $request['estadoTercero'],
            'Compania_idCompania' => \Session::get('idCompania')
        ]);

        return redirect('tercero');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Tercero::destroy($id);
        return redirect('tercero');
    }
}
