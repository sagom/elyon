<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function consultarPermisos($opcion)
    {
        $consulta = DB::table("rolopcion")
        ->leftJoin("rol", "rolopcion.Rol_idRol", "=", "rol.idRol")
        ->leftJoin("opcion", "rolopcion.Opcion_idOpcion", "=", "opcion.idOpcion")
        ->leftJoin("userscompaniarol", "rol.idRol", "=", "userscompaniarol.Rol_idRol")
        ->select(DB::raw("adicionarRolOpcion, modificarRolOpcion, eliminarRolOpcion, imprimirRolOpcion, aprobarRolOpcion"))
        ->where("rutaOpcion", "=", "$opcion")
        ->where("Users_id", "=", \Session::get("idUsuario"))
        ->where('userscompaniarol.Compania_idCompania', '=', \Session::get('idCompania'))
        ->get();

        return($consulta);
    }    
}
