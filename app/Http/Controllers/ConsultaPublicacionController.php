<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Mail;
use File;

class ConsultaPublicacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publicacion = DB::table('publicacion')
            ->leftJoin('dependencia', 'publicacion.Dependencia_idDependencia', '=', 'dependencia.idDependencia')
            ->leftJoin('serie', 'publicacion.Serie_idSerie', '=', 'serie.idSerie')
            ->leftJoin('subserie', 'publicacion.SubSerie_idSubSerie', '=', 'subserie.idSubSerie')
            ->leftJoin('documento', 'publicacion.Documento_idDocumento', '=', 'documento.idDocumento')
            ->leftJoin('dependenciarol', 'dependencia.idDependencia', '=', 'dependenciarol.Dependencia_idDependencia')
            ->leftJoin('userscompaniarol', 'dependenciarol.Rol_idRol', '=', 'userscompaniarol.Rol_idRol')
            ->leftJoin('documentorol', 'documento.idDocumento', '=', 'documentorol.Documento_idDocumento')
            ->leftJoin('userscompaniarol as ucrdoc', 'documentorol.Rol_idRol', '=', 'ucrdoc.Rol_idRol')
            ->leftJoin('publicaciondocumentometadato', 'publicacion.idPublicacion', '=', 'publicaciondocumentometadato.Publicacion_idPublicacion')
            ->leftJoin('publicacionversion', 'publicacion.idPublicacion', '=', 'publicacionversion.Publicacion_idPublicacion')
            ->select(DB::raw('idDependencia, idSerie, idSubSerie, idDocumento, nombreDependencia, nombreSerie, nombreSubSerie, nombreDocumento'))
            ->where('userscompaniarol.Users_id', '=', \Session::get('idUsuario'))
            ->where('ucrdoc.Users_id', '=', \Session::get('idUsuario'))
            ->where("publicacion.Compania_idCompania", "=", \Session::get('idCompania'))
            ->where('consultarDependenciaRol', '=', '1')
            ->where('numeroPublicacionversion', '=', '1')
            ->groupBy('idDependencia', 'idSerie', 'idSubSerie', 'idDocumento')
            ->orderBy('nombreDependencia')
            ->orderBy('nombreSerie')
            ->orderBy('nombreSubSerie')
            ->orderBy('nombreDocumento');

        $dependencia = (isset($_GET['idDependencia']) && ($_GET['idDependencia'] != '' && $_GET['idDependencia'] != 'undefined')) ? 'publicacion.Dependencia_idDependencia IN (' . $_GET['idDependencia'] . ')' : '';
        $condGeneral = '';
        if ($dependencia != '') {
            $publicacion->whereRaw($dependencia);
            $condGeneral = $condGeneral . ($condGeneral != '' ? ' and ' : '') . $dependencia;
        }

        $documento = (isset($_GET['idDocumento']) && ($_GET['idDocumento'] != '' && $_GET['idDocumento'] != 'undefined')) ? 'publicacion.Documento_idDocumento IN (' . $_GET['idDocumento'] . ')' : '';
        if ($documento != '') {
            $publicacion->whereRaw($documento);
            $condGeneral = $condGeneral . ($condGeneral != '' ? ' and ' : '') . $documento;
        }

        $condicion = (isset($_GET['condicion']) && $_GET['condicion'] != '') ? $_GET['condicion'] : '';
        if ($condicion != '') {
            $condicion = str_replace("|", "'", $condicion);
            $condicion = str_replace("**", "%", $condicion);
            $publicacion->whereRaw($condicion);
            $condGeneral = $condGeneral . ($condGeneral != '' ? ' and ' : '') . $condicion;
        }

        $condGeneral = str_replace("%", "**", $condGeneral);

        $publicacion = $publicacion->get();

        return view('consultapublicacionform', compact('publicacion', 'condGeneral'));
    }

    public function consultapublicacionfiltro()
    {
        $idDependencia = \App\Dependencia::where("Compania_idCompania", "=", \Session::get('idCompania'))->lists('idDependencia');
        $nombreDependencia = \App\Dependencia::where("Compania_idCompania", "=", \Session::get('idCompania'))->lists('nombreDependencia');
        $idDocumento = \App\Documento::where("Compania_idCompania", "=", \Session::get('idCompania'))->lists('idDocumento');
        $nombreDocumento = \App\Documento::where("Compania_idCompania", "=", \Session::get('idCompania'))->lists('nombreDocumento');
        $idMetadato = \App\Metadato::where("Compania_idCompania", "=", \Session::get('idCompania'))->lists('idMetadato');
        $nombreMetadato = \App\Metadato::where("Compania_idCompania", "=", \Session::get('idCompania'))->lists('nombreMetadato');

        return view('consultapublicacionfiltroform', compact('idDependencia', 'nombreDependencia', 'idDocumento', 'nombreDocumento', 'idMetadato', 'nombreMetadato'));
    }

    public function consultapublicaciondocumento($idDependencia, $idSerie, $idSubSerie, $idDocumento, $condGeneral)
    {
        $documentos = DB::table('publicacion')
            ->leftJoin('dependencia', 'publicacion.Dependencia_idDependencia', '=', 'dependencia.idDependencia')
            ->leftJoin('documento', 'publicacion.Documento_idDocumento', '=', 'documento.idDocumento')
            ->leftJoin('dependenciarol', 'dependencia.idDependencia', '=', 'dependenciarol.Dependencia_idDependencia')
            ->leftJoin('publicaciondocumentometadato', 'publicacion.idPublicacion', '=', 'publicaciondocumentometadato.Publicacion_idPublicacion')
            ->leftJoin('publicacionversion', 'publicacion.idPublicacion', '=', 'publicacionversion.Publicacion_idPublicacion')
            ->leftJoin('userscompaniarol AS udr', 'dependenciarol.Rol_idRol', '=', 'udr.Rol_idRol')
            ->leftJoin('users AS upv', 'publicacionversion.Users_idPublicacionVersion', '=', 'upv.id')
            ->leftJoin('documentometadato', 'publicaciondocumentometadato.DocumentoMetadato_idDocumentoMetadato', '=', 'documentometadato.idDocumentoMetadato')
            ->leftJoin('metadato', 'documentometadato.Metadato_idMetadato', '=', 'metadato.idMetadato')
            ->select(DB::raw('idPublicacion, nombreDocumento, fechaPublicacionVersion, numeroPublicacionVersion, rutaImagenPublicacionVersion, upv.name, nombreMetadato, valorPublicacionDocumentoMetadato,
                            descargarDependenciaRol, imprimirDependenciaRol, enviarDependenciaRol, eliminarDependenciaRol, versionDocumento'))
            ->where('udr.Users_id', '=', \Session::get('idUsuario'))
            ->where('consultarDependenciaRol', '=', '1')
            ->where('publicacion.Dependencia_idDependencia', '=', $idDependencia)
            ->where('Serie_idSerie', '=', $idSerie)
            ->where('SubSerie_idSubSerie', '=', $idSubSerie)
            ->where('publicacion.Documento_idDocumento', '=', $idDocumento)
            ->where('numeroPublicacionversion', '=', '1')
            ->where("publicacion.Compania_idCompania", "=", \Session::get('idCompania'))
            ->groupBy('idDocumento', 'idPublicacion', 'idPublicacionDocumentoMetadato')
            ->orderBy('nombreDocumento');

        if ($condGeneral != '|') {
            $condGeneral = str_replace("**", "%", $condGeneral);
            $documentos->whereRaw($condGeneral);
        }

        $documentos = $documentos->get();

        return view('consultapublicaciondocumentoform', compact('documentos'));
    }

    public function consultarRutaVistaPrevia()
    {
        $idPublicacion = (isset($_POST['idPublicacion']) ? $_POST['idPublicacion'] : '');

        $rutapublicacion = DB::table('publicacion')
            ->leftJoin('publicacionversion', 'publicacion.idPublicacion', '=', 'publicacionversion.Publicacion_idPublicacion')
            ->select(DB::raw('rutaImagenPublicacionVersion'))
            ->where('idPublicacion', '=', $idPublicacion)
            ->where("publicacion.Compania_idCompania", "=", \Session::get('idCompania'))
            ->get();

        return $rutapublicacion;
    }

    public function consultarMetadatos()
    {
        $idPublicacion = (isset($_POST['idPublicacion']) ? $_POST['idPublicacion'] : '');

        if ($idPublicacion != '') {
            $sw = true;

            $metadatos = DB::table('publicacion')
                ->leftJoin('publicaciondocumentometadato', 'publicacion.idPublicacion', '=', 'publicaciondocumentometadato.Publicacion_idPublicacion')
                ->leftJoin('publicacionversion', 'publicacion.idPublicacion', '=', 'publicacionversion.Publicacion_idPublicacion')
                ->leftJoin('documentometadato', 'publicaciondocumentometadato.DocumentoMetadato_idDocumentoMetadato', '=', 'documentometadato.idDocumentoMetadato')
                ->leftJoin('metadato', 'documentometadato.Metadato_idMetadato', '=', 'metadato.idMetadato')
                ->leftJoin('users AS upv', 'publicacionversion.Users_idPublicacionVersion', '=', 'upv.id')
                ->select(DB::raw('codigoPublicacion, paginasPublicacion, fechaPublicacionVersion, upv.name, nombreMetadato, valorPublicacionDocumentoMetadato'))
                ->where('idPublicacion', '=', $idPublicacion)
                ->orderBy('ordenDocumentoMetadato')
                ->get();

            $estructura = '';

            if (count($metadatos) > 0) {
                $datosEnc = get_object_vars($metadatos[0]);

                $estructura .=
                    "<div id='form-group' class='div-responsive-doce div-agrupador'>
                            <label for='codigoPublicacion' class='label-responsive-dos'>Código</label>
                            <div class='div-input-responsive'>
                                <div class='input-group'>
                                    <span class='input-group-addon'>
                                        <i class='fa fa-barcode'></i>
                                    </span>
                                    <input type='text' class='form-control' name='codigoPublicacion' id='codigoPublicacion' value='" . $datosEnc['codigoPublicacion'] . "' readonly>
                                </div>
                            </div>
                        </div>

                        <div id='form-group' class='div-responsive-doce div-agrupador'>
                            <label for='fechaPublicacionVersion' class='label-responsive-dos'>Fecha</label>
                            <div class='div-input-responsive'>
                                <div class='input-group'>
                                    <span class='input-group-addon'>
                                        <i class='fa fa-calendar'></i>
                                    </span>
                                    <input type='text' class='form-control' name='fechaPublicacionVersion' id='fechaPublicacionVersion' value='" . $datosEnc['fechaPublicacionVersion'] . "' readonly>
                                </div>
                            </div>
                        </div>

                        <div id='form-group' class='div-responsive-doce div-agrupador'>
                            <label for='paginasPublicacion' class='label-responsive-dos'>Páginas</label>
                            <div class='div-input-responsive'>
                                <div class='input-group'>
                                    <span class='input-group-addon'>
                                        <i class='fa fa-file'></i>
                                    </span>
                                    <input type='text' class='form-control' name='paginasPublicacion' id='paginasPublicacion' value='" . $datosEnc['paginasPublicacion'] . "' readonly>
                                </div>
                            </div>
                        </div>

                        <div id='form-group' class='div-responsive-doce div-agrupador'>
                            <label for='name' class='label-responsive-dos'>Publicador</label>
                            <div class='div-input-responsive'>
                                <div class='input-group'>
                                    <span class='input-group-addon'>
                                        <i class='fa fa-user'></i>
                                    </span>
                                    <input type='text' class='form-control' name='name' id='name' value='" . $datosEnc['name'] . "' readonly>
                                </div>
                            </div>
                        </div>

                        <div class='form-group'>
                            <div class='div-responsive-doce'>
                                <div class='panel panel-success'>
                                    <div class='panel-heading'>
                                        <h4 class='panel-title'>Propiedades</h4>
                                    </div>
                                    <div class='panel-body'>";

                for ($i = 0; $i < count($metadatos); $i++) {
                    $datos = get_object_vars($metadatos[$i]);

                    $estructura .= "
                    <div id='form-group' class='div-responsive-doce div-agrupador'>
                        <label for='" . $datos['nombreMetadato'] . "' class='label-responsive-dos'>" . $datos['nombreMetadato'] . "</label>
                        <div class='div-input-responsive'>
                            <div class='input-group'>
                                <span class='input-group-addon'>
                                    <i class='fa fa-pen-square'></i>
                                </span>
                                <input type='text' class='form-control' name='" . $datos['nombreMetadato'] . "' id='" . $datos['nombreMetadato'] . "' value='" . $datos['valorPublicacionDocumentoMetadato'] . "' readonly>
                            </div>
                        </div>
                    </div>";
                }

                $estructura .= "</div>
                        </div>
                    </div>
                </div>";
            }
        }

        echo json_encode($estructura);
    }

    public function enviarCorreoPublicacion()
    {
        $datos = ['rutaAdjunto' => $_POST['rutaAdjunto'], 'direccion' => $_POST['direccion'], 'asunto' => $_POST['asunto'], 'mensaje' => $_POST['mensaje']];

        // Mail::send('email.correo', $datos, function ($msj) use ($datos) {
        //     $msj->to(explode(';', $datos['direccion']));
        //     $msj->subject($datos['asunto']);

        //     $msj->attach(public_path() . '/' . $datos['rutaAdjunto']);
        // });

        echo json_encode('Mensaje enviado correctamente');
    }

    public function publicacionVersion($idPublicacion)
    {
        $documento = \App\Publicacion::select(DB::raw('nombreDocumento'))
            ->leftJoin('documento', 'publicacion.Documento_idDocumento', '=', 'documento.idDocumento')
            ->where('idPublicacion', '=', $idPublicacion)
            ->first();

        return view('publicacionversionform', compact('idPublicacion', 'documento'));
    }

    public function cargarArchivoPublicacionVersion()
    {
        $nombreArchivo = $_POST['filename'];
        $nombreArchivoReplace = str_replace(" ", "_", $nombreArchivo);
        $nombreArchivoReplace = str_replace("(", "_", $nombreArchivoReplace);
        $nombreArchivoReplace = str_replace(")", "_", $nombreArchivoReplace);
        $idPublicacion = $_POST['idPublicacion'];
        $ubicacionPublicacionVersion = $_POST['ubicacionPublicacionVersion'];
        $temporal = public_path() . "/storage/temporal";

        $publicacionversion = \App\PublicacionVersion::where('Publicacion_idPublicacion', '=', $idPublicacion)->lists('rutaImagenPublicacionVersion');

        $nuevoArchivo = "$temporal/$nombreArchivo";
        $actualArchivo = public_path() . "/" . $publicacionversion[0];

        File::move($nuevoArchivo,  "$temporal/$nombreArchivoReplace");
        $nuevoArchivo = "$temporal/$nombreArchivoReplace";

        $temporalFile = public_path() . '/storage/temporal.pdf';

        if ($ubicacionPublicacionVersion == 'Arriba') {
            $cmd = "pdfunite $nuevoArchivo $actualArchivo $temporalFile";
        } else {
            $cmd = "pdfunite $actualArchivo $nuevoArchivo $temporalFile";
        }

        shell_exec($cmd);
        File::copy($temporalFile, $actualArchivo);

        unlink($nuevoArchivo);
        unlink($temporalFile);

        echo json_encode($cmd);
    }

    private function getOrientationPDF($fileName)
    {
        $orientation = shell_exec(' pdfinfo \
        -f 1 \
        -l 1 \
         ' . $fileName . ' \
    | grep "Page.* size:" \
    | \
     while read Page _pageno size _width x _height rest; do
      [ "$(echo "${_width} / 1"|bc)" -gt "$(echo "${_height} / 1"|bc)" ] \
         && echo "landscape" \
        || echo "portrait"  ; \
     done');

        if (trim($orientation) == "portrait") {
            return 'vertical';
        } else {
            return 'horizontal';
        }
    }
}
