<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\DocumentoRequest;
use DB;


class DocumentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("documento");
        if (!empty($datos))
            return view('documentogrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $idMetadato = \App\Metadato::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('idMetadato');
        $nombreMetadato = \App\Metadato::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreMetadato');
        $tablas = $this->convertirArray(DB::SELECT('SHOW FULL TABLES FROM  elyon'));

        $idRol = \App\Rol::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('idRol');
        $nombreRol = \App\Rol::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreRol');

        return view("documentoform", compact('idMetadato', 'nombreMetadato', 'tablas', 'idRol', 'nombreRol'));
    }

    public function convertirArray($dato)
    {
        for ($i = 0; $i < count($dato); $i++) {
            $nuevo[get_object_vars($dato[$i])["Tables_in_elyon"]] = get_object_vars($dato[$i])["Tables_in_elyon"];
        }

        return $nuevo;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocumentoRequest $request)
    {
        if ($request->ajax())
            return;

        \App\Documento::create([
            'codigoDocumento' => $request['codigoDocumento'],
            'nombreDocumento' => $request['nombreDocumento'],
            'directorioDocumento' => $request['directorioDocumento'],
            'versionDocumento' => isset($request['versionDocumento']) ? 1 : 0,
            'radicadoDocumento' => isset($request['radicadoDocumento']) ? 1 : 0,
            'generaMetadatoFlujoDocumento' => isset($request['generaMetadatoFlujoDocumento']) ? 1 : 0,
            'tablaDocumento' => $request['tablaDocumento'],
            'Compania_idCompania' => \Session::get("idCompania")
        ]);

        $documento = \App\Documento::All()->last();

        $this->grabarDetalle($request, $documento->idDocumento);
        $this->grabarPermisos($request, $id);

        return redirect('documento');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $documento = \App\Documento::selectRaw(
            'idDocumento,
        codigoDocumento,
        nombreDocumento,
        directorioDocumento,
        versionDocumento,
        radicadoDocumento,
        generaMetadatoFlujoDocumento,
        tablaDocumento,
        IF(Documento_idDocumento IS NULL, 0, 1) AS Documento_idDocumento'
        )
            ->leftJoin('estructura', 'documento.idDocumento', '=', 'estructura.Documento_idDocumento')
            ->where('idDocumento', '=', $id)
            ->firstOrFail();

        $idMetadato = \App\Metadato::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('idMetadato');
        $nombreMetadato = \App\Metadato::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreMetadato');
        $tablas = $this->convertirArray(DB::SELECT('SHOW FULL TABLES FROM  elyon'));

        $idRol = \App\Rol::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('idRol');
        $nombreRol = \App\Rol::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreRol');

        $documentometadato = DB::table('documentometadato')
            ->select('*')
            ->where('Documento_idDocumento', '=', $id)
            ->get();

        $documentorol = DB::table('documentorol')
            ->select('*')
            ->where('Documento_idDocumento', '=', $id)
            ->get();

        return view('documentoform', compact('documento', 'idMetadato', 'nombreMetadato', 'documentometadato', 'tablas', 'idRol', 'nombreRol', 'documentorol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DocumentoRequest $request, $id)
    {
        if ($request->ajax())
            return;

        $documento = \App\Documento::find($id);
        $documento->update([
            'codigoDocumento' => $request['codigoDocumento'],
            'nombreDocumento' => $request['nombreDocumento'],
            'directorioDocumento' => $request['directorioDocumento'],
            'versionDocumento' => isset($request['versionDocumento']) ? 1 : 0,
            'radicadoDocumento' => isset($request['radicadoDocumento']) ? 1 : 0,
            'generaMetadatoFlujoDocumento' => isset($request['generaMetadatoFlujoDocumento']) ? 1 : 0,
            'tablaDocumento' => $request['tablaDocumento'],
            'Compania_idCompania' => \Session::get("idCompania")
        ]);

        $this->grabarDetalle($request, $id);
        $this->grabarPermisos($request, $id);

        return redirect('documento');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Documento::destroy($id);
        return redirect('documento');
    }

    public function grabarDetalle($request, $id)
    {
        $idsEliminar = explode(',', $request['eliminarDocumentoMetadato']);
        \App\DocumentoMetadato::whereIn('idDocumentoMetadato', $idsEliminar)->delete();
        for ($i = 0; $i < count($request['Metadato_idMetadato']); $i++) {
            $indice = array(
                'idDocumentoMetadato' => $request['idDocumentoMetadato'][$i]
            );

            $datos = array(
                'Documento_idDocumento' => $id,
                'ordenDocumentoMetadato' => $request['ordenDocumentoMetadato'][$i],
                'Metadato_idMetadato' => $request['Metadato_idMetadato'][$i],
                'campoTablaDocumentoMetadato' => ($request['campoTablaDocumentoMetadato'][$i] == '' || $request['campoTablaDocumentoMetadato'][$i] == NULL) ? NULL : $request['campoTablaDocumentoMetadato'][$i],
                'indiceDocumentoMetadato' => $request['indiceDocumentoMetadato'][$i]
            );

            $guardar = \App\DocumentoMetadato::updateOrCreate($indice, $datos);
        }
    }

    public function grabarPermisos($request, $id)
    {
        $idsEliminar = explode(',', $request['eliminarDocRol']);
        \App\DocumentoRol::whereIn('idDocumentoRol', $idsEliminar)->delete();
        for ($i = 0; $i < count($request['Rol_idRol']); $i++) {
            $indice = array(
                'idDocumentoRol' => $request['idDocumentoRol'][$i]
            );

            $datos = array(
                'Documento_idDocumento' => $id,
                'Rol_idRol' => $request['Rol_idRol'][$i]
            );

            $guardar = \App\DocumentoRol::updateOrCreate($indice, $datos);
        }
    }

    public function consultarCamposTablaDocumento()
    {
        $tabla = (isset($_POST['tabla']) && $_POST['tabla'] != '') ? $_POST['tabla'] : '';

        $campos = DB::table('information_schema.COLUMNS')
            ->select('COLUMN_NAME as Campo', 'COLUMN_COMMENT as Comentario')
            ->where('TABLE_NAME', '=', $tabla)
            ->where('TABLE_NAME', '!=', 'audits')
            ->where('TABLE_SCHEMA', '=', 'elyon')
            ->get();

        echo json_encode($campos);
    }

    public function documentoMetadatoInforme()
    {
        $documento = \App\Documento::where('Compania_idCompania', \Session::get('idCompania'))->lists('nombreDocumento', 'idDocumento');

        return view('documentometadatoinformeform', compact('documento'));
    }

    public function generarDocumentoMetadatoInforme()
    {
        $condicion = (isset($_GET['condicion']) && $_GET['condicion'] != '') ? "WHERE " . $_GET['condicion'] : '';
        $tipoInforme = $_GET['tipo'];
        $documento = $_GET['documento'];

        $campos = '';
        $columnasMetadatos = [];

        $consultaMetadatos = DB::table('documentometadato')
            ->leftjoin('metadato', 'documentometadato.Metadato_idMetadato', "=", 'metadato.idMetadato')
            ->select(DB::raw('metadato.*, Documento_idDocumento'))
            ->where('Documento_idDocumento', "=", $documento)
            ->get();

        foreach ($consultaMetadatos as $metadato) {
            $campos .= "MAX(IF(nombreMetadato = '" . $metadato->nombreMetadato . "', valorPublicacionDocumentoMetadato, '')) as " . str_replace(" ", "_", $metadato->nombreMetadato) . ",";
            $columnasEncabezadoMetadatos[] = $metadato->nombreMetadato;
            $columnasCuerpoMetadatos[] = str_replace(" ", "_", $metadato->nombreMetadato);
        };

        $campos = substr($campos, 0, strlen($campos) - 1);

        $consulta = DB::select(
            "SELECT
                publicacion.codigoPublicacion,
                documento.nombreDocumento,
                publicacionversion.fechaPublicacionVersion,
                $campos
            FROM
                publicacion
            LEFT JOIN
                publicaciondocumentometadato ON publicacion.idPublicacion = publicaciondocumentometadato.Publicacion_idPublicacion
            LEFT JOIN
                publicacionversion ON publicacion.idPublicacion = publicacionversion.Publicacion_idPublicacion
            LEFT JOIN
                documentometadato ON publicaciondocumentometadato.DocumentoMetadato_idDocumentoMetadato = documentometadato.idDocumentoMetadato
            LEFT JOIN
                metadato ON documentometadato.Metadato_idMetadato = metadato.idMetadato
            LEFT JOIN
                documento ON publicacion.Documento_idDocumento = documento.idDocumento
            $condicion
            GROUP BY publicacion.idPublicacion
            ORDER BY publicacion.idPublicacion"
        );

        if (empty($consulta)) {
            return 'No hay resultados para los filtros indicados';
        }

        return view('formatos.documentometadatoinformeimpresion', compact('consulta', 'columnasEncabezadoMetadatos', 'columnasCuerpoMetadatos', 'tipoInforme'));
    }
}
