<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmpleadoRequest;
use App\Http\Requests\FlujoManualRequest;
use Exception;
use Illuminate\Support\Facades\DB;
use Session;

class FlujoManualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dependencia = \App\Dependencia::All()->pluck('nombreDependencia', 'idDependencia');
        $serie = \App\Serie::All()->pluck('nombreSerie', 'idSerie');
        $subSerie = \App\SubSerie::All()->pluck('nombreSubSerie', 'idSubSerie');
        $documento = \App\Documento::All()->pluck('nombreDocumento', 'idDocumento');
        $flujo = \App\Flujo::All()->pluck('nombreFlujo', 'idFlujo');

        return view('flujomanualform', compact('dependencia', 'serie', 'subSerie', 'documento', 'flujo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $codigo = DB::table('publicacion')
            ->select(DB::raw("LPAD(IF(codigoPublicacion IS NULL, '1', MAX(codigoPublicacion) +1), 9,'0') AS codigoPublicacion"))
            ->get();

        $codigoPublicacion = get_object_vars($codigo[0])['codigoPublicacion'];

        DB::beginTransaction();
        try {
            \App\FlujoManual::create([
                'nombreFlujoManual' => $request['nombreFlujoManual'],
                'Users_idFlujoManual' => \Session::get('idUsuario')
            ]);
            $flujoManual = \App\FlujoManual::All()->last();

            \App\Publicacion::create([
                'codigoPublicacion' => $codigoPublicacion,
                'Dependencia_idDependencia' => $request['Dependencia_idDependencia'],
                'Serie_idSerie' => $request['Serie_idSerie'],
                'SubSerie_idSubSerie' => $request['SubSerie_idSubSerie'],
                'Documento_idDocumento' => $request['Documento_idDocumento'],
                'paginasPublicacion' => $request['paginasPublicacion'],
                'Radicado_idRadicadoIngreso' => ($request['Radicado_idRadicadoIngreso'] == '' ? null : $request['Radicado_idRadicadoIngreso']),
                'Radicado_idRadicadoSalida' => ($request['Radicado_idRadicadoSalida'] == '' ? null : $request['Radicado_idRadicadoSalida']),
                'Radicado_idRadicadoInterno' => ($request['Radicado_idRadicadoInterno'] == '' ? null : $request['Radicado_idRadicadoInterno']),
                'FlujoManual_idFlujoManual' => $flujoManual->idFlujoManual,
                'Compania_idCompania' => \Session::get("idCompania")
            ]);

            $publicacion = \App\Publicacion::All()->last();

            $directorio = DB::table('estructura')
                ->leftJoin('dependencia', 'estructura.Dependencia_idDependencia', '=', 'dependencia.idDependencia')
                ->leftJoin('serie', 'estructura.Serie_idSerie', '=', 'serie.idSerie')
                ->leftJoin('subserie', 'estructura.SubSerie_idSubSerie', '=', 'subserie.idSubSerie')
                ->leftJoin('documento', 'estructura.Documento_idDocumento', '=', 'documento.idDocumento')
                ->where('estructura.Dependencia_idDependencia', '=', $request['Dependencia_idDependencia'])
                ->where('estructura.Serie_idSerie', '=', $request['Serie_idSerie'])
                ->where('estructura.SubSerie_idSubSerie', '=', $request['SubSerie_idSubSerie'])
                ->where('estructura.Documento_idDocumento', '=', $request['Documento_idDocumento'])
                ->select(DB::raw('CONCAT(directorioDependencia,"/",directorioSerie,"/",directorioSubSerie,"/",directorioDocumento) AS directorio'))
                ->get();

            $nombreDir = 'storage/estructura/' . get_object_vars($directorio[0])['directorio'];
            $rutaReplace = str_replace(" ", "_", $request['rutaImagenPublicacionVersion']);
            $rutaReplace = str_replace("(", "_", $rutaReplace);
            $rutaReplace = str_replace(")", "_", $rutaReplace);
            $nombreDoc = 'storage/estructura/' . get_object_vars($directorio[0])['directorio'] . '/' . $codigoPublicacion . '_' . $rutaReplace;

            $origen = public_path() . '/storage/temporal/' . $request['rutaImagenPublicacionVersion'];
            $destino = public_path() . '/' . $nombreDir . '/' . $codigoPublicacion . '_' . $rutaReplace;

            \App\PublicacionVersion::create([
                'Publicacion_idPublicacion' => $publicacion->idPublicacion,
                'fechaPublicacionVersion' => $request['fechaPublicacionVersion'],
                'numeroPublicacionVersion' => $request['numeroPublicacionVersion'],
                'rutaImagenPublicacionVersion' => $nombreDoc,
                'Users_idPublicacionVersion' => \Session::get('idUsuario')
            ]);

            $publicacionversion = \App\PublicacionVersion::All()->last();

            $metadatos = DB::table('documentometadato')
                ->leftjoin('metadato', 'documentometadato.Metadato_idMetadato', "=", 'metadato.idMetadato')
                ->select(DB::raw('documentometadato.*,metadato.*'))
                ->where('Documento_idDocumento', "=", $request['Documento_idDocumento'])
                ->get();

            for ($i = 0; $i < count($metadatos); $i++) {
                $metadato = get_object_vars($metadatos[$i]);
                $valor = $request[$metadato["idDocumentoMetadato"]];

                \App\PublicacionDocumentoMetadato::create([
                    'Publicacion_idPublicacion' => $publicacion->idPublicacion,
                    'DocumentoMetadato_idDocumentoMetadato' => $metadato["idDocumentoMetadato"],
                    'valorPublicacionDocumentoMetadato' => $valor,
                    'PublicacionVersion_idPublicacionVersion' => $publicacionversion->idPublicacionVersion
                ]);
            }

            if (file_exists($origen)) {
                copy($origen, $destino);
                unlink($origen);
                self::iniciarFlujo($request['Flujo_idFlujo'], $flujoManual->idFlujoManual);
                $informacion = "Flujo iniciado correctamente";
                DB::commit();
            }
        } catch (\Exception $e) {
            $informacion = $e->getMessage();
            DB::rollback();
        }

        echo json_encode($informacion);
    }

    private function iniciarFlujo($idFlujo, $idFlujoManual)
    {
        $tareas = \App\FlujoTarea::where('Flujo_idFlujo', '=', $idFlujo)->lists('idFlujoTarea');

        foreach ($tareas as $key => $tarea) {
            \App\FlujoTareaRadicado::create([
                'FlujoTarea_idFlujoTarea' => $tarea,
                'Radicado_idRadicadoIngreso' => null,
                'Radicado_idRadicadoSalida' => null,
                'FlujoManual_idFlujoManual' => $idFlujoManual,
                'estadoFlujoTareaRadicado' => 'SinAsignar'
            ]);
        }

        $tareainicial = \App\FlujoTarea::where('Flujo_idFlujo', $idFlujo)->first();

        \App\FlujoTareaRadicado::where('FlujoTarea_idFlujoTarea', $tareainicial->idFlujoTarea)
            ->where('FlujoManual_idFlujoManual', $idFlujoManual)
            ->update(['estadoFlujoTareaRadicado' => 'EnProceso']);
    }
}
