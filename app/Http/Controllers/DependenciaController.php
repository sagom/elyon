<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\DependenciaRequest;
use DB;



class DependenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $datos = $this->consultarPermisos("dependencia");
        if(!empty($datos))
            return view('dependenciagrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $idRol = \App\Rol::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('idRol');
        $nombreRol = \App\Rol::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreRol');

        return view('dependenciaform', compact('idRol', 'nombreRol'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DependenciaRequest $request)
    {
        if($request->ajax()) 
            return;
            
        \App\Dependencia::create([
            'codigoDependencia' => $request['codigoDependencia'],
            'nombreDependencia' => $request['nombreDependencia'],
            'directorioDependencia' => $request['directorioDependencia'],
            'Compania_idCompania' => \Session::get("idCompania")
        ]);

        $dependencia = \App\Dependencia::All()->last();

        $this->grabarDetalle($request, $dependencia->idDependencia);
        
        return redirect('dependencia');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dependencia = \App\Dependencia::selectRaw(
            'idDependencia,
            codigoDependencia,
            nombreDependencia,
            directorioDependencia,
            IF(Dependencia_idDependencia IS NULL, 0, 1) AS Dependencia_idDependencia')
            ->leftJoin('estructura', 'dependencia.idDependencia', '=', 'estructura.Dependencia_idDependencia')
            ->where('idDependencia', '=', $id)
            ->firstOrFail();

        $idRol = \App\Rol::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('idRol');
        $nombreRol = \App\Rol::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreRol');

        $dependenciarol = DB::table('dependenciarol')
                    ->select('*')
                    ->where('Dependencia_idDependencia', '=', $id)
                    ->get();

        return view('dependenciaform', compact('dependencia', 'idRol', 'nombreRol', 'dependenciarol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DependenciaRequest $request, $id)
    {
        if($request->ajax()) 
            return;

        $dependencia = \App\Dependencia::find($id);
        $dependencia->update([
                'codigoDependencia' => $request['codigoDependencia'],
                'nombreDependencia' => $request['nombreDependencia'],
                'directorioDependencia' => $request['directorioDependencia'],
                'Compania_idCompania' => \Session::get("idCompania")
        ]);

        $this->grabarDetalle($request, $id);

        return redirect('dependencia');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Dependencia::destroy($id);
        return redirect('dependencia');
    }

    public function grabarDetalle($request, $id)
    {
        $idsEliminar = explode(',', $request['eliminarDepRol']);
        \App\DependenciaRol::whereIn('idDependenciaRol',$idsEliminar)->delete();
        for($i = 0; $i < count($request['Rol_idRol']); $i++)
        {
            $indice = array(
                'idDependenciaRol' => $request['idDependenciaRol'][$i]);

            $datos= array(
                'Dependencia_idDependencia' => $id,
                'Rol_idRol' => $request['Rol_idRol'][$i],
                'cargarDependenciaRol' => $request['cargarDependenciaRol'][$i],
                'descargarDependenciaRol' => $request['descargarDependenciaRol'][$i],
                'consultarDependenciaRol' => $request['consultarDependenciaRol'][$i],
                'imprimirDependenciaRol' => $request['imprimirDependenciaRol'][$i],
                'enviarDependenciaRol' => $request['enviarDependenciaRol'][$i],
                'eliminarDependenciaRol' => $request['eliminarDependenciaRol'][$i]);

            $guardar = \App\DependenciaRol::updateOrCreate($indice, $datos);
        }
    }
}
