<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmpleadoRequest;
use Illuminate\Support\Facades\DB;
use Session;


class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("empleado");
        if(!empty($datos))
            return view('empleadogrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $centrocosto = \App\CentroCosto::All()->lists('nombreCentroCosto', 'idCentroCosto');

        return view('empleadoform', compact('centrocosto'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmpleadoRequest $request)
    {
        if($request->ajax()) 
            return;
        
        \App\Empleado::create([
            'documentoEmpleado' => $request['documentoEmpleado'],
            'nombreCompletoEmpleado' => $request['nombreCompletoEmpleado'],
            'estadoEmpleado' => $request['estadoEmpleado'],
            'CentroCosto_idCentroCosto'=> ($request['CentroCosto_idCentroCosto'] == 0 || $request['CentroCosto_idCentroCosto'] == '') ? NULL : $request['CentroCosto_idCentroCosto'],
            'Compania_idCompania' => \Session::get('idCompania')
        ]);
    
        return redirect('empleado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empleado = \App\Empleado::find($id);
        $centrocosto = \App\CentroCosto::All()->lists('nombreCentroCosto', 'idCentroCosto');

        return view('empleadoform', compact('empleado', 'centrocosto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmpleadoRequest $request, $id)
    {
        if($request->ajax()) 
            return;

        $empleado = \App\Empleado::find($id);
        $empleado->update([
            'documentoEmpleado' => $request['documentoEmpleado'],
            'nombreCompletoEmpleado' => $request['nombreCompletoEmpleado'],
            'estadoEmpleado' => $request['estadoEmpleado'],
            'CentroCosto_idCentroCosto'=> ($request['CentroCosto_idCentroCosto'] == 0 || $request['CentroCosto_idCentroCosto'] == '') ? NULL : $request['CentroCosto_idCentroCosto'],
            'Compania_idCompania' => \Session::get('idCompania')
        ]);

        $this->actualizarEmpleadoPublicacion($request['documentoEmpleado'], $request['estadoEmpleado']);

        return redirect('empleado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Empleado::destroy($id);
        return redirect('empleado');
    }

    public function empleadoInforme()
    {
        $centroCosto = \App\CentroCosto::where('Compania_idCompania', \Session::get('idCompania'))->pluck('nombreCentroCosto', 'idCentroCosto');

        return view('empleadoinformeform', compact('centroCosto'));
    }

    private function actualizarEmpleadoPublicacion($documentoEmpleado, $estadoEmpleado){
        $publicacion = \App\PublicacionDocumentoMetadato::where('valorPublicacionDocumentoMetadato', $documentoEmpleado)->select('Publicacion_idPublicacion')->get();

        foreach ($publicacion as $value) {
            $metadatos = DB::table('publicacion')
            ->leftJoin('publicaciondocumentometadato', 'publicacion.idPublicacion', '=', 'publicaciondocumentometadato.Publicacion_idPublicacion')
            ->leftJoin('publicacionversion', 'publicacion.idPublicacion', '=', 'publicacionversion.Publicacion_idPublicacion')
            ->leftJoin('documentometadato', 'publicaciondocumentometadato.DocumentoMetadato_idDocumentoMetadato', '=', 'documentometadato.idDocumentoMetadato')
            ->leftJoin('metadato', 'documentometadato.Metadato_idMetadato', '=', 'metadato.idMetadato')
            ->select(DB::raw('idPublicacionDocumentoMetadato, campoTablaDocumentoMetadato'))
            ->where('idPublicacion', '=', $value->Publicacion_idPublicacion)
            ->where('campoTablaDocumentoMetadato', '=', 'estadoEmpleado')
            ->first();

            if($metadatos){
                \App\PublicacionDocumentoMetadato::where('idPublicacionDocumentoMetadato', $metadatos->idPublicacionDocumentoMetadato)->update(['valorPublicacionDocumentoMetadato' => $estadoEmpleado]);
            }
        }
    }

    public function generarEmpleadoInforme()
    {
        $condicion = (isset($_GET['condicion']) ? $_GET['condicion'] : '');
        $tipoInforme = $_GET['tipo'];

        $consulta = DB::table('empleado as e')
        ->leftJoin('centrocosto AS cc', 'e.CentroCosto_idCentroCosto', '=', 'cc.idCentroCosto')
        ->select(DB::raw('documentoEmpleado, 
                        nombreCompletoEmpleado, 
                        estadoEmpleado, 
                        nombreCentroCosto'))
        ->where('e.Compania_idCompania', \Session::get('idCompania'));

        if($condicion != '')
        {
            $consulta->whereRaw($condicion);
        }

        $consulta = $consulta->get();

        if(empty($consulta))
        {
            return 'No hay resultados para los filtros indicados';
        }

        return view('formatos.empleadoinformeimpresion',compact('consulta', 'tipoInforme'));
    }
}