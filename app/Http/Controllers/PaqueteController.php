<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PaqueteRequest;


class PaqueteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("paquete");
        if(!empty($datos))
            return view('paquetegrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('paqueteform');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaqueteRequest $request)
    {
        if($request->ajax()) 
            return;
            
        \App\Paquete::create([
            'ordenPaquete' => $request['ordenPaquete'],
            'iconoPaquete' => $request['iconoPaquete'],
            'nombrePaquete' => $request['nombrePaquete'] 
        ]);
        
        return redirect('paquete');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paquete = \App\Paquete::find($id);

        return view('paqueteform', compact('paquete'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PaqueteRequest $request, $id)
    {
        if($request->ajax()) 
            return;

        $paquete = \App\Paquete::find($id);
        $paquete->update([
            'ordenPaquete' => $request['ordenPaquete'],
            'iconoPaquete' => $request['iconoPaquete'],
            'nombrePaquete' => $request['nombrePaquete'] 
        ]);

        return redirect('paquete');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Paquete::destroy($id);
        return redirect('paquete');
    }
}
