<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\RetencionRequest;
use DB;

class RetencionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $retencion = DB::table('retencion')
        ->select('idRetencion', 'Dependencia_idDependencia', 'Serie_idSerie', 'SubSerie_idSubSerie', 'Documento_idDocumento', 'tiempoGestionRetencion', 'tiempoCentralRetencion', 
        'soporteRetencion', 'disposicionFinalRetencion', 'procedimientoRetencion', 'estadoRetencion', 'Compania_idCompania')
        ->where('Compania_idCompania', '=', \Session::get('idCompania'))
        ->get();
        $idDependencia = \App\Dependencia::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('idDependencia');
        $nombreDependencia = \App\Dependencia::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreDependencia');
        $idSerie = \App\Serie::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('idSerie');
        $nombreSerie = \App\Serie::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreSerie');
        $idSubSerie = \App\SubSerie::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('idSubSerie');
        $nombreSubSerie = \App\SubSerie::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreSubSerie');
        $idDocumento = \App\Documento::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('idDocumento');
        $nombreDocumento = \App\Documento::where('Compania_idCompania', '=', \Session::get('idCompania'))->lists('nombreDocumento');

        return view('retencionform', compact('retencion', 'idDependencia', 'nombreDependencia', 'idSerie', 'nombreSerie', 'idSubSerie', 'nombreSubSerie', 'idDocumento', 'nombreDocumento'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RetencionRequest $request)
    {
        if($request->ajax()) 
            return;

        $idsEliminar = explode(',', $request['eliminarRetencion']);
        \App\Retencion::whereIn('idRetencion',$idsEliminar)->delete();
        for($i = 0; $i < count($request['Dependencia_idDependencia']); $i++)
        {
            $indice = array(
                'idRetencion' => $request['idRetencion'][$i]);

            $datos= array(
                'Dependencia_idDependencia' => $request['Dependencia_idDependencia'][$i],
                'Serie_idSerie' => $request['Serie_idSerie'][$i],
                'SubSerie_idSubSerie' => $request['SubSerie_idSubSerie'][$i],
                'Documento_idDocumento' => $request['Documento_idDocumento'][$i],
                'tiempoGestionRetencion' => $request['tiempoGestionRetencion'][$i],
                'tiempoCentralRetencion' => $request['tiempoCentralRetencion'][$i],
                'soporteRetencion' => $request['soporteRetencion'][$i],
                'disposicionFinalRetencion' => $request['disposicionFinalRetencion'][$i],
                'procedimientoRetencion' => $request['procedimientoRetencion'][$i],
                'estadoRetencion' => $request['estadoRetencion'][$i],
                'Compania_idCompania' => \Session::get("idCompania"));

            $guardar = \App\Retencion::updateOrCreate($indice, $datos);
        }

        return redirect('retencion');
    }
}
