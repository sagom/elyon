<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\RadicadoIngresoRequest;
use DB;
use Mail;
use Codedge\Fpdf\Facades\Fpdf;
use Storage;

class RadicadoIngresoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("radicadoingreso");
        $dependencia = \App\Dependencia::All()->lists('nombreDependencia', 'idDependencia');
        if (!empty($datos))
            return view('radicadoingresogrid', compact('datos', 'dependencia'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = \App\User::All()->lists('name', 'id');
        $dependencia = \App\Dependencia::All()->lists('nombreDependencia', 'idDependencia');

        return view('radicadoingresoform', compact('user', 'dependencia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RadicadoIngresoRequest $request)
    {
        if ($request->ajax())
            return;

        $numeroR = DB::table('radicadoingreso')
            ->select(DB::raw('IF(
                                MAX(SUBSTRING(numeroRadicadoIngreso, 1, 4)) = SUBSTRING(CURDATE(), 1, 4),
                                CONCAT(MAX(SUBSTRING(numeroRadicadoIngreso, 1, 4)), "-", LPAD(IF(numeroRadicadoIngreso IS NULL, "1",
                                SUBSTRING(MAX(REPLACE(numeroRadicadoIngreso, "-", "")) + 1, 5, 4)), 3, "0")),
                                CONCAT(YEAR(CURDATE()), "-", LPAD("1", 3, "0"))
                            ) AS numeroRadicadoIngreso'))
            ->where('Compania_idCompania', '=', \Session::get('idCompania'))
            ->get();

        \App\RadicadoIngreso::create([
            'numeroRadicadoIngreso' => get_object_vars($numeroR[0])['numeroRadicadoIngreso'],
            'fechaRadicadoIngreso' => $request['fechaRadicadoIngreso'],
            'tipoRadicadoIngreso' => $request['tipoRadicadoIngreso'],
            'nombreRemitenteRadicadoIngreso' => $request['nombreRemitenteRadicadoIngreso'],
            'identificacionRemitenteRadicadoIngreso' => $request['identificacionRemitenteRadicadoIngreso'],
            'asuntoRadicadoIngreso' => $request['asuntoRadicadoIngreso'],
            'fechaDocumentoRadicadoIngreso' => $request['fechaDocumentoRadicadoIngreso'],
            'Users_idRadicadoIngreso' => $request['Users_idRadicadoIngreso'],
            'Dependencia_idRadicadoIngreso' => $request['Dependencia_idRadicadoIngreso'],
            'correoRadicadoIngreso' => 0,
            'observacionRadicadoIngreso' => $request['observacionRadicadoIngreso'],
            'Compania_idCompania' => \Session::get('idCompania'),
            'Users_idRadica' => \Session::get('idUsuario'),
        ]);

        return redirect('radicadoingreso');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $radicadoingreso = \App\RadicadoIngreso::find($id);
        $user = \App\User::All()->lists('name', 'id');
        $dependencia = \App\Dependencia::All()->lists('nombreDependencia', 'idDependencia');

        return view('radicadoingresoform', compact('radicadoingreso', 'user', 'dependencia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(radicadoingresoRequest $request, $id)
    {
        if ($request->ajax())
            return;

        $radicadoingreso = \App\RadicadoIngreso::find($id);
        $radicadoingreso->update([
            'numeroRadicadoIngreso' => $request['numeroRadicadoIngreso'],
            'fechaRadicadoIngreso' => $request['fechaRadicadoIngreso'],
            'tipoRadicadoIngreso' => $request['tipoRadicadoIngreso'],
            'nombreRemitenteRadicadoIngreso' => $request['nombreRemitenteRadicadoIngreso'],
            'identificacionRemitenteRadicadoIngreso' => $request['identificacionRemitenteRadicadoIngreso'],
            'asuntoRadicadoIngreso' => $request['asuntoRadicadoIngreso'],
            'fechaDocumentoRadicadoIngreso' => $request['fechaDocumentoRadicadoIngreso'],
            'Users_idRadicadoIngreso' => $request['Users_idRadicadoIngreso'],
            'Dependencia_idRadicadoIngreso' => $request['Dependencia_idRadicadoIngreso'],
            'observacionRadicadoIngreso' => $request['observacionRadicadoIngreso'],
            'Compania_idCompania' => \Session::get('idCompania'),
            'Users_idRadica' => \Session::get('idUsuario'),
        ]);

        $this->enviarCorreoRadicadoIngreso($request, $id);

        return redirect('radicadoingreso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\RadicadoIngreso::destroy($id);
        return redirect('radicadoingreso');
    }

    public function guardarRadicadoIngreso()
    {
        $tipo = $_POST['tipo'];
        $asunto = $_POST['asunto'];
        $remitente = $_POST['remitente'];
        $identificacion = $_POST['identificacion'];
        $dependencia = $_POST['dependencia'];

        $numeroR = DB::table('radicadoingreso')
            ->select(DB::raw('IF(
                            MAX(SUBSTRING(numeroRadicadoIngreso, 1, 4)) = SUBSTRING(CURDATE(), 1, 4),
                            CONCAT(MAX(SUBSTRING(numeroRadicadoIngreso, 1, 4)), "-", LPAD(IF(numeroRadicadoIngreso IS NULL, "1",
                            SUBSTRING(MAX(REPLACE(numeroRadicadoIngreso, "-", "")) + 1, 5, 4)), 3, "0")),
                            CONCAT(YEAR(CURDATE()), "-", LPAD("1", 3, "0"))
                        ) AS numeroRadicadoIngreso'))
            ->where('Compania_idCompania', '=', \Session::get('idCompania'))
            ->get();

        \App\RadicadoIngreso::create([
            'numeroRadicadoIngreso' => get_object_vars($numeroR[0])['numeroRadicadoIngreso'],
            'fechaRadicadoIngreso' => date('Y-m-d H:i:s'),
            'tipoRadicadoIngreso' => $tipo,
            'nombreRemitenteRadicadoIngreso' => $remitente,
            'identificacionRemitenteRadicadoIngreso' => $identificacion,
            'asuntoRadicadoIngreso' => $asunto,
            'Dependencia_idRadicadoIngreso' => $dependencia,
            'correoRadicadoIngreso' => 0,
            'Compania_idCompania' => \Session::get('idCompania'),
            'Users_idRadica' => \Session::get('idUsuario'),
        ]);

        echo json_encode('Radicado generado correctamente');
    }

    public function imprimirRadicadoIngreso($idRadicadoIngreso)
    {
        $codigoCompania = \Session::get('codigoCompania');
        $radicado = DB::table('radicadoingreso')
            ->leftJoin('users', 'radicadoingreso.Users_idRadica', '=', 'users.id')
            ->leftJoin('compania', 'radicadoingreso.Compania_idCompania', '=', 'compania.idCompania')
            ->leftJoin('dependencia', 'radicadoingreso.Dependencia_idRadicadoIngreso', '=', 'dependencia.idDependencia')
            ->select(DB::raw('tipoRadicadoIngreso AS tipoRadicado, numeroRadicadoIngreso AS numeroRadicado, fechaRadicadoIngreso AS fechaRadicado,
        asuntoRadicadoIngreso AS asuntoRadicado, nombreRemitenteRadicadoIngreso AS terceroRadicado, name, nombreCompania, nombreDependencia'))
            ->where('idRadicadoIngreso', '=', $idRadicadoIngreso)
            ->get();

        \App\RadicadoIngreso::where('idRadicadoIngreso', $idRadicadoIngreso)
            ->update(['impresionRadicadoIngreso' => 1]);

        return view("formatos.radicadoentrada.radicadoimpresionEntrada$codigoCompania", compact('radicado'));
    }

    public function enviarCorreoRadicadoIngreso($request, $id)
    {
        $radicadoingreso = DB::table('radicadoingreso')
            ->leftJoin('publicacion', 'radicadoingreso.idRadicadoIngreso', '=', 'publicacion.Radicado_idRadicadoIngreso')
            ->leftJoin('publicacionversion', 'publicacion.idPublicacion', '=', 'publicacionversion.Publicacion_idPublicacion')
            ->leftJoin('users AS ur', 'radicadoingreso.Users_idRadicadoIngreso', '=', 'ur.id')
            ->leftJoin('users AS urd', 'radicadoingreso.Users_idRadica', '=', 'urd.id')
            ->leftJoin('dependencia', 'radicadoingreso.Dependencia_idRadicadoIngreso', '=', 'dependencia.idDependencia')
            ->select(DB::raw('radicadoingreso.*, Radicado_idRadicadoIngreso, ur.name, urd.name AS userRadica, ur.email, nombreDependencia, rutaImagenPublicacionVersion'))
            ->where('idRadicadoIngreso', '=', $id)
            ->where('numeroPublicacionVersion', '=', 1)
            ->get();

        $datosRadicadoIngreso = get_object_vars($radicadoingreso[0]);

        $datos = [
            'rutaAdjunto' => $datosRadicadoIngreso['rutaImagenPublicacionVersion'],
            'direccion' => $datosRadicadoIngreso['email'],
            'asunto' => 'Recepción de documentos',
            'mensaje' => '
        Ha llegado un documento para usted. El archivo lo encontrará adjunto a este mensaje. <br><br>

        <b>Tipo de radicado: </b>' . $datosRadicadoIngreso['tipoRadicadoIngreso'] . ' </br>
        <b>Número de radicado: </b>' . $datosRadicadoIngreso['numeroRadicadoIngreso'] . ' </br>
        <b>Fecha de radicado: </b>' . $datosRadicadoIngreso['fechaRadicadoIngreso'] . '<br>
        <b>Origen: </b>' . $datosRadicadoIngreso['nombreRemitenteRadicadoIngreso'] . '<br>
        <b>Asunto: </b>' . $datosRadicadoIngreso['asuntoRadicadoIngreso'] . '<br>
        <b>Usuario radicador</b>: ' . $datosRadicadoIngreso['userRadica']
        ];

        // Mail::send('email.correo', $datos, function ($msj) use ($datos) {
        //     $msj->to(explode(';', $datos['direccion']));
        //     $msj->subject($datos['asunto']);

        //     if (($datos['rutaAdjunto'] != '' || $datos['rutaAdjunto'] != null) && file_exists(public_path() . '/' . $datos['rutaAdjunto'])) {
        //         $msj->attach(public_path() . '/' . $datos['rutaAdjunto']);
        //     }
        // });

        \App\RadicadoIngreso::where('idRadicadoIngreso', $id)
            ->update(['correoRadicadoIngreso' => 1]);
    }

    public function cargarImagenRadicadoIngreso()
    {
        $idRadicadoIngreso = (isset($_POST['idRadicadoIngreso']) && $_POST['idRadicadoIngreso'] != '') ? $_POST['idRadicadoIngreso'] : 0;

        $imagen = DB::table('publicacion')
            ->leftJoin('publicacionversion', 'publicacion.idPublicacion', '=', 'publicacionversion.Publicacion_idPublicacion')
            ->select(DB::raw('rutaImagenPublicacionVersion'))
            ->where('Radicado_idRadicadoIngreso', '=', $idRadicadoIngreso)
            ->get();

        echo json_encode($imagen);
    }

    public function radicadoinforme()
    {
        $dependencia = \App\Dependencia::All()->lists('nombreDependencia', 'idDependencia');

        return view('radicadoinformeform', compact('dependencia'));
    }

    public function generarRadicadoInforme()
    {
        $condicion = (isset($_GET['condicion']) ? $_GET['condicion'] : '');
        $modeloRadicado = $_GET['modelo'];
        $tipoInforme = $_GET['tipo'];

        if ($modeloRadicado == 'ingreso') {
            $consulta = DB::table('radicadoingreso')
                ->leftJoin('users AS u', 'radicadoingreso.Users_idRadicadoIngreso', '=', 'u.id')
                ->leftJoin('users AS ur', 'radicadoingreso.Users_idRadica', '=', 'ur.id')
                ->leftJoin('dependencia', 'radicadoingreso.Dependencia_idRadicadoIngreso', '=', 'dependencia.idDependencia')
                ->select(DB::raw('numeroRadicadoIngreso AS numeroRadicado,
                            fechaRadicadoIngreso AS fechaRadicado,
                            tipoRadicadoIngreso AS tipoRadicado,
                            nombreRemitenteRadicadoIngreso AS terceroRadicado,
                            identificacionRemitenteRadicadoIngreso AS documentoTerceroRadicado,
                            asuntoRadicadoIngreso AS asuntoRadicado,
                            fechaDocumentoRadicadoIngreso AS fechaDocumentoRadicado,
                            u.name AS nombreUsuarioRadicado,
                            nombreDependencia AS nombreDependenciaRadicado,
                            observacionRadicadoIngreso AS observacionRadicado,
                            ur.name AS nombreUsuarioRadicador'));

            if ($condicion != '') {
                $consulta->whereRaw($condicion);
            }

            $consulta = $consulta->get();
        } else {
            $consulta = DB::table('radicadosalida')
                ->leftJoin('users AS u', 'radicadosalida.Users_idRadicadoSalida', '=', 'u.id')
                ->leftJoin('users AS ur', 'radicadosalida.Users_idRadica', '=', 'ur.id')
                ->leftJoin('dependencia', 'radicadosalida.Dependencia_idRadicadoSalida', '=', 'dependencia.idDependencia')
                ->select(DB::raw('numeroRadicadoSalida AS numeroRadicado,
                            fechaRadicadoSalida AS fechaRadicado,
                            "Correspondencia enviada" AS tipoRadicado,
                            nombreDestinatarioRadicadoSalida AS terceroRadicado,
                            identificacionDestinatarioRadicadoSalida AS documentoTerceroRadicado,
                            asuntoRadicadoSalida AS asuntoRadicado,
                            fechaDocumentoRadicadoSalida AS fechaDocumentoRadicado,
                            u.name AS nombreUsuarioRadicado,
                            nombreDependencia AS nombreDependenciaRadicado,
                            observacionRadicadoSalida AS observacionRadicado,
                            ur.name AS nombreUsuarioRadicador'));

            if ($condicion != '') {
                $consulta->whereRaw($condicion);
            }

            $consulta = $consulta->get();
        }

        if (empty($consulta)) {
            return 'No hay resultados para los filtros indicados';
        }

        return view('formatos.radicadoinformeimpresion', compact('consulta', 'tipoInforme'));
    }

    public function guardarFlujoRadicadoIngreso()
    {
        $idFlujo = (isset($_POST['idFlujo']) ? $_POST['idFlujo'] : '');
        $idRadicado = (isset($_POST['idRadicado']) ? $_POST['idRadicado'] : '');

        if ($idFlujo != '' && $idRadicado != '') {
            $tarearadicado = DB::table('flujotarearadicado')
                ->select(DB::raw('Radicado_idRadicadoIngreso'))
                ->where('Radicado_idRadicadoIngreso', '=', $idRadicado)
                ->get();

            if (empty($tarearadicado)) {
                $tareas = \App\FlujoTarea::where('Flujo_idFlujo', '=', $idFlujo)->lists('idFlujoTarea');

                foreach ($tareas as $key => $tarea) {
                    \App\FlujoTareaRadicado::create([
                        'FlujoTarea_idFlujoTarea' => $tarea,
                        'Radicado_idRadicadoIngreso' => $idRadicado,
                        'Radicado_idRadicadoSalida' => null,
                        'estadoFlujoTareaRadicado' => 'SinAsignar'
                    ]);
                }

                $tareainicial = \App\FlujoTarea::where('Flujo_idFlujo', $idFlujo)->first();

                \App\FlujoTareaRadicado::where('FlujoTarea_idFlujoTarea', $tareainicial->idFlujoTarea)
                    ->where('Radicado_idRadicadoIngreso', $idRadicado)
                    ->update(['estadoFlujoTareaRadicado' => 'EnProceso']);

                echo json_encode(array(true, 'Flujo iniciado correctamente. Diríjase al panel de tareas para ejecutarlo.'));
            } else {
                echo json_encode(array(false, 'Este radicado ya está asociado a un flujo'));
            }
        } else {
            echo json_encode(array(false, 'Debe seleccionar un flujo válido'));
        }
    }
}
