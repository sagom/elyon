<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\MetadatoRequest;


class MetadatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("metadato");
        if (!empty($datos))
            return view('metadatogrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('metadatoform');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MetadatoRequest $request)
    {
        if ($request->ajax())
            return;

        \App\Metadato::create([
            'codigoMetadato' => $request['codigoMetadato'],
            'nombreMetadato' => $request['nombreMetadato'],
            'tipoMetadato' => $request['tipoMetadato'],
            'opcionMetadato' => ($request['opcionMetadato'] == '' ? NULL : $request['opcionMetadato']),
            'longitudMetadato' => $request['longitudMetadato'],
            'Compania_idCompania' => \Session::get("idCompania"),
            'informeFlujoMetadato' => isset($request['informeFlujoMetadato']) ? 1 : 0,
        ]);

        return redirect('metadato');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $metadato = \App\Metadato::find($id);

        return view('metadatoform', compact('metadato'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MetadatoRequest $request, $id)
    {
        if ($request->ajax())
            return;

        $metadato = \App\Metadato::find($id);
        $metadato->update([
            'codigoMetadato' => $request['codigoMetadato'],
            'nombreMetadato' => $request['nombreMetadato'],
            'tipoMetadato' => $request['tipoMetadato'],
            'opcionMetadato' => ($request['opcionMetadato'] == '' ? NULL : $request['opcionMetadato']),
            'longitudMetadato' => $request['longitudMetadato'],
            'Compania_idCompania' => \Session::get("idCompania"),
            'informeFlujoMetadato' => isset($request['informeFlujoMetadato']) ? 1 : 0,
        ]);

        return redirect('metadato');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Metadato::destroy($id);
        return redirect('metadato');
    }
}
