<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CentroCostoRequest;


class CentroCostoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("centrocosto");
        if(!empty($datos))
            return view('centrocostogrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('centrocostoform');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CentroCostoRequest $request)
    {
        if($request->ajax()) 
            return;
            
        \App\CentroCosto::create([
            'codigoCentroCosto' => $request['codigoCentroCosto'],
            'nombreCentroCosto' => $request['nombreCentroCosto'],
            'Compania_idCompania' => \Session::get("idCompania")
        ]);
        
        return redirect('centrocosto');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $centrocosto = \App\CentroCosto::find($id);

        return view('centrocostoform', compact('centrocosto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CentroCostoRequest $request, $id)
    {
        if($request->ajax()) 
            return;

        $centrocosto = \App\CentroCosto::find($id);
        $centrocosto->update([
                'codigoCentroCosto' => $request['codigoCentroCosto'],
                'nombreCentroCosto' => $request['nombreCentroCosto'],
                'Compania_idCompania' => \Session::get("idCompania")
        ]);

        return redirect('centrocosto');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\CentroCosto::destroy($id);
        return redirect('centrocosto');
    }
}