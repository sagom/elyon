<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\RolRequest;
use DB;


class RolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("rol");
        if(!empty($datos))
            return view('rolgrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $idOpcion = \App\Opcion::All()->lists('idOpcion');
        $nombreOpcion = \App\Opcion::All()->lists('nombreOpcion');
        return view('rolform', compact('idOpcion', 'nombreOpcion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RolRequest $request)
    {
        if($request->ajax())
            return;
        \App\Rol::create([
            'codigoRol' => $request['codigoRol'],
            'nombreRol' => $request['nombreRol'],
            'Compania_idCompania' => \Session::get("idCompania")
        ]);

        $rol = \App\Rol::All()->last();
        $this->grabarDetalle($request, $rol->idRol);
        return redirect('rol');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rol = \App\Rol::find($id);
        $idOpcion = \App\Opcion::All()->lists('idOpcion');
        $nombreOpcion = \App\Opcion::All()->lists('nombreOpcion');

        $rolopcion = DB::table('rolopcion')
                    ->select('*')
                    ->where('Rol_idRol', '=', $id)
                    ->get();

        return view('rolform', compact('rol', 'idOpcion', 'nombreOpcion', 'rolopcion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RolRequest $request, $id)
    {
        if($request->ajax()) 
            return;

        $rol = \App\Rol::find($id);
        $rol->update([
            'codigoRol' => $request['codigoRol'],
            'nombreRol' => $request['nombreRol'],
            'Compania_idCompania' => \Session::get("idCompania") 
        ]);

        $this->grabarDetalle($request, $id);

        return redirect('rol');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Rol::destroy($id);
        return redirect('rol');
    }

    public function grabarDetalle($request, $id)
    {
        $idsEliminar = explode(',', $request['eliminarOpcion']);
        \App\RolOpcion::whereIn('idRolOpcion',$idsEliminar)->delete();
        for($i = 0; $i < count($request['Opcion_idOpcion']); $i++)
        {
            $indice = array(
                'idRolOpcion' => $request['idRolOpcion'][$i]);

            $datos= array(
                'Rol_idRol' => $id,
                'Opcion_idOpcion' => $request['Opcion_idOpcion'][$i],
                'adicionarRolOpcion' => $request['adicionarRolOpcion'][$i],
                'modificarRolOpcion' => $request['modificarRolOpcion'][$i],
                'eliminarRolOpcion' => $request['eliminarRolOpcion'][$i],
                'imprimirRolOpcion' => $request['imprimirRolOpcion'][$i],
                'aprobarRolOpcion' => $request['aprobarRolOpcion'][$i]);

            $guardar = \App\RolOpcion::updateOrCreate($indice, $datos);
        }
    }
}
