<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use DB;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("user");
        if(!empty($datos))
            return view('usergrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $idCompania = \App\Compania::All()->lists('idCompania');
        $nombreCompania = \App\Compania::All()->lists('nombreCompania');
        $dependencia = \App\Dependencia::All()->lists('nombreDependencia', 'idDependencia');

        return view('userform', compact('idCompania', 'nombreCompania', 'dependencia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if($request->ajax()) 
            return;

        \App\User::create([
            'user' => $request['user'],
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'Dependencia_idDependencia' => $request['Dependencia_idDependencia']
        ]);

        $user = \App\User::All()->last();

        $this->grabarDetalle($request, $user->id);

        return redirect('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\User::find($id);
        $idCompania = \App\Compania::All()->lists('idCompania');
        $nombreCompania = \App\Compania::All()->lists('nombreCompania');
        $dependencia = \App\Dependencia::All()->lists('nombreDependencia', 'idDependencia');

        $userscompaniarol = DB::table('userscompaniarol')
                    ->select('*')
                    ->where('Users_id', '=', $id)
                    ->get();

        return view('userform', compact('user', 'idCompania', 'nombreCompania', 'userscompaniarol', 'dependencia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        if($request->ajax()) 
            return;

        $user = \App\User::find($id);
        if($request['password'] != '')
        {
            $user->update([
                'user' => $request['user'],
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
                'Dependencia_idDependencia' => $request['Dependencia_idDependencia']
            ]);
        }
        else
        {
            $user->update([
                'user' => $request['user'],
                'name' => $request['name'],
                'email' => $request['email'],
                'Dependencia_idDependencia' => $request['Dependencia_idDependencia']
            ]);
        }

        $this->grabarDetalle($request, $id);
        return redirect('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\User::destroy($id);
        return redirect('user');
    }

    public function grabarDetalle($request, $id)
    {
        $idsEliminar = explode(',', $request['eliminarCompaniaRol']);
        \App\UserCompaniaRol::whereIn('idUsersCompaniaRol',$idsEliminar)->delete();
        for($i = 0; $i < count($request['Rol_idRol']); $i++)
        {
            $indice = array(
                'idUsersCompaniaRol' => $request['idUsersCompaniaRol'][$i]);

            $datos= array(
                'Users_id' => $id,
                'Compania_idCompania' => $request['Compania_idCompania'][$i],
                'Rol_idRol' => $request['Rol_idRol'][$i]);

            $guardar = \App\UserCompaniaRol::updateOrCreate($indice, $datos);
        }
    }

    public function consultarRol()
    {
        $idCompania = $_POST['idCompania'];

        $rol = DB::table('rol')
        ->select(DB::raw('rol.*'))
        ->where('Compania_idCompania', '=', $idCompania)
        ->get();

        echo json_encode($rol);
    }
}
