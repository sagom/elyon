<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompaniaRequest;
use Input;
use File;
use Validator;
use Response;
use Intervention\Image\ImageManager;
use DB;
use Session;


class CompaniaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("compania");
        if(!empty($datos))
            return view('companiagrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companiaform');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompaniaRequest $request)
    {
        if($request->ajax())
            return;

        if(null !== Input::file('rutaImagenCompania'))
        {
            $image = Input::file('rutaImagenCompania');
            $imageName = 'storage/images/compania/'.$request->file('rutaImagenCompania')->getClientOriginalName();
            $manager = new ImageManager();
            $manager->make($image->getRealPath())->heighten(500)->save($imageName);
        }
        else
        {
            $imageName = null;
        }
            
        $compania = \App\Compania::create([
            'codigoCompania' => $request['codigoCompania'],
            'nombreCompania' => $request['nombreCompania'],
            'rutaImagenCompania' => $imageName
        ]);

        $rol = \App\Rol::create([
            'codigoRol' => '1',
            'nombreRol' => 'Administrador '.$request['nombreCompania'],
            'Compania_idCompania' => $compania->idCompania 
        ]);

        $opciones = \App\Opcion::All()->pluck('idOpcion');

        for ($i=0; $i < count($opciones); $i++) { 
            
            \App\RolOpcion::create([
                'Rol_idRol' =>  $rol->idRol,
                'Opcion_idOpcion' => $opciones[$i],
                'adicionarRolOpcion' => 1,
                'modificarRolOpcion' => 1,
                'eliminarRolOpcion' => 1,
                'imprimirRolOpcion' => 1,
                'aprobarRolOpcion' => 1
            ]);
        }
        
        return redirect('compania');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $compania = \App\Compania::find($id);

        return view('companiaform', compact('compania'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompaniaRequest $request, $id)
    {
        if($request->ajax())
            return;
        $compania = \App\Compania::find($id);
        $compania->fill($request->all());

        if(null !== Input::file('rutaImagenCompania'))
        {
            $image = Input::file('rutaImagenCompania');
            $imageName = 'storage/images/compania/'.$request->file('rutaImagenCompania')->getClientOriginalName();
            $manager = new ImageManager();
            $manager->make($image->getRealPath())->heighten(500)->save($imageName);
            $compania->rutaImagenCompania = $imageName;
        }

        $compania->save();

        return redirect('compania');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Compania::destroy($id);
        return redirect('compania');
    }

    protected function actualizarVariablesSession()
    {
        $idCompania = isset($_POST['idCompania']) ? $_POST['idCompania'] : '';

        $compania = DB::table('userscompaniarol')
                    ->leftJoin('compania', 'userscompaniarol.Compania_idCompania', '=', 'compania.idCompania')
                    ->select('idCompania', 'codigoCompania', 'nombreCompania', 'rutaImagenCompania')
                    ->where('idCompania', '=', $idCompania)
                    ->get();

        if(!empty($compania))
        {
            $datosCompania = get_object_vars($compania[0]);
            Session::put("idCompania", $datosCompania['idCompania']);
            Session::put("codigoCompania", $datosCompania['codigoCompania']);
            Session::put("nombreCompania", $datosCompania['nombreCompania']);
            Session::put("rutaImagenCompania", $datosCompania['rutaImagenCompania']);

            $informacion = 'Se ha cargado correctamente la sessión para la compañía '.$datosCompania['nombreCompania'];
        }
        else
            $informacion = 'No se ha podido cargar la sessión';

        echo json_encode($informacion);

    }
}
