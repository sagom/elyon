<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubSerieRequest;


class SubSerieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("subserie");
        if(!empty($datos))
            return view('subseriegrid', compact('datos'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subserieform');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubSerieRequest $request)
    {
        if($request->ajax()) 
            return;
            
        \App\SubSerie::create([
            'codigoSubSerie' => $request['codigoSubSerie'],
            'nombreSubSerie' => $request['nombreSubSerie'],
            'directorioSubSerie' => $request['directorioSubSerie'],
            'descripcionSubSerie' => $request['descripcionSubSerie'],
            'Compania_idCompania' => \Session::get("idCompania") 
        ]);
        
        return redirect('subserie');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subserie = \App\SubSerie::selectRaw(
            'idSubSerie,
            codigoSubSerie,
            nombreSubSerie,
            directorioSubSerie,
            descripcionSubSerie,
            IF(SubSerie_idSubSerie IS NULL, 0, 1) AS SubSerie_idSubSerie')
            ->leftJoin('estructura', 'subserie.idSubSerie', '=', 'estructura.SubSerie_idSubSerie')
            ->where('idSubSerie', '=', $id)
            ->firstOrFail();

        return view('subserieform', compact('subserie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubSerieRequest $request, $id)
    {
        if($request->ajax()) 
            return;

        $subserie = \App\SubSerie::find($id);
        $subserie->update([
                'codigoSubSerie' => $request['codigoSubSerie'],
                'nombreSubSerie' => $request['nombreSubSerie'],
                'directorioSubSerie' => $request['directorioSubSerie'],
                'descripcionSubSerie' => $request['descripcionSubSerie'],
                'Compania_idCompania' => \Session::get("idCompania") 
        ]);

        return redirect('subserie');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\SubSerie::destroy($id);
        return redirect('subserie');
    }
}
