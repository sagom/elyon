<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\RadicadoInternoRequest;
use DB;
use Mail;
use Codedge\Fpdf\Facades\Fpdf;
use Storage;

class RadicadoInternoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = $this->consultarPermisos("radicadointerno");
        $depremitente = DB::table('users')
            ->leftJoin('dependencia', 'users.Dependencia_idDependencia', '=', 'dependencia.idDependencia')
            ->where('id', '=', \Session::get('idUsuario'))
            ->select('nombreDependencia')
            ->get();

        if (!empty($depremitente))
            $dependenciaremitente = get_object_vars($depremitente[0])['nombreDependencia'];
        else
            $dependenciaremitente = '';

        $userdestinatario = \App\User::All()->lists('name', 'id');

        if (!empty($datos))
            return view('radicadointernogrid', compact('datos', 'userdestinatario', 'dependenciaremitente'));
        else
            return view("accesodenegado");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $depremitente = DB::table('users')
            ->leftJoin('dependencia', 'users.Dependencia_idDependencia', '=', 'dependencia.idDependencia')
            ->where('id', '=', \Session::get('idUsuario'))
            ->select('nombreDependencia')
            ->get();

        if (!empty($depremitente))
            $dependenciaremitente = get_object_vars($depremitente[0])['nombreDependencia'];
        else
            $dependenciaremitente = '';

        $userdestinatario = \App\User::All()->lists('name', 'id');

        return view('radicadointernoform', compact('depremitente', 'dependenciaremitente', 'userdestinatario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RadicadoInternoRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $radicadointerno = \App\RadicadoInterno::selectRaw(
            'idRadicadoInterno, numeroRadicadoInterno, fechaRadicadoInterno, fechaDocumentoRadicadoInterno, asuntoRadicadoInterno, observacionRadicadoInterno, correoRadicadoInterno, ur.name as nombreUsuarioRemitente, ud.name as nombreDestinatario, dr.nombreDependencia as nombreDependenciaRemitente, dd.nombreDependencia as nombreDependenciaDestinataria, ur.id as idUsuarioRemitente, UsersRemitente_idRadicadoInterno, UsersDestinatario_idRadicadoInterno'
        )
            ->leftJoin('users AS ur', 'radicadointerno.UsersRemitente_idRadicadoInterno', '=', 'ur.id')
            ->leftJoin('users AS ud', 'radicadointerno.UsersDestinatario_idRadicadoInterno', '=', 'ud.id')
            ->leftJoin('dependencia AS dr', 'ur.Dependencia_idDependencia', '=', 'dr.idDependencia')
            ->leftJoin('dependencia AS dd', 'ud.Dependencia_idDependencia', '=', 'dd.idDependencia')
            ->where('idRadicadoInterno', '=', $id)
            ->firstOrFail();

        $userdestinatario = \App\User::All()->lists('name', 'id');

        return view('radicadointernoform', compact('radicadointerno', 'userdestinatario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RadicadoInternoRequest $request, $id)
    {
        if ($request->ajax())
            return;

        $radicadointerno = \App\RadicadoInterno::find($id);
        $radicadointerno->update([
            'numeroRadicadoInterno' => $request['numeroRadicadoInterno'],
            'fechaRadicadoInterno' => $request['fechaRadicadoInterno'],
            'UsersRemitente_idRadicadoInterno' => $request['idUsuarioRemitente'],
            'fechaDocumentoRadicadoInterno' => $request['fechaDocumentoRadicadoInterno'],
            'asuntoRadicadoInterno' => $request['asuntoRadicadoInterno'],
            'asuntoRadicadoSalida' => $request['asuntoRadicadoSalida'],
            'UsersDestinatario_idRadicadoInterno' => $request['UsersDestinatario_idRadicadoInterno'],
            'correoRadicadoInterno' => $request['correoRadicadoInterno'],
            'observacionRadicadoInterno' => $request['observacionRadicadoInterno']
        ]);

        $this->enviarCorreoRadicadoInterno($request, $id);
        $this->generarStickerCorrespondenciaInterna($id);

        return redirect('radicadointerno');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Dependencia::destroy($id);
        return redirect('dependencia');
    }

    public function buscarDependenciaUsuario()
    {
        $idUsuario = $_POST['idUsuario'];

        $dependenciadestinatario = DB::table('users')
            ->leftJoin('dependencia', 'users.Dependencia_idDependencia', '=', 'dependencia.idDependencia')
            ->where('id', '=', $idUsuario)
            ->select('nombreDependencia')
            ->get();

        echo json_encode($dependenciadestinatario);
    }

    public function guardarRadicadoInterno()
    {
        $asunto = $_POST['asunto'];
        $destinatario = $_POST['destinatario'];

        $numeroR = DB::table('radicadointerno')
            ->select(DB::raw('IF(
                                MAX(SUBSTRING(numeroRadicadoInterno, 1, 4)) = SUBSTRING(CURDATE(), 1, 4),
                                CONCAT(MAX(SUBSTRING(numeroRadicadoInterno, 1, 4)), "-", LPAD(IF(numeroRadicadoInterno IS NULL, "1",
                                SUBSTRING(MAX(REPLACE(numeroRadicadoInterno, "-", "")) + 1, 5, 4)), 3, "0")),
                                CONCAT(YEAR(CURDATE()), "-", LPAD("1", 3, "0"))
                            ) AS numeroRadicadoInterno'))
            ->where('Compania_idCompania', '=', \Session::get('idCompania'))
            ->get();

        \App\RadicadoInterno::create([
            'numeroRadicadoInterno' => get_object_vars($numeroR[0])['numeroRadicadoInterno'],
            'fechaRadicadoInterno' => date("Y-m-d H:i:s"),
            'UsersRemitente_idRadicadoInterno' => \Session::get('idUsuario'),
            'asuntoRadicadoInterno' => $asunto,
            'UsersDestinatario_idRadicadoInterno' => $destinatario,
            'Compania_idCompania' => \Session::get('idCompania')
        ]);

        $radicadointerno = \App\RadicadoInterno::All()->last();

        $this->generarStickerCorrespondenciaInterna($radicadointerno->idRadicadoInterno);

        echo json_encode('Radicado generado correctamente');
    }

    public function imprimirRadicadoInterno($idRadicadoInterno)
    {
        $radicadointerno = \App\RadicadoInterno::find($idRadicadoInterno);
        $rutaRadicadoInterno = 'storage/radicadointerno/' . $radicadointerno['numeroRadicadoInterno'] . '.pdf';
        $nombrePDF = $radicadointerno['numeroRadicadoInterno'] . '.pdf';

        return view('formatos.radicadoimpresionpdf', compact('rutaRadicadoInterno'));
    }

    public function enviarCorreoRadicadoInterno($request, $id)
    {
        $radicadointerno = DB::table('radicadointerno')
            ->leftJoin('publicacion', 'radicadointerno.idRadicadoInterno', '=', 'publicacion.Radicado_idRadicadoInterno')
            ->leftJoin('publicacionversion', 'publicacion.idPublicacion', '=', 'publicacionversion.Publicacion_idPublicacion')
            ->leftJoin('users AS ur', 'radicadointerno.UsersRemitente_idRadicadoInterno', '=', 'ur.id')
            ->leftJoin('users AS ud', 'radicadointerno.UsersDestinatario_idRadicadoInterno', '=', 'ud.id')
            ->leftJoin('dependencia AS dr', 'ur.Dependencia_idDependencia', '=', 'dr.idDependencia')
            ->leftJoin('dependencia AS dd', 'ud.Dependencia_idDependencia', '=', 'dd.idDependencia')
            ->select(DB::raw('radicadointerno.*, Radicado_idRadicadoInterno, ur.name as nombreRemitente, ud.name as nombreDestinatario, dr.nombreDependencia as nombreDependenciaRemitente, dd.nombreDependencia as nombreDependenciaDestinataria, rutaImagenPublicacionVersion, ud.email'))
            ->where('idRadicadoInterno', '=', $id)
            ->where('numeroPublicacionVersion', '=', 1)
            ->get();

        $datosRadicadoInterno = get_object_vars($radicadointerno[0]);

        $datos = [
            'rutaAdjunto' => $datosRadicadoInterno['rutaImagenPublicacionVersion'],
            'direccion' => $datosRadicadoInterno['email'],
            'asunto' => 'Recepción de documento interno',
            'mensaje' => '
        Ha llegado un documento para usted. El archivo lo encontrará adjunto a este mensaje. <br><br>

        <b>Número de radicado: </b>' . $datosRadicadoInterno['numeroRadicadoInterno'] . ' </br>
        <b>Fecha de radicado: </b>' . $datosRadicadoInterno['fechaRadicadoInterno'] . '<br>
        <b>Origen: </b>' . $datosRadicadoInterno['nombreRemitente'] . '<br>
        <b>Asunto: </b>' . $datosRadicadoInterno['asuntoRadicadoInterno']
        ];

        // Mail::send('email.correo',$datos,function($msj) use ($datos)
        // {
        //     $msj->to(explode(';',$datos['direccion']));
        //     $msj->subject($datos['asunto']);

        //     if (($datos['rutaAdjunto'] != '' || $datos['rutaAdjunto'] != null) && file_exists(public_path().'/'.$datos['rutaAdjunto']))
        //     {
        //         $msj->attach(public_path().'/'.$datos['rutaAdjunto']);
        //     }
        // });

        \App\RadicadoInterno::where('idRadicadoInterno', $id)
            ->update(['correoRadicadoInterno' => 1]);
    }

    public function generarStickerCorrespondenciaInterna($id)
    {
        $radicadointerno = DB::table('radicadointerno')
            ->leftJoin('publicacion', 'radicadointerno.idRadicadoInterno', '=', 'publicacion.Radicado_idRadicadoInterno')
            ->leftJoin('users AS ur', 'radicadointerno.UsersRemitente_idRadicadoInterno', '=', 'ur.id')
            ->leftJoin('users AS ud', 'radicadointerno.UsersDestinatario_idRadicadoInterno', '=', 'ud.id')
            ->leftJoin('dependencia AS dr', 'ur.Dependencia_idDependencia', '=', 'dr.idDependencia')
            ->leftJoin('dependencia AS dd', 'ud.Dependencia_idDependencia', '=', 'dd.idDependencia')
            ->select(DB::raw('radicadointerno.*, Radicado_idRadicadoInterno, ur.name as nombreRemitente, ud.name as nombreDestinatario, dr.nombreDependencia as nombreDependenciaRemitente, dd.nombreDependencia as nombreDependenciaDestinataria'))
            ->where('idRadicadoInterno', '=', $id)
            ->get();

        $datosRadicadoInterno = get_object_vars($radicadointerno[0]);


        Fpdf::AddPage('L', 'A4');
        Fpdf::SetFont('Helvetica', '', 12);

        Fpdf::SetXY(2, 10);
        Fpdf::Cell(10, 0, 'UNIDAD DE CORRESPONDENCIA', 0, 1, 'L');
        Fpdf::SetXY(2, 17);
        Fpdf::Cell(10, 0, 'Fecha/Hora: ' . $datosRadicadoInterno['fechaRadicadoInterno'], 0, 0, 'L');
        Fpdf::SetXY(2, 24);
        Fpdf::Cell(10, 0, 'Origen: ' . $datosRadicadoInterno['nombreRemitente'] . ' / ' . $datosRadicadoInterno['nombreDependenciaRemitente'], 0, 0, 'L');
        Fpdf::SetXY(2, 31);
        Fpdf::Cell(10, 0, 'Destino: ' . $datosRadicadoInterno['nombreDestinatario'] . ' / ' . $datosRadicadoInterno['nombreDependenciaDestinataria'], 0, 0, 'L');
        Fpdf::SetXY(2, 38);
        Fpdf::Cell(10, 0, 'Asunto: ' . $datosRadicadoInterno['asuntoRadicadoInterno'], 0, 0, 'L');
        Fpdf::SetXY(210, 30);
        Fpdf::Cell(10, 0, 'I-' . $datosRadicadoInterno['numeroRadicadoInterno'], 0, 0, 'L');
        Fpdf::SetXY(2, 50);
        Fpdf::Cell(10, 0, 'Sticker electronico, generado automaticamente desde Elyonsoft para la correspondencia interna', 0, 0, 'L');
        Fpdf::SetXY(2, 80);
        Fpdf::Image(public_path() . '/' . \Session::get('rutaImagenCompania'), 200, 8, 50);
        $destino = public_path() . '/storage/radicadointerno/' . $datosRadicadoInterno['numeroRadicadoInterno'] . '.pdf';
        Fpdf::Output('F', $destino);
    }

    public function cargarImagenRadicadoInterno()
    {
        $idRadicadoInterno = (isset($_POST['idRadicadoInterno']) && $_POST['idRadicadoInterno'] != '') ? $_POST['idRadicadoInterno'] : 0;

        $imagen = DB::table('publicacion')
            ->leftJoin('publicacionversion', 'publicacion.idPublicacion', '=', 'publicacionversion.Publicacion_idPublicacion')
            ->select(DB::raw('rutaImagenPublicacionVersion'))
            ->where('Radicado_idRadicadoInterno', '=', $idRadicadoInterno)
            ->get();

        echo json_encode($imagen);
    }
}
