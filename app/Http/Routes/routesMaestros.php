<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('dependencia', 'DependenciaController');
Route::resource('serie', 'SerieController');
Route::resource('subserie', 'SubSerieController');
Route::resource('documento', 'DocumentoController');
Route::resource('metadato', 'MetadatoController');
Route::resource('flujo', 'FlujoController');
Route::resource('clasificaciontarea', 'ClasificacionTareaController');
Route::resource('flujo', 'FlujoController');
Route::resource('empleado', 'EmpleadoController');
Route::resource('tercero', 'TerceroController');
Route::resource('centrocosto', 'CentroCostoController');

Route::get('seriedata', function()
{
    include public_path().'/ajax/seriedata.php';
});

Route::get('subseriedata', function()
{
    include public_path().'/ajax/subseriedata.php';
});

Route::get('dependenciadata', function()
{
    include public_path().'/ajax/dependenciadata.php';
});

Route::get('documentodata', function()
{
    include public_path().'/ajax/documentodata.php';
});
        
Route::get('metadatodata', function()
{
    include public_path().'/ajax/metadatodata.php';
});

Route::get('flujodata', function()
{
    include public_path().'/ajax/flujodata.php';
});

Route::get('flujodataselect', function()
{
    include public_path().'/ajax/flujodataselect.php';
});

Route::get('empleadodata', function()
{
    include public_path().'/ajax/empleadodata.php';
});

Route::get('clasificaciontareadata', function()
{
    include public_path().'/ajax/clasificaciontareadata.php';
});

Route::get('tercerodata', function()
{
    include public_path().'/ajax/tercerodata.php';
});

Route::get('centrocostodata', function()
{
    include public_path().'/ajax/centrocostodata.php';
});

Route::post('consultarCamposTablaDocumento','DocumentoController@consultarCamposTablaDocumento');
Route::get('flujoselect','FlujoController@indexFlujoSelect');