<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('publicacionce', 'PublicacionController');
Route::resource('consultapublicacion', 'ConsultaPublicacionController');
Route::resource('radicadoingreso', 'RadicadoIngresoController');
Route::resource('radicadosalida', 'RadicadoSalidaController');
Route::resource('radicadointerno', 'RadicadoInternoController');
Route::resource('tarea', 'TareaController');
Route::resource('flujomanual', 'FlujoManualController');

Route::get('consultapublicacionfiltro', 'ConsultaPublicacionController@consultapublicacionfiltro');
Route::get('consultapublicaciondocumento/{idDependencia}/{idSerie}/{idSubSerie}/{idDocumento}/{condGeneral}', 'ConsultaPublicacionController@consultapublicaciondocumento');

Route::post('cargarArchivoPublicacion', 'PublicacionController@cargarArchivoPublicacion');
Route::post('armarMetadatos', 'PublicacionController@armarMetadatos');
Route::get('eliminarPublicacion/{id}', 'PublicacionController@destroy');
Route::post('consultarMetadatos', 'ConsultaPublicacionController@consultarMetadatos');
Route::post('enviarCorreoPublicacion', 'ConsultaPublicacionController@enviarCorreoPublicacion');
Route::post('guardarRadicadoIngreso', 'RadicadoIngresoController@guardarRadicadoIngreso');
Route::post('guardarRadicadoSalida', 'RadicadoSalidaController@guardarRadicadoSalida');
Route::post('guardarRadicadoInterno', 'RadicadoInternoController@guardarRadicadoInterno');
Route::post('consultarRutaVistaPrevia', 'ConsultaPublicacionController@consultarRutaVistaPrevia');

Route::get('radicadoingresodata', function () {
    include public_path() . '/ajax/radicadoingresodata.php';
});

Route::get('radicadosalidadata', function () {
    include public_path() . '/ajax/radicadosalidadata.php';
});

Route::get('radicadointernodata', function () {
    include public_path() . '/ajax/radicadointernodata.php';
});

Route::get('tareadata', function () {
    include public_path() . '/ajax/tareadata.php';
});

Route::get('imprimirRadicadoIngreso/{id}', 'RadicadoIngresoController@imprimirRadicadoIngreso');
Route::get('imprimirRadicadoSalida/{id}', 'RadicadoSalidaController@imprimirRadicadoSalida');
Route::get('imprimirRadicadoInterno/{id}', 'RadicadoInternoController@imprimirRadicadoInterno');

Route::post('cargarImagenRadicadoIngreso', 'RadicadoIngresoController@cargarImagenRadicadoIngreso');
Route::post('cargarImagenRadicadoSalida', 'RadicadoSalidaController@cargarImagenRadicadoSalida');
Route::post('cargarImagenRadicadoInterno', 'RadicadoInternoController@cargarImagenRadicadoInterno');
Route::post('consultarDatosMetadatos', 'PublicacionController@consultarDatosMetadatos');
Route::post('buscarDependenciaUsuario', 'RadicadoInternoController@buscarDependenciaUsuario');
Route::post('guardarFlujoRadicadoIngreso', 'RadicadoIngresoController@guardarFlujoRadicadoIngreso');
Route::post('guardarFlujoRadicadoSalida', 'RadicadoSalidaController@guardarFlujoRadicadoSalida');
Route::get('tareas', 'FlujoController@listarTareas');
Route::get('ejecutartarea/{idFlujoTareaRadicado}/{tipoRadicado}/{idFlujo}/{tipoEjecucion}/{idRadicado}', 'FlujoController@ejecutarTarea');
Route::resource('aceptarTarea', 'FlujoController@aceptarTarea');
Route::resource('rechazarTarea', 'FlujoController@rechazarTarea');
Route::get('generarStickerCorrespondenciaInterna/{id}', 'RadicadoInternoController@generarStickerCorrespondenciaInterna');
Route::post('buscarClasificacionDetalle', 'TareaController@buscarClasificacionDetalle');
Route::post('buscarUsuarioResponsable', 'TareaController@buscarUsuarioResponsable');
Route::get('publicacionversion/{idPublicacion}', 'ConsultaPublicacionController@publicacionVersion');
Route::post('cargarArchivoPublicacionVersion', 'ConsultaPublicacionController@cargarArchivoPublicacionVersion');
Route::get('tareadashboard', 'TareaController@mostrarDashboardTarea');
