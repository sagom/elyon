<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//RUTAS PARA EL LOGIN

Route::get('login', [
    'uses' => 'Auth\AuthController@getLogin'
]);

Route::post('login', [
    'uses' => 'Auth\AuthController@postLogin', 
    'as' =>'login'
]);

Route::get('cerrarsesion', [
    'uses' => 'Auth\AuthController@getLogout', 
    'as' => 'cerrarsesion'
]);
