<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('user', 'UserController');
Route::resource('rol', 'RolController');
Route::resource('paquete', 'PaqueteController');
Route::resource('opcion', 'OpcionController');
Route::resource('compania', 'CompaniaController');

Route::get('roldata', function()
{
    include public_path().'/ajax/roldata.php';
});

Route::get('paquetedata', function()
{
    include public_path().'/ajax/paquetedata.php';
});

Route::get('opciondata', function()
{
    include public_path().'/ajax/opciondata.php';
});

Route::get('userdata', function()
{
    include public_path().'/ajax/userdata.php';
});

Route::get('companiadata', function()
{
    include public_path().'/ajax/companiadata.php';
});

Route::post('consultarRol', 'UserController@consultarRol');
Route::post('actualizarVariablesSession', 'CompaniaController@actualizarVariablesSession');