<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('radicadoinforme', 'RadicadoIngresoController@radicadoInforme');
Route::get('generarradicadoinforme', 'RadicadoIngresoController@generarRadicadoInforme');
Route::resource('auditing', 'AuditingController');
Route::get('empleadosinforme', 'EmpleadoController@empleadoInforme');
Route::get('generarempleadoinforme', 'EmpleadoController@generarEmpleadoInforme');

Route::get('flujoinforme', 'FlujoController@flujoInforme');
Route::get('generarflujoinforme', 'FlujoController@generarFlujoInforme');
Route::get('documentometadatoinforme', 'DocumentoController@documentoMetadatoInforme');
Route::get('generardocumentometadatoinforme', 'DocumentoController@generarDocumentoMetadatoInforme');

Route::get('auditingdata', function () {
    include public_path() . '/ajax/auditingdata.php';
});
