<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

#Rutas de login
require app_path() . '/Http//Routes/routesLogin.php';

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', function () {
        return view('index');
    });

    Route::get('index', function () {
        return view('index');
    });

    Route::get('rename', function () {
        $rutaPrincipal = public_path() . '/storage/estructura';
        foreach (Storage::disk('localEstructura')->directories() as $dependencia) {
            $newDependencia = str_replace(" ", "_", $dependencia);
            echo $newDependencia . ' Dependencia <br>';
            rename("$rutaPrincipal/$dependencia", "$rutaPrincipal/$newDependencia");

            foreach (Storage::disk('localEstructura')->directories("$dependencia") as $serie) {
                $newSerie = str_replace(" ", "_", $serie);
                echo $newSerie . ' Serie <br>';
                rename("$rutaPrincipal/$serie", "$rutaPrincipal/$newSerie");

                foreach (Storage::disk('localEstructura')->directories("$serie") as $subSerie) {
                    $newSubSerie = str_replace(" ", "_", $subSerie);
                    echo $newSubSerie . ' Sub serie<br>';
                    rename("$rutaPrincipal/$subSerie", "$rutaPrincipal/$newSubSerie");

                    foreach (Storage::disk('localEstructura')->directories("$subSerie") as $documento) {
                        $newDocumento = str_replace(" ", "_", $documento);
                        echo $newDocumento . ' Documento <br>';
                        rename("$rutaPrincipal/$documento", "$rutaPrincipal/$newDocumento");

                        foreach (Storage::disk('localEstructura')->files("$documento") as $fileName) {
                            $newFileName = str_replace(" ", "_", $fileName);
                            echo $newFileName . ' File <br>';
                            rename("$rutaPrincipal/$fileName", "$rutaPrincipal/$newFileName");
                        }
                    }
                }
            }
        }

        DB::update("UPDATE publicacionversion set rutaImagenPublicacionVersion = REPLACE(rutaImagenPublicacionVersion , ' ', '_')");
        DB::update("UPDATE dependencia set directorioDependencia = REPLACE(directorioDependencia , ' ', '_')");
        DB::update("UPDATE serie set directorioSerie = REPLACE(directorioSerie , ' ', '_')");
        DB::update("UPDATE subserie set directorioSubSerie = REPLACE(directorioSubSerie , ' ', '_')");
        DB::update("UPDATE documento set directorioDocumento = REPLACE(directorioDocumento , ' ', '_')");
    });

    #Rutas de maestros
    require app_path() . '/Http//Routes/routesMaestros.php';

    #Rutas de clasificación
    require app_path() . '/Http//Routes/routesClasificacion.php';

    #Rutas de movimientos
    require app_path() . '/Http//Routes/routesMovimientos.php';

    #Rutas de comunicación
    require app_path() . '/Http//Routes/routesComunicacion.php';

    #Rutas de informes
    require app_path() . '/Http//Routes/routesInformes.php';

    #Rutas de seguridad
    require app_path() . '/Http//Routes/routesSeguridad.php';
});
