<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SubSerieRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigoSubSerie' => 'required|string|max:20|unique:subserie,codigoSubSerie,'.$this->get("idSubSerie").',idSubSerie,Compania_idCompania,'.\Session::get('idCompania'),
            'nombreSubSerie' => 'required|string',
            'directorioSubSerie' => 'required|string'
        ];
    }
    public function messages()
    {
        return 
        [
            'codigoSubSerie.required' => 'El código es obligatorio',
            'codigoSubSerie.max' => 'El código no puede ser mayor a 20 caracteres',
            'codigoSubSerie.unique' => 'El código ya existe', 
            'nombreSubSerie.required' => 'El nombre es obligatorio',
            'directorioSubSerie.required' => 'El directorio es obligatorio'
        ];
    }
}
