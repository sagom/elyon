<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EstructuraRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $dependencia = count($this->get('Dependencia_idDependencia'));
        $serie = count($this->get('Serie_idSerie')); 
        $subserie = count($this->get('SubSerie_idSubSerie')); 
        $documento = count($this->get('Documento_idDocumento')); 

        $validacion = array();
        
        for($i = 0; $i < $dependencia; $i++)
        {
            if(trim($this->get('Dependencia_idDependencia')[$i]) == '' || trim($this->get('Dependencia_idDependencia')[$i]) == null)
            {    
                $validacion['Dependencia_idDependencia'.$i] =  'required';
            }
        }  
        
        for($i = 0; $i < $serie; $i++)
        {
            if(trim($this->get('Serie_idSerie')[$i]) == '' || trim($this->get('Serie_idSerie')[$i]) == null)
            {    
                $validacion['Serie_idSerie'.$i] =  'required';
            }
        }  

        for($i = 0; $i < $subserie; $i++)
        {
            if(trim($this->get('SubSerie_idSubSerie')[$i]) == '' || trim($this->get('SubSerie_idSubSerie')[$i]) == null)
            {    
                $validacion['SubSerie_idSubSerie'.$i] =  'required';
            }
        }  

        for($i = 0; $i < $documento; $i++)
        {
            if(trim($this->get('Documento_idDocumento')[$i]) == '' || trim($this->get('Documento_idDocumento')[$i]) == null)
            {    
                $validacion['Documento_idDocumento'.$i] =  'required';
            }
        }
        
        return $validacion;
    }

    public function messages()
    {
        $dependencia = count($this->get('Dependencia_idDependencia'));
        $serie = count($this->get('Serie_idSerie')); 
        $subserie = count($this->get('SubSerie_idSubSerie')); 
        $documento = count($this->get('Documento_idDocumento')); 

        $mensajes = array();

        for($i = 0; $i < $dependencia; $i++)
        {
            if(trim($this->get('Dependencia_idDependencia')[$i]) == '' || trim($this->get('Dependencia_idDependencia')[$i]) == null)
            {    
                $mensajes["Dependencia_idDependencia".$i.".required"] = "La dependencia es obligatoria en el registro  ".($i+1);
            }
        }  
        
        for($i = 0; $i < $serie; $i++)
        {
            if(trim($this->get('Serie_idSerie')[$i]) == '' || trim($this->get('Serie_idSerie')[$i]) == null)
            {    
                $mensajes["Serie_idSerie".$i.".required"] = "La serie es obligatoria en el registro  ".($i+1);
            }
        }  

        for($i = 0; $i < $subserie; $i++)
        {
            if(trim($this->get('SubSerie_idSubSerie')[$i]) == '' || trim($this->get('SubSerie_idSubSerie')[$i]) == null)
            {    
                $mensajes["SubSerie_idSubSerie".$i.".required"] = "La sub serie es obligatoria en el registro  ".($i+1);
            }
        }  

        for($i = 0; $i < $documento; $i++)
        {
            if(trim($this->get('Documento_idDocumento')[$i]) == '' || trim($this->get('Documento_idDocumento')[$i]) == null)
            {    
                $mensajes["Documento_idDocumento".$i.".required"] = "El documento es obligatorio en el registro  ".($i+1);
            }
        }

        return $mensajes;
    }
}
