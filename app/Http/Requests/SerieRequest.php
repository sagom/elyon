<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SerieRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigoSerie' => 'required|string|max:20|unique:serie,codigoSerie,'.$this->get("idSerie").',idSerie,Compania_idCompania,'.\Session::get('idCompania'),
            'nombreSerie' => 'required|string',
            'directorioSerie' => 'required|string'
        ];
    }

    public function messages()
    {
        return 
        [
            'codigoSerie.required' => 'El código es obligatorio',
            'codigoSerie.max' => 'El código no puede ser mayor a 20 caracteres',
            'codigoSerie.unique' => 'El código ya existe', 
            'nombreSerie.required' => 'El nombre es obligatorio',
            'directorioSerie.required' => 'El directorio es obligatorio'
        ];
    }
}
