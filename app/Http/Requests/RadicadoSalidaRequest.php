<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RadicadoSalidaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Users_idRadicadoSalida' => 'required|string',
            'Dependencia_idRadicadoSalida' => 'required|string',
            'fechaDocumentoRadicadoSalida' => 'date|required',
            'asuntoRadicadoSalida' => 'required|string',
            'nombreDestinatarioRadicadoSalida' => 'required|string',
            // 'identificacionDestinatarioRadicadoSalida' => 'numeric|required|digits_between:6,15',
        ];
    }
    public function messages()
    {
        return 
        [
            'Users_idRadicadoSalida.required' => 'El usuario es obligatorio',
            'Dependencia_idRadicadoSalida.required' => 'La dependencia es obligatoria',
            // 'fechaDocumentoRadicadoSalida.date' =>'La fecha no es valida',
            // 'fechaDocumentoRadicadoSalida.required' =>'La fecha es obligatoria',
            'asuntoRadicadoSalida.required' => 'El asunto es obligatorio',
            'nombreDestinatarioRadicadoSalida.required' => 'El nombre es obligatorio',
            // 'identificacionDestinatarioRadicadoSalida.required' => 'El número de identificación es obligatorio',
            // 'identificacionDestinatarioRadicadoSalida.digits_between' => 'La cantidad de dígitos de la identificación debe ser entre 6 y 15',   
        ];
    }
}
