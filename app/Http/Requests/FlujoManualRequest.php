<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FlujoManualRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombreFlujoManual' => 'required|string',
            'Dependencia_idDependencia' => 'required|int',
            'Serie_idSerie' => 'required|int',
            'SubSerie_idSubSerie' => 'required|int',
            'Documento_idDocumento' => 'required|int',
            'Flujo_idFlujo' => 'required|int',
        ];
    }

    public function messages()
    {
        return
            [
                'nombreFlujoManual.required' => 'El nombre del flujo es obligatorio',
                'Dependencia_idDependencia.required' => 'La dependencia es obligatoria',
                'Serie_idSerie.required' => 'La serie es obligatoria',
                'SubSerie_idSubSerie.required' => 'La sub serie es obligatoria',
                'Documento_idDocumento.required' => 'El documento es obligatorio',
                'Flujo_idFlujo.required' => 'El flujo es obligatorio'
            ];
    }
}
