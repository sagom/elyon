<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RadicadoIngresoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipoRadicadoIngreso' => 'required|string',
            'nombreRemitenteRadicadoIngreso' => 'required|string',
            // 'identificacionRemitenteRadicadoIngreso' => 'numeric|required|digits_between:6,15',
            'asuntoRadicadoIngreso' => 'required|string',
            // 'fechaDocumentoRadicadoIngreso' => 'date|required',
            'Users_idRadicadoIngreso' => 'required|string',
            'Dependencia_idRadicadoIngreso' => 'required|string',
        ];
    }
    public function messages()
    {
        return 
        [
            'tipoRadicadoIngreso.required' => 'El tipo es obligatorio',
            'nombreRemitenteRadicadoIngreso.required' => 'El nombre es obligatorio',
            // 'identificacionRemitenteRadicadoIngreso.required' => 'El número de identificación es obligatorio',
            // 'identificacionRemitenteRadicadoIngreso.digits_between' => 'La cantidad de dígitos de la identificación debe ser entre 6 y 15',
            'asuntoRadicadoIngreso.required' => 'El asunto es obligatorio',
            // 'fechaDocumentoRadicadoIngreso.date' =>'La fecha no es valida',
            // 'fechaDocumentoRadicadoIngreso.required' =>'La fecha es obligatoria',
            'Users_idRadicadoIngreso.required' => 'El usuario es obligatorio',
            'Dependencia_idRadicadoIngreso.required' => 'La dependencia es obligatoria',
        ];
    }
}
