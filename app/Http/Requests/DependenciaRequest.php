<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DependenciaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rol = count($this->get('Rol_idRol')); 

        $validaciones = array(
            'codigoDependencia' => 'required|string|max:20|unique:dependencia,codigoDependencia,'.$this->get("idDependencia").',idDependencia,Compania_idCompania,'.\Session::get('idCompania'),
            'nombreDependencia' => 'required|string',
            'directorioDependencia' => 'required|string'
        );

        for($i = 0; $i < $rol; $i++)
        {
            if(trim($this->get('Rol_idRol')[$i]) == '' || trim($this->get('Rol_idRol')[$i]) == null)
            {    
                $validaciones['Rol_idRol'.$i] =  'required';
            }
        }  

        return $validaciones;
    }
    public function messages()
    {
        $rol = count($this->get('Rol_idRol')); 

        $mensajes = array(
            'codigoDependencia.required' => 'El código es obligatorio',
            'codigoDependencia.max' => 'El código no puede ser mayor a 20 caracteres',
            'codigoDependencia.unique' => 'El código ya existe', 
            'nombreDependencia.required' => 'El nombre es obligatorio',
            'directorioDependencia.required' => 'El directorio es obligatorio'
        );

        for($i = 0; $i < $rol; $i++)
        {
            if(trim($this->get('Rol_idRol')[$i]) == '' || trim($this->get('Rol_idRol')[$i]) == null)
            {    
                $mensajes["Rol_idRol".$i.".required"] = "El rol es obligatorio en el registro  ".($i+1);
            }
        }  

        return $mensajes;
    }
}
