<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TareaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'asuntoTarea' => 'required|string',
            'ClasificacionTarea_idClasificacionTarea' => 'required',
            'ClasificacionTareaDetalle_idClasificacionTareaDetalle' => 'required',
            'Dependencia_idResponsable' => 'required',
            'descripcionTarea' => 'required|string'
        ];
    }

    public function messages()
    {
        return 
        [
            'asuntoTarea.required' => 'El asunto es obligatorio',
            'ClasificacionTarea_idClasificacionTarea.required' => 'La clasificación es obligatoria',
            'ClasificacionTareaDetalle_idClasificacionTareaDetalle.required' => 'La sub clasificación es obligatoria',
            'Dependencia_idResponsable.required' => 'La dependencia responsable es obligatoria',
            'descripcionTarea.required' => 'La descripción es obligatoria'
        ];
    }
}
