<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CompaniaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'codigoCompania' => 'required|string|max:20|unique:compania,codigoCompania,'.$this->get("idCompania").',idCompania',
            'nombreCompania' => 'required|string',
            //'rutaImagenCompania' => 'required'
        ];
    }
    public function messages()
    {
        return 
        [
            'codigoCompania.required' => 'El código es obligatorio',
            'codigoCompania.max' => 'El código no puede ser mayor a 20 caracteres',
            'codigoCompania.unique' => 'El código ya existe', 
            'nombreCompania.required' => 'El nombre es obligatorio',
            //'rutaImagenCompania.required' => 'La imagen es obligatoria',
            
        ];
    }
}
