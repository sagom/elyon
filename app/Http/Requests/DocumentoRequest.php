<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DocumentoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'codigoDocumento' => 'required|string|max:20|unique:documento,codigoDocumento,'.$this->get("idDocumento").',idDocumento,Compania_idCompania,'.\Session::get('idCompania'),
                'nombreDocumento' => 'required|string',
                'directorioDocumento' => 'required|string'

        ];
    }
    public function messages()
    {
        return 
        [
            'codigoDocumento.required' => 'El código es obligatorio',
            'codigoDocumento.max' => 'El código no puede ser mayor a 20 caracteres',
            'codigoDocumento.unique' => 'El código ya existe', 
            'nombreDocumento.required' => 'El nombre es obligatorio',
            'directorioDocumento.required' => 'El directorio es obligatorio'
        ];
    }
}
