<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PaqueteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ordenPaquete' => 'numeric|required|min:1|max:99999999|unique:paquete,ordenPaquete,'.$this->get("idPaquete").',idPaquete',
            'iconoPaquete' => 'required|string',
            'nombrePaquete' => 'required|string'
        ];
    }
    
    public function messages()
    {
        return 
        [
            'ordenPaquete.required' => 'El orden es obligatorio',
            'ordenPaquete.min' => 'Debe ingresar números mayores que cero',
            'ordenPaquete.unique' => 'El orden ya existe', 
            'iconoPaquete.required' => 'El icono es obligatorio',
            'nombrePaquete.required' => 'El nombre es obligatorio'
         ];
    }
    }
