<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MetadatoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigoMetadato' => 'required|string|max:20|unique:metadato,codigoMetadato,'.$this->get("idMetadato").',idMetadato,Compania_idCompania,'.\Session::get('idCompania'),
            'nombreMetadato' => 'required|string',
            'tipoMetadato' => 'required|string',
            'longitudMetadato' => 'numeric|required|min:1|max:99999999',

        ];
    }
    public function messages()
    {
        return 
        [
            'codigoMetadato.required' => 'El código es obligatorio',
            'codigoMetadato.max' => 'El código no puede ser mayor a 20 caracteres',
            'codigoMetadato.unique' => 'El código ya existe', 
            'nombreMetadato.required' => 'El nombre es obligatorio',
            'tipoMetadato.required' => 'El tipo es obligatorio',
            'longitudMetadato.required' => 'La longitud es obligatoria',
            'longitudMetadato.min' => 'Debe ingresar números mayores que cero',
        ];
    }
}
