<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OpcionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ordenOpcion' => 'numeric|required|min:1|max:99999999',
            'nombreOpcion' => 'required|string',
            'rutaOpcion' => 'required|string',
            'Paquete_idPaquete' => 'required|string'
        ];
    }
    public function messages()
    {
        return 
        [
            'ordenOpcion.required' => 'El orden es obligatorio',
            'ordenOpcion.min' => 'Debe ingresar números mayores que cero', 
            'nombreOpcion.required' => 'El nombre es obligatorio',
            'rutaOpcion.required' => 'La ruta es obligatoria',
            'Paquete_idPaquete.required' => 'El paquete es obligatorio'
        ];
    }
}
