<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MovimientoPQRSFRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombreSolicitanteMovimientoPQRSF' => 'required|string',
            'telefonoSolicitanteMovimientoPQRSF' => 'numeric|min:0000000|max:9999999',
            'movilSolicitanteMovimientoPQRSF' => 'numeric|min:0000000000|max:9999999999',
            'correoSolicitanteMovimientoPQRSF' => 'required|string|email',
            'tipoMovimientoPQRSF' => 'required|string',
            'mensajeMovimientoPQRSF' => 'required|string',
            'autorizacionMovimientoPQRSF' => 'required|string',
            
        ];
    }
    public function messages()
    {
        return 
        [
            'nombreSolicitanteMovimientoPQRSF.required' => 'El nombre es obligatorio',
            'telefonoSolicitanteMovimientoPQRSF.max' => 'El número de teléfono no es válido',
            'movilSolicitanteMovimientoPQRSF.max' => 'El número móvil no es válido',
            'correoSolicitanteMovimientoPQRSF.required' => 'El correo es obligatorio',
            'correoSolicitanteMovimientoPQRSF.email' => 'El correo no es válido',
            'tipoMovimientoPQRSF.required' => 'El tipo es obligatorio',
            'mensajeMovimientoPQRSF.required' => 'El mensaje es obligatorio',
            'autorizacionMovimientoPQRSF.required' => 'La autorización es obligatoria',
        ];
    }
}
