<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RadicadoInternoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'UsersDestinatario_idRadicadoInterno' => 'required',
            'asuntoRadicadoInterno' => 'required|string',
        ];
    }
    public function messages()
    {
        return 
        [
            'UsersDestinatario_idRadicadoInterno.required' => 'El destinatario es obligatorio',
            'asuntoRadicadoInterno.required' => 'El asunto es obligatorio',
        ];
    }
}
