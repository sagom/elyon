<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmpleadoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'documentoEmpleado' => 'required|string|max:20|unique:empleado,documentoEmpleado,'.$this->get("idEmpleado").',idEmpleado,Compania_idCompania,'.\Session::get('idCompania'),
            'nombreCompletoEmpleado' => 'required|string',
            'estadoEmpleado' => 'required|string',
            // 'CentroCosto_idCentroCosto' => 'required|int'
        ];
    }

    public function messages()
    {
        return 
        [
            'documentoEmpleado.required' => 'El número de documento es obligatorio',
            'documentoEmpleado.max' => 'El número de documento no puede ser mayor a 20 caracteres',
            'documentoEmpleado.unique' => 'El número de documento ya existe', 
            'nombreCompletoEmpleado.required' => 'El nombre es obligatorio',
            'estadoEmpleado.required' => 'El estado del empleado es obligatorio',
            'CentroCosto_idCentroCosto.required' => 'El centro de costo es obligatorio'
        ];
    }
}
