<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RolRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $opcion = count($this->get('Opcion_idOpcion')); 

        $validaciones = array(
            'codigoRol' => 'required|string',
            'nombreRol' => 'required|string'
        );

        for($i = 0; $i < $opcion; $i++)
        {
            if(trim($this->get('Opcion_idOpcion')[$i]) == '' || trim($this->get('Opcion_idOpcion')[$i]) == null)
            {    
                $validaciones['Opcion_idOpcion'.$i] =  'required';
            }
        }  

        return $validaciones;
    }

    public function messages()
    {
        $opcion = count($this->get('Opcion_idOpcion')); 

        $mensajes = array(
            'codigoRol.required' => 'El código es obligatorio',
            'nombreRol.required' => 'El nombre es obligatorio'
        );

        for($i = 0; $i < $opcion; $i++)
        {
            if(trim($this->get('Opcion_idOpcion')[$i]) == '' || trim($this->get('Opcion_idOpcion')[$i]) == null)
            {    
                $mensajes["Opcion_idOpcion".$i.".required"] = "La opción es obligatoria en el registro  ".($i+1);
            }
        }  

        return $mensajes;
    }
}
