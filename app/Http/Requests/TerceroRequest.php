<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TerceroRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'documentoTercero' => 'required|string|max:20|unique:tercero,documentoTercero,'.$this->get("idTercero").',idTercero,Compania_idCompania,'.\Session::get('idCompania'),
            'nombreCompletoTercero' => 'required|string',
            'codigoTercero' => 'required|string|max:20|unique:tercero,codigoTercero,'.$this->get("idTercero").',idTercero,Compania_idCompania,'.\Session::get('idCompania'),
            'tipoTercero' => 'required|string',
            'estadoTercero' => 'required|string'
        ];
    }

    public function messages()
    {
        return 
        [
            'documentoTercero.required' => 'El número de documento es obligatorio',
            'documentoTercero.max' => 'El número de documento no puede ser mayor a 20 caracteres',
            'documentoTercero.unique' => 'El número de documento ya existe', 
            'nombreCompletoTercero.required' => 'El nombre es obligatorio',
            'codigoTercero.required' => 'El código es obligatorio',
            'codigoTercero.max' => 'El código no puede ser mayor a 20 caracteres',
            'codigoTercero.unique' => 'El código ya existe',
            'tipoTercero.required' => 'El tipo es obligatorio', 
            'estadoTercero.required' => 'El estado del tercero es obligatorio'
        ];
    }
}
