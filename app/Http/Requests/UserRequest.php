<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $compania = count($this->get('Compania_idCompania'));
        $rol = count($this->get('Rol_idRol'));

        $validaciones = array(
            'user' => 'required|string|max:20|unique:users,user,' . $this->get("id") . ',id',
            'name' => 'required|string',
            'Dependencia_idDependencia' => 'required',
            'email' => 'required|email'
        );

        if ($this->get('id') == null or $this->get('id') == '') {
            $validaciones["password"] = "required|min:6|max:20|confirmed";
            $validaciones["password_confirmation"] = "required|min:6|max:20";
        }
        // si estamos editando, verificamos los password iguales solo si alguno de los 2 no esta vacio
        else {
            if ($this->get('password') != '' or  $this->get('password_confirmation')  != '') {

                $validaciones["password"] = "required|min:6|max:20|confirmed";
                $validaciones["password_confirmation"] = "required|min:6|max:20";
            }
        }

        for ($i = 0; $i < $compania; $i++) {
            if (trim($this->get('Compania_idCompania')[$i]) == '' || trim($this->get('Compania_idCompania')[$i]) == null) {
                $validaciones['Compania_idCompania' . $i] =  'required';
            }
        }

        for ($i = 0; $i < $rol; $i++) {
            if (trim($this->get('Rol_idRol')[$i]) == '' || trim($this->get('Rol_idRol')[$i]) == null) {
                $validaciones['Rol_idRol' . $i] =  'required';
            }
        }


        return $validaciones;
    }
    public function messages()
    {
        $compania = count($this->get('Compania_idCompania'));
        $rol = count($this->get('Rol_idRol'));

        $mensajes = array(
            'user.required' => 'El usuario es obligatorio',
            'user.min' => 'El usuario debe contener como mínimo 4 caracteres',
            'user.unique' => 'El usuario ya existe',
            'email.required' => 'El correo electrónico es obligatorio',
            'email.unique' => 'El correo electrónico que ingresó ya existe en el sistema',
            'password.required' => 'La contraseña es obligatoria',
            'password.min' => 'La contraseña debe contener como mínimo 6 caracteres',
            'password.confirmed' => 'Las contraseñas no coinciden',
            'password_confirmation.required' => 'La confirmación de la contraseña es obligatoria',
            'Dependencia_idDependencia.required' => 'La dependencia es obligatoria'
        );

        for ($i = 0; $i < $compania; $i++) {
            if (trim($this->get('Compania_idCompania')[$i]) == '' || trim($this->get('Compania_idCompania')[$i]) == null) {
                $mensajes["Compania_idCompania" . $i . ".required"] = "La compañía es obligatoria en el registro  " . ($i + 1);
            }
        }

        for ($i = 0; $i < $rol; $i++) {
            if (trim($this->get('Rol_idRol')[$i]) == '' || trim($this->get('Rol_idRol')[$i]) == null) {
                $mensajes["Rol_idRol" . $i . ".required"] = "El rol es obligatorio en el registro  " . ($i + 1);
            }
        }

        return $mensajes;
    }
}
