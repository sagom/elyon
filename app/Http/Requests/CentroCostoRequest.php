<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CentroCostoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigoCentroCosto' => 'required|string|max:20|unique:centrocosto,codigoCentroCosto,'.$this->get("idCentroCosto").',idCentroCosto,Compania_idCompania,'.\Session::get('idCompania'),
            'nombreCentroCosto' => 'required|string'
        ];
    }

    public function messages()
    {
        return 
        [
            'codigoCentroCosto.required' => 'El código es obligatorio',
            'codigoCentroCosto.max' => 'El código no puede ser mayor a 20 caracteres',
            'codigoCentroCosto.unique' => 'El código ya existe',
            'nombreCentroCosto.required' => 'El nombre es obligatorio'
        ];
    }
}