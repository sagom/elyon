<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RetencionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $dependencia = count($this->get('Dependencia_idDependencia'));
        $serie = count($this->get('Serie_idSerie')); 
        $subserie = count($this->get('SubSerie_idSubSerie')); 
        $documento = count($this->get('Documento_idDocumento')); 
        $soporte = count($this->get('soporteRetencion')); 
        $disposicion = count($this->get('disposicionFinalRetencion')); 
        $estado = count($this->get('estadoRetencion')); 

        $validacion = array();
        
        for($i = 0; $i < $dependencia; $i++)
        {
            if(trim($this->get('Dependencia_idDependencia')[$i]) == '' || trim($this->get('Dependencia_idDependencia')[$i]) == null)
            {    
                $validacion['Dependencia_idDependencia'.$i] =  'required';
            }
        }  
        
        for($i = 0; $i < $serie; $i++)
        {
            if(trim($this->get('Serie_idSerie')[$i]) == '' || trim($this->get('Serie_idSerie')[$i]) == null)
            {    
                $validacion['Serie_idSerie'.$i] =  'required';
            }
        }  

        for($i = 0; $i < $subserie; $i++)
        {
            if(trim($this->get('SubSerie_idSubSerie')[$i]) == '' || trim($this->get('SubSerie_idSubSerie')[$i]) == null)
            {    
                $validacion['SubSerie_idSubSerie'.$i] =  'required';
            }
        }  

        for($i = 0; $i < $documento; $i++)
        {
            if(trim($this->get('Documento_idDocumento')[$i]) == '' || trim($this->get('Documento_idDocumento')[$i]) == null)
            {    
                $validacion['Documento_idDocumento'.$i] =  'required';
            }
        }

        for($i = 0; $i < $soporte; $i++)
        {
            if(trim($this->get('soporteRetencion')[$i]) == '' || trim($this->get('soporteRetencion')[$i]) == null)
            {    
                $validacion['soporteRetencion'.$i] =  'required';
            }
        }

        for($i = 0; $i < $disposicion; $i++)
        {
            if(trim($this->get('disposicionFinalRetencion')[$i]) == '' || trim($this->get('disposicionFinalRetencion')[$i]) == null)
            {    
                $validacion['disposicionFinalRetencion'.$i] =  'required';
            }
        }

        for($i = 0; $i < $estado; $i++)
        {
            if(trim($this->get('estadoRetencion')[$i]) == '' || trim($this->get('estadoRetencion')[$i]) == null)
            {    
                $validacion['estadoRetencion'.$i] =  'required';
            }
        }
        
        return $validacion;
    }

    public function messages()
    {
        $dependencia = count($this->get('Dependencia_idDependencia'));
        $serie = count($this->get('Serie_idSerie')); 
        $subserie = count($this->get('SubSerie_idSubSerie')); 
        $documento = count($this->get('Documento_idDocumento')); 
        $soporte = count($this->get('soporteRetencion')); 
        $disposicion = count($this->get('disposicionFinalRetencion')); 
        $estado = count($this->get('estadoRetencion')); 

        $mensajes = array();

        for($i = 0; $i < $dependencia; $i++)
        {
            if(trim($this->get('Dependencia_idDependencia')[$i]) == '' || trim($this->get('Dependencia_idDependencia')[$i]) == null)
            {    
                $mensajes["Dependencia_idDependencia".$i.".required"] = "La dependencia es obligatoria en el registro  ".($i+1);
            }
        }  
        
        for($i = 0; $i < $serie; $i++)
        {
            if(trim($this->get('Serie_idSerie')[$i]) == '' || trim($this->get('Serie_idSerie')[$i]) == null)
            {    
                $mensajes["Serie_idSerie".$i.".required"] = "La serie es obligatoria en el registro  ".($i+1);
            }
        }  

        for($i = 0; $i < $subserie; $i++)
        {
            if(trim($this->get('SubSerie_idSubSerie')[$i]) == '' || trim($this->get('SubSerie_idSubSerie')[$i]) == null)
            {    
                $mensajes["SubSerie_idSubSerie".$i.".required"] = "La sub serie es obligatoria en el registro  ".($i+1);
            }
        }  

        for($i = 0; $i < $documento; $i++)
        {
            if(trim($this->get('Documento_idDocumento')[$i]) == '' || trim($this->get('Documento_idDocumento')[$i]) == null)
            {    
                $mensajes["Documento_idDocumento".$i.".required"] = "El documento es obligatorio en el registro  ".($i+1);
            }
        }

        for($i = 0; $i < $soporte; $i++)
        {
            if(trim($this->get('soporteRetencion')[$i]) == '' || trim($this->get('soporteRetencion')[$i]) == null)
            {    
                $mensajes["soporteRetencion".$i.".required"] = "El soporte es obligatorio en el registro  ".($i+1);
            }
        }

        for($i = 0; $i < $disposicion; $i++)
        {
            if(trim($this->get('disposicionFinalRetencion')[$i]) == '' || trim($this->get('disposicionFinalRetencion')[$i]) == null)
            {    
                $mensajes["disposicionFinalRetencion".$i.".required"] = "La disposición es obligatoria en el registro  ".($i+1);
            }
        }

        for($i = 0; $i < $estado; $i++)
        {
            if(trim($this->get('estadoRetencion')[$i]) == '' || trim($this->get('estadoRetencion')[$i]) == null)
            {    
                $mensajes["estadoRetencion".$i.".required"] = "El estado es obligatorio en el registro  ".($i+1);
            }
        }

        return $mensajes;
    }
}
