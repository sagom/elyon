<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FlujoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $tarea = count($this->get('nombreFlujoTarea'));
        $usuario = count($this->get('Users_idFlujoTarea'));
        
        $validacion = array(
            'codigoFlujo' => 'required|string',
            'nombreFlujo' => 'required|string');

        for($i = 0; $i < $tarea; $i++)
        {
            if(trim($this->get('nombreFlujoTarea')[$i]) == '')
            {    
                $validacion['nombreFlujoTarea'.$i] =  'required';
            }
        }  

        for($i = 0; $i < $usuario; $i++)
        {
            if(trim($this->get('Users_idFlujoTarea')[$i]) == '' || trim($this->get('Users_idFlujoTarea')[$i]) == null)
            {    
                $validacion['Users_idFlujoTarea'.$i] =  'required';
            }
        } 
        
        return $validacion;
    }

    public function messages()
    {
        $tarea = count($this->get('nombreFlujoTarea'));
        $usuario = count($this->get('Users_idFlujoTarea'));

        $mensajes = array(
            'codigoFlujo.required' => 'El código es obligatorio',
            'nombreFlujo.required' => 'El nombre es obligatorio');

        for($i = 0; $i < $tarea; $i++)
        {
            if(trim($this->get('nombreFlujoTarea')[$i]) == '')
            {    
                $mensajes["nombreFlujoTarea".$i.".required"] = "El nombre de la tarea es obligatorio en el registro  ".($i+1);
            }
        }  

        for($i = 0; $i < $usuario; $i++)
        {
            if(trim($this->get('Users_idFlujoTarea')[$i]) == '' || trim($this->get('Users_idFlujoTarea')[$i]) == null)
            {    
                $mensajes["Users_idFlujoTarea".$i.".required"] = "El usuario es obligatorio en el registro  ".($i+1);
            }
        }  

        return $mensajes;
    }
}
