<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClasificacionTareaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $codigo = count($this->get('codigoClasificacionTareaDetalle')); 
        $nombre = count($this->get('nombreClasificacionTareaDetalle')); 

        $validaciones = array(
            'codigoClasificacionTarea' => 'required|string|max:20|unique:clasificaciontarea,codigoClasificacionTarea,'.$this->get("idClasificacionTarea").',idClasificacionTarea,Compania_idCompania,'.\Session::get('idCompania'),
            'nombreClasificacionTarea' => 'required|string',
        );

        for($i = 0; $i < $codigo; $i++)
        {
            if(trim($this->get('codigoClasificacionTareaDetalle')[$i]) == '' || trim($this->get('codigoClasificacionTareaDetalle')[$i]) == null)
            {    
                $validaciones['codigoClasificacionTareaDetalle'.$i] =  'required';
            }
        }
        
        for($i = 0; $i < $nombre; $i++)
        {
            if(trim($this->get('nombreClasificacionTareaDetalle')[$i]) == '' || trim($this->get('nombreClasificacionTareaDetalle')[$i]) == null)
            {    
                $validaciones['nombreClasificacionTareaDetalle'.$i] =  'required';
            }
        }

        return $validaciones;
    }
    public function messages()
    {
        $codigo = count($this->get('codigoClasificacionTareaDetalle')); 
        $nombre = count($this->get('nombreClasificacionTareaDetalle')); 

        $mensajes = array(
            'codigoClasificacionTarea.required' => 'El código es obligatorio',
            'codigoClasificacionTarea.unique' => 'El código ya existe', 
            'nombreClasificacionTarea.required' => 'El nombre es obligatorio'
        );

        for($i = 0; $i < $codigo; $i++)
        {
            if(trim($this->get('codigoClasificacionTareaDetalle')[$i]) == '' || trim($this->get('codigoClasificacionTareaDetalle')[$i]) == null)
            {    
                $mensajes["codigoClasificacionTareaDetalle".$i.".required"] = "El código es obligatorio en el registro  ".($i+1);
            }
        }  

        for($i = 0; $i < $nombre; $i++)
        {
            if(trim($this->get('nombreClasificacionTareaDetalle')[$i]) == '' || trim($this->get('nombreClasificacionTareaDetalle')[$i]) == null)
            {    
                $mensajes["nombreClasificacionTareaDetalle".$i.".required"] = "El nombre es obligatorio en el registro  ".($i+1);
            }
        }  

        return $mensajes;
    }
}
