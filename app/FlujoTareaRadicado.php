<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class FlujoTareaRadicado extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'flujotarearadicado';
    protected $primaryKey = 'idFlujoTareaRadicado';

    protected $fillable = ['FlujoTarea_idFlujoTarea', 'Radicado_idRadicadoIngreso', 'Radicado_idRadicadoSalida', 'FlujoManual_idFlujoManual', 'estadoFlujoTareaRadicado'];

    public $timestamps = false;

    protected $auditInclude = [
        'FlujoTarea_idFlujoTarea',
        'Radicado_idRadicadoIngreso',
        'Radicado_idRadicadoSalida',
        'estadoFlujoTareaRadicado'
    ];
}
