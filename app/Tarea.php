<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Tarea extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'tarea';
    protected $primaryKey = 'idTarea';

    protected $fillable = ['numeroTarea', 'fechaElaboracionTarea', 'asuntoTarea', 'prioridadTarea', 'Users_idSolicitante', 'ClasificacionTarea_idClasificacionTarea', 'ClasificacionTareaDetalle_idClasificacionTareaDetalle', 'estadoTarea', 'Dependencia_idResponsable', 'Users_idResponsable', 'fechaVencimientoTarea', 'descripcionTarea', 'solucionTarea', 'fechaSolucionTarea', 'Compania_idCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'numeroTarea', 'fechaElaboracionTarea', 'asuntoTarea', 'prioridadTarea', 'Users_idSolicitante', 'ClasificacionTarea_idClasificacionTarea', 'ClasificacionTareaDetalle_idClasificacionTareaDetalle', 'estadoTarea', 'Dependencia_idResponsable', 'Users_idResponsable', 'fechaVencimientoTarea', 'descripcionTarea', 'solucionTarea', 'fechaSolucionTarea'
    ];
}
