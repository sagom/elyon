<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class FlujoTareaRadicadoComentario extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'flujotarearadicadocomentario';
    protected $primaryKey = 'idFlujoTareaRadicadoComentario';

    protected $fillable = ['FlujoTareaRadicado_idFlujoTareaRadicado', 'estadoFlujoTareaRadicadoComentario', 'fechaFlujoTareaRadicadoComentario', 'observacionFlujoTareaRadicadoComentario', 'respuestaFlujoTareaRadicadoComentario'];

    public $timestamps = false;

    protected $auditInclude = [
        'FlujoTareaRadicado_idFlujoTareaRadicado',
        'estadoFlujoTareaRadicadoComentario',
        'fechaFlujoTareaRadicadoComentario',
        'respuestaFlujoTareaRadicadoComentario'
    ];
}
