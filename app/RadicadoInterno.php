<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class RadicadoInterno extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $table = 'radicadointerno';
    protected $primaryKey = 'idRadicadoInterno';

    protected $fillable = ['numeroRadicadoInterno', 'fechaRadicadoInterno', 'UsersRemitente_idRadicadoInterno', 'fechaDocumentoRadicadoInterno', 'asuntoRadicadoInterno', 'UsersDestinatario_idRadicadoInterno', 'correoRadicadoInterno', 'observacionRadicadoInterno', 'Compania_idCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'numeroRadicadoInterno', 
        'fechaRadicadoInterno', 
        'UsersRemitente_idRadicadoInterno', 
        'fechaDocumentoRadicadoInterno', 
        'asuntoRadicadoInterno', 
        'UsersDestinatario_idRadicadoInterno', 
        'correoRadicadoInterno', 
        'observacionRadicadoInterno', 
        'Compania_idCompania'
    ];
}
