<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DocumentoMetadato extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'documentometadato';
    protected $primaryKey = 'idDocumentoMetadato';

    protected $fillable = ['Documento_idDocumento', 'ordenDocumentoMetadato', 'Metadato_idMetadato', 'campoTablaDocumentoMetadato', 'indiceDocumentoMetadato'];

    public $timestamps = false;
    
    protected $auditInclude = [ 
        'ordenDocumentoMetadato', 
        'Metadato_idMetadato',
        'campoTablaDocumentoMetadato',
        'indiceDocumentoMetadato'
    ];
}
