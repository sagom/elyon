<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Flujo extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'flujo';
    protected $primaryKey = 'idFlujo';

    protected $fillable = ['codigoFlujo', 'nombreFlujo', 'Compania_idCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'codigoFlujo',
        'nombreFlujo'
    ];
}
