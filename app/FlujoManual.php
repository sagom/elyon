<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class FlujoManual extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'flujomanual';
    protected $primaryKey = 'idFlujoManual';

    protected $fillable = ['nombreFlujoManual', 'Users_idFlujoManual'];

    public $timestamps = false;

    protected $auditInclude = [
        'nombreFlujoManual'
    ];
}
