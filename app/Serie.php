<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Serie extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'serie';
    protected $primaryKey = 'idSerie';

    protected $fillable = ['codigoSerie', 'nombreSerie', 'directorioSerie' , 'descripcionSerie', 'Compania_idCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'codigoSerie', 
        'nombreSerie', 
        'directorioSerie' , 
        'descripcionSerie'
    ];
}
