<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PublicacionVersion extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;


    protected $table = 'publicacionversion';
    protected $primaryKey = 'idPublicacionVersion';

    protected $fillable = ['Publicacion_idPublicacion', 'fechaPublicacionVersion', 'numeroPublicacionVersion', 'rutaImagenPublicacionVersion', 'Users_idPublicacionVersion'];

    public $timestamps = false;

    protected $auditInclude = [
        'fechaPublicacionVersion',
        'numeroPublicacionVersion'
    ];
}
