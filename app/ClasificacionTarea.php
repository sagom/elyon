<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ClasificacionTarea extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'clasificaciontarea';
    protected $primaryKey = 'idClasificacionTarea';

    protected $fillable = ['codigoClasificacionTarea', 'nombreClasificacionTarea', 'Compania_idCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'codigoClasificacionTarea',
        'nombreClasificacionTarea'
    ];
}
