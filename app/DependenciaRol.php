<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DependenciaRol extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'dependenciarol';
    protected $primaryKey = 'idDependenciaRol';

    protected $fillable = ['Dependencia_idDependencia', 'Rol_idRol', 'cargarDependenciaRol', 'descargarDependenciaRol', 'consultarDependenciaRol', 'imprimirDependenciaRol', 'enviarDependenciaRol', 'eliminarDependenciaRol'];

    public $timestamps = false;

    protected $auditInclude = [
        'Rol_idRol', 
        'cargarDependenciaRol', 
        'descargarDependenciaRol', 
        'consultarDependenciaRol', 
        'imprimirDependenciaRol', 
        'enviarDependenciaRol', 
        'eliminarDependenciaRol'
    ];
}
