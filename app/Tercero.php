<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Tercero extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'tercero';
    protected $primaryKey = 'idTercero';

    protected $fillable = ['documentoTercero' , 'nombreCompletoTercero' , 'codigoTercero', 'tipoTercero' , 'estadoTercero', 'Compania_idCompania'];

    public $timestamps = false;

    protected $auditInclude = [
        'documentoTercero', 
        'nombreCompletoTercero', 
        'codigoTercero',
        'tipoTercero',
        'estadoTercero',
        'Compania_idCompania' 
    ];
}