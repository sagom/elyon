@extends('layouts.grid') 
@section('content')
{!!Form::model($datos)!!}

<script type="text/javascript">
    $(document).ready( function () 
    {
        configurarGrid('tercero',"{!! URL::to ('/tercerodata')!!}");
    }); 
</script>

<?php
    $visible = '';

    if(isset($datos[0])) 
    {
        $dato = get_object_vars($datos[0]);
        if ($dato['adicionarRolOpcion'] == 1) 
            $visible = 'inline-block;'; 
        else
            $visible = 'none;';
    }
    else
        $visible = 'none;';
?>

<legend>Terceros</legend>
<table id="tercero" class="table table-striped table-bordered nowrap" style="width:100%">
    <thead class="btn-success">
        <tr>
            <th style="width:60px;padding: 1px 8px;" data-orderable="false">
                <a href="tercero/create"><span style="color:white; <?php echo $visible ?>" class="glyphicon glyphicon-plus"></span></a>
            </th>
            <th>ID</th>
            <th>Documento</th>
            <th>Nombre</th>
            <th>Código</th>
            <th>Tipo</th>
            <th>Estado</th>
        </tr>
    </thead>
    <tfoot class="btn-default active">
        <tr>
            <th>
                &nbsp;
            </th>
            <th>ID</th>
            <th>Documento</th>
            <th>Nombre</th>
            <th>Código</th>
            <th>Tipo</th>
            <th>Estado</th>
        </tr>
    </tfoot>
</table>

{!!Form::close()!!}
@stop