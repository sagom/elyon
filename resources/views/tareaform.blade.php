@extends('layouts.vista')

@section('content')
    {!!Html::script('js/tarea.js'); !!} 
    @include('alerts.request')

    @if(isset($tarea))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($tarea,['route'=>['tarea.destroy',$tarea->idTarea],'method'=>'DELETE', 'id' => 'form-tarea'])!!}
		@else
			{!!Form::model($tarea,['route'=>['tarea.update',$tarea->idTarea],'method'=>'PUT', 'id' => 'form-tarea'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'tarea.store','method'=>'POST', 'id' => 'form-tarea'])!!}
    @endif

    <script>
        $(document).ready(function(){ 
            if($("#idTarea").val() > 0)
            {
                buscarClasificacionDetalle($("#ClasificacionTarea_idClasificacionTarea").val(), '<?php echo isset($tarea) ? $tarea->ClasificacionTareaDetalle_idClasificacionTareaDetalle : "" ;?>');
                buscarUsuarioResponsable($("#Dependencia_idResponsable").val(), '<?php echo isset($tarea) ? $tarea->Users_idResponsable : "";?>');
            }
        });
    </script>

    <fieldset>
        <legend>Información de la Tarea</legend>
        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('numeroTarea', 'Número', array('class' => 'label-responsive-cuatro')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!!Form::hidden('idTarea', null, array('id' => 'idTarea')) !!}
                    {!!Form::text('numeroTarea',null,['class'=>'form-control','placeholder'=>'Automático', 'readonly'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('fechaElaboracionTarea', 'Elaboración', array('class' => 'label-responsive-cuatro')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    {!!Form::text('fechaElaboracionTarea',(isset($tarea) ? $tarea->fechaElaboracionTarea : date('Y-m-d H:i:s')),['class'=>'form-control', 'readonly'])!!}
                </div>
            </div>
        </div>
    
        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('asuntoTarea', 'Asunto', array('class' => 'label-responsive-cuatro')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::text('asuntoTarea',null,['class'=>'form-control','placeholder'=>'Ingrese el asunto'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('prioridadTarea', 'Prioridad', array('class' => 'label-responsive-cuatro')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-exclamation-circle"></i>
                    </span>
                    {!!Form::select('prioridadTarea',['Alta' => 'Alta', 'Media' => 'Media', 'Baja' => 'Baja'], (isset($tarea) ? $tarea->prioridadTarea : 'Baja'),["class" => "select form-control"])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('nombreUsuarioSolicitante', 'Solicitante', array('class' => 'label-responsive-cuatro')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-user"></i>
                    </span>
                    {!!Form::text('nombreUsuarioSolicitante',(isset($tarea) ? $tarea->nombreUsuarioSolicitante : \Session::get('nombreUsuario')),['class'=>'form-control','placeholder'=>'Nombre del solicitante', 'readonly'])!!}
                    {!!Form::hidden('Users_idSolicitante',(isset($tarea) ? $tarea->Users_idSolicitante : \Session::get('idUsuario')),['id'=>'Users_idSolicitante'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('nombreDependenciaSolicitante', 'Dep Solicitante', array('class' => 'label-responsive-cuatro')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-university"></i>
                    </span>
                    {!!Form::text('nombreDependenciaSolicitante',(isset($tarea) ? $tarea->nombreDependenciaSolicitante : $depsolicitante),['class'=>'form-control','placeholder'=>'Dependencia del solicitante', 'readonly'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('ClasificacionTarea_idClasificacionTarea', 'Clasificación', array('class' => 'label-responsive-cuatro')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-bezier-curve"></i>
                    </span>
                    {!!Form::select('ClasificacionTarea_idClasificacionTarea',$clasificacion, (isset($tarea) ? $tarea->ClasificacionTarea_idClasificacionTarea : null),["class" => "select form-control", "placeholder" =>"- Seleccione la clasificación -", "onchange" => "buscarClasificacionDetalle(this.value)"])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('ClasificacionTareaDetalle_idClasificacionTareaDetalle', 'Sub Clasificación', array('class' => 'label-responsive-cuatro')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-cogs"></i>
                    </span>
                    {!!Form::select('ClasificacionTareaDetalle_idClasificacionTareaDetalle',$clasificaciondet, (isset($tarea) ? $tarea->ClasificacionTareaDetalle_idClasificacionTareaDetalle : null),["class" => "select form-control", "placeholder" =>"- Seleccione la sub clasificación -"])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('Dependencia_idResponsable', 'Dep Responsable', array('class' => 'label-responsive-cuatro')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-university"></i>
                    </span>
                    {!!Form::select('Dependencia_idResponsable',$dependenciaresp, (isset($tarea) ? $tarea->Dependencia_idResponsable : null),["class" => "select form-control", "placeholder" =>"- Seleccione la dependencia responsable -", "onchange" => "buscarUsuarioResponsable(this.value)"])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('Users_idResponsable', 'Responsable', array('class' => 'label-responsive-cuatro')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-user"></i>
                    </span>
                    {!!Form::select('Users_idResponsable',$userresp, (isset($tarea) ? $tarea->Users_idResponsable : null),["class" => "select form-control", "placeholder" =>"- Seleccione el responsable -"])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('estadoTarea', 'Estado', array('class' => 'label-responsive-cuatro')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-list"></i>
                    </span>
                    {!!Form::select('estadoTarea',['Nuevo' => 'Nuevo', 'En proceso' => 'En proceso', 'Terminado' => 'Terminado', 'Rechazado' => 'Rechazado'], (isset($tarea) ? $tarea->estadoTarea : 'Nuevo'),["class" => "select form-control"])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('fechaVencimientoTarea', 'Fech vencimiento', array('class' => 'label-responsive-cuatro')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-calendar"></i>
                    </span>
                    {!!Form::date('fechaVencimientoTarea',null,['class'=>'form-control','placeholder'=>'Dependencia del destinatario'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
                {!!Form::label('fechaSolucionTarea', 'Fecha solución', array('class' => 'label-responsive-cuatro')) !!}
                <div class="div-input-responsive">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fas fa-calendar"></i>
                        </span>
                        {!!Form::text('fechaSolucionTarea',null,['class'=>'form-control','placeholder'=>'Fecha de solución', 'readonly'])!!}
                    </div>
                </div>
            </div>

    </fieldset>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">
                Detalle
            </h4>
        </div>

        <div class="panel-body">
            <div class="form-group" id='test'>

                <div class="div-responsive-doce bhoechie-tab-container">
                    <div class="div-responsive-uno bhoechie-tab-menu">
                        <div class="list-group">
                            <a href="#" class="list-group-item active text-center">
                                <h4 class="fa fa-file-signature fa-3x"></h4><br/>Descripción
                            </a>
                            <a href="#" class="list-group-item text-center">
                                <h4 class="fa fa-check-circle fa-3x"></h4><br/>Solución
                            </a>
                            {{-- <a href="#" class="list-group-item text-center">
                                <h4 class="fa fa-folder-open fa-3x"></h4><br/>Adjuntos
                            </a> --}}
                        </div>
                    </div>

                    <div class="div-responsive-once bhoechie-tab">

                        <div class="bhoechie-tab-content active">
                            {!!Form::textarea('descripcionTarea',null,['class'=>'form-control','placeholder'=>'Ingrese la descripción'])!!}        
                        </div>

                        <div class="bhoechie-tab-content">
                            {!!Form::textarea('solucionTarea',null,['class'=>'form-control','placeholder'=>'Ingrese la solución'])!!}        
                        </div>

                        {{-- <div class="bhoechie-tab-content">
                            3
                        </div> --}}
                    </div>
                </div>

            </div>
        </div>
        
    </div>
    

    @if(isset($tarea))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'tarea')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'tarea')"])!!}
    @endif 
     
    {!!Form::close()!!}

@stop