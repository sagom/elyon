@extends('layouts.vista')

@section('content')
    {!!Html::script('js/empleadoinforme.js'); !!} 
    @include('alerts.request')

	{!!Form::open(['method'=>'POST', 'id' => 'form-empleadoinforme'])!!}

    <fieldset>
        <legend>Informe de empleados</legend>
        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('estadoEmpleado', 'Estado', array('class' => 'label-responsive-tres')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-list"></i>
                    </span>
                    {!!Form::select('estadoEmpleado',['Activo' => 'Activo', 'Inactivo' => 'Inactivo'],null,['class' => 'form-control','placeholder'=>'- Seleccione un estado -']) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('CentroCosto_idCentroCosto', 'Centro de costo', array('class' => 'label-responsive-tres')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::select('CentroCosto_idCentroCosto', $centroCosto, null,["class" => "select form-control", "placeholder" =>"- Seleccione un centro de costo -"])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('documentoEmpleado', 'Documento', array('class' => 'label-responsive-tres')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!!Form::number('documentoEmpleado',null,['class'=>'form-control','placeholder'=>'Ingrese el número de documento'])!!}
                </div>
            </div>
        </div>

    </fieldset>
    
      {!!Form::button('Consultar',["class"=>"btn btn-success", "onclick"=>"consultarEmpleado(1)"])!!} 
      {!!Form::button('Descargar',["class"=>"btn btn-success", "onclick"=>"consultarEmpleado(2)"])!!}
     
    {!!Form::close()!!}

@stop