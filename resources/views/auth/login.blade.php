@extends('layouts.acceso')

@section('content')
<center>@include('alerts.logError')</center>
<div class="container">
	<div class="login-container">
        <div id="output"></div>
            <div class="avatar">
                <img src="{!!URL::to('storage/Elyon.png')!!}" style="width:100%; vertical-align: middle;">
                <input type="hidden" id="token" value="{{csrf_token()}}"/>
            </div>
            <div class="form-box">
                {!!Form::open(['route' => 'login', 'method' => 'post', 'class' => 'form'])!!}
                    {!!Form::text('user','',["required"=>"required",'class'=> 'form-control','placeholder'=>'Usuario'])!!}
                    <div class="input-group">
                    {!!Form::password('password', ["required"=>"required",'class'=> 'form-control','placeholder' => 'Contraseña', "id"=>"password"])!!}
                        <span class="input-group-addon" style="cursor:pointer" onmouseout="ocultarContraseña()" onmouseover="visualizarContraseña()">
                            <i id="iconoOjo" class="glyphicon glyphicon-eye-close"></i>
                        </span>
                </div>          
                    {!!Form::button('Login',['class' =>'btn btn-success btn-block login', 'type' => 'submit']) !!}
                {!!Form::close()!!}
            </div>
    </div>
</div>

@stop

<script>
    function visualizarContraseña()
    {
        $("#password").prop("type", "text");
        $("#iconoOjo").attr("class", "glyphicon glyphicon-eye-open");

    }

    function ocultarContraseña()
    {
        $("#password").prop("type", "password");    
        $("#iconoOjo").attr("class", "glyphicon glyphicon-eye-close");   
    }
</script>