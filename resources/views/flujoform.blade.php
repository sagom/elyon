@extends('layouts.vista')

@section('content')
    {!! Html::script('js/flujo.js') !!}
    @include('alerts.request')

    @if (isset($flujo))
        @if (isset($_GET['accion']) and $_GET['accion'] == 'destroy')
            {!! Form::model($flujo, [
                'route' => ['flujo.destroy', $flujo->idFlujo],
                'method' => 'DELETE',
                'id' => 'form-flujo',
            ]) !!}
        @else
            {!! Form::model($flujo, ['route' => ['flujo.update', $flujo->idFlujo], 'method' => 'PUT', 'id' => 'form-flujo']) !!}
        @endif
    @else
        {!! Form::open(['route' => 'flujo.store', 'method' => 'POST', 'id' => 'form-flujo']) !!}
    @endif

    <fieldset>
        <legend>Información de Flujo</legend>
        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('codigoFlujo', 'Código', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!! Form::hidden('idFlujo', null, ['id' => 'idFlujo']) !!}
                    {!! Form::hidden('eliminarFlujoTarea', null, ['id' => 'eliminarFlujoTarea']) !!}
                    {!! Form::text('codigoFlujo', null, ['class' => 'form-control', 'placeholder' => 'Ingrese el código']) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('nombreFlujo', 'Nombre', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!! Form::text('nombreFlujo', null, ['class' => 'form-control', 'placeholder' => 'Ingrese el nombre']) !!}
                </div>
            </div>
        </div>
    </fieldset>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#divTareas">Tareas del flujo</a>
            </h4>
        </div>
        <div id="divTareas" class="panel-collapse">
            <div class="panel-body">
                <div class="form-group" id='test'>
                    <div class="col-sm-12">
                        <div class="row show-grid">
                            <div style="overflow:auto; height:170px;">
                                <div style="width: 1300px; display: inline-block;">
                                    <div class="btn-default active col-md-1" onclick="tarea.agregarCampos(valorTarea,'A')"
                                        style="width:40px; height:40px; cursor:pointer; display:inline-block">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </div>
                                    <div class="btn-default active col-md-1"
                                        style="width:40px; height:40px; display:inline-block">
                                        <span></span>
                                    </div>
                                    <div class="btn-default active col-md-1"
                                        style="width:400px; height:40px; display:inline-block">Tarea</div>
                                    <div class="btn-default active col-md-1"
                                        style="width:250px; height:40px; display:inline-block">Responsable</div>
                                    <div class="btn-default active col-md-1"
                                        style="width:100px; height:40px; display:inline-block">Permite anular</div>
                                </div>

                                <div id="contenedortareas">
                                </div>

                                <div id="modalEncuesta" class="modalDialog" style="display:none;">
                                    <div class="modal-content" style="width:80%; height:80%; overflow:auto;">
                                        <div class="modal-header">
                                            <a onclick="cerrarModalEncuesta()" title="Cerrar" class="close">X</a>
                                            <h4 class="modal-title">Crear o modificar formulario de encuesta</h4>
                                        </div>
                                        <div class="modal-body" style="width:100%; height:100%; ">

                                            <div style="width: 1300px; display: inline-block;">
                                                <div class="btn-default active col-md-1"
                                                    onclick="encuesta.agregarCampos(valorEncuesta,'A')"
                                                    style="width:40px; height:40px; cursor:pointer; display:inline-block">
                                                    <span class="glyphicon glyphicon-plus"></span>
                                                </div>
                                                <div class="btn-default active col-md-1"
                                                    style="width:700px; height:40px; display:inline-block">Pregunta</div>
                                            </div>

                                            <div id="contenedorencuestas">
                                            </div>

                                            {!! Form::hidden('registroTareaFlujo', null, ['id' => 'registroTareaFlujo']) !!}
                                            {!! Form::button('Adicionar', ['class' => 'btn btn-success', 'onclick' => 'setDataJsonFlujoTarea()']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @if (isset($flujo))
        @if (isset($_GET['accion']) and $_GET['accion'] == 'destroy')
            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
        @else
            {!! Form::submit('Modificar', ['class' => 'btn btn-success', 'onclick' => "validarFormulario(event,'flujo')"]) !!}
        @endif
    @else
        {!! Form::submit('Adicionar', ['class' => 'btn btn-success', 'onclick' => "validarFormulario(event,'flujo')"]) !!}
    @endif

    {!! Form::close() !!}

    <script>
        var idUsuario = '<?php echo isset($idUsuario) ? $idUsuario : ''; ?>';
        var nombreUsuario = '<?php echo isset($nombreUsuario) ? $nombreUsuario : ''; ?>';

        var usuario = [JSON.parse(idUsuario), JSON.parse(nombreUsuario)];

        var abrirModalEncuesta = ['onclick', 'modalEncuesta(this.id)'];

        var tareas = '<?php echo isset($flujotarea) ? json_encode($flujotarea) : 0; ?>';
        tareas = (tareas != '' ? JSON.parse(tareas) : '');
        var valorTarea = ['', '', '', ''];

        var encuestas = '0';
        encuestas = (encuestas != '' ? JSON.parse(encuestas) : '');
        var valorEncuesta = [''];
    </script>

@stop
