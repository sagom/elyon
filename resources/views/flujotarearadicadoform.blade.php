@extends('layouts.modal')

@section('content')
    @include('alerts.request')

    {!! Html::script('js/flujotarearadicado.js') !!}
    {!! Form::model($flujotarearadicado, ['method' => 'PUT', 'id' => 'form-flujotarearadicado']) !!}

    <fieldset>
        <legend>{{ $flujotarearadicado->nombreFlujoTarea }}</legend>
        <input type="hidden" id="token" value="{{ csrf_token() }}" />
        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('nombreFlujo', 'Flujo', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!! Form::hidden('idFlujoTareaRadicado', null, ['id' => 'idFlujoTareaRadicado']) !!}
                    {!! Form::hidden('idFlujo', null, ['id' => 'idFlujo']) !!}
                    {!! Form::hidden('tipoRadicado', null, ['id' => 'tipoRadicado']) !!}
                    {!! Form::hidden('idRadicado', null, ['id' => 'idRadicado']) !!}
                    {!! Form::hidden('idPublicacion', null, ['id' => 'idPublicacion']) !!}
                    {!! Form::text('nombreFlujo', null, ['class' => 'form-control', 'readonly']) !!}
                    {!! Form::hidden('encuestaFlujoTarea', null, [
                        'class' => 'form-control',
                        'readonly',
                        'id' => 'encuestaFlujoTarea',
                    ]) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('fechaEjecucionFlujoTareaRadicado', 'Fecha', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    {!! Form::text('fechaEjecucionFlujoTareaRadicado', date('Y-m-d H:i:s'), ['class' => 'form-control', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('nombreDocumento', 'Documento', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!! Form::text('nombreDocumento', null, ['class' => 'form-control', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('rutaImagenPublicacionVersion', 'Adjunto', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file"></i>
                    </span>
                    <iframe style="width:100%; height:550px; " id="iframeImagenRadicado" src=""></iframe>
                </div>
            </div>
        </div>

        <div id="modalEncuesta" class="modalDialog" style="display:none;">
            <div class="modal-content" style="width:80%; height:80%; overflow:auto;">
                <div class="modal-header">
                    <a onclick="cerrarModalEncuesta()" title="Cerrar" class="close">X</a>
                    <h4 class="modal-title">Diligenciar encuesta</h4>
                </div>
                <div class="modal-body" style="width:100%; height:100%; ">

                    <div class="div-responsive-seis div-agrupador">
                        <div class="div-responsive-doce">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-file"></i>
                                </span>
                                <iframe style="width:100%; height:550px; " id="iframeImagenRadicadoModal"
                                    src=""></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="div-responsive-seis div-agrupador">
                        <div style="width: 1050px; height:550px; overflow:auto;" class="panel panel-success">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    Encuesta
                                </h4>
                            </div>
                            <div class="panel-body">
                                <div style="display: inline-block;">
                                    <div class="btn-default active col-md-1"
                                        style="width:400px; height:40px; display:inline-block">
                                        Pregunta</div>
                                    <div class="btn-default active col-md-1"
                                        style="width:200px; height:40px; display:inline-block">
                                        Respuesta</div>
                                    <div class="btn-default active col-md-1"
                                        style="width:400px; height:40px; display:inline-block">
                                        Observación</div>
                                </div>

                                <div id="contenedorencuestas">
                                </div>

                                {!! Form::button('Adicionar', ['class' => 'btn btn-success', 'onclick' => 'setDataJsonFlujoTarea()']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @foreach ($flujotarearadicadocomentario as $key => $comentario)
            <div class="div-responsive-doce div-agrupador">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            [{{ $comentario->estadoFlujoTareaRadicadoComentario }}]
                            {{ $comentario->nombreFlujoTarea }}:
                            {{ $comentario->name }} - {{ $comentario->fechaFlujoTareaRadicadoComentario }}
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group" id='test'>
                            {!! Form::text(
                                'observacionFlujoTareaRadicadoComentarioHistorico',
                                $comentario->observacionFlujoTareaRadicadoComentario,
                                ['class' => 'form-control', 'id' => 'observacionFlujoTareaRadicadoComentarioHistorico', 'readonly'],
                            ) !!}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        @if ($tipoEjecucion == 'e')
            <div class="div-responsive-doce div-agrupador">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            {{ $flujotarearadicado->nombreFlujoTarea }}: {{ $flujotarearadicado->name }} -
                            <?php echo date('Y-m-d H:i:s'); ?>
                            {!! Form::button('', ['class' => 'btn btn-info fa fa-edit', 'onclick' => 'modalEncuesta()']) !!}
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group" id='test'>
                            {!! Form::text('observacionFlujoTareaRadicadoComentario', null, [
                                'class' => 'form-control',
                                'id' => 'observacionFlujoTareaRadicadoComentario',
                            ]) !!}
                        </div>
                        {!! Form::hidden('respuestaFlujoTareaRadicadoComentario', null, [
                            'class' => 'form-control',
                            'readonly',
                            'id' => 'respuestaFlujoTareaRadicadoComentario',
                        ]) !!}
                        {!! Form::hidden('rechazarFlujoRadicado', null, [
                            'class' => 'form-control',
                            'readonly',
                            'id' => 'rechazarFlujoRadicado',
                        ]) !!}
                    </div>
                </div>
            </div>
            {!! Form::button('Aceptar', [
                'class' => 'btn btn-success',
                'onclick' => 'aceptarTarea()',
                'id' => 'aceptarFlujoRadicado',
            ]) !!}
            {!! Form::button('Rechazar', ['class' => 'btn btn-danger', 'onclick' => 'rechazarTarea()']) !!}
            {!! Form::button('Adicionar páginas', ['class' => 'btn btn-info', 'onclick' => 'cargarVersionPublicacion()']) !!}
        @else
            {!! Form::button('Cerrar', ['class' => 'btn btn-success', 'onclick' => 'cerrarModal()']) !!}
            {!! Form::button('Adicionar páginas', ['class' => 'btn btn-info', 'onclick' => 'cargarVersionPublicacion()']) !!}
        @endif

    </fieldset>

    {!! Form::close() !!}

    <script>
        $(document).ready(function() {
            var iframe = $("#iframeImagenRadicado");
            url = 'http://' + location.host + '/<?php echo $flujotarearadicado->rutaImagenPublicacionVersion; ?>';
            iframe.attr('src', '');
            if (iframe.length) {
                iframe.attr('src', url);
            }
        });

        $(document).ready(function() {
            var iframe = $("#iframeImagenRadicadoModal");
            url = 'http://' + location.host + '/<?php echo $flujotarearadicado->rutaImagenPublicacionVersion; ?>';
            iframe.attr('src', '');
            if (iframe.length) {
                iframe.attr('src', url);
            }
        });

        function aceptarTarea() {
            observacion = $("#observacionFlujoTareaRadicadoComentario").val();
            respuesta = $("#respuestaFlujoTareaRadicadoComentario").val();
            idFlujoTareaRadicado = $("#idFlujoTareaRadicado").val();
            idFlujo = $("#idFlujo").val();
            tipoRadicado = $("#tipoRadicado").val();
            idRadicado = $("#idRadicado").val();

            var token = document.getElementById('token').value;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                dataType: "json",
                data: {
                    'observacion': observacion,
                    'respuesta': respuesta,
                    'idFlujoTareaRadicado': idFlujoTareaRadicado,
                    'idFlujo': idFlujo,
                    'tipoRadicado': tipoRadicado,
                    'idRadicado': idRadicado
                },
                url: 'http://' + location.host + '/aceptarTarea',
                type: 'post',
                async: false,
                beforeSend: function() {
                    abrirDistractor();
                },
                success: function(respuesta) {
                    mensajeAlerta("guardar", "¡Bien!", respuesta, "green", "fa fa-exclamation-circle", "10000");
                    cerrarDistractor();
                    $("#modalTarea").css('display', 'none');
                    var timpoEspera = 5000;
                    setTimeout(function() {
                        window.parent.location.reload();
                    }, timpoEspera);
                },
                error: function() {
                    cerrarDistractor();
                    mensajeAlerta("guardar", "Error", "Ha ocurrido un problema aceptando la tarea", "red",
                        "fa fa-times", "10000");
                }
            });
        }

        function rechazarTarea() {
            observacion = $("#observacionFlujoTareaRadicadoComentario").val();
            respuesta = $("#respuestaFlujoTareaRadicadoComentario").val();
            rechazar = $("#rechazarFlujoRadicado").val() != '' ? $("#rechazarFlujoRadicado").val() : false;
            idFlujoTareaRadicado = $("#idFlujoTareaRadicado").val();
            idFlujo = $("#idFlujo").val();
            tipoRadicado = $("#tipoRadicado").val();
            idRadicado = $("#idRadicado").val();

            var token = document.getElementById('token').value;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                dataType: "json",
                data: {
                    'observacion': observacion,
                    'respuesta': respuesta,
                    'rechazar': rechazar,
                    'idFlujoTareaRadicado': idFlujoTareaRadicado,
                    'idFlujo': idFlujo,
                    'tipoRadicado': tipoRadicado,
                    'idRadicado': idRadicado
                },
                url: 'http://' + location.host + '/rechazarTarea',
                type: 'post',
                async: false,
                beforeSend: function() {
                    abrirDistractor();
                },
                success: function(respuesta) {
                    mensajeAlerta("guardar", "¡Alerta!", respuesta, "orange", "fa fa-exclamation-circle",
                        "10000");
                    cerrarDistractor();
                    $("#modalTarea").css('display', 'none');
                    var timpoEspera = 5000;
                    setTimeout(function() {
                        window.parent.location.reload();
                    }, timpoEspera);
                },
                error: function() {
                    cerrarDistractor();
                    mensajeAlerta("guardar", "Error", "Ha ocurrido un problema rechazando la tarea", "red",
                        "fa fa-times", "10000");
                }
            });
        }

        var respuestas = [
            ['Si', 'No', 'N/A'],
            ['Si', 'No', 'N/A']
        ];

        // var encuestas = '0';
        // encuestas = (encuestas != '' ? JSON.parse(encuestas) : '');
        // var valorEncuesta = [''];
    </script>

@stop
