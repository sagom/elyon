@extends('layouts.vista')

@section('content')
    {!!Html::script('js/radicadoinforme.js'); !!} 
    @include('alerts.request')

	{!!Form::open(['method'=>'POST', 'id' => 'form-radicadoinforme'])!!}

    <fieldset>
        <legend>Informe de radicado</legend>
        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('tipoRadicado', 'Tipo', array('class' => 'label-responsive-tres')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!!Form::select('tipoRadicado',['Correspondencia recibida' => 'Correspondencia recibida', 'Factura' => 'Factura', 'Correspondencia enviada' => 'Correspondencia enviada'],null,['class' => 'form-control','placeholder'=>'- Seleccione un tipo -']) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('Dependencia_idDependencia', 'Dependencia', array('class' => 'label-responsive-tres')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::select('Dependencia_idDependencia',$dependencia, null,["class" => "select form-control", "placeholder" =>"- Seleccione una dependencia -"])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('fechaRadicadoInicio', 'Generados desde', array('class' => 'label-responsive-tres')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    {!!Form::date('fechaRadicadoInicio',null,['class'=>'form-control','placeholder'=>'Inserte una fecha inicial'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('fechaRadicadoFin', 'Generados hasta', array('class' => 'label-responsive-tres')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    {!!Form::date('fechaRadicadoFin',null,['class'=>'form-control','placeholder'=>'Inserte una fecha final'])!!}
                </div>
            </div>
        </div>

    </fieldset>
    
      {!!Form::button('Consultar',["class"=>"btn btn-success", "onclick"=>"consultarRadicado(1)"])!!} 
      {!!Form::button('Descargar',["class"=>"btn btn-success", "onclick"=>"consultarRadicado(2)"])!!}
     
    {!!Form::close()!!}

@stop