@extends('layouts.vista')

@section('content')
    {!! Html::script('js/flujoinforme.js') !!}
    @include('alerts.request')

    {!! Form::open(['method' => 'POST', 'id' => 'form-flujoinforme']) !!}

    <fieldset>
        <legend>Informe de flujo</legend>
        <div class="div-responsive-seis div-agrupador">
            {!! Form::label('estadoFlujo', 'Estado', ['class' => 'label-responsive-tres']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!! Form::select(
                        'estadoFlujo',
                        [
                            'SinAsignar' => 'Sin asignar',
                            'EnProceso' => 'En proceso',
                            'Terminada' => 'Terminada',
                            'Rechazado' => 'Rechazado',
                            'Anulado' => 'Anulado',
                        ],
                        null,
                        [
                            'class' => 'form-control',
                            'placeholder' => '- Seleccione un estado del flujo -',
                        ],
                    ) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!! Form::label('Flujo_idFlujo', 'Flujo', ['class' => 'label-responsive-tres']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!! Form::select('Flujo_idFlujo', $flujo, null, [
                        'class' => 'select form-control',
                        'placeholder' => '- Seleccione un flujo -',
                    ]) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!! Form::label('fechaFlujoInicio', 'Generados desde', ['class' => 'label-responsive-tres']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    {!! Form::date('fechaFlujoInicio', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Inserte una fecha inicial',
                    ]) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!! Form::label('fechaFlujoFin', 'Generados hasta', ['class' => 'label-responsive-tres']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    {!! Form::date('fechaFlujoFin', null, ['class' => 'form-control', 'placeholder' => 'Inserte una fecha final']) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!! Form::label('estadoFlujoComentario', 'Estado tarea', ['class' => 'label-responsive-tres']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!! Form::select(
                        'estadoFlujoComentario',
                        ['Aceptado' => 'Aceptado', 'Rechazado' => 'Rechazado', 'Anulado' => 'Anulado'],
                        null,
                        [
                            'class' => 'form-control',
                            'placeholder' => '- Seleccione un estado del proceso -',
                        ],
                    ) !!}
                </div>
            </div>
        </div>

    </fieldset>

    {!! Form::button('Consultar', ['class' => 'btn btn-success', 'onclick' => 'consultarFlujo(1)']) !!}
    {!! Form::button('Descargar', ['class' => 'btn btn-success', 'onclick' => 'consultarFlujo(2)']) !!}

    {!! Form::close() !!}

@stop
