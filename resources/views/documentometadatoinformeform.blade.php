@extends('layouts.vista')

@section('content')
    {!!Html::script('js/documentometadatoinforme.js'); !!}
    @include('alerts.request')

	{!!Form::open(['method'=>'POST', 'id' => 'form-documentometadatoinforme'])!!}

    <fieldset>
        <legend>Informe de documentos con metadato</legend>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('Documento_idDocumento', 'Documento', array('class' => 'label-responsive-tres')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::select('Documento_idDocumento',$documento, null,["class" => "select form-control", "placeholder" =>"- Seleccione un documento -"])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('fechaInicioPublicacion', 'Publicados desde', array('class' => 'label-responsive-tres')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    {!!Form::date('fechaInicioPublicacion',null,['class'=>'form-control','placeholder'=>'Inserte una fecha inicial'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            {!!Form::label('fechaFinPublicacion', 'Publicados hasta', array('class' => 'label-responsive-tres')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    {!!Form::date('fechaFinPublicacion',null,['class'=>'form-control','placeholder'=>'Inserte una fecha final'])!!}
                </div>
            </div>
        </div>

    </fieldset>

      {!!Form::button('Consultar',["class"=>"btn btn-success", "onclick"=>"consultarDocumentoMetadato(1)"])!!}
      {!!Form::button('Descargar',["class"=>"btn btn-success", "onclick"=>"consultarDocumentoMetadato(2)"])!!}

    {!!Form::close()!!}

@stop