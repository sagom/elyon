@extends('layouts.vista')

@section('content')
    @include('alerts.request')
    {!!Html::script('js/radicadosalida.js'); !!}
    @if(isset($radicadosalida))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($radicadosalida,['route'=>['radicadosalida.destroy',$radicadosalida->idRadicadoSalida],'method'=>'DELETE', 'id' => 'form-radicadosalida'])!!}
		@else
			{!!Form::model($radicadosalida,['route'=>['radicadosalida.update',$radicadosalida->idRadicadoSalida],'method'=>'PUT', 'id' => 'form-radicadosalida'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'radicadosalida.store','method'=>'POST', 'id' => 'form-radicadosalida'])!!}
    @endif

    <fieldset>
        <legend>Información de correspondencia enviada</legend>
    
        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('numeroRadicadoSalida', 'Número', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                    <i class="fas fa-sort-numeric-down"></i>
                    </span>
                    {!!Form::text('numeroRadicadoSalida',(isset($radicadosalida) ? $radicadosalida->numeroRadicadoSalida : 'Automático'),['class'=>'form-control','placeholder'=>'Automático','readonly'])!!}
                    {!!Form::hidden('idRadicadoSalida', null, array('id' => 'idRadicadoSalida')) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('fechaRadicadoSalida', 'Fecha', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                    <i class="far fa-calendar-alt"></i>
                    </span>
                    {!!Form::text('fechaRadicadoSalida',(isset($radicadosalida) ? $radicadosalida->fechaRadicadoSalida : date('Y-m-d H:i:s')),['class'=>'form-control','placeholder'=>'Automático','readonly'])!!}
                </div>
            </div>
        </div>

    </fieldset>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#divRem">Remitente</a>
            </h4>
        </div>
        <div id="divRem" class="panel-collapse collapse">
            <div class="panel-body">

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('Users_idRadicadoSalida', 'Usuario', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </span>
                            {!!Form::select('Users_idRadicadoSalida',$user, (isset($radicadosalida) ? $radicadosalida->Users_idRadicadoSalida : null),["class" => "select form-control", "placeholder" =>'Seleccione el usuario'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('Dependencia_idRadicadoSalida', 'Dependencia', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fas fa-clipboard-list"></i>
                            </span>
                            {!!Form::select('Dependencia_idRadicadoSalida',$dependencia, (isset($radicadosalida) ? $radicadosalida->Dependencia_idRadicadoSalida : null),["class" => "select form-control", "placeholder" =>'Seleccione la dependencia'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('fechaDocumentoRadicadoSalida', 'Fecha', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="far fa-calendar-alt"></i>
                            </span>
                        {!!Form::date('fechaDocumentoRadicadoSalida',null,['class'=>'form-control','placeholder'=>'Año-Mes-Día'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('asuntoRadicadoSalida', 'Asunto', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fas fa-file-import"></i>
                            </span>
                            {!!Form::text('asuntoRadicadoSalida',null,['class'=>'form-control','placeholder'=>'Ingrese el asunto'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('observacionRadicadoSalida', 'Observación', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fas fa-align-left"></i>
                            </span>
                            {!!Form::text('observacionRadicadoSalida',null,['class'=>'form-control','placeholder'=>'Ingrese las observacion'])!!}
                        </div>
                    </div>
                </div>
        
            </div>
        </div>
    </div>

        <div class="panel panel-success">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#divdes">Destinatario</a>
                </h4>
            </div>
            <div id="divdes" class="panel-collapse collapse">
                <div class="panel-body">

                    <div class="div-responsive-doce div-agrupador">
                        {!!Form::label('nombreDestinatarioRadicadoSalida', 'Nombre', array('class' => 'label-responsive-dos')) !!}
                        <div class="div-input-responsive">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-file-signature"></i>
                                </span>
                                {!!Form::text('nombreDestinatarioRadicadoSalida',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre'])!!}
                            </div>
                        </div>
                    </div>

                    <div class="div-responsive-doce div-agrupador">
                        {!!Form::label('identificacionDestinatarioRadicadoSalida', 'Identificación', array('class' => 'label-responsive-dos')) !!}
                        <div class="div-input-responsive">
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fas fa-id-card"></i>
                                </span>
                                {!!Form::text('identificacionDestinatarioRadicadoSalida',null,['class'=>'form-control','placeholder'=>'Ingrese el número de identificación'])!!}
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    

    @if(isset($radicadosalida))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'radicadosalida')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'radicadosalida')"])!!}
    @endif 
     
    {!!Form::close()!!}

@stop