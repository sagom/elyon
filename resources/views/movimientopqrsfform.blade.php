@extends('layouts.menu')

@section('content')
    @include('alerts.request')
    {!!Html::script('js/movimientopqrsf.js'); !!} 
    @if(isset($movimientopqrsf))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($movimientopqrsf,['route'=>['movimientopqrsf.destroy',$movimientopqrsf->idMovimientoPQRSF],'method'=>'DELETE', 'id' => 'form-movimientopqrsf'])!!}
		@else
			{!!Form::model($movimientopqrsf,['route'=>['movimientopqrsf.update',$movimientopqrsf->idMovimientoPQRSF],'method'=>'PUT', 'id' => 'form-movimientopqrsf', 'files' => 'true'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'movimientopqrsf.store','method'=>'POST', 'id' => 'form-movimientopqrsf', 'files' => 'true'])!!}
    @endif

    <script>
        $(document).ready(function(){ 
            movimientopqrsf = '<?php echo isset($movimientopqrsf) ? $movimientopqrsf : "";?>';

            if(movimientopqrsf != '')
                ejecutarRespuesta();
        });
    </script>

    <fieldset>
        <legend>PQRSF</legend>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('fechaSolicitudMovimientoPQRSF', 'Fecha', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    {!!Form::text('fechaSolicitudMovimientoPQRSF',(isset($movimientopqrsf) ? $movimientopqrsf->fechaSolicitudMovimientoPQRSF : date('Y-m-d H:i:s')),['class'=>'form-control','readonly'])!!}
                    {!!Form::hidden('idMovimientoPQRSF', null, array('id' => 'idMovimientoPQRSF')) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('nombreSolicitanteMovimientoPQRSF', 'Nombre', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::text('nombreSolicitanteMovimientoPQRSF',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('telefonoSolicitanteMovimientoPQRSF', 'Teléfono', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                    <i class="fas fa-phone-square"></i>
                    </span>
                    {!!Form::number('telefonoSolicitanteMovimientoPQRSF',null,['class'=>'form-control','placeholder'=>'Ingrese el número de teléfono'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('movilSolicitanteMovimientoPQRSF', 'Móvil', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                    <i class="fas fa-mobile-alt"></i>
                    </span>
                    {!!Form::number('movilSolicitanteMovimientoPQRSF',null,['class'=>'form-control','placeholder'=>'Ingrese el número móvil'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('correoSolicitanteMovimientoPQRSF', 'Correo', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-at"></i>
                    </span>
                    {!!Form::text('correoSolicitanteMovimientoPQRSF',null,['class'=>'form-control','placeholder'=>'Ingrese el correo'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('tipoMovimientoPQRSF', 'Tipo', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-window-restore"></i>
                    </span>
                    {!!Form::select('tipoMovimientoPQRSF',['Peticion'=>'Petición','Queja'=>'Queja','Reclamo'=>'Reclamo','Sugerencia'=>'Sugerencia','Felicitaciones'=>'Felicitaciones'],null,['class' => 'form-control','placeholder'=>'Seleccione un registro de la lista']) !!}                
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('mensajeMovimientoPQRSF', 'Mensaje', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::textarea('mensajeMovimientoPQRSF',null,['class'=>'form-control','placeholder'=>'Ingrese el mensaje'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('autorizacionMovimientoPQRSF', 'Autorización', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!! Form::checkbox('autorizacionMovimientoPQRSF', 1, null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>

        <div id="divrespuesta" class="div-responsive-doce div-agrupador" style="display:none">
            {!!Form::label('respuestaMovimientoPQRSF', 'Respuesta', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::textarea('respuestaMovimientoPQRSF',null,['class'=>'form-control','placeholder'=>'Ingrese la respuesta'])!!}
                </div>
            </div>
        </div>

        <div id="divfecharespuesta" class="div-responsive-doce div-agrupador" style="display:none">
            {!!Form::label('fechaRespuestaMovimientoPQRSF', 'Fecha', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    {!!Form::text('fechaRespuestaMovimientoPQRSF', date('Y-m-d H:i:s'),['class'=>'form-control','readonly'])!!}
                </div>
            </div>
        </div>

        <div id="divusuariorespuesta" class="div-responsive-doce div-agrupador" style="display:none">
            {!!Form::label('usuarioRespuesta', 'Usuario', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-user"></i>
                    </span>
                    {!!Form::text('usuarioRespuesta', \Session::get('nombreUsuario'),['class'=>'form-control','readonly'])!!}
                </div>
            </div>
        </div>

        <div id="divadjuntorespuesta" class="div-responsive-doce div-agrupador" style="display:none">
            {!!Form::label('rutaAdjuntoRespuestaMovimientoPQRSF', 'Adjunto', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file"></i>
                    </span>
                    <input id="rutaAdjuntoRespuestaMovimientoPQRSF" name="rutaAdjuntoRespuestaMovimientoPQRSF" type="file" >
                </div>
            </div>
        </div>

    </fieldset>
    

    @if(isset($movimientopqrsf))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'movimientopqrsf')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'movimientopqrsf')"])!!}
    @endif 
     
    {!!Form::close()!!}

<script>

    $('#rutaAdjuntoRespuestaMovimientoPQRSF').fileinput({
        language: 'es',
        uploadUrl: '#',
        allowedFileExtensions : ['jpg','png','gif','pdf'],
            initialPreview: [
            '<?php if(isset($movimientopqrsf->rutaAdjuntoRespuestaMovimientoPQRSF))
                    echo Html::image($movimientopqrsf->rutaAdjuntoRespuestaMovimientoPQRSF,"Imagen no encontrada",array("style"=>"width:148px;height:158px;"));
                                        ;?>'
        ],
        dropZoneTitle: 'Seleccione su foto',
        removeLabel: '',
        uploadLabel: '',
        browseLabel: '',
        uploadClass: "",
        uploadLabel: "",
        uploadIcon: "",
    });

</script>

@stop