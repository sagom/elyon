@extends('layouts.grid') 
@section('content')
{!!Form::model($datos)!!}
{!!Html::script('js/tarea.js'); !!} 

<?php
    $visible = '';

    if(isset($datos[0])) 
    {
        $dato = get_object_vars($datos[0]);
        if ($dato['adicionarRolOpcion'] == 1) 
            $visible = 'inline-block;'; 
        else
            $visible = 'none;';
    }
    else
        $visible = 'none;';

    $tipo = (isset($_GET['tipo']) ? $_GET['tipo'] : '');
?>

<script type="text/javascript">
    var tipo = "<?php echo (isset($tipo) ? $tipo : '');?>";
    $(document).ready( function () 
    {
        configurarGrid('tarea',"{!! URL::to ('/tareadata?tipo="+tipo+"')!!}");
    }); 
</script>

<legend>Tareas</legend>
<a style="color:green" href="#" onclick="cambiarEstado('');" title="Todas">
    <i class="fas fa-filter fa-2x"></i>
</a>
<a style="color:green" href="#" onclick="cambiarEstado('Nuevo');" title="Mostrar Nuevas">
    <i class="far fa-file-alt fa-2x"></i>
</a>
<a style="color:green" href="#" onclick="cambiarEstado('En proceso');" title="Mostrar En Proceso">
    <i class="fas fa-file-signature fa-2x"></i>
</a>
<a style="color:green" href="#" onclick="cambiarEstado('Terminado');" title="Terminadas">
    <i class="fas fa-check-circle fa-2x"></i>
</a>
<a style="color:green" href="#" onclick="cambiarEstado('Rechazado');" title="Rechazadas">
    <i class="fas fa-times-circle fa-2x"></i>
</a>
<a style="float:right; color:green" href="#" onclick="mostrarDashboard();" title="Dashboard">
    <i class="fas fa-chart-line fa-2x"></i>
</a>
<table id="tarea" class="table table-striped table-bordered nowrap" style="width:100%">
    <thead class="btn-success">
        <tr>
            <th style="width:60px;padding: 1px 8px;" data-orderable="false">
                <a href="tarea/create"><span style="color:white; display: <?php echo $visible ?>" class="glyphicon glyphicon-plus"></span></a>
            </th>
            <th>ID</th>
            <th>Número</th>
            <th>Elaboración</th>
            <th>Asunto</th>
            <th>Prioridad</th>
            <th>Responsable</th>
            <th>Clasificación</th>
            <th>Estado</th>
            <th>Vencimiento</th>
            <th>Solicitante</th>
        </tr>
    </thead>
    <tfoot class="btn-default active">
        <tr>
            <th>
                &nbsp;
            </th>
            <th>ID</th>
            <th>Número</th>
            <th>Elaboración</th>
            <th>Asunto</th>
            <th>Prioridad</th>
            <th>Responsable</th>
            <th>Clasificación</th>
            <th>Estado</th>
            <th>Vencimiento</th>
            <th>Solicitante</th>     
        </tr>
    </tfoot>
</table>

{!!Form::close()!!}
@stop