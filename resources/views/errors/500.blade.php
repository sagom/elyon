<!DOCTYPE html>
<html>
    <head>
        <title>Error interno del servidor</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="error mx-auto" data-text="500">500</div>
                <p class="lead text-gray-800 mb-5">Error interno del servidor</p>
                <div class="title"><a href="{{URL::to('/index')}}">&larr; Ir al inicio</a></div>
            </div>
        </div>
    </body>
</html>
