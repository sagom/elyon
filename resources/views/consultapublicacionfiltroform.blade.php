@extends('layouts.vista')

@section('content')
    {!!Html::style('css/consultapublicacion.css'); !!} 
    {!!Html::script('js/consultapublicacion.js'); !!} 
    @include('alerts.request')
	{!!Form::open(['route'=>'publicacionce.store','method'=>'POST', 'id' => 'form-publicacion'])!!}

    <fieldset>
        <legend>Filtro de Consulta para Publicaciones</legend>
        
    </fieldset>

    <div class="panel panel-success">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#divDependencia">Dependencias</a>
                </h4>
            </div>
            <div id="divDependencia" class="panel-collapse">
                
                <div class="panel-body">
                    <div class="form-group" id='test'>
                        <div class="col-sm-12">
                            <div class="row show-grid">
                                <div style="overflow:auto; height:150px;">
                                    <div style="width: 1200px; display: inline-block;">
                                        <div class="btn-default active col-md-1" onclick="dependencia.agregarCampos(valorDependencia,'A')" style="width:40px; height:40px; cursor:pointer; display:inline-block">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </div>
                                        <div class="btn-default active col-md-1" style="width:700px; height:40px; display:inline-block">Dependencia</div>

                                        <div id="contenedordependencia">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="panel panel-success">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#divDocumento">Documentos</a>
                </h4>
            </div>
            <div id="divDocumento" class="panel-collapse">
                
                <div class="panel-body">
                    <div class="form-group" id='test'>
                        <div class="col-sm-12">
                            <div class="row show-grid">
                                <div style="overflow:auto; height:150px;">
                                    <div style="width: 1200px; display: inline-block;">
                                        <div class="btn-default active col-md-1" onclick="documento.agregarCampos(valorDocumento,'A')" style="width:40px; height:40px; cursor:pointer; display:inline-block">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </div>
                                        <div class="btn-default active col-md-1" style="width:700px; height:40px; display:inline-block">Documento</div>

                                        <div id="contenedordocumento">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="panel panel-success">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#divCondicion">Condición</a>
                </h4>
            </div>
            <div id="divCondicion" class="panel-collapse">
                
                <div class="panel-body">
                    <div class="form-group" id='test'>
                        <div class="col-sm-12">
                            <div class="row show-grid">
                                <div style="overflow:auto; height:150px;">
                                    <div style="width: 1200px; display: inline-block;">
                                        <div class="btn-default active col-md-1" onclick="metadato.agregarCampos(valorMetadato,'A')" style="width:40px; height:40px; cursor:pointer; display:inline-block">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </div>
                                        <div class="btn-default active col-md-1" style="width:120px; height:40px; display:inline-block">Agrupador</div>
                                        <div class="btn-default active col-md-1" style="width:300px; height:40px; display:inline-block">Campo</div>
                                        <div class="btn-default active col-md-1" style="width:190px; height:40px; display:inline-block">Operador</div>
                                        <div class="btn-default active col-md-1" style="width:220px; height:40px; display:inline-block">Valor</div>
                                        <div class="btn-default active col-md-1" style="width:130px; height:40px; display:inline-block">Agrupador</div>
                                        <div class="btn-default active col-md-1" style="width:130px; height:40px; display:inline-block">Conector</div>

                                        <div id="contenedormetadato">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!!Form::button('Filtrar',["class"=>"btn btn-success", 'onclick' => 'ejecutarConsulta("Archivo");', 'id'=>'consulta'])!!}
                    {!!Form::button('Limpiar',["class"=>"btn btn-danger", 'onclick' => 'limpiarCondicion();'])!!}
                    {!! Form::hidden('condicionMetadato', null, array('id' => 'condicionMetadato')) !!}
                </div>

            </div>
        </div>

    <script>

        var idDependencia = '<?php echo isset($idDependencia) ? $idDependencia : "";?>';
        var nombreDependencia = '<?php echo isset($nombreDependencia) ? $nombreDependencia : "";?>';
        var datodependencia = [JSON.parse(idDependencia), JSON.parse(nombreDependencia)];
        var valorDependencia = [0];

        var idDocumento = '<?php echo isset($idDocumento) ? $idDocumento : "";?>';
        var nombreDocumento = '<?php echo isset($nombreDocumento) ? $nombreDocumento : "";?>';
        var datodocumento = [JSON.parse(idDocumento), JSON.parse(nombreDocumento)];
        var valorDocumento = [0];

        var idMetadato = '<?php echo isset($idMetadato) ? $idMetadato : "";?>';
        var nombreMetadato = '<?php echo isset($nombreMetadato) ? $nombreMetadato : "";?>';
        var parentesisAbre = [["", "(", "((", "(((", "(((("], ["", "(", "((", "(((", "(((("]];
        var parentesisCierra = [["", ")", "))", ")))", "))))"], ["", ")", "))", ")))", "))))"]];
        var operador = [["=", ">", ">=", "<", "<=", "like"],
                        ["Igual a", "Mayor que", "Mayor o igual", "Menor que", "Menor o igual que", "Contiene"]];
        var datometadato = [JSON.parse(idMetadato), JSON.parse(nombreMetadato)];
        var valorMetadato = ['','','','','',''];
        var conector =  [["AND", "OR"], ["Y", "O"]];

    </script>

{!!Form::close()!!}
@stop