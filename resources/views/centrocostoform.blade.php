@extends('layouts.vista')

@section('content')
    @include('alerts.request')

    @if(isset($centrocosto))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($centrocosto,['route'=>['centrocosto.destroy',$centrocosto->idCentroCosto],'method'=>'DELETE', 'id' => 'form-centrocosto'])!!}
		@else
			{!!Form::model($centrocosto,['route'=>['centrocosto.update',$centrocosto->idCentroCosto],'method'=>'PUT', 'id' => 'form-centrocosto'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'centrocosto.store','method'=>'POST', 'id' => 'form-centrocosto'])!!}
    @endif

    <fieldset>
        <legend>Información de Centros de costo</legend>
        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('codigoCentroCosto', 'Código', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!!Form::number('codigoCentroCosto',null,['class'=>'form-control','placeholder'=>'Ingrese el código'])!!}
                    {!!Form::hidden('idCentroCosto', null, array('id' => 'idCentroCosto')) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('nombreCentroCosto', 'Nombre', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::text('nombreCentroCosto',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre'])!!}
                </div>
            </div>
        </div>

    </fieldset>

    @if(isset($centrocosto))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'centrocosto')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'centrocosto')"])!!}
    @endif 
     
    {!!Form::close()!!}

@stop