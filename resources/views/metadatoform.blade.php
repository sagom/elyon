@extends('layouts.vista')

@section('content')
    {!! Html::script('js/metadato.js') !!}
    @include('alerts.request')

    @if (isset($metadato))
        @if (isset($_GET['accion']) and $_GET['accion'] == 'destroy')
            {!! Form::model($metadato, [
                'route' => ['metadato.destroy', $metadato->idMetadato],
                'method' => 'DELETE',
                'id' => 'form-metadato',
            ]) !!}
        @else
            {!! Form::model($metadato, [
                'route' => ['metadato.update', $metadato->idMetadato],
                'method' => 'PUT',
                'id' => 'form-metadato',
            ]) !!}
        @endif
    @else
        {!! Form::open(['route' => 'metadato.store', 'method' => 'POST', 'id' => 'form-metadato']) !!}
    @endif

    <fieldset>
        <legend>Información de Metadatos</legend>
        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('codigoMetadato', 'Código', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!! Form::hidden('idMetadato', null, ['id' => 'idMetadato']) !!}
                    {!! Form::text('codigoMetadato', null, ['class' => 'form-control', 'placeholder' => 'Ingrese el código']) !!}
                </div>
            </div>
        </div>


        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('nombreMetadato', 'Nombre', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!! Form::text('nombreMetadato', null, ['class' => 'form-control', 'placeholder' => 'Ingrese el nombre']) !!}
                </div>
            </div>
        </div>


        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('tipoMetadato', 'Tipo', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-window-restore"></i>
                    </span>
                    {!! Form::select(
                        'tipoMetadato',
                        [
                            'Texto' => 'Alfanumérico',
                            'Numero' => 'Número',
                            'Fecha' => 'Fecha',
                            'Hora' => 'Hora',
                            'Lista' => 'Lista',
                            'Editor' => 'Editor',
                        ],
                        null,
                        [
                            'class' => 'form-control',
                            'placeholder' => '- Seleccione un registro de la lista -',
                            'onchange' => 'activarOpciones(this.value)',
                        ],
                    ) !!}
                </div>
            </div>
        </div>


        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('opcionMetadato', 'Opción', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-list"></i>
                    </span>
                    {!! Form::text('opcionMetadato', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Separe las opciones por coma (,)',
                        'readonly',
                    ]) !!}
                </div>
            </div>
        </div>


        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('longitudMetadato', 'Longitud', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-ruler"></i>
                    </span>
                    {!! Form::number('longitudMetadato', null, ['class' => 'form-control', 'placeholder' => 'Ingrese la longitud']) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('informeFlujoMetadato', 'Informe flujo', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-check-square"></i>
                    </span>
                    {!! Form::checkbox('informeFlujoMetadato', 1, null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </fieldset>


    @if (isset($metadato))
        @if (isset($_GET['accion']) and $_GET['accion'] == 'destroy')
            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
        @else
            {!! Form::submit('Modificar', [
                'class' => 'btn btn-success',
                'onclick' => "validarFormulario(event,'metadato')",
            ]) !!}
        @endif
    @else
        {!! Form::submit('Adicionar', [
            'class' => 'btn btn-success',
            'onclick' => "validarFormulario(event,'metadato')",
        ]) !!}
    @endif

    {!! Form::close() !!}

@stop
