@extends('layouts.vista')

@section('content')
    {!! Html::script('js/documento.js') !!}
    @include('alerts.request')

    @if (isset($documento))
        @if (isset($_GET['accion']) and $_GET['accion'] == 'destroy')
            {!! Form::model($documento, [
                'route' => ['documento.destroy', $documento->idDocumento],
                'method' => 'DELETE',
                'id' => 'form-documento',
            ]) !!}
        @else
            {!! Form::model($documento, [
                'route' => ['documento.update', $documento->idDocumento],
                'method' => 'PUT',
                'id' => 'form-documento',
            ]) !!}
        @endif
    @else
        {!! Form::open(['route' => 'documento.store', 'method' => 'POST', 'id' => 'form-documento']) !!}
    @endif

    <fieldset>
        <legend>Información de Documento</legend>
        <input type="hidden" id="token" value="{{ csrf_token() }}" />
        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('codigoDocumento', 'Código', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!! Form::hidden('idDocumento', null, ['id' => 'idDocumento']) !!}
                    {!! Form::hidden('eliminarDocumentoMetadato', null, ['id' => 'eliminarDocumentoMetadato']) !!}
                    {!! Form::hidden('eliminarDocRol', null, ['id' => 'eliminarDocRol']) !!}
                    {!! Form::text('codigoDocumento', null, ['class' => 'form-control', 'placeholder' => 'Ingrese el código']) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('nombreDocumento', 'Nombre', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!! Form::text('nombreDocumento', null, ['class' => 'form-control', 'placeholder' => 'Ingrese el nombre']) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('directorioDocumento', 'Directorio', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-folder-open"></i>
                    </span>
                    {!! Form::text('directorioDocumento', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingrese el nombre del directorio',
                        'onkeyup' => 'replaceSpacesWithUnderLine(this.value, this.id)',
                        'id' => 'directorioDocumento',
                    ]) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('versionDocumento', 'Versión', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-check-square"></i>
                    </span>
                    {!! Form::checkbox('versionDocumento', 1, null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('radicadoDocumento', 'Genera radicado', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-check-square"></i>
                    </span>
                    {!! Form::checkbox('radicadoDocumento', 1, null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('generaMetadatoFlujoDocumento', 'Genera metadato', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-check-square"></i>
                    </span>
                    {!! Form::checkbox('generaMetadatoFlujoDocumento', 1, null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!! Form::label('tablaDocumento', 'Tabla a conectar', ['class' => 'label-responsive-dos']) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-cube"></i>
                    </span>
                    {!! Form::select('tablaDocumento', $tablas, isset($documento) ? $documento->tablaDocumento : null, [
                        'class' => 'select form-control',
                        'placeholder' => '- Seleccione la tabla a conectarse -',
                        'onchange' => 'consultarCamposTablaDocumento(this.value)',
                    ]) !!}
                </div>
            </div>
        </div>

    </fieldset>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#divMetadato">Indexación</a>
            </h4>
        </div>
        <div id="divMetadato" class="panel-collapse">
            <div class="panel-body">
                <div class="form-group" id='test'>
                    <div class="col-sm-12">
                        <div class="row show-grid">
                            <div style="overflow:auto; height:125px;">
                                <div style="width: 1300px; display: inline-block;">
                                    <div class="btn-default active col-md-1"
                                        onclick="metadato.agregarCampos(valorMetadato,'A')"
                                        style="width:40px; height:40px; cursor:pointer; display:inline-block">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </div>
                                    <div class="btn-default active col-md-1"
                                        style="width:100px; height:40px; display:inline-block">Orden</div>
                                    <div class="btn-default active col-md-1"
                                        style="width:270px; height:40px; display:inline-block">Metadato</div>
                                    <div class="btn-default active col-md-1"
                                        style="width:270px; height:40px; display:inline-block">Campo a conectar</div>
                                    <div class="btn-default active col-md-1"
                                        style="width:60px; height:40px; display:inline-block">Índice</div>
                                    <div id="contenedormetadatos">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#divDocRol">Permisos</a>
            </h4>
        </div>
        <div id="divDocRol" class="panel-collapse">

            <div class="panel-body">
                <div class="form-group" id='test'>
                    <div class="col-sm-12">
                        <div class="row show-grid">
                            <div style="overflow:auto; height:150px;">
                                <div style="width: 1300px; display: inline-block;">
                                    <div class="btn-default active col-md-1" onclick="docrol.agregarCampos(valorDocRol,'A')"
                                        style="width:40px; height:40px; cursor:pointer; display:inline-block">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </div>
                                    <div class="btn-default active col-md-1"
                                        style="width:600px; height:40px; display:inline-block">Rol</div>

                                    <div id="contenedordocrol">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @if (isset($documento))
        @if (isset($_GET['accion']) and $_GET['accion'] == 'destroy')
            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
        @else
            {!! Form::submit('Modificar', [
                'class' => 'btn btn-success',
                'onclick' => "validarFormulario(event,'documento')",
            ]) !!}
        @endif
    @else
        {!! Form::submit('Adicionar', [
            'class' => 'btn btn-success',
            'onclick' => "validarFormulario(event,'documento')",
        ]) !!}
    @endif

    {!! Form::close() !!}

    <script>
        var idMetadato = '<?php echo isset($idMetadato) ? $idMetadato : ''; ?>';
        var nombreMetadato = '<?php echo isset($nombreMetadato) ? $nombreMetadato : ''; ?>';

        var valormetadato = [JSON.parse(idMetadato), JSON.parse(nombreMetadato)];

        var metadatos = '<?php echo isset($documentometadato) ? json_encode($documentometadato) : 0; ?>';
        metadatos = (metadatos != '' ? JSON.parse(metadatos) : '');
        var valorMetadato = ['', 0, 0];

        documentorelacion = '<?php echo isset($documento) ? $documento->Documento_idDocumento : ''; ?>';

        if (documentorelacion != '' && documentorelacion == 1) {
            $("#directorioDocumento").attr('readonly', true);
        }

        var idRol = '<?php echo isset($idRol) ? $idRol : ''; ?>';
        var nombreRol = '<?php echo isset($nombreRol) ? $nombreRol : ''; ?>';

        var valorrol = [JSON.parse(idRol), JSON.parse(nombreRol)];

        var docroles = '<?php echo isset($documentorol) ? json_encode($documentorol) : 0; ?>';
        docroles = (docroles != '' ? JSON.parse(docroles) : '');
        var valorDocRol = ['', ''];
    </script>

@stop
