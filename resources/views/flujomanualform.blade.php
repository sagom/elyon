@extends('layouts.vista')

@section('content')
    {!!Html::script('js/flujomanual.js'); !!}
    @include('alerts.request')
	{!!Form::open(['route'=>'flujomanual.store','method'=>'POST', 'id' => 'form-flujomanual'])!!}

    <fieldset>
        <legend>Flujo manual</legend>
        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('nombreFlujoManual', 'Nombre', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::hidden('idFlujoManual', null, array('id' => 'idFlujoManual')) !!}
                    {!!Form::text('nombreFlujoManual',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del flujo'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('Dependencia_idDependencia', 'Dependencia', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-folder-open"></i>
                    </span>
                    {!!Form::select('Dependencia_idDependencia',$dependencia, (isset($flujoManual) ? $flujoManual->Dependencia_idDependencia : null),["class" => "select form-control", "placeholder" =>"- Seleccione la dependencia -"])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('Serie_idSerie', 'Serie', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-folder-open"></i>
                    </span>
                    {!!Form::select('Serie_idSerie',$serie, (isset($flujoManual) ? $flujoManual->Serie_idSerie : null),["class" => "select form-control", "placeholder" =>"- Seleccione la serie -"])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('SubSerie_idSubSerie', 'Sub serie', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-folder-open"></i>
                    </span>
                    {!!Form::select('SubSerie_idSubSerie',$subSerie, (isset($flujoManual) ? $flujoManual->SubSerie_idSubSerie : null),["class" => "select form-control", "placeholder" =>"- Seleccione la sub serie -"])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('Documento_idDocumento', 'Documento', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-folder-open"></i>
                    </span>
                    {!!Form::select('Documento_idDocumento',$documento, (isset($flujoManual) ? $flujoManual->Documento_idDocumento : null),["class" => "select form-control", "placeholder" =>"- Seleccione la documento -"])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('Flujo_idFlujo', 'Flujo', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-folder-open"></i>
                    </span>
                    {!!Form::select('Flujo_idFlujo',$flujo, (isset($flujoManual) ? $flujoManual->Flujo_idFlujo : null),["class" => "select form-control", "placeholder" =>"- Seleccione el flujo -"])!!}
                </div>
            </div>
        </div>

    </fieldset>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">
                Adjunto
            </h4>
        </div>
        <div id="divArchivo" class="panel-collapse">

            <div class="panel-body">
                <div class="form-group" id="test">
                    <div class="div-responsive-doce">

                        <div class="panel panel-success div-responsive-seis">
                            <div class="panel-heading">
                                <h4 class="panel-title">Cargar</h4>
                            </div>
                            <div class="panel-body">
                                <div class="dropzone dropzone-previews" id="dropzoneFileUpload_1"></div>
                            </div>
                        </div>

                        <div class="panel panel-success div-responsive-seis">
                            <div class="panel-heading">
                                <h4 class="panel-title">Visualización</h4>
                            </div>
                            <div class="panel-body">
                                <div id="botonIndexacion_1"></div>
                                <iframe style="width:100%; height:550px; " id="iframeImagen_1" name="iframeImagen_1" src=""></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        var baseUrl = "http://"+location.host;
        var token = $('#token').val();
        Dropzone.autoDiscover = false;

        var myDropzone = new Dropzone("div#dropzoneFileUpload_1",
        {
            url: baseUrl + "/cargarArchivoPublicacion",
            params:
            {
                _token: token
            }
        });

        myDropzone.options.myAwesomeDropzone =
        {
            paramName: "file",
            maxFilesize: 40, // MB
            addRemoveLinks: true,
            clickable: true,
            previewsContainer: ".dropzone-previews",
            clickable: false,
            uploadMultiple: true,
            accept: function(file, done)
            {

            }
        };

        myDropzone.on("addedfile", function(file)
        {
            file.previewElement.addEventListener("click", function(reg)
            {
                var idDrop = this.parentNode.id;
                cargarVistaPreviaVersion(file, document.getElementById('Documento_idDocumento').value);
            });

            // Validar si el archivo ya existe
            if (this.files.length > 1)
            {
                mensajeAlerta("validarDuplicacion", "No se puede cargar el documento", "", "orange", "fa fa-info-circle", "5000");
                this.removeFile(file);
            }

            var ext = file.name.split(".").pop();
            ext = ext.toLowerCase();

            if(ext == "pdf")
            {
                $(file.previewElement).find(".dz-image img").attr("src", "/storage/images/pdf.png");
            }
            else if(ext.indexOf("doc") != -1 || ext.indexOf("docx") != -1)
            {
                $(file.previewElement).find(".dz-image img").attr("src", "/storage/images/word.png");
            }
            else if(ext.indexOf("xls") != -1 || ext.indexOf("xlsx") != -1)
            {
                $(file.previewElement).find(".dz-image img").attr("src", "/storage/images/excel.png");
            }
            else if(ext.indexOf("jpg") != -1 || ext.indexOf("png") != -1 || ext.indexOf("jpeg") != -1 || ext.indexOf("gif") != -1)
            {
                $(file.previewElement).find(".dz-image img").attr("src", "/storage/images/image.png");
            }
        });

    </script>


    @if(isset($flujoManual))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
            {!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
        @else
            {!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'empleado')"])!!}
        @endif
    @else
    {!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'empleado')"])!!}
    @endif

    <div id="modalIndexacion" class="modalDialog" style="display:none;">
        <div class="modal-content" style="width:80%; height:80%; overflow:auto;">
            <div class="modal-header">
                <a onclick="cerrarModal()" title="Cerrar" class="close">X</a>
                <h4 class="modal-title">Indexación del documento</h4>
            </div>
            <div class="modal-body" style="width:100%; height:100%; ">

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('codigoPublicacion', 'Código', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-barcode"></i>
                            </span>
                            {!!Form::text('codigoPublicacion','Automático',['class'=>'form-control', 'readonly'])!!}
                            {!!Form::hidden('rutaImagenPublicacionVersion', null, array('id' => 'rutaImagenPublicacionVersion'))!!}
                            {!!Form::hidden('numeroPublicacionVersion', 1, array('id' => 'numeroPublicacionVersion'))!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('fechaPublicacionVersion', 'Fecha', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            {!!Form::text('fechaPublicacionVersion',date('Y-m-d H:i:s'),['class'=>'form-control', 'readonly'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('paginasPublicacion', 'Páginas', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-file"></i>
                            </span>
                            {!!Form::number('paginasPublicacion',null,['class'=>'form-control', 'required', 'min' => '1', 'placeholder' => 'Ingrese el número de páginas'])!!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="div-responsive-doce">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h4 class="panel-title">Propiedades</h4>
                            </div>
                            <div class="panel-body">

                                <div id="divIndexacion"></div>

                            </div>
                        </div>
                    </div>
                </div>

                {!!Form::button('Adicionar',["class"=>"btn btn-success", "onclick"=>"guardarDatos(event, 'flujomanual')"])!!}
            </div>
        </div>
    </div>

    {!!Form::close()!!}

@stop