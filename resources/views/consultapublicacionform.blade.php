@extends('layouts.vista')

@section('content')
    {!!Html::style('css/consultapublicacion.css'); !!} 
    {!!Html::script('js/consultapublicacion.js'); !!}
    @include('alerts.request')
	{!!Form::open(['route'=>'publicacionce.store','method'=>'POST', 'id' => 'form-publicacion'])!!}

    <fieldset>
        <legend>Consulta de Publicación</legend>

            <div class="panel-body">
                <div class="form-group" id="test">
                    <div class="div-responsive-doce">
                        <input type="hidden" id="token" value="{{csrf_token()}}"/>
                        {!!Form::hidden('Dependencia_idDependencia', null, array('id' => 'Dependencia_idDependencia'))!!}
                        {!!Form::hidden('Serie_idSerie', null, array('id' => 'Serie_idSerie'))!!}
                        {!!Form::hidden('SubSerie_idSubSerie', null, array('id' => 'SubSerie_idSubSerie'))!!}
                        {!!Form::hidden('Documento_idDocumento', null, array('id' => 'Documento_idDocumento'))!!} 
                        {!!Form::hidden('rutaImagenPublicacionVersion', null, array('id' => 'rutaImagenPublicacionVersion'))!!} 
                        {!!Form::hidden('idPublicacion', null, array('id' => 'idPublicacion'))!!} 
                        {!!Form::hidden('condGeneral', $condGeneral, array('id' => 'condGeneral'))!!} 

                        <!-- <div class="div-responsive-doce div-agrupador">
                            {!!Form::label('valorBusquedaMetadato', 'Buscar', array('class' => 'label-responsive-dos')) !!}
                            <div class="div-input-responsive">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-search"></i>
                                    </span>
                                    {!!Form::text('valorBusquedaMetadato',null,['class'=>'form-control','placeholder'=>'Ingrese el valor a buscar'])!!}
                                </div>
                            </div>
                        </div> -->

                        <div class="panel panel-success div-responsive-cuatro">
                            <div class="panel-heading">
                                <h4 class="panel-title">Estructura</h4>
                            </div>
                            <div class="panel-body">

                                <?php 

                                    $datos = array();
                                    for ($i = 0, $c = count($publicacion); $i < $c; ++$i) 
                                    {
                                        $datos[$i] = (array) $publicacion[$i];
                                    }

                                    $total = count($publicacion);
                                    $i = 0;
                                    $estructura = '';
                                    $isdDependencia = '';

                                    while ($i < $total) 
                                    {
                                        $dependencia = $datos[$i]['nombreDependencia'];

                                        $isdDependencia .= $datos[$i]['idDependencia'].',';

                                        $estructura .= 
                                            '<ul id="'.$datos[$i]['idDependencia'].'">
                                                <li onclick="asignarValoresMaestros('.$datos[$i]['idDependencia'].', \'D\')">'.$datos[$i]['nombreDependencia'];

                                        while ($i < $total && $dependencia == $datos[$i]['nombreDependencia']) 
                                        {
                                            $serie = $datos[$i]['nombreSerie'];

                                            $estructura .= 
                                                    '<ul>
                                                        <li onclick="asignarValoresMaestros('.$datos[$i]['idSerie'].', \'S\')">'.$datos[$i]['nombreSerie'];

                                            while ($i < $total && $dependencia == $datos[$i]['nombreDependencia'] && $serie == $datos[$i]['nombreSerie']) 
                                            {
                                                $subserie = $datos[$i]['nombreSubSerie'];

                                                $estructura .= 
                                                    '<ul>
                                                        <li onclick="asignarValoresMaestros('.$datos[$i]['idSubSerie'].', \'SS\')">'.$datos[$i]['nombreSubSerie'];

                                                while ($i < $total && $dependencia == $datos[$i]['nombreDependencia'] && $serie == $datos[$i]['nombreSerie'] && $subserie == $datos[$i]['nombreSubSerie']) 
                                                {
                                                    $documento = $datos[$i]['nombreDocumento'];

                                                    $estructura .= 
                                                    '<ul>
                                                        <li style="cursor:pointer" onclick="asignarValoresMaestros('.$datos[$i]['idDocumento'].', \'Dc\')">'.$datos[$i]['nombreDocumento'];

                                                                $estructura .=
                                                        '</li>
                                                    </ul>';
                                                    $i++;
                                                }
                                                $estructura .=
                                                        '</li>
                                                    </ul>';
                                            }
                                            $estructura .=
                                                    '</li>
                                                </ul>';
                                        }
                                        $estructura .=
                                                '</li>
                                            </ul>';
                                    }
                                    
                                    echo "<input type='hidden' id='idsDependencia' value='".substr($isdDependencia, 0, -1)."'>";
                                    echo $estructura;

                                ?>

                            </div>
                        </div>
                
                        <div class="panel panel-success div-responsive-ocho">
                            <div class="panel-heading">
                                <h4 class="panel-title">Documentos</h4>
                            </div>
                            <div class="panel-body">
                                <iframe style="width:100%; height:550px; " id="iframeDocumento" src=""></iframe>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
    
    </fieldset>
    
    <div id="modalConsulta" class="modalDialog" style="display:none;">
        <div class="modal-content" style="width:90%; height:90%; overflow:auto;">
            <div class="modal-header">
                <a onclick="cerrarModal('modalConsulta')" title="Cerrar" class="close">X</a>
                <h4 class="modal-title">Consulta de publicación</h4>
            </div>
            <div class="modal-body" style="width:100%; height:100%; ">

                <div class="panel panel-success div-responsive-cuatro">
                    <div class="panel-heading">
                        <h4 class="panel-title">Datos</h4>
                    </div>
                    <div class="panel-body">
                        <div id="datosDocumento"></div>
                    </div>
                </div>

                <div class="panel panel-success div-responsive-ocho">
                    <div class="panel-heading">
                        <h4 class="panel-title">Visualización</h4>
                    </div>
                    <div class="panel-body">
                        <div id="botonesDocumento"></div>
                        <iframe style="width:100%; height:650px; " id="iframeImagen" src=""></iframe>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div id="modalCorreo" class="modalDialog" style="display:none;">
        <div class="modal-content" style="width:80%; height:80%; overflow:auto;">
            <div class="modal-header">
                <a onclick="cerrarModal('modalCorreo')" title="Cerrar" class="close">X</a>
                <h4 class="modal-title">Envío de correo</h4>
            </div>
            <div class="modal-body" style="width:100%; height:100%; ">

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('direccionCorreo', 'Destinatario(s)', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-at"></i>
                            </span>
                            {!!Form::text('direccionCorreo',null,['class'=>'form-control','placeholder'=>'Ingrese el destinatario'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('asuntoCorreo', 'Asunto', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-pen-square"></i>
                            </span>
                            {!!Form::text('asuntoCorreo',null,['class'=>'form-control','placeholder'=>'Ingrese el asunto'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('mensajeCorreo', 'Mensaje', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-envelope"></i>
                            </span>
                            {!!Form::textarea('mensajeCorreo',null,['class'=>'form-control','placeholder'=>'Ingrese el mensaje'])!!}
                        </div>
                    </div>
                </div>

                {!!Form::button('Enviar',["class"=>"btn btn-success", "onclick" => "enviarCorreo()"])!!}

            </div>
        </div>
    </div>

{!!Form::close()!!}
@stop