@extends('layouts.grid') 
@section('content')
{!!Form::model($datos)!!}
{!!Html::script('js/radicadointerno.js'); !!}

<script type="text/javascript">
    $(document).ready( function () 
    {
        configurarGrid('radicadosalida',"{!! URL::to ('/radicadointernodata')!!}");
    }); 
</script>

<?php
    $visible = '';

    if(isset($datos[0])) 
    {
        $dato = get_object_vars($datos[0]);
        if ($dato['adicionarRolOpcion'] == 1) 
            $visible = 'inline-block;'; 
        else
            $visible = 'none;';
    }
    else
        $visible = 'none;';
?>

<legend>Correspondencia interna</legend>
<table id="radicadosalida" class="table table-striped table-bordered nowrap" style="width:100%">
    <thead class="btn-success">
        <tr>
            <th onclick="abrirModalInicial()" style="width:60px;padding: 1px 8px; cursor:pointer" data-orderable="false">
                <a ><span style="color:white; display: <?php echo $visible ?>" class="glyphicon glyphicon-plus"></span></a>
            </th>
            <th>ID</th>
            <th>Número</th>
            <th>Fecha</th>
            <th>Nombre remitente</th>
            <th>Dependencia remitente</th>
            <th>Asunto</th>
            <th>Fecha documento</th>
            <th>Nombre destinatario</th>
            <th>Dependencia destinataria</th>
            <th>Observación</th>
        </tr>
    </thead>
    <tfoot class="btn-default active">
        <tr>
            <th>
                &nbsp;
            </th>
            <th>ID</th>
            <th>Número</th>
            <th>Fecha</th>
            <th>Nombre remitente</th>
            <th>Dependencia remitente</th>
            <th>Asunto</th>
            <th>Fecha documento</th>
            <th>Nombre destinatario</th>
            <th>Dependencia destinataria</th>
            <th>Observación</th>        
        </tr>
    </tfoot>
</table>

    <div id="modalRadicado" class="modalDialog" style="display:none;">
        <div class="modal-content" style="width:80%; height:80%; overflow:auto;">
            <div class="modal-header">
                <a onclick="cerrarModal()" title="Cerrar" class="close">X</a>
                <h4 class="modal-title">Digite la información básica</h4>
            </div>
            <div class="modal-body" style="width:100%; height:100%; ">

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('nombreUsuarioRemitente', 'Remitente', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fas fa-user"></i>
                            </span>
                            {!!Form::text('nombreUsuarioRemitente',\Session::get('nombreUsuario'),['class'=>'form-control','placeholder'=>'Dependencia del destinatario', 'readonly'])!!}
                            {!!Form::hidden('idUsuarioRemitente',\Session::get('idUsuario'),['id'=>'idUsuarioRemitente'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('nombreDependenciaRemitente', 'Dependencia remitente', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fas fa-university"></i>
                            </span>
                            {!!Form::text('nombreDependenciaRemitente',$dependenciaremitente,['class'=>'form-control','placeholder'=>'Dependencia del remitente', 'readonly'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('asuntoRadicadoInterno', 'Asunto', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fas fa-file-import"></i>
                            </span>
                            {!!Form::text('asuntoRadicadoInterno',null,['class'=>'form-control','placeholder'=>'Ingrese el asunto'])!!}
                            <input type="hidden" id="token" value="{{csrf_token()}}"/>
                        </div>
                    </div>
                </div>

                <div class="form-group div-responsive-doce div-agrupador">
                    {!!Form::label('UsersDestinatario_idRadicadoInterno', 'Destinatario', array('class' => 'label-responsive-dos control-label')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </span>
                            {!!Form::select('UsersDestinatario_idRadicadoInterno',$userdestinatario, null,["class" => "select form-control", "placeholder" =>'Seleccione el usuario destinatario', 'onchange' => 'buscarDependenciaUsuario(this.value)'])!!}                
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('nombreDependenciaDestinatario', 'Dependencia destinatario', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fas fa-university"></i>
                            </span>
                            {!!Form::text('nombreDependenciaDestinatario',null,['class'=>'form-control','placeholder'=>'Dependencia del destinatario', 'readonly'])!!}
                        </div>
                    </div>
                </div>

                {!!Form::button('Guardar',["class"=>"btn btn-success", "onclick" => "guardarDatosIniciales()"])!!}

            </div>
        </div>
    </div>

{!!Form::close()!!}
@stop

<div id="modalPublicacion" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:80%; height:80%;">
        <!-- Modal content-->
        <div  class="modal-content" style="width:100%; height:100%; ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Digitalizar radicado</h4>
            </div>
            <div class="modal-body" style="width:100%; height:100%; overflow:auto;">
                <div id="divPublicacion"></div>
            </div>
        </div>
    </div>
</div>

<div id="modalFlujo" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:80%; height:80%;">
        <!-- Modal content-->
        <div  class="modal-content" style="width:100%; height:100%; ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Seleccionar flujo</h4>
            </div>
            <div class="modal-body" style="width:100%; height:100%; overflow:auto;">
                <div id="divFlujo"></div>
            </div>
        </div>
    </div>
</div>