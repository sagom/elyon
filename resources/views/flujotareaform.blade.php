@extends('layouts.vista')

@section('content')
    @include('alerts.request')
    {!! Html::script('js/flujo.js') !!}
    {!! Form::open(['id' => 'form-flujotarea']) !!}
    <style>
        #listaPendientes,
        #listaInconclusas,
        #listaTerminadas {
            list-style-type: none;
            padding: 0;
            margin: 0;
        }

        #listaPendientes li,
        #listaInconclusas li,
        #listaTerminadas li {
            padding: 5px;
            border-bottom: 1px solid #ccc;
        }

        input[type="text"] {
            width: 100%;
            padding: 5px;
            box-sizing: border-box;
            /* Incluye el padding en el ancho total */
        }
    </style>

    <fieldset>
        <legend>Tareas</legend>

        <div class="div-responsive-seis div-agrupador">
            <div class="div-responsive-doce div-agrupador">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="div-responsive-tres">
                                <i class="fa fa-clipboard-list fa-5x"></i>
                            </div>
                            <div class="div-responsive-nueve text-right">
                                PENDIENTES
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <input type="text" id="buscadorPendientes" placeholder="Buscar..."
                            onchange="filterList('buscadorPendientes', 'listaPendientes')">
                        <div style="overflow:auto; height:200px;">
                            <ul id="listaPendientes">
                                @foreach ($pendientes as $key => $pendiente)
                                    <a style="cursor:pointer;"
                                        onclick="ejecutarTarea({{ $pendiente->idFlujoTareaRadicado }}, '{{ $pendiente->tipoRadicado }}', {{ $pendiente->Flujo_idFlujo }}, 'e', {{ $pendiente->idRadicado }})">
                                        <li>{{ $pendiente->nombreFlujoTarea }}</li>
                                    </a>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="div-responsive-doce div-agrupador">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="div-responsive-tres">
                                <i class="fa fa-user-times fa-5x"></i>
                            </div>
                            <div class="div-responsive-nueve text-right">
                                INCONCLUSAS
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <input type="text" id="buscadorInconclusas" placeholder="Buscar..."
                            onchange="filterList('buscadorInconclusas', 'listaInconclusas')">
                        <div style="overflow:auto; height:200px;">
                            <ul id="listaInconclusas">
                                @foreach ($anuladas as $key => $anulada)
                                    <a style="cursor:pointer;"
                                        onclick="ejecutarTarea({{ $anulada->idFlujoTareaRadicado }}, '{{ $anulada->tipoRadicado }}', {{ $anulada->Flujo_idFlujo }}, 'v', {{ $anulada->idRadicado }})">
                                        <li>{{ $anulada->nombreFlujoTarea }}</li>
                                    </a>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="div-responsive-seis div-agrupador">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="row">
                        <div class="div-responsive-tres">
                            <i class="fa fa-user-check fa-5x"></i>
                        </div>
                        <div class="div-responsive-nueve text-right">
                            TERMINADAS
                        </div>
                    </div>
                </div>

                <div class="panel-footer">
                    <input type="text" id="buscadorTerminadas" placeholder="Buscar..."
                        onchange="filterList('buscadorTerminadas', 'listaTerminadas')">
                    <div style="overflow:auto; height:200px;">
                        <ul id="listaTerminadas">
                            @foreach ($terminadas as $key => $terminada)
                                <a style="cursor:pointer;"
                                    onclick="ejecutarTarea({{ $terminada->idFlujoTareaRadicado }}, '{{ $terminada->tipoRadicado }}', {{ $terminada->Flujo_idFlujo }}, 'v', {{ $terminada->idRadicado }})">
                                    <li>{{ $terminada->nombreFlujoTarea }}</li>
                                </a>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </fieldset>

    <div id="modalTarea" class="modalDialog" style="display:none;">
        <div class="modal-content" style="width:80%; height:80%; overflow:auto;">
            <div class="modal-header">
                <a onclick="cerrarModal()" title="Cerrar" class="close">X</a>
                <h4 class="modal-title">Ejecutar tarea</h4>
            </div>
            <div class="modal-body" style="width:100%; height:100%;">
                <div id="divTarea"></div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}

@stop
