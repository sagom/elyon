@extends('layouts.grid') 
@section('content')
{!!Form::model($datos)!!}

<script type="text/javascript">
    $(document).ready( function () 
    {
        configurarGrid('metadato',"{!! URL::to ('/metadatodata')!!}");
    }); 
</script>

<?php
    $visible = '';

    if(isset($datos[0])) 
    {
        $dato = get_object_vars($datos[0]);
        if ($dato['adicionarRolOpcion'] == 1) 
            $visible = 'inline-block;'; 
        else
            $visible = 'none;';
    }
    else
        $visible = 'none;';
?>

<legend>Metadatos</legend>
<table id="metadato" class="table table-striped table-bordered nowrap" style="width:100%">
    <thead class="btn-success">
        <tr>
            <th style="width:60px;padding: 1px 8px;" data-orderable="false">
                <a href="metadato/create"><span style="color:white; <?php echo $visible ?>" class="glyphicon glyphicon-plus"></span></a>
            </th>
            <th>ID</th>
            <th>Código</th>
            <th>Nombre</th>
            <th>Tipo</th>
            <th>Opción</th>
            <th>Longitud</th>
        </tr>
    </thead>
    <tfoot class="btn-default active">
        <tr>
            <th>
                &nbsp;
            </th>
            <th>ID</th>
            <th>Código</th>
            <th>Nombre</th>
            <th>Tipo</th>
            <th>Opción</th>
            <th>Longitud</th>
        </tr>
    </tfoot>
</table>

{!!Form::close()!!}
@stop