@extends('layouts.vista')

@section('content')
    {!!Html::script('js/dependencia.js'); !!}
    @include('alerts.request')

    @if(isset($dependencia))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($dependencia,['route'=>['dependencia.destroy',$dependencia->idDependencia],'method'=>'DELETE', 'id' => 'form-dependencia'])!!}
		@else
			{!!Form::model($dependencia,['route'=>['dependencia.update',$dependencia->idDependencia],'method'=>'PUT', 'id' => 'form-dependencia'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'dependencia.store','method'=>'POST', 'id' => 'form-dependencia'])!!}
    @endif

    <fieldset>
        <legend>Información de Dependencia</legend>
        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('codigoDependencia', 'Código', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!!Form::hidden('idDependencia', null, array('id' => 'idDependencia')) !!}
                    {!!Form::hidden('eliminarDepRol', null, array('id' => 'eliminarDepRol')) !!}
                    {!!Form::text('codigoDependencia',null,['class'=>'form-control','placeholder'=>'Ingrese el código'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('nombreDependencia', 'Nombre', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::text('nombreDependencia',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('directorioDependencia', 'Directorio', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-folder-open"></i>
                    </span>
                    {!!Form::text('directorioDependencia',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del directorio', 'onkeyup' => 'replaceSpacesWithUnderLine(this.value, this.id)', 'id' => 'directorioDependencia'])!!}
                </div>
            </div>
        </div>


    </fieldset>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#divDepRol">Permisos</a>
            </h4>
        </div>
        <div id="divDepRol" class="panel-collapse">

            <div class="panel-body">
                <div class="form-group" id='test'>
                    <div class="col-sm-12">
                        <div class="row show-grid">
                            <div style="overflow:auto; height:150px;">
                                <div style="width: 1300px; display: inline-block;">
                                    <div class="btn-default active col-md-1" onclick="deprol.agregarCampos(valorDepRol,'A')" style="width:40px; height:40px; cursor:pointer; display:inline-block">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </div>
                                    <div class="btn-default active col-md-1" style="width:300px; height:40px; display:inline-block">Rol</div>
                                    <div class="btn-default active col-md-1" style="width:100px; height:40px; display:inline-block">Cargar</div>
                                    <div class="btn-default active col-md-1" style="width:100px; height:40px; display:inline-block">Descargar</div>
                                    <div class="btn-default active col-md-1" style="width:100px; height:40px; display:inline-block">Consultar</div>
                                    <div class="btn-default active col-md-1" style="width:100px; height:40px; display:inline-block">Imprimir</div>
                                    <div class="btn-default active col-md-1" style="width:100px; height:40px; display:inline-block">Email</div>
                                    <div class="btn-default active col-md-1" style="width:100px; height:40px; display:inline-block">Eliminar</div>

                                    <div id="contenedordeprol">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @if(isset($dependencia))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'dependencia')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'dependencia')"])!!}
    @endif

    {!!Form::close()!!}

    <script>
        var idRol = '<?php echo isset($idRol) ? $idRol : "";?>';
        var nombreRol = '<?php echo isset($nombreRol) ? $nombreRol : "";?>';

        var valorrol = [JSON.parse(idRol), JSON.parse(nombreRol)];

        var deproles = '<?php echo (isset($dependenciarol) ? json_encode($dependenciarol) : 0);?>';
        deproles = (deproles != '' ? JSON.parse(deproles) : '');
        var valorDepRol = ['','',1,1,1,1,1];

        deprelacion = '<?php echo isset($dependencia) ? $dependencia->Dependencia_idDependencia : "";?>';

        if(deprelacion != '' && deprelacion == 1)
        {
            $("#directorioDependencia").attr('readonly', true);
        }
    </script>

@stop