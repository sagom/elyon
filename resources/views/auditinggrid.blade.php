@extends('layouts.grid') 
@section('content')
{!!Form::open(['method'=>'POST', 'id' => 'form-auditing', 'files' => 'true'])!!}

<script type="text/javascript">
    $(document).ready( function () 
    {
        configurarGrid('subserie',"{!! URL::to ('/auditingdata')!!}");
    }); 
</script>

<legend>Log de transacción</legend>
<table id="subserie" class="table table-striped table-bordered nowrap" style="width:100%">
    <thead class="btn-success">
        <tr>
            <th>ID Modulo</th>
            <th>Modulo</th>
            <th>Usuario</th>
            <th>Evento</th>
            <th>Valor Anterior</th>
            <th>Nuevo Valor</th>
            <th>Fecha de transacción</th>
        </tr>
    </thead>
    <tfoot class="btn-default active">
        <tr>
            <th>ID Modulo</th>
            <th>Modulo</th>
            <th>Usuario</th>
            <th>Evento</th>
            <th>Valor Anterior</th>
            <th>Nuevo Valor</th>
            <th>Fecha de transacción</th>
        </tr>
    </tfoot>
</table>

{!!Form::close()!!}
@stop