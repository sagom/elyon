@extends('layouts.vista')

@section('content')
    @include('alerts.request')
    {!!Html::script('js/radicadointerno.js'); !!}
    @if(isset($radicadointerno))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($radicadointerno,['route'=>['radicadointerno.destroy',$radicadointerno->idRadicadoInterno],'method'=>'DELETE', 'id' => 'form-radicadointerno'])!!}
		@else
			{!!Form::model($radicadointerno,['route'=>['radicadointerno.update',$radicadointerno->idRadicadoInterno],'method'=>'PUT', 'id' => 'form-radicadointerno'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'radicadointerno.store','method'=>'POST', 'id' => 'form-radicadointerno'])!!}
    @endif

    <fieldset>
        <legend>Información de correspondencia interna</legend>
    
        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('numeroRadicadoInterno', 'Número', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                    <i class="fas fa-sort-numeric-down"></i>
                    </span>
                    {!!Form::text('numeroRadicadoInterno',(isset($radicadointerno) ? $radicadointerno->numeroRadicadoInterno : 'Automático'),['class'=>'form-control','placeholder'=>'Automático','readonly'])!!}
                    {!!Form::hidden('idRadicadoInterno', null, array('id' => 'idRadicadoInterno')) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('fechaRadicadoInterno', 'Fecha', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                    <i class="far fa-calendar-alt"></i>
                    </span>
                    {!!Form::text('fechaRadicadoInterno',(isset($radicadointerno) ? $radicadointerno->fechaRadicadoInterno : date('Y-m-d H:i:s')),['class'=>'form-control','placeholder'=>'Automático','readonly'])!!}
                </div>
            </div>
        </div>

    </fieldset>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#divRem">Remitente</a>
            </h4>
        </div>
        <div id="divRem" class="panel-collapse collapse">
            <div class="panel-body">


                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('nombreUsuarioRemitente', 'Remitente', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fas fa-user"></i>
                            </span>
                            {!!Form::text('nombreUsuarioRemitente',null,['class'=>'form-control','placeholder'=>'Dependencia del destinatario', 'readonly'])!!}
                            {!!Form::hidden('idUsuarioRemitente',null,['id'=>'idUsuarioRemitente'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('nombreDependenciaRemitente', 'Dependencia remitente', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fas fa-university"></i>
                            </span>
                            {!!Form::text('nombreDependenciaRemitente',null,['class'=>'form-control','placeholder'=>'Dependencia del remitente', 'readonly'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('fechaDocumentoRadicadoInterno', 'Fecha', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="far fa-calendar-alt"></i>
                            </span>
                        {!!Form::date('fechaDocumentoRadicadoInterno',null,['class'=>'form-control','placeholder'=>'Año-Mes-Día'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('asuntoRadicadoInterno', 'Asunto', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fas fa-file-import"></i>
                            </span>
                            {!!Form::text('asuntoRadicadoInterno',null,['class'=>'form-control','placeholder'=>'Ingrese el asunto'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('observacionRadicadoInterno', 'Observación', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fas fa-align-left"></i>
                            </span>
                            {!!Form::text('observacionRadicadoInterno',null,['class'=>'form-control','placeholder'=>'Ingrese las observacion'])!!}
                        </div>
                    </div>
                </div>
        
            </div>
        </div>
    </div>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#divdes">Destinatario</a>
            </h4>
        </div>
        <div id="divdes" class="panel-collapse collapse">
            <div class="panel-body">

                <div class="form-group div-responsive-doce div-agrupador">
                    {!!Form::label('UsersDestinatario_idRadicadoInterno', 'Destinatario', array('class' => 'label-responsive-dos control-label')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </span>
                            {!!Form::select('UsersDestinatario_idRadicadoInterno',$userdestinatario, null,["class" => "select form-control", "placeholder" =>'Seleccione el usuario destinatario', 'onchange' => 'buscarDependenciaUsuario(this.value)'])!!}                
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('nombreDependenciaDestinatario', 'Dependencia destinatario', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fas fa-university"></i>
                            </span>
                            {!!Form::text('nombreDependenciaDestinatario',null,['class'=>'form-control','placeholder'=>'Dependencia del destinatario', 'readonly'])!!}
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    

    @if(isset($radicadointerno))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'radicadointerno')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'radicadointerno')"])!!}
    @endif 
     
    {!!Form::close()!!}

@stop