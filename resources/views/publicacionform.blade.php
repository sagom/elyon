<?php

    $idRadicadoIngreso = (isset($_GET['idRadicadoIngreso']) ? $_GET['idRadicadoIngreso'] : null);
    $idRadicadoSalida = (isset($_GET['idRadicadoSalida']) ? $_GET['idRadicadoSalida'] : null);
    $idRadicadoInterno = (isset($_GET['idRadicadoInterno']) ? $_GET['idRadicadoInterno'] : null);

    if($idRadicadoIngreso == null && $idRadicadoSalida == null && $idRadicadoInterno == null)
    {
        $tipo = 'vista';
    }
    else
    {
        $tipo = 'modal';
    }
?>

@extends('layouts.'.$tipo)

@section('content')
    {!!Html::script('js/publicacion.js'); !!}
    @include('alerts.request')

	{!!Form::open(['route'=>'publicacionce.store','method'=>'POST', 'id' => 'form-publicacion'])!!}

    <fieldset>
        <legend>Publicación</legend>
    <?php

        $datos = array();
        for ($i = 0, $c = count($estructura); $i < $c; ++$i)
        {
            $datos[$i] = (array) $estructura[$i];
        }

        $i=0;
        $registros = count($estructura);

        $ns = 0;
        $liserie = array();
        $divserie = array();

        while ($i < $registros)
        {
            $dependencia = $datos[$i]['idDependencia'].$datos[$i]['idSerie'];
            $nss = 0;
            $lisubserie = array();
            $divsubserie = array();

            if(!isset($liserie[$ns]))
                $liserie[$ns] = '';

            if(!isset($divserie[$ns]))
                $divserie[$ns] = '';

            $liserie[$ns] .= '
                <li onclick="asignarIdDependenciaSerie('.$datos[$i]["idDependencia"].','.$datos[$i]["idSerie"].')"><a data-toggle="tab" href="#'.$datos[$i]["idDependencia"].'_'.$datos[$i]["idSerie"].'">'.$datos[$i]["nombreDependencia"].'-'.$datos[$i]["nombreSerie"].'</a></li>';

            $divserie[$ns] .= '
                <div id="'.$datos[$i]["idDependencia"].'_'.$datos[$i]["idSerie"].'" class="tab-pane fade">
                    <ul class="nav nav-tabs">';


            while ($i < $registros && $dependencia == $datos[$i]['idDependencia'].$datos[$i]['idSerie'] )
            {
                $subserie = $datos[$i]['idSubSerie'];
                if(!isset($lisubserie[$nss]))
                $lisubserie[$nss] = '';

                if(!isset($divsubserie[$nss]))
                $divsubserie[$nss] = '';

                $lisubserie[$nss] .= '
                    <li onclick="asignarIdSubSerie('.$datos[$i]["idSubSerie"].')"><a data-toggle="tab" href="#'.$datos[$i]["idDependencia"].'_'.$datos[$i]["idSerie"].'_'.$datos[$i]["idSubSerie"].'">'.$datos[$i]["nombreSubSerie"].'</a></li>';
                $divsubserie[$nss] .= '
                    <div id="'.$datos[$i]["idDependencia"].'_'.$datos[$i]["idSerie"].'_'.$datos[$i]["idSubSerie"].'" class="tab-pane fade">';

                while ($i < $registros && $dependencia == $datos[$i]['idDependencia'].$datos[$i]['idSerie'] && $subserie == $datos[$i]['idSubSerie'])
                {
                    $divsubserie[$nss] .=
                    '<div class="panel-body">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#documento_'.$datos[$i]["idDependencia"].'_'.$datos[$i]["idSerie"].'_'.$datos[$i]["idSubSerie"].'_'.$datos[$i]["idDocumento"].'">'.$datos[$i]["nombreDocumento"].'</a>
                                    </h4>
                                </div>
                                <div id="documento_'.$datos[$i]["idDependencia"].'_'.$datos[$i]["idSerie"].'_'.$datos[$i]["idSubSerie"].'_'.$datos[$i]["idDocumento"].'" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="form-group" id="test">
                                            <div class="div-responsive-doce">

                                                <div class="panel panel-success div-responsive-seis">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">Cargar</h4>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="dropzone dropzone-previews" id="dropzoneFileUpload_'.$datos[$i]["idDependencia"].'_'.$datos[$i]["idSerie"].'_'.$datos[$i]["idSubSerie"].'_'.$datos[$i]["idDocumento"].'"></div>
                                                    </div>
                                                </div>

                                                <div class="panel panel-success div-responsive-seis">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">Visualización</h4>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div id="botonIndexacion'.$datos[$i]["idDependencia"].'_'.$datos[$i]["idSerie"].'_'.$datos[$i]["idSubSerie"].'_'.$datos[$i]["idDocumento"].'" name="iframeImagen_'.$datos[$i]["idDependencia"].'_'.$datos[$i]["idSerie"].'_'.$datos[$i]["idSubSerie"].'_'.$datos[$i]["idDocumento"].'"></div>
                                                            <iframe style="width:100%; height:550px; " id="iframeImagen_'.$datos[$i]["idDependencia"].'_'.$datos[$i]["idSerie"].'_'.$datos[$i]["idSubSerie"].'_'.$datos[$i]["idDocumento"].'" name="iframeImagen_'.$datos[$i]["idDependencia"].'_'.$datos[$i]["idSerie"].'_'.$datos[$i]["idSubSerie"].'_'.$datos[$i]["idDocumento"].'" src=""></iframe>
                                                    </div>
                                                </div>
                                                </br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                        var baseUrl = "http://'.$_SERVER["HTTP_HOST"].'";
                        var token = "'.Session::getToken().'";
                        Dropzone.autoDiscover = false;

                        var myDropzone = new Dropzone("div#dropzoneFileUpload_'.$datos[$i]["idDependencia"].'_'.$datos[$i]["idSerie"].'_'.$datos[$i]["idSubSerie"].'_'.$datos[$i]["idDocumento"].'",
                        {
                            url: baseUrl + "/cargarArchivoPublicacion",
                            params:
                            {
                                _token: token
                            }
                        });

                        myDropzone.options.myAwesomeDropzone =
                        {
                            paramName: "file",
                            maxFilesize: 40, // MB
                            addRemoveLinks: true,
                            clickable: true,
                            previewsContainer: ".dropzone-previews",
                            clickable: false,
                            uploadMultiple: true,
                            accept: function(file, done)
                            {

                            }
                        };

                        myDropzone.on("addedfile", function(file)
                        {
                            file.previewElement.addEventListener("click", function(reg)
                            {
                                var idDrop = this.parentNode.id;
                                cargarVistaPrevia(file, '.$datos[$i]["idDocumento"].', "'.$datos[$i]["idDependencia"].'_'.$datos[$i]["idSerie"].'_'.$datos[$i]["idSubSerie"].'_'.$datos[$i]["idDocumento"].'");
                            });

                            // Validar si el archivo ya existe
                            if (this.files.length)
                            {
                                var i, _len;
                                for (i = 0, _len = this.files.length; i < _len - 1; i++)
                                {
                                    if(this.files[i].name === file.name && this.files[i].size === file.size && this.files[i].lastModifiedDate.toString() === file.lastModifiedDate.toString())
                                    {
                                        mensajeAlerta("validarExistencia", "No se puede cargar el documento", "El documento ya está cargado", "orange", "fa fa-info-circle", "5000");
                                        this.removeFile(file);
                                    }
                                }
                            }

                            var ext = file.name.split(".").pop();
                            ext = ext.toLowerCase();

                            if(ext == "pdf")
                            {
                                $(file.previewElement).find(".dz-image img").attr("src", "/storage/images/pdf.png");
                            }
                            else if(ext.indexOf("doc") != -1 || ext.indexOf("docx") != -1)
                            {
                                $(file.previewElement).find(".dz-image img").attr("src", "/storage/images/word.png");
                            }
                            else if(ext.indexOf("xls") != -1 || ext.indexOf("xlsx") != -1)
                            {
                                $(file.previewElement).find(".dz-image img").attr("src", "/storage/images/excel.png");
                            }
                            else if(ext.indexOf("jpg") != -1 || ext.indexOf("png") != -1 || ext.indexOf("jpeg") != -1 || ext.indexOf("gif") != -1)
                            {
                                $(file.previewElement).find(".dz-image img").attr("src", "/storage/images/image.png");
                            }
                        });

                    </script>';

                    $i++;
                }
                $divsubserie[$nss] .= '</div>';
                $nss ++;
            }

            $divserie[$ns] .= implode('',$lisubserie).
                    '</ul>
                    <div class="tab-content">'.
                        implode('',$divsubserie).'
                    </div>
                </div>';
            $ns ++;
        }

        echo '
        <div id="form-radicado">
            <ul class="nav nav-tabs">
                '.implode('', $liserie).'
            </ul>
            <div class="tab-content">
                '.implode('', $divserie).'
            </div>
        </div>';
    ?>

    </fieldset>

    <div id="modalIndexacion" class="modalDialog" style="display:none;">
        <div class="modal-content" style="width:80%; height:80%; overflow:auto;">
            <div class="modal-header">
                <a onclick="cerrarModal()" title="Cerrar" class="close">X</a>
                <h4 class="modal-title">Indexación del documento</h4>
            </div>
            <div class="modal-body" style="width:100%; height:100%; ">

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('codigoPublicacion', 'Código', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-barcode"></i>
                            </span>
                            {!!Form::text('codigoPublicacion','Automático',['class'=>'form-control', 'readonly'])!!}
                            {!!Form::hidden('Dependencia_idDependencia', null, array('id' => 'Dependencia_idDependencia'))!!}
                            {!!Form::hidden('Serie_idSerie', null, array('id' => 'Serie_idSerie'))!!}
                            {!!Form::hidden('SubSerie_idSubSerie', null, array('id' => 'SubSerie_idSubSerie'))!!}
                            {!!Form::hidden('Documento_idDocumento', null, array('id' => 'Documento_idDocumento'))!!}
                            {!!Form::hidden('rutaImagenPublicacionVersion', null, array('id' => 'rutaImagenPublicacionVersion'))!!}
                            {!!Form::hidden('numeroPublicacionVersion', 1, array('id' => 'numeroPublicacionVersion'))!!}
                            {!!Form::hidden('Radicado_idRadicadoIngreso', $idRadicadoIngreso, array('id' => 'Radicado_idRadicadoIngreso')) !!}
                            {!!Form::hidden('Radicado_idRadicadoSalida', $idRadicadoSalida, array('id' => 'Radicado_idRadicadoSalida')) !!}
                            {!!Form::hidden('Radicado_idRadicadoInterno', $idRadicadoInterno, array('id' => 'Radicado_idRadicadoInterno')) !!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('fechaPublicacionVersion', 'Fecha', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            {!!Form::text('fechaPublicacionVersion',date('Y-m-d H:i:s'),['class'=>'form-control', 'readonly'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('paginasPublicacion', 'Páginas', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-file"></i>
                            </span>
                            {!!Form::number('paginasPublicacion',null,['class'=>'form-control', 'required', 'min' => '1', 'placeholder' => 'Ingrese el número de páginas'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('Flujo_idFlujo', 'Flujo', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-folder-open"></i>
                            </span>
                            {!!Form::select('Flujo_idFlujo', $flujo, null,["class" => "select form-control", "placeholder" =>"- Seleccione el flujo -"])!!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="div-responsive-doce">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h4 class="panel-title">Propiedades</h4>
                            </div>
                            <div class="panel-body">

                                <div id="divIndexacion"></div>

                            </div>
                        </div>
                    </div>
                </div>

                {!!Form::button('Adicionar',["class"=>"btn btn-success", "onclick"=>"guardarDatos(event, 'publicacion')"])!!}
            </div>
        </div>
    </div>
{!!Form::close()!!}
@stop