@extends('layouts.vista')

@section('content')
    {!!Html::script('js/publicacion.js'); !!} 
    @include('alerts.request')

	{!!Form::open(['method'=>'POST', 'id' => 'form-publicacion'])!!}

    <fieldset>
        <legend>Publicación versión</legend>
        {!!Form::hidden('idPublicacion', $idPublicacion, array('id' => 'idPublicacion')) !!} 
        <input type="hidden" id="token" value="{{csrf_token()}}"/>
        <div class="panel-body">
            <div class="panel-group" id="accordion">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            {{$documento->nombreDocumento}}
                        </h4>
                    </div>
                        
                    <div class="panel-body">
                        <div class="form-group" id="test">
                            <div class="div-responsive-doce">

                                <div class="panel panel-success div-responsive-seis">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Cargar</h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="dropzone dropzone-previews" id="dropzoneFileUpload_{{$idPublicacion}}"></div>  
                                    </div>
                                </div>
                        
                                <div class="panel panel-success div-responsive-seis">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Visualización</h4>
                                    </div>
                                    <div class="panel-body">
                                        <div id="botonIndexacion_{{$idPublicacion}}"></div>
                                        <iframe style="width:100%; height:550px; " id="iframeImagen_{{$idPublicacion}}" name="iframeImagen_{{$idPublicacion}}" src=""></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
                
        <script type="text/javascript">
            idPublicacion = '<?php echo $idPublicacion;?>'
            var baseUrl = "http://"+location.host;
            var token = $('#token').val();
            Dropzone.autoDiscover = false;

            var myDropzone = new Dropzone("div#dropzoneFileUpload_{{$idPublicacion}}", 
            {
                url: baseUrl + "/cargarArchivoPublicacion",
                params: 
                {
                    _token: token
                }
            });

            myDropzone.options.myAwesomeDropzone =  
            {
                paramName: "file", 
                maxFilesize: 40, // MB
                addRemoveLinks: true,
                clickable: true,
                previewsContainer: ".dropzone-previews",
                clickable: false,
                uploadMultiple: true,
                accept: function(file, done) 
                {
    
                }
            };

            myDropzone.on("addedfile", function(file) 
            {
                file.previewElement.addEventListener("click", function(reg) 
                {
                    var idDrop = this.parentNode.id;
                    cargarVistaPreviaVersion(file, idPublicacion);
                });

                // Validar si el archivo ya existe
                if (this.files.length > 1) 
                {
                    mensajeAlerta("validarDuplicacion", "No se puede cargar el documento", "Para agregar páginas al PDF, solo es posible cargar un documento", "orange", "fa fa-info-circle", "5000");
                    this.removeFile(file);
                }

                var ext = file.name.split(".").pop();
                ext = ext.toLowerCase();

                if(ext == "pdf")
                {
                    $(file.previewElement).find(".dz-image img").attr("src", "/storage/images/pdf.png");
                }
                else if(ext.indexOf("doc") != -1 || ext.indexOf("docx") != -1)
                {
                    $(file.previewElement).find(".dz-image img").attr("src", "/storage/images/word.png");
                }
                else if(ext.indexOf("xls") != -1 || ext.indexOf("xlsx") != -1)
                {
                    $(file.previewElement).find(".dz-image img").attr("src", "/storage/images/excel.png");
                }
                else if(ext.indexOf("jpg") != -1 || ext.indexOf("png") != -1 || ext.indexOf("jpeg") != -1 || ext.indexOf("gif") != -1)
                {
                    $(file.previewElement).find(".dz-image img").attr("src", "/storage/images/image.png");
                }
            });
            
        </script>
    
    </fieldset>
{!!Form::close()!!}
@stop