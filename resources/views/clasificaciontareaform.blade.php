@extends('layouts.vista')

@section('content')
    {!!Html::script('js/clasificaciontarea.js'); !!} 
    @include('alerts.request')

    @if(isset($clasificaciontarea))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($clasificaciontarea,['route'=>['clasificaciontarea.destroy',$clasificaciontarea->idClasificacionTarea],'method'=>'DELETE', 'id' => 'form-clasificaciontarea'])!!}
		@else
			{!!Form::model($clasificaciontarea,['route'=>['clasificaciontarea.update',$clasificaciontarea->idClasificacionTarea],'method'=>'PUT', 'id' => 'form-clasificaciontarea'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'clasificaciontarea.store','method'=>'POST', 'id' => 'form-clasificaciontarea'])!!}
    @endif

    <fieldset>
        <legend>Información de Clasificación Tarea</legend>
        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('codigoClasificacionTarea', 'Código', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!!Form::hidden('idClasificacionTarea', null, array('id' => 'idClasificacionTarea')) !!}
                    {!!Form::hidden('eliminarClasificacionTareaDet', null, array('id' => 'eliminarClasificacionTareaDet')) !!}
                    {!!Form::text('codigoClasificacionTarea',null,['class'=>'form-control','placeholder'=>'Ingrese el código'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('nombreClasificacionTarea', 'Nombre', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::text('nombreClasificacionTarea',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre'])!!}
                </div>
            </div>
        </div>
        
    </fieldset>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#divDetalle">Detalle</a>
            </h4>
        </div>
        <div id="divDetalle" class="panel-collapse">
            
            <div class="panel-body">
                <div class="form-group" id='test'>
                    <div class="col-sm-12">
                        <div class="row show-grid">
                            <div style="overflow:auto; height:150px;">
                                <div style="width: 1300px; display: inline-block;">
                                    <div class="btn-default active col-md-1" onclick="clastarea.agregarCampos(valorClasTarea,'A')" style="width:40px; height:40px; cursor:pointer; display:inline-block">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </div>
                                    <div class="btn-default active col-md-1" style="width:150px; height:40px; display:inline-block">Código</div>
                                    <div class="btn-default active col-md-1" style="width:500px; height:40px; display:inline-block">Nombre</div>
                                    
                                    <div id="contenedorclasificaciontarea">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @if(isset($clasificaciontarea))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'clasificaciontarea')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'clasificaciontarea')"])!!}
    @endif 
     
    {!!Form::close()!!}

    <script>
        var clastareadet = '<?php echo (isset($clasificaciontareadetalle) ? json_encode($clasificaciontareadetalle) : 0);?>';
        clastareadet = (clastareadet != '' ? JSON.parse(clastareadet) : '');
        var valorClasTarea = ['','',''];

        deprelacion = '<?php echo isset($clasificaciontarea) ? $clasificaciontarea->ClasificacionTarea_idClasificacionTarea : "";?>';
    </script>

@stop