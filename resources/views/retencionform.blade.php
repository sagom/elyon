@extends('layouts.vista')

@section('content')
    {!!Html::script('js/retencion.js'); !!} 
    @include('alerts.request')

	{!!Form::open(['route'=>'retencion.store','method'=>'POST', 'id' => 'form-retencion'])!!}

    <fieldset>
        <legend>Retencion</legend>
        {!!Form::hidden('idRetencion', null, array('id' => 'idRetencion')) !!}
        {!!Form::hidden('eliminarRetencion', null, array('id' => 'eliminarRetencion')) !!}
        
    </fieldset>

    <div class="panel panel-success">    
        <div class="panel-body">
            <div class="form-group" id='test'>
                <div class="col-sm-12">
                    <div class="row show-grid">
                        <div style="overflow:auto; height:350px;">
                            <div style="width: 2500px; display: inline-block;">
                                <div class="btn-default active col-md-1" onclick="retencion.agregarCampos(valorRetencion,'A')" style="width:40px; height:40px; cursor:pointer; display:inline-block">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </div>
                                <div class="btn-default active col-md-1" style="width:300px; height:40px; display:inline-block">Dependecia</div>
                                <div class="btn-default active col-md-1" style="width:300px; height:40px; display:inline-block">Serie</div>
                                <div class="btn-default active col-md-1" style="width:300px; height:40px; display:inline-block">Sub serie</div>
                                <div class="btn-default active col-md-1" style="width:300px; height:40px; display:inline-block">Documento</div>
                                <div class="btn-default active col-md-1" style="width:100px; height:40px; display:inline-block">Gestión</div>
                                <div class="btn-default active col-md-1" style="width:100px; height:40px; display:inline-block">Central</div>
                                <div class="btn-default active col-md-1" style="width:200px; height:40px; display:inline-block">Soporte</div>
                                <div class="btn-default active col-md-1" style="width:200px; height:40px; display:inline-block">Disposición</div>
                                <div class="btn-default active col-md-1" style="width:400px; height:40px; display:inline-block">Procedimiento</div>
                                <div class="btn-default active col-md-1" style="width:200px; height:40px; display:inline-block">Estado</div>

                                <div id="contenedorretencion">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            

    {!!Form::submit('Guardar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'retencion')"])!!}
  	 
    {!!Form::close()!!}

    <script>
        var idDependencia = '<?php echo isset($idDependencia) ? $idDependencia : "";?>';
        var nombreDependencia = '<?php echo isset($nombreDependencia) ? $nombreDependencia : "";?>';
        var valordependencia = [JSON.parse(idDependencia), JSON.parse(nombreDependencia)];

        var idSerie = '<?php echo isset($idSerie) ? $idSerie : "";?>';
        var nombreSerie = '<?php echo isset($nombreSerie) ? $nombreSerie : "";?>';
        var valorserie = [JSON.parse(idSerie), JSON.parse(nombreSerie)];

        var idSubSerie = '<?php echo isset($idSubSerie) ? $idSubSerie : "";?>';
        var nombreSubSerie = '<?php echo isset($nombreSubSerie) ? $nombreSubSerie : "";?>';
        var valorsubserie = [JSON.parse(idSubSerie), JSON.parse(nombreSubSerie)];
        
        var idDocumento = '<?php echo isset($idDocumento) ? $idDocumento : "";?>';
        var nombreDocumento = '<?php echo isset($nombreDocumento) ? $nombreDocumento : "";?>';
        var valordocumento = [JSON.parse(idDocumento), JSON.parse(nombreDocumento)];

        var soporte = [["Papel", "Electronico"], ["Papel", "Electrónico"]];
        var disposicion = [["E", "S", "C"], ["Eliminación", "Selección", "Conservación"]];
        var estado = [["Activa", "Inactiva"], ["Activa", "Inactiva"]];

        var retenciones = '<?php echo (isset($retencion) ? json_encode($retencion) : 0);?>';
        retenciones = (retenciones != '' ? JSON.parse(retenciones) : '');
        var valorRetencion = [0,0,0,0,0,0,0,0,0,'',0];
    </script>

@stop