@extends('layouts.vista')

@section('content')
    {!!Html::script('js/user.js'); !!} 
    @include('alerts.request')

    @if(isset($user))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($user,['route'=>['user.destroy',$user->id],'method'=>'DELETE', 'id' => 'form-user'])!!}
		@else
			{!!Form::model($user,['route'=>['user.update',$user->id],'method'=>'PUT', 'id' => 'form-user'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'user.store','method'=>'POST', 'id' => 'form-user'])!!}
    @endif

    <fieldset>
        <legend>Información del usuario</legend>
        <div class="form-group div-responsive-doce div-agrupador">
            {!!Form::label('user', 'Usuario', array('class' => 'label-responsive-dos control-label')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-user"></i>
                    </span>
                    {!!Form::text('user',null,['class'=>'form-control','placeholder'=>'Usuario'])!!}
                    {!!Form::hidden('id', null, array('id' => 'id')) !!}
                    {!!Form::hidden('eliminarCompaniaRol', null, array('id' => 'eliminarCompaniaRol')) !!}
                    <input type="hidden" id="token" value="{{csrf_token()}}"/>
                </div>
            </div>
        </div>

        <div class="form-group div-responsive-doce div-agrupador">
            {!!Form::label('name', 'Nombre', array('class' => 'label-responsive-dos control-label')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-user-edit"></i>
                    </span>
                    {!!Form::text('name',null,['class'=>'form-control','placeholder'=>'Nombre'])!!}
                </div>
            </div>
        </div>

        <div class="form-group div-responsive-doce div-agrupador">
            {!!Form::label('email', 'Correo', array('class' => 'label-responsive-dos control-label')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-at"></i>
                    </span>
                    {!!Form::text('email',null,['class'=>'form-control','placeholder'=>'Correo'])!!}
                </div>
            </div>
        </div>

        <div class="form-group div-responsive-doce div-agrupador">
            {!!Form::label('password', 'Contraseña', array('class' => 'label-responsive-dos control-label')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-key"></i>
                    </span>
                    {!!Form::hidden('claveOriginal',(isset($user) ? $user->password : null), array())!!}
                    {!!Form::password('password',array('class'=>'form-control','placeholder'=>'**********'))!!}
                    <span class="input-group-addon" style="cursor:pointer" onmouseout="ocultarContraseña()" onmouseover="visualizarContraseña()">
                        <i id="iconoOjo" class="glyphicon glyphicon-eye-close"></i>
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group div-responsive-doce div-agrupador">
            {!!Form::label('password_confirmation', 'Confirmar contraseña', array('class' => 'label-responsive-dos control-label')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-key"></i>
                    </span>
                    {!!Form::password('password_confirmation',array('class'=>'form-control','placeholder'=>'**********'))!!}
                    <span class="input-group-addon" style="cursor:pointer" onmouseout="ocultarContraseñaConfirmacion()" onmouseover="visualizarContraseñaConfirmacion()">
                        <i id="iconoOjoConfirmacion" class="glyphicon glyphicon-eye-close"></i>
                    </span>
                </div>          
            </div>
        </div>

        <div class="form-group div-responsive-doce div-agrupador">
            {!!Form::label('Dependencia_idDependencia', 'Dependencia', array('class' => 'label-responsive-dos control-label')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-university"></i>
                    </span>
                    {!!Form::select('Dependencia_idDependencia',$dependencia, null,["class" => "select form-control", "placeholder" =>'Seleccione la dependencia'])!!}                
                </div>
            </div>
        </div>
    </fieldset>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#divCompaniaRol">Permisos</a>
            </h4>
        </div>
        <div id="divCompaniaRol" class="panel-collapse">
            
            <div class="panel-body">
                <div class="form-group" id='test'>
                    <div class="col-sm-12">
                        <div class="row show-grid">
                            <div style="overflow:auto; height:150px;">
                                <div style="width: 1300px; display: inline-block;">
                                    <div class="btn-default active col-md-1" onclick="companiarol.agregarCampos(valorcompaniarol,'A')" style="width:40px; height:40px; cursor:pointer; display:inline-block">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </div>
                                    <div class="btn-default active col-md-1" style="width:400px; height:40px; display:inline-block">Compañía</div>
                                    <div class="btn-default active col-md-1" style="width:400px; height:40px; display:inline-block">Rol</div>

                                    <div id="contenedorcomaniaprol">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    

    @if(isset($user))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'user')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'user')"])!!}
    @endif 
     
    {!!Form::close()!!}

    <script>

        var idCompania = '<?php echo isset($idCompania) ? $idCompania : "";?>';
        var nombreCompania = '<?php echo isset($nombreCompania) ? $nombreCompania : "";?>';

        var buscarrol = ['onchange', 'consultarRol(this.value, this.id)'];

        var valorcompania = [JSON.parse(idCompania), JSON.parse(nombreCompania)];

        var companiaroles = '<?php echo (isset($userscompaniarol) ? json_encode($userscompaniarol) : 0);?>';
        companiaroles = (companiaroles != '' ? JSON.parse(companiaroles) : '');
        var valorcompaniarol = ['',0,0];

        function visualizarContraseña()
        {
            $("#password").prop("type", "text");
            $("#iconoOjo").attr("class", "glyphicon glyphicon-eye-open");
        }

        function ocultarContraseña()
        {
            $("#password").prop("type", "password");    
            $("#iconoOjo").attr("class", "glyphicon glyphicon-eye-close");   
        }

        function visualizarContraseñaConfirmacion()
        {
            $("#password_confirmation").prop("type", "text");
            $("#iconoOjoConfirmacion").attr("class", "glyphicon glyphicon-eye-open");
        }

        function ocultarContraseñaConfirmacion()
        {
            $("#password_confirmation").prop("type", "password");    
            $("#iconoOjoConfirmacion").attr("class", "glyphicon glyphicon-eye-close");   
        }
    </script>

@stop