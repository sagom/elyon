@extends('layouts.vista')

@section('content')
    {!!Html::script('js/estructura.js'); !!} 
    @include('alerts.request')

	{!!Form::open(['route'=>'estructura.store','method'=>'POST', 'id' => 'form-estructura'])!!}

    <fieldset>
        <legend>Estructura</legend>
        {!!Form::hidden('idEstructura', null, array('id' => 'idEstructura')) !!}
        {!!Form::hidden('eliminarEstructura', null, array('id' => 'eliminarEstructura')) !!}
        
    </fieldset>

    <div class="panel panel-success">    
        <div class="panel-body">
            <div class="form-group" id='test'>
                <div class="col-sm-12">
                    <div class="row show-grid">
                        <div style="overflow:auto; height:350px;">
                            <div style="width: 1300px; display: inline-block;">
                                <div class="btn-default active col-md-1" onclick="estructura.agregarCampos(valorEstructura,'A')" style="width:40px; height:40px; cursor:pointer; display:inline-block">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </div>
                                <div class="btn-default active col-md-1" style="width:300px; height:40px; display:inline-block">Dependecia</div>
                                <div class="btn-default active col-md-1" style="width:300px; height:40px; display:inline-block">Serie</div>
                                <div class="btn-default active col-md-1" style="width:300px; height:40px; display:inline-block">Sub serie</div>
                                <div class="btn-default active col-md-1" style="width:300px; height:40px; display:inline-block">Documento</div>

                                <div id="contenedorestructura">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            

    {!!Form::submit('Guardar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'estructura')"])!!}
  	 
    {!!Form::close()!!}

    <script>
        var idDependencia = '<?php echo isset($idDependencia) ? $idDependencia : "";?>';
        var nombreDependencia = '<?php echo isset($nombreDependencia) ? $nombreDependencia : "";?>';
        var valordependencia = [JSON.parse(idDependencia), JSON.parse(nombreDependencia)];

        var idSerie = '<?php echo isset($idSerie) ? $idSerie : "";?>';
        var nombreSerie = '<?php echo isset($nombreSerie) ? $nombreSerie : "";?>';
        var valorserie = [JSON.parse(idSerie), JSON.parse(nombreSerie)];

        var idSubSerie = '<?php echo isset($idSubSerie) ? $idSubSerie : "";?>';
        var nombreSubSerie = '<?php echo isset($nombreSubSerie) ? $nombreSubSerie : "";?>';
        var valorsubserie = [JSON.parse(idSubSerie), JSON.parse(nombreSubSerie)];
        
        var idDocumento = '<?php echo isset($idDocumento) ? $idDocumento : "";?>';
        var nombreDocumento = '<?php echo isset($nombreDocumento) ? $nombreDocumento : "";?>';
        var valordocumento = [JSON.parse(idDocumento), JSON.parse(nombreDocumento)];

        var estructuras = '<?php echo (isset($estructura) ? json_encode($estructura) : 0);?>';
        estructuras = (estructuras != '' ? JSON.parse(estructuras) : '');
        var valorEstructura = [0,0,0,0,0];
    </script>

@stop