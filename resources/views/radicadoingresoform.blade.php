@extends('layouts.vista')

@section('content')
    @include('alerts.request')
    {!!Html::script('js/radicadoingreso.js'); !!}
    @if(isset($radicadoingreso))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($radicadoingreso,['route'=>['radicadoingreso.destroy',$radicadoingreso->idRadicadoIngreso],'method'=>'DELETE', 'id' => 'form-radicadoingreso'])!!}
		@else
			{!!Form::model($radicadoingreso,['route'=>['radicadoingreso.update',$radicadoingreso->idRadicadoIngreso],'method'=>'PUT', 'id' => 'form-radicadoingreso'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'radicadoingreso.store','method'=>'POST', 'id' => 'form-radicadoingreso'])!!}
    @endif

    <fieldset>
        <legend>Información de correspondencia recibida</legend>
    
        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('numeroRadicadoIngreso', 'Número', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                    <i class="fas fa-sort-numeric-down"></i>
                    </span>
                    {!!Form::text('numeroRadicadoIngreso',(isset($radicadoingreso) ? $radicadoingreso->numeroRadicadoIngreso : 'Automático'),['class'=>'form-control','placeholder'=>'Automático','readonly'])!!}
                    {!!Form::hidden('idRadicadoIngreso', null, array('id' => 'idRadicadoIngreso')) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('fechaRadicadoIngreso', 'Fecha', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                    <i class="far fa-calendar-alt"></i>
                    </span>
                    {!!Form::text('fechaRadicadoIngreso',(isset($radicadoingreso) ? $radicadoingreso->fechaRadicadoIngreso : date('Y-m-d H:i:s')),['class'=>'form-control','placeholder'=>'Automático','readonly'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('tipoRadicadoIngreso', 'Tipo', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                    <i class="fa fa-window-restore"></i>
                    </span>
                    {!!Form::select('tipoRadicadoIngreso',['Correspondencia recibida'=>'Correspondencia recibida','Factura'=>'Factura'],null,['class' => 'form-control','placeholder'=>'Seleccione un registro de la lista']) !!}
                </div>
            </div>
        </div>

    </fieldset>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#divRem">Remitente</a>
            </h4>
        </div>
        <div id="divRem" class="panel-collapse collapse">
            <div class="panel-body">

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('nombreRemitenteRadicadoIngreso', 'Nombre', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-file-signature"></i>
                            </span>
                            {!!Form::text('nombreRemitenteRadicadoIngreso',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('identificacionRemitenteRadicadoIngreso', 'Identificación', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fas fa-id-card"></i>
                            </span>
                            {!!Form::text('identificacionRemitenteRadicadoIngreso',null,['class'=>'form-control','placeholder'=>'Ingrese el número de identificación'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('asuntoRadicadoIngreso', 'Asunto', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fas fa-file-import"></i>
                            </span>
                            {!!Form::text('asuntoRadicadoIngreso',null,['class'=>'form-control','placeholder'=>'Ingrese el asunto'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('fechaDocumentoRadicadoIngreso', 'Fecha', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="far fa-calendar-alt"></i>
                            </span>
                            {!!Form::date('fechaDocumentoRadicadoIngreso',null,['class'=>'form-control','placeholder'=>'Año-Mes-Día'])!!}
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#divdes">Destinatario</a>
            </h4>
        </div>
        <div id="divdes" class="panel-collapse collapse">
            <div class="panel-body">

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('Users_idRadicadoIngreso', 'Usuario', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </span>
                            {!!Form::select('Users_idRadicadoIngreso',$user, (isset($radicadoingreso) ? $radicadoingreso->Users_idRadicadoIngreso : null),["class" => "select form-control", "placeholder" =>'Seleccione el usuario'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('Dependencia_idRadicadoIngreso', 'Dependencia', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fas fa-clipboard-list"></i>
                            </span>
                            {!!Form::select('Dependencia_idRadicadoIngreso',$dependencia, (isset($radicadoingreso) ? $radicadoingreso->Dependencia_idRadicadoIngreso : null),["class" => "select form-control", "placeholder" =>'Seleccione la dependencia'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('observacionRadicadoIngreso', 'Observación', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fas fa-align-left"></i>
                            </span>
                            {!!Form::text('observacionRadicadoIngreso',null,['class'=>'form-control','placeholder'=>'Ingrese las observacion'])!!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    

    @if(isset($radicadoingreso))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'radicadoingreso')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'radicadoingreso')"])!!}
    @endif 
     
    {!!Form::close()!!}

@stop