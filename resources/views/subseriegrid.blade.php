@extends('layouts.grid') 
@section('content')
{!!Form::model($datos)!!}

<script type="text/javascript">
    $(document).ready( function () 
    {
        configurarGrid('subserie',"{!! URL::to ('/subseriedata')!!}");
    }); 
</script>

<?php
    $visible = '';

    if(isset($datos[0])) 
    {
        $dato = get_object_vars($datos[0]);
        if ($dato['adicionarRolOpcion'] == 1) 
            $visible = 'inline-block;'; 
        else
            $visible = 'none;';
    }
    else
        $visible = 'none;';
?>

<legend>SubSeries</legend>
<table id="subserie" class="table table-striped table-bordered nowrap" style="width:100%">
    <thead class="btn-success">
        <tr>
            <th style="width:60px;padding: 1px 8px;" data-orderable="false">
                <a href="subserie/create"><span style="color:white; display: <?php echo $visible ?>" class="glyphicon glyphicon-plus"></span></a>
            </th>
            <th>ID</th>
            <th>Código</th>
            <th>Nombre</th>
            <th>Directorio</th>
            <th>Descripción</th>
        </tr>
    </thead>
    <tfoot class="btn-default active">
        <tr>
            <th>
                &nbsp;
            </th>
            <th>ID</th>
            <th>Código</th>
            <th>Nombre</th>
            <th>Directorio</th>
            <th>Descripción</th>
        </tr>
    </tfoot>
</table>

{!!Form::close()!!}
@stop