@extends('layouts.vista')

@section('content')
    @include('alerts.request')

    @if(isset($opcion))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($opcion,['route'=>['opcion.destroy',$opcion->idOpcion],'method'=>'DELETE', 'id' => 'form-opcion'])!!}
		@else
			{!!Form::model($opcion,['route'=>['opcion.update',$opcion->idOpcion],'method'=>'PUT', 'id' => 'form-opcion'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'opcion.store','method'=>'POST', 'id' => 'form-opcion'])!!}
    @endif

    <fieldset>
        <legend>Información de Opciones</legend>
        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('ordenOpcion', 'Orden', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-sort"></i>
                    </span>
                    {!!Form::number('ordenOpcion',null,['class'=>'form-control','placeholder'=>'Ingrese el orden'])!!}
                    {!!Form::hidden('idOpcion', null, array('id' => 'idOpcion')) !!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('nombreOpcion', 'Nombre', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::text('nombreOpcion',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('rutaOpcion', 'Ruta', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-map-marked-alt"></i>
                    </span>
                    {!!Form::text('rutaOpcion',null,['class'=>'form-control','placeholder'=>'Ingrese la ruta'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('Paquete_idPaquete', 'Paquete', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-cube"></i>
                    </span>
                    {!!Form::select('Paquete_idPaquete',$paquete, (isset($opcion) ? $opcion->Paquete_idPaquete : null),["class" => "select form-control", "placeholder" =>"- Seleccione el paquete -"])!!}
                </div>
            </div>
        </div>
    </fieldset>

    @if(isset($opcion))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'opcion')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'opcion')"])!!}
    @endif 
     
    {!!Form::close()!!}

@stop