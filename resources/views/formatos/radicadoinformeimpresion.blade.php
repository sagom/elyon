@extends('layouts.formato')

<title>Informe de Radicado</title>
@section('content')

{!!Form::model($consulta)!!}

<?php 

    function base64($archivo)
    {  
        $logo = '&nbsp;';
        $fp = fopen($archivo,"r", 0);
        if($archivo != '' and $fp)
        {
        $imagen = fread($fp,filesize($archivo));
        fclose($fp);

        $base64 = chunk_split(base64_encode($imagen));
        $logo =  '<img src="data:image/png;base64,' . $base64 .'" alt="Texto alternativo" width="130px"/>';
        }
        return $logo;
    }    

    $compania = \App\Compania::find(\Session::get('idCompania'));
    $img = base64($compania->rutaImagenCompania);

    $datosRadicado = array();
    
    for ($i = 0, $c = count($consulta); $i < $c; ++$i) 
    {
        $datosRadicado[$i] = (array) $consulta[$i];
    }  
?>

    <div class="form-group">
        <div class="div-responsive-doce">
            <div class="div-responsive-uno">
                <?php echo $img ?> 
            </div>
            <div class="div-responsive-once" style="text-align:center">
                <h1>Radicado</h1>
            </div>
        </div>
        
        <div class="div-responsive-doce table-responsive">
            <table class='table table-striped table-bordered' style='font-size:10pt'>
                <thead>
                    <tr>
                        <th>Número</th>
                        <th>Fecha de Radicado</th>
                        <th>Tipo de Radicado</th>
                        <th><?php echo $datosRadicado[0]['tipoRadicado'] == 'Correspondencia enviada' ? 'Destinatario' : 'Remitente' ?></th>
                        <th>Documento de Identificación</th>
                        <th>Asunto</th>
                        <th>Fecha de Documento</th>
                        <th>Usuario Interno</th>
                        <th>Dependencia</th>
                        <th>Observación</th>
                        <th>Usuario Radicador</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $cantidad = 0;
                    for ($i=0; $i < count($datosRadicado); $i++) 
                    { 
                        echo 
                        "<tr>
                            <td>".$datosRadicado[$i]['numeroRadicado']."</td>
                            <td>".$datosRadicado[$i]['fechaRadicado']."</td>
                            <td>".$datosRadicado[$i]['tipoRadicado']."</td>
                            <td>".$datosRadicado[$i]['terceroRadicado']."</td>
                            <td>".$datosRadicado[$i]['documentoTerceroRadicado']."</td>
                            <td>".$datosRadicado[$i]['asuntoRadicado']."</td>
                            <td>".$datosRadicado[$i]['fechaDocumentoRadicado']."</td>
                            <td>".$datosRadicado[$i]['nombreUsuarioRadicado']."</td>
                            <td>".$datosRadicado[$i]['nombreDependenciaRadicado']."</td>
                            <td>".$datosRadicado[$i]['observacionRadicado']."</td>
                            <td>".$datosRadicado[$i]['nombreUsuarioRadicador']."</td>
                        </tr>";

                        $cantidad = ($i+1);
                    }
                ?>
                </tbody>
                <tfoot>
                    <th colspan="11">Total de registros <?php echo $cantidad ?></th>
                </tfoot>
            </table>
        </div>
    </div>

    <?php
        if($tipoInforme == 2)
        {
            header('Content-type: application/vnd.ms-excel');
            header("Content-Disposition: attachment; filename=Informe de radcado.xls");
            header("Pragma: no-cache");
            header("Expires: 0");
        }
    ?>

    
{!!Form::close()!!}
@stop
