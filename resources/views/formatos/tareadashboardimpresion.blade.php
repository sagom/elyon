@extends('layouts.formato')

<title>Dashboard</title>
@section('content')

{!!Form::model($totalSolicitante)!!}

<?php 

    function base64($archivo)
    {  
        $logo = '&nbsp;';
        $fp = fopen($archivo,"r", 0);
        if($archivo != '' and $fp)
        {
            $imagen = fread($fp,filesize($archivo));
            fclose($fp);

            $base64 = chunk_split(base64_encode($imagen));
            $logo =  '<img src="data:image/png;base64,' . $base64 .'" alt="Texto alternativo" width="130px"/>';
        }
        return $logo;
    }    

    $compania = \App\Compania::find(\Session::get('idCompania'));
    $img = base64($compania->rutaImagenCompania);
?>

    <div class="form-group">
        <div class="div-responsive-doce">
            <div class="div-responsive-uno">
                <?php echo $img ?> 
            </div>
            <div class="div-responsive-once" style="text-align:center">
                <h1>Dashboard</h1>
            </div>
        </div>
        
        <div class="div-responsive-doce">
            
            <!-- SOLICITUDES -->
            <div class="div-responsive-doce">
                <div class="panel panel-success" >
                    <div class="panel-heading">
                        <i class="fa fa-pie-chart fa-fw"></i> Total solicitantes
                    </div>
                    <div class="panel-body" style="width: 100%; height: auto;">
                        <canvas style="width: 100%; height: auto;" id="divSolicitante"></canvas>
                    </div>
                </div>
            </div>

            <?php
                $arrayLabels = '[';
                $arrayDatos = '[';

                
                for ($i = 0, $c = count($totalSolicitante); $i < $c; ++$i) 
                {
                    $datosTotSolicitante = get_object_vars($totalSolicitante[$i]);
                    $arrayLabels .= "'".$datosTotSolicitante['nombreSolicitanteTarea']."',";
                    $arrayDatos .= $datosTotSolicitante['totalSolicitudes'].",";
                }  

                $arrayLabels = substr($arrayLabels,0,strlen($arrayLabels)-1);
                $arrayLabels .= "]";
                $arrayDatos = substr($arrayDatos,0,strlen($arrayDatos)-1);
                $arrayDatos .= "]";

                graficoBarra('divSolicitante', $arrayLabels, $arrayDatos);
            ?>

            <!-- RESPONSABLES -->
            <div class="div-responsive-doce">
                <div class="panel panel-success" >
                    <div class="panel-heading">
                        <i class="fa fa-pie-chart fa-fw"></i> Total responsables
                    </div>
                    <div class="panel-body" style="width: 100%; height: auto;">
                        <canvas style="width: 100%; height: auto;" id="divResponsable"></canvas>
                    </div>
                </div>
            </div>

            <?php
                $arrayLabels = '[';
                $arrayDatos = '[';

                
                for ($i = 0, $c = count($totalResponsable); $i < $c; ++$i) 
                {
                    $datosTotResponsable = get_object_vars($totalResponsable[$i]);
                    $arrayLabels .= "'".$datosTotResponsable['nombreResponsableTarea']."',";
                    $arrayDatos .= $datosTotResponsable['totalResponsable'].",";
                }  

                $arrayLabels = substr($arrayLabels,0,strlen($arrayLabels)-1);
                $arrayLabels .= "]";
                $arrayDatos = substr($arrayDatos,0,strlen($arrayDatos)-1);
                $arrayDatos .= "]";

                graficoBarra('divResponsable', $arrayLabels, $arrayDatos);
            ?>

            <!-- ESTADO -->
            <div class="div-responsive-doce">
                <div class="panel panel-success" >
                    <div class="panel-heading">
                        <i class="fa fa-pie-chart fa-fw"></i> Total estado
                    </div>
                    <div class="panel-body" style="width: 100%; height: auto;">
                        <canvas style="width: 100%; height: auto;" id="divEstado"></canvas>
                    </div>
                </div>
            </div>

            <?php
                $arrayLabels = '[';
                $arrayDatos = '[';

                
                for ($i = 0, $c = count($totalEstado); $i < $c; ++$i) 
                {
                    $datosTotEstado = get_object_vars($totalEstado[$i]);
                    $arrayLabels .= "'".$datosTotEstado['estadoTarea']."',";
                    $arrayDatos .= $datosTotEstado['totalEstado'].",";
                }  

                $arrayLabels = substr($arrayLabels,0,strlen($arrayLabels)-1);
                $arrayLabels .= "]";
                $arrayDatos = substr($arrayDatos,0,strlen($arrayDatos)-1);
                $arrayDatos .= "]";

                graficoBarra('divEstado', $arrayLabels, $arrayDatos);
            ?>

            <!-- DEPENDENCIA SOLICITANTE -->
            <div class="div-responsive-doce">
                <div class="panel panel-success" >
                    <div class="panel-heading">
                        <i class="fa fa-pie-chart fa-fw"></i> Total dependencia solicitante
                    </div>
                    <div class="panel-body" style="width: 100%; height: auto;">
                        <canvas style="width: 100%; height: auto;" id="divDepSolicitante"></canvas>
                    </div>
                </div>
            </div>

            <?php
                $arrayLabels = '[';
                $arrayDatos = '[';

                
                for ($i = 0, $c = count($totalDepSolicitante); $i < $c; ++$i) 
                {
                    $datosTotDepSolicitante = get_object_vars($totalDepSolicitante[$i]);
                    $arrayLabels .= "'".$datosTotDepSolicitante['nombreDependenciaSolicitante']."',";
                    $arrayDatos .= $datosTotDepSolicitante['totalDepSolicitante'].",";
                }  

                $arrayLabels = substr($arrayLabels,0,strlen($arrayLabels)-1);
                $arrayLabels .= "]";
                $arrayDatos = substr($arrayDatos,0,strlen($arrayDatos)-1);
                $arrayDatos .= "]";

                graficoBarra('divDepSolicitante', $arrayLabels, $arrayDatos);
            ?>

            <!-- DEPENDENCIA RESPONSABLE -->
            <div class="div-responsive-doce">
                <div class="panel panel-success" >
                    <div class="panel-heading">
                        <i class="fa fa-pie-chart fa-fw"></i> Total dependencia responsable
                    </div>
                    <div class="panel-body" style="width: 100%; height: auto;">
                        <canvas style="width: 100%; height: auto;" id="divDepResponsable"></canvas>
                    </div>
                </div>
            </div>

            <?php
                $arrayLabels = '[';
                $arrayDatos = '[';

                
                for ($i = 0, $c = count($totalDepResponsable); $i < $c; ++$i) 
                {
                    $datosTotDepResponsable = get_object_vars($totalDepResponsable[$i]);
                    $arrayLabels .= "'".$datosTotDepResponsable['nombreDependenciaResponsable']."',";
                    $arrayDatos .= $datosTotDepResponsable['totalDepResponsable'].",";
                }  

                $arrayLabels = substr($arrayLabels,0,strlen($arrayLabels)-1);
                $arrayLabels .= "]";
                $arrayDatos = substr($arrayDatos,0,strlen($arrayDatos)-1);
                $arrayDatos .= "]";

                graficoBarra('divDepResponsable', $arrayLabels, $arrayDatos);
            ?>

            <!-- CLASIFICACIOÓN -->
            <div class="div-responsive-doce">
                <div class="panel panel-success" >
                    <div class="panel-heading">
                        <i class="fa fa-pie-chart fa-fw"></i> Total clasificación
                    </div>
                    <div class="panel-body" style="width: 100%; height: auto;">
                        <canvas style="width: 100%; height: auto;" id="divClasificacion"></canvas>
                    </div>
                </div>
            </div>

            <?php
                $arrayLabels = '[';
                $arrayDatos = '[';

                
                for ($i = 0, $c = count($totalClasificacion); $i < $c; ++$i) 
                {
                    $datosTotClasificacion = get_object_vars($totalClasificacion[$i]);
                    $arrayLabels .= "'".$datosTotClasificacion['nombreClasificacionTarea']."',";
                    $arrayDatos .= $datosTotClasificacion['totalClasificacion'].",";
                }  

                $arrayLabels = substr($arrayLabels,0,strlen($arrayLabels)-1);
                $arrayLabels .= "]";
                $arrayDatos = substr($arrayDatos,0,strlen($arrayDatos)-1);
                $arrayDatos .= "]";

                graficoBarra('divClasificacion', $arrayLabels, $arrayDatos);
            ?>

        </div>
    </div>

<?php
    function graficoLinea($marco, $arrayLabels, $arrayDatos)
    {
        echo '
        <script type="text/javascript">
            var ch = document.getElementById("'.$marco.'").getContext("2d");
                    var data = 
                            {
                                labels: '.$arrayLabels.',
                                datasets: 
                                [
                                    {
                                        fillColor: "rgba(151,187,205,0.2)",
                                        strokeColor: "rgba(151,187,205,1)",
                                        pointColor: "rgba(151,187,205,1)",
                                        highlightFill: "rgba(220,220,220,0.75)",
                                        highlightStroke: "rgba(220,220,220,1)",
                                        data: '.$arrayDatos.'
                                    }
                                ]
                            };
                    var myLineChart = new Chart(ch).Line(data);

        </script>';
    }

    function graficoArea($marco, $arrayLabels, $arrayDatos)
    {
        echo '
        <script type="text/javascript">
            var ch = document.getElementById("'.$marco.'").getContext("2d");
                    var data = 
                            {
                                labels: '.$arrayLabels.',
                                datasets: 
                                [
                                    {
                                        fillColor: "rgba(151,187,205,0.2)",
                                        strokeColor: "rgba(151,187,205,1)",
                                        pointColor: "rgba(151,187,205,1)",
                                        highlightFill: "rgba(220,220,220,0.75)",
                                        highlightStroke: "rgba(220,220,220,1)",
                                        data: '.$arrayDatos.'
                                    }
                                ]
                            };
                    var myLineChart = new Chart(ch).Line(data);

        </script>';
    }


    function graficoBarra($marco, $arrayLabels, $arrayDatos)
    {
        echo '
        <script type="text/javascript">
            
            var chrt = document.getElementById("'.$marco.'").getContext("2d");
                    var data = {
                        labels: '.$arrayLabels.',
                        datasets: [
                            {
                                fillColor: "rgba(220,120,220,0.8)",
                                strokeColor: "rgba(220,120,220,0.8)",
                                highlightFill: "rgba(220,220,220,0.75)",
                                highlightStroke: "rgba(220,220,220,1)",
                                data: '.$arrayDatos.',
                            }
                        ]
                    };
                    var myFirstChart = new Chart(chrt).Bar(data);

            </script>';
    }

    function graficoDona($marco, $arrayDatos)
    {
        echo '
            <script type="text/javascript">
            var ctx = $("#'.$marco.'").get(0).getContext("2d");

                    var data = 
                        '.$arrayDatos.';

                    //draw
                    var piechart = new Chart(ctx).Doughnut(data);
                
            </script>';
    }
?>
    
{!!Form::close()!!}
@stop
