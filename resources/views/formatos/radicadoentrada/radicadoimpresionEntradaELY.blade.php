@extends('layouts.formato')

<title>Impresion de Radicado</title>
@section('content')

{!!Form::model($radicado)!!}

<style type="text/css">
    .responsive {
        /* width: 100%; */
        max-width: 4.5cm;
        max-height: 4cm;
        /* height: auto; */
    }
</style>

<?php

    $datosRadicado = get_object_vars($radicado[0]);

    function base64($archivo)
    {
        $logo = '&nbsp;';
        $fp = fopen($archivo,"r", 0);
        if($archivo != '' and $fp)
        {
            $imagen = fread($fp,filesize($archivo));
            fclose($fp);

            $base64 = chunk_split(base64_encode($imagen));
            $logo =  '<img src="data:image/jpg;base64,' . $base64 .'" alt="Texto alternativo" class="responsive" style="top: 0;"/>';
        }

        return $logo;
    }

    $compania = \App\Compania::find(\Session::get('idCompania'));
    $img = base64($compania->rutaImagenCompania);

?>
    <div style="width:8.5cm; height:2.5cm; font-size:6.5pt; margin:2% 0% 0% 10%;">
        <div style="width:5.0cm; display:inline-block;">
            <p><b style="font-size:8pt">UNIDAD DE CORRESPONDENCIA</b><p>
            <b>Fecha/Hora: </b><?php echo $datosRadicado['fechaRadicado']?><br>
            <?php
                echo '<b>Origen: </b>'.$datosRadicado['terceroRadicado']."<br>";
                echo '<b>Destino </b>'.$datosRadicado['nombreDependencia']."<br>";
            ?>
            <b>Asunto: </b><?php echo $datosRadicado['asuntoRadicado']?><br>
            <b>Responsable: </b><?php echo $datosRadicado['name']?><br>
        </div>
        <div style="width:2.5cm; display:inline-block; text-align:center">
            <?php echo $img?>
            <br><br>
            <p><b style="font-size:5.9pt">Radicado No. R <?php echo $datosRadicado['numeroRadicado']?></b></p>
            <b style="font-size:5pt">Documento sujeto a verificación.</b>
        </div>
    </div>


{!!Form::close()!!}
@stop
