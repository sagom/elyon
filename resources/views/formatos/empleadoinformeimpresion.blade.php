@extends('layouts.formato')

<title>Informe de empleado</title>
@section('content')

{!!Form::model($consulta)!!}

<?php 

    function base64($archivo)
    {  
        $logo = '&nbsp;';
        $fp = fopen($archivo,"r", 0);
        if($archivo != '' and $fp)
        {
        $imagen = fread($fp,filesize($archivo));
        fclose($fp);

        $base64 = chunk_split(base64_encode($imagen));
        $logo =  '<img src="data:image/png;base64,' . $base64 .'" alt="Texto alternativo" width="130px"/>';
        }
        return $logo;
    }    

    $compania = \App\Compania::find(\Session::get('idCompania'));
    $img = base64($compania->rutaImagenCompania);

    $datosEmpleado = array();
    
    for ($i = 0, $c = count($consulta); $i < $c; ++$i) 
    {
        $datosEmpleado[$i] = (array) $consulta[$i];
    }  
?>

    <div class="form-group">
        <div class="div-responsive-doce">
            <div class="div-responsive-uno">
                <?php echo $img ?> 
            </div>
            <div class="div-responsive-once" style="text-align:center">
                <h1>Empleado</h1>
            </div>
        </div>
        
        <div class="div-responsive-doce table-responsive">
            <table class='table table-striped table-bordered' style='font-size:10pt'>
                <thead>
                    <tr>
                        <th>Documento</th>
                        <th>Nombre</th>
                        <th>Estado</th>
                        <th>Centro de costo</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $cantidad = 0;
                    for ($i=0; $i < count($datosEmpleado); $i++) 
                    { 
                        echo 
                        "<tr>
                            <td>".$datosEmpleado[$i]['documentoEmpleado']."</td>
                            <td>".$datosEmpleado[$i]['nombreCompletoEmpleado']."</td>
                            <td>".$datosEmpleado[$i]['estadoEmpleado']."</td>
                            <td>".$datosEmpleado[$i]['nombreCentroCosto']."</td>
                        </tr>";

                        $cantidad = ($i+1);
                    }
                ?>
                </tbody>
                <tfoot>
                    <th colspan="4">Total de registros <?php echo $cantidad ?></th>
                </tfoot>
            </table>
        </div>
    </div>

    <?php
        if($tipoInforme == 2)
        {
            header('Content-type: application/vnd.ms-excel');
            header("Content-Disposition: attachment; filename=Informe de radcado.xls");
            header("Pragma: no-cache");
            header("Expires: 0");
        }
    ?>

    
{!!Form::close()!!}
@stop
