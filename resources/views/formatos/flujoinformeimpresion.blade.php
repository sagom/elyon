@extends('layouts.formato')

<title>Informe de Radicado</title>
@section('content')

    {!! Form::model($consulta) !!}

    <?php
    
    function base64($archivo)
    {
        $logo = '&nbsp;';
        $fp = fopen($archivo, 'r', 0);
        if ($archivo != '' and $fp) {
            $imagen = fread($fp, filesize($archivo));
            fclose($fp);
    
            $base64 = chunk_split(base64_encode($imagen));
            $logo = '<img src="data:image/png;base64,' . $base64 . '" alt="Texto alternativo" width="130px"/>';
        }
        return $logo;
    }
    
    $compania = \App\Compania::find(\Session::get('idCompania'));
    $img = base64($compania->rutaImagenCompania);
    
    $datosFlujo = [];
    $datosMetadatos = [];
    
    for ($i = 0, $c = count($consulta); $i < $c; ++$i) {
        $datosFlujo[$i] = (array) $consulta[$i];
    }
    
    for ($i = 0, $c = count($metadatos); $i < $c; ++$i) {
        $datosMetadatos[$i] = (array) $metadatos[$i];
    }
    ?>

    <div class="form-group">
        <div class="div-responsive-doce">
            <div class="div-responsive-uno">
                <?php echo $img; ?>
            </div>
            <div class="div-responsive-once" style="text-align:center">
                <h1>Flujo</h1>
            </div>
        </div>

        <div class="div-responsive-doce table-responsive">
            <table class='table table-striped table-bordered' style='font-size:10pt'>
                <thead>
                    <tr>
                        <th>Flujo</th>
                        <th>Tarea</th>
                        <th>Usuario</th>
                        <th>Estado del flujo</th>
                        <th>Flujo manual</th>
                        <th>Fecha tarea</th>
                        <th>Estado de la tarea</th>
                        <th>Observación tarea</th>
                        @foreach ($metadatos as $item)
                            <th>{{ $item->nombreMetadato }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    <?php
                    for ($i = 0; $i < count($datosFlujo); $i++) {
                        echo "<tr>
                                                                                                                                                                                                                <td>" .
                            $datosFlujo[$i]['nombreFlujo'] .
                            "</td>
                                                                                                                                                                                                                <td>" .
                            $datosFlujo[$i]['nombreFlujoTarea'] .
                            "</td>
                                                                                                                                                                                                                <td>" .
                            $datosFlujo[$i]['name'] .
                            "</td>
                                                                                                                                                                                                                <td>" .
                            $datosFlujo[$i]['estadoFlujoTareaRadicado'] .
                            "</td>
                                                                                                                                                                                                                <td>" .
                            $datosFlujo[$i]['nombreFlujoManual'] .
                            "</td>
                                                                                                                                                                                                                <td>" .
                            $datosFlujo[$i]['fechaFlujoTareaRadicadoComentario'] .
                            "</td>
                                                                                                                                                                                                                <td>" .
                            $datosFlujo[$i]['estadoFlujoTareaRadicadoComentario'] .
                            "</td>
                                                                                                                                                                                                                <td>" .
                            $datosFlujo[$i]['observacionFlujoTareaRadicadoComentario'] .
                            '</td>';
                        for ($j = 0; $j < count($datosMetadatos); $j++) {
                            echo '<td>' . $datosFlujo[$i][$datosMetadatos[$j]['nombreMetadato']] . '</td>';
                        }
                        ".
                                                                                                                                                                                                            </tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php
    if ($tipoInforme == 2) {
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename=Informe de flujo.xls');
        header('Pragma: no-cache');
        header('Expires: 0');
    }
    ?>


    {!! Form::close() !!}
@stop
