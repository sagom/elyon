@extends('layouts.formato')

<title>Impresion de Radicado</title>
@section('content')

{!!Form::model($radicado)!!}

<style type="text/css">

    H1.SaltoDePagina
    {
        PAGE-BREAK-AFTER: always
    }

    body { font: 0.9em Century Gothic; background-color:#FFF;}
    td {font: 1.2em Century Gothic;}
    label {font: 1.2em Century Gothic; font-weight: bold;}

    hr { 
        display: block;
        margin-top: 0.5em;
        margin-bottom: 0.5em;
        border-width: 3px;
        color: #f20909;
        
    }
</style>

<?php 

    $datosRadicado = get_object_vars($radicado[0]);
    $margin = ($datosRadicado['tipoRadicado'] != 'Correspondencia enviada' ? 'margin-top:-30%;' : '');

    function base64($archivo)
    {
        $logo = '&nbsp;';
        $fp = fopen($archivo,"r", 0);
        if($archivo != '' and $fp)
        {
            $imagen = fread($fp,filesize($archivo));
            fclose($fp);

            $base64 = chunk_split(base64_encode($imagen));
            $logo =  '<img src="data:image/jpg;base64,' . $base64 .'" alt="Texto alternativo" style="width:120%"/>';
        }

        return $logo;
    }    

    $compania = \App\Compania::find(\Session::get('idCompania'));
    $img = base64($compania->rutaImagenCompania);

    $margin = '';
    $vigilado = '';
    
    if($compania->codigoCompania == 'FUBA')
    {
        $margin = 'margin: 2%;';
        $vigilado = '<p style="font-size:5pt"><b>Vigilado Mineducación</b></p>';
    }
?>
    <div style="width:8.5cm; height:2.5cm; font-size:6.5pt; margin:0% 0% 0% 0%;">
        <div style="width:5.0cm; display:inline-block;">
            <b style="font-size:7.5pt">UNIDAD DE CORRESPONDENCIA</b>
            <!-- <b>Tipo de Radicado: </b><?php echo $datosRadicado['tipoRadicado']?><br> -->
            <b>Fecha/Hora: </b><?php echo $datosRadicado['fechaRadicado']?><br>
            <?php $tercero = ($datosRadicado['tipoRadicado'] == 'Correspondencia enviada' ? 
            '<b>Destino: </b>'.$datosRadicado['terceroRadicado']."<br>" : 
            '<b>Origen: </b>'.$datosRadicado['terceroRadicado']."<br>");
            echo $tercero ?>
            <?php $tercero = ($datosRadicado['tipoRadicado'] != 'Correspondencia enviada' ? 
            '<b>Destino: </b>'.$datosRadicado['nombreDependencia']."<br>" : 
            '<b>Origen: </b>'.$datosRadicado['nombreDependencia']."<br>");
            echo $tercero ?>
            <b>Asunto: </b><?php echo $datosRadicado['asuntoRadicado']?><br>
            <b>Responsable: </b><?php echo $datosRadicado['name']?><br>
        </div>
        <div style="width:2.5cm; display:inline-block; text-align:center">
            <?php echo $img.$vigilado?>
            
            <b style="font-size:6pt">Radicado No. <?php echo ($datosRadicado['tipoRadicado'] == 'Correspondencia enviada' ? 'E' : 'R').$datosRadicado['numeroRadicado']?></b><br>
            <?php 
                $verificacion = ($datosRadicado['tipoRadicado'] == 'Correspondencia enviada' ? 
                '' :
                '<p style="font-size:5pt"><b>Documento sujeto a verificación.</b></p>');
                echo $verificacion 
            ?>
        
        </div>
    </div>


{!!Form::close()!!}
@stop
