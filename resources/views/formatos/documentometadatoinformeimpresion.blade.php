@extends('layouts.formato')

<title>Informe de Radicado</title>
@section('content')

{!!Form::model($consulta)!!}

<?php

    function base64($archivo)
    {
        $logo = '&nbsp;';
        $fp = fopen($archivo,"r", 0);
        if($archivo != '' and $fp)
        {
        $imagen = fread($fp,filesize($archivo));
        fclose($fp);

        $base64 = chunk_split(base64_encode($imagen));
        $logo =  '<img src="data:image/png;base64,' . $base64 .'" alt="Texto alternativo" width="130px"/>';
        }
        return $logo;
    }

    $compania = \App\Compania::find(\Session::get('idCompania'));
    $img = base64($compania->rutaImagenCompania);

    $datosDocumentoMetadato = array();

    for ($i = 0, $c = count($consulta); $i < $c; ++$i)
    {
        $datosDocumentoMetadato[$i] = (array) $consulta[$i];
    }
?>

    <div class="form-group">
        <div class="div-responsive-doce">
            <div class="div-responsive-uno">
                <?php echo $img ?>
            </div>
            <div class="div-responsive-once" style="text-align:center">
                <h1>Documento metadato</h1>
            </div>
        </div>

        <div class="div-responsive-doce table-responsive">
            <table class='table table-striped table-bordered' style='font-size:10pt'>
                <thead>
                    <tr>
                        <th>Publicación</th>
                        <th>Fecha publicación</th>
                        <th>Documento</th>
                        <?php
                            for ($i=0; $i < count($columnasEncabezadoMetadatos) ; $i++) {
                                echo "<th>".$columnasEncabezadoMetadatos[$i]."</th>";
                            }
                        ?>
                    </tr>
                </thead>
                <tbody>
                <?php
                    for ($i=0; $i < count($datosDocumentoMetadato); $i++)
                    {
                        echo
                        "<tr>
                            <td>".$datosDocumentoMetadato[$i]['codigoPublicacion']."</td>
                            <td>".$datosDocumentoMetadato[$i]['fechaPublicacionVersion']."</td>
                            <td>".$datosDocumentoMetadato[$i]['nombreDocumento']."</td>";

                            for ($j=0; $j < count($columnasCuerpoMetadatos); $j++) {
                                echo "<td>".$datosDocumentoMetadato[$i]["$columnasCuerpoMetadatos[$j]"]."</td>";
                            }

                        echo "
                        </tr>";
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php
        if($tipoInforme == 2)
        {
            header('Content-type: application/vnd.ms-excel');
            header("Content-Disposition: attachment; filename=Informe de documentos metadatos.xls");
            header("Pragma: no-cache");
            header("Expires: 0");
        }
    ?>


{!!Form::close()!!}
@stop
