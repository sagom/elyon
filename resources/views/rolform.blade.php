@extends('layouts.vista')

@section('content')
    {!!Html::script('js/rol.js'); !!} 
    @include('alerts.request')

    @if(isset($rol))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($rol,['route'=>['rol.destroy',$rol->idRol],'method'=>'DELETE', 'id' => 'form-rol'])!!}
		@else
			{!!Form::model($rol,['route'=>['rol.update',$rol->idRol],'method'=>'PUT', 'id' => 'form-rol'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'rol.store','method'=>'POST', 'id' => 'form-rol'])!!}
    @endif

    <fieldset>
        <legend>Información de Rol</legend>
        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('codigoRol', 'Código', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!!Form::hidden('idRol', null, array('id' => 'idRol')) !!}
                    {!!Form::hidden('eliminarOpcion', null, array('id' => 'eliminarOpcion')) !!}
                    {!!Form::text('codigoRol',null,['class'=>'form-control','placeholder'=>'Ingrese el código'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('nombreRol', 'Nombre', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::text('nombreRol',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre'])!!}
                </div>
            </div>
        </div>
    </fieldset>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#divOpciones">Opciones del rol</a>
            </h4>
        </div>
        <div id="divOpciones" class="panel-collapse">
            <div class="panel-body">
                <div class="form-group" id='test'>
                    <div class="col-sm-12">
                        <div class="row show-grid">
                            <div style="overflow:auto; height:170px;">
                                <div style="width: 1300px; display: inline-block;">
                                    <div class="btn-default active col-md-1" onclick="opcion.agregarCampos(valorOpcion,'A')" style="width:40px; height:40px; cursor:pointer; display:inline-block">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </div>
                                    <div class="btn-default active col-md-1" style="width:300px; height:40px; display:inline-block">Opcion</div>
                                    <div class="btn-default active col-md-1" style="width:100px; height:40px; display:inline-block">Adicionar</div>
                                    <div class="btn-default active col-md-1" style="width:100px; height:40px; display:inline-block">Modificar</div>
                                    <div class="btn-default active col-md-1" style="width:100px; height:40px; display:inline-block">Eliminar</div>
                                    <div class="btn-default active col-md-1" style="width:100px; height:40px; display:inline-block">Imprimir</div>
                                    <div class="btn-default active col-md-1" style="width:100px; height:40px; display:inline-block">Aprobar</div>
                                </div>

                                <div id="contenedoropciones">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    


    @if(isset($rol))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'rol')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'rol')"])!!}
    @endif 
     
    {!!Form::close()!!}

    <script>
        var idOpcion = '<?php echo isset($idOpcion) ? $idOpcion : "";?>';
        var nombreOpcion = '<?php echo isset($nombreOpcion) ? $nombreOpcion : "";?>';

        var valoropcion = [JSON.parse(idOpcion), JSON.parse(nombreOpcion)];

        var opciones = '<?php echo (isset($rolopcion) ? json_encode($rolopcion) : 0);?>';
        opciones = (opciones != '' ? JSON.parse(opciones) : '');
        var valorOpcion = ['','',1,1,1,1,1];
    </script>

@stop