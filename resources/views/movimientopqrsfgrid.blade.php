@extends('layouts.grid') 
@section('content')
{!!Form::model($datos)!!}

<script type="text/javascript">
    $(document).ready( function () 
    {
        configurarGrid('movimientopqrsf',"{!! URL::to ('/movimientopqrsfdata')!!}");
    }); 
</script>

<legend>PQRSF</legend>
<table id="movimientopqrsf" class="table table-striped table-bordered nowrap" style="width:100%">
    <thead class="btn-success">
        <tr>
            <th style="width:60px;padding: 1px 8px;" data-orderable="false">
                &nbsp;
            </th>
            <th>ID</th>
            <th>Nombre solicitante</th>
            <th>Fecha solicitud</th>
            <th>Correo solicitante</th>
            <th>Tipo</th>
            <th>Mensaje</th>
            <th>Respuesta</th>
            <th>Fecha respuesta</th>
            <th>Usuario</th>
        </tr>
    </thead>
    <tfoot class="btn-default active">
        <tr>
            <th>
                &nbsp;
            </th>
            <th>ID</th>
            <th>Nombre solicitante</th>
            <th>Fecha solicitud</th>
            <th>Correo solicitante</th>
            <th>Tipo</th>
            <th>Mensaje</th>
            <th>Respuesta</th>
            <th>Fecha respuesta</th>
            <th>Usuario</th>
        </tr>
    </tfoot>
</table>

{!!Form::close()!!}
@stop