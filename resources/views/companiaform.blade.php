@extends('layouts.vista')

@section('content')
    @include('alerts.request')

    @if(isset($compania))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($compania,['route'=>['compania.destroy',$compania->idCompania],'method'=>'DELETE', 'id' => 'form-compania'])!!}
		@else
			{!!Form::model($compania,['route'=>['compania.update',$compania->idCompania],'method'=>'PUT', 'id' => 'form-compania', 'files' => 'true'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'compania.store','method'=>'POST', 'id' => 'form-compania', 'files' => 'true'])!!}
    @endif

    <fieldset>
        <legend>Información de Compañía</legend>
        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('codigoCompania', 'Código', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!!Form::text('codigoCompania',null,['class'=>'form-control','placeholder'=>'Ingrese el código'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('nombreCompania', 'Nombre', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::text('nombreCompania',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('rutaImagenCompania', 'Logo', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file"></i>
                    </span>
                    <input id="rutaImagenCompania" name="rutaImagenCompania" type="file" >
                </div>
            </div>
        </div>

    </fieldset>
    

    @if(isset($compania))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'compania')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'compania')"])!!}
    @endif 
     
    {!!Form::close()!!}

    <script>

        $('#rutaImagenCompania').fileinput({
            language: 'es',
            uploadUrl: '#',
            allowedFileExtensions : ['jpg','png','gif','pdf'],
                initialPreview: [
                '<?php if(isset($compania->rutaImagenCompania))
                        echo Html::image($compania->rutaImagenCompania,"Imagen no encontrada",array("style"=>"width:148px;height:158px;"));
                                            ;?>'
            ],
            dropZoneTitle: 'Seleccione su foto',
            removeLabel: '',
            uploadLabel: '',
            browseLabel: '',
            uploadClass: "",
            uploadLabel: "",
            uploadIcon: "",
        });

    </script>

@stop