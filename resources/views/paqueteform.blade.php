@extends('layouts.vista')

@section('content')
    @include('alerts.request')

    @if(isset($paquete))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($paquete,['route'=>['paquete.destroy',$paquete->idPaquete],'method'=>'DELETE', 'id' => 'form-paquete'])!!}
		@else
			{!!Form::model($paquete,['route'=>['paquete.update',$paquete->idPaquete],'method'=>'PUT', 'id' => 'form-paquete'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'paquete.store','method'=>'POST', 'id' => 'form-paquete'])!!}
    @endif

    <fieldset>
        <legend>Información de Paquete</legend>
        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('ordenPaquete', 'Orden', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-sort"></i>
                    </span>
                    {!!Form::hidden('idPaquete', null, array('id' => 'idPaquete')) !!}
                    {!!Form::text('ordenPaquete',null,['class'=>'form-control','placeholder'=>'Ingrese el orden'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('iconoPaquete', 'Icono', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-image"></i>
                    </span>
                    {!!Form::text('iconoPaquete',null,['class'=>'form-control','placeholder'=>'Ingrese la clase del icono'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('nombrePaquete', 'Nombre', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::text('nombrePaquete',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre'])!!}
                </div>
            </div>
        </div>
    </fieldset>
    

    @if(isset($paquete))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'paquete')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'paquete')"])!!}
    @endif 
     
    {!!Form::close()!!}

@stop