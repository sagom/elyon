@extends('layouts.modal')

	{!!Form::open(['route'=>'publicacionce.store','method'=>'POST', 'id' => 'form-publicacion'])!!}

    <div class="panel-body">
        <div class="form-group" id="test">
            <div class="div-responsive-doce">

                <?php

                    $datos = array();
                    // por facilidad de manejo convierto el stdclass a tipo array con un cast (array)
                    for ($i = 0, $c = count($documentos); $i < $c; ++$i) 
                    {
                        $datos[$i] = (array) $documentos[$i];
                    }

                    $estructura = '';
                    $i = 0;
                    $total = count($documentos);

                    while($i < $total)
                    {
                        $publicacion = $datos[$i]['idPublicacion'];

                        $nombreArch = explode('/', $datos[$i]['rutaImagenPublicacionVersion']);
                        
                        $estructura .= 
                            '<div class="panel panel-success div-responsive-cuatro">
                                <div class="panel-body">
                                    <div class="div-responsive-doce">
                                        <center><i onclick="cargarVistaPrevia(\''.$datos[$i]['rutaImagenPublicacionVersion'].'\','.$datos[$i]['descargarDependenciaRol'].','.$datos[$i]['imprimirDependenciaRol'].','.$datos[$i]['enviarDependenciaRol'].','.$datos[$i]['eliminarDependenciaRol'].','.$datos[$i]['versionDocumento'].','.$datos[$i]['idPublicacion'].')" class="fa fa-book fa-5x" style="color: green; cursor:pointer"></i><br><br>
                                        '.substr($nombreArch[6], 10).'</center>
                                    </div>  
                                </div>
                            </div>';
                        
                        while($i < $total && $publicacion == $datos[$i]['idPublicacion'])
                        {
                            $i++;
                        }
                    }

                    echo $estructura;
                ?>

            </div>
        </div>
    </div>

{!!Form::close()!!}
<script>
    function cargarVistaPrevia(file, descargar, imprimir, enviar, eliminar, version, publicacion)
    {
        window.parent.$("#idPublicacion").val(publicacion);
        window.parent.$("#rutaImagenPublicacionVersion").val(file);
        window.parent.$('#modalConsulta').css('display','block');
        botones = '';
        nombreArchivo = file.split('/');

        if(descargar == 1)
            botones +='<a href="http://'+location.host+'/'+file+'" download="'+nombreArchivo[6]+'"><button type="button" class="btn btn-success fa fa-download"></button></a>&nbsp;&nbsp;&nbsp;';

        if(imprimir == 1)
            botones +='<button type="button" class="btn btn-success fa fa-print" onclick="imprimirPublicacion(\''+file+'\')"></button>&nbsp;&nbsp;&nbsp;';

        if(enviar == 1)
            botones +='<button type="button" class="btn btn-success fa fa-envelope" onclick="abrirModalCorreo(\''+file+'\')"></button>&nbsp;&nbsp;&nbsp;';

        if(eliminar == 1)
            botones +='<button type="button" class="btn btn-success fa fa-trash" onclick="eliminarPublicacion('+publicacion+')"></button>&nbsp;&nbsp;&nbsp;';

        if(version == 1)
            botones +='<button type="button" class="btn btn-success fa fa-file-upload" onclick="cargarVersionPublicacion('+publicacion+')"></button>&nbsp;&nbsp;&nbsp;';

        window.parent.$("#botonesDocumento").html(botones);
        
        url = 'http://'+location.host+'/'+file;
        
        var iframe = window.parent.$("#iframeImagen");
        iframe.attr('src',''); 
        if (iframe.length) 
        {
            iframe.attr('src',url);
        }

        consultarMetadatos(publicacion);
    }

    function consultarMetadatos(idPublicacion)
    {
        var token = window.parent.document.getElementById('token').value;
        $.ajax({
            headers: {'X-CSRF-TOKEN': token},
            dataType: "json",
            data: {idPublicacion: idPublicacion},
            url:   'http://'+location.host+'/consultarMetadatos',
            type:  'post',
            beforeSend: function(){
                abrirDistractor();
            },
            success: function(respuesta){
                cerrarDistractor();
                
                window.parent.$("#datosDocumento").html(respuesta);
            },
            error: function(xhr,err)
            { 
                cerrarDistractor();
                mensajeAlerta("guardar", "Error", "Ha ocurrido un problema consultando los metadatos de la publicación", "red", "fa fa-times", "10000");
            }
        });
    }
</script>
