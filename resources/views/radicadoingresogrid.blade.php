@extends('layouts.grid') 
@section('content')
{!!Form::model($datos)!!}
{!!Html::script('js/radicadoingreso.js'); !!}

<script type="text/javascript">
    $(document).ready( function () 
    {
        configurarGrid('radicadoingreso',"{!! URL::to ('/radicadoingresodata')!!}");
    }); 
</script>

<?php
    $visible = '';

    if(isset($datos[0])) 
    {
        $dato = get_object_vars($datos[0]);
        if ($dato['adicionarRolOpcion'] == 1) 
            $visible = 'inline-block;'; 
        else
            $visible = 'none;';
    }
    else
        $visible = 'none;';
?>

<legend>Correspondencia recibida</legend>
<table id="radicadoingreso" class="table table-striped table-bordered nowrap" style="width:100%">
    <thead class="btn-success">
        <tr>
            <th onclick="abrirModalInicial()" style="width:100px;padding: 1px 8px; cursor:pointer" data-orderable="false">
                <a ><span style="color:white; display: <?php echo $visible ?>" class="glyphicon glyphicon-plus"></span></a>
            </th>
            <th>ID</th>
            <th>Número</th>
            <th>Fecha</th>
            <th>Tipo</th>
            <th>Nombre remitente</th>
            <th>Identificación remitente</th>
            <th>Asunto</th>
            <th>Fecha documento</th>
            <th>Usuario</th>
            <th>Dependencia</th>
            <th>Compañía</th>
            <th>Observación</th>
        </tr>
    </thead>
    <tfoot class="btn-default active">
        <tr>
            <th>
                &nbsp;
            </th>
            <th>ID</th>
            <th>Número</th>
            <th>Fecha</th>
            <th>Tipo</th>
            <th>Nombre remitente</th>
            <th>Identificación remitente</th>
            <th>Asunto</th>
            <th>Fecha documento</th>
            <th>Usuario</th>
            <th>Dependencia</th>
            <th>Compañía</th>
            <th>Observación</th>     
        </tr>
    </tfoot>
</table>

    <div id="modalRadicado" class="modalDialog" style="display:none;">
        <div class="modal-content" style="width:80%; height:80%; overflow:auto;">
            <div class="modal-header">
                <a onclick="cerrarModal()" title="Cerrar" class="close">X</a>
                <h4 class="modal-title">Digite la información básica</h4>
            </div>
            <div class="modal-body" style="width:100%; height:100%; ">

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('tipoRadicadoIngreso', 'Tipo', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-window-restore"></i>
                            </span>
                            {!!Form::select('tipoRadicadoIngreso',['Correspondencia recibida'=>'Correspondencia recibida','Factura'=>'Factura'],null,['class' => 'form-control','placeholder'=>'Seleccione un registro de la lista']) !!}
                            <input type="hidden" id="token" value="{{csrf_token()}}"/>
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('asuntoRadicadoIngreso', 'Asunto', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fas fa-file-import"></i>
                            </span>
                            {!!Form::text('asuntoRadicadoIngreso',null,['class'=>'form-control','placeholder'=>'Ingrese el asunto'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('nombreRemitenteRadicadoIngreso', 'Remitente', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-file-signature"></i>
                            </span>
                            {!!Form::text('nombreRemitenteRadicadoIngreso',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del remitente'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('identificacionRemitenteRadicadoIngreso', 'Identificación', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fas fa-id-card"></i>
                            </span>
                            {!!Form::text('identificacionRemitenteRadicadoIngreso',null,['class'=>'form-control','placeholder'=>'Ingrese la identificación'])!!}
                        </div>
                    </div>
                </div>

                <div class="div-responsive-doce div-agrupador">
                    {!!Form::label('Dependencia_idRadicadoIngreso', 'Destino', array('class' => 'label-responsive-dos')) !!}
                    <div class="div-input-responsive">
                        <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fas fa-clipboard-list"></i>
                            </span>
                            {!!Form::select('Dependencia_idRadicadoIngreso',$dependencia, null,["class" => "select form-control", "placeholder" =>'Seleccione la dependencia'])!!}
                        </div>
                    </div>
                </div>
                
                {!!Form::button('Guardar',["class"=>"btn btn-success", "onclick" => "guardarDatosIniciales()"])!!}

            </div>
        </div>
    </div>

{!!Form::close()!!}
@stop

<div id="modalPublicacion" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:80%; height:80%;">
        <!-- Modal content-->
        <div  class="modal-content" style="width:100%; height:100%; ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Digitalizar radicado</h4>
            </div>
            <div class="modal-body" style="width:100%; height:100%; overflow:auto;">
                <div id="divPublicacion"></div>
            </div>
        </div>
    </div>
</div>

<div id="modalFlujo" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:80%; height:80%;">
        <!-- Modal content-->
        <div  class="modal-content" style="width:100%; height:100%; ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Seleccionar flujo</h4>
            </div>
            <div class="modal-body" style="width:100%; height:100%; overflow:auto;">
                <div id="divFlujo"></div>
            </div>
        </div>
    </div>
</div>