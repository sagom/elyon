@extends('layouts.vista')

@section('content')
    @include('alerts.request')

    @if(isset($serie))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($serie,['route'=>['serie.destroy',$serie->idSerie],'method'=>'DELETE', 'id' => 'form-serie'])!!}
		@else
			{!!Form::model($serie,['route'=>['serie.update',$serie->idSerie],'method'=>'PUT', 'id' => 'form-serie'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'serie.store','method'=>'POST', 'id' => 'form-serie'])!!}
    @endif

    <fieldset>
        <legend>Información de Serie</legend>
        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('codigoSerie', 'Código', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!!Form::hidden('idSerie', null, array('id' => 'idSerie')) !!}
                    {!!Form::text('codigoSerie',null,['class'=>'form-control','placeholder'=>'Ingrese el código'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('nombreSerie', 'Nombre', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::text('nombreSerie',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('directorioSerie', 'Directorio', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-folder-open"></i>
                    </span>
                    {!!Form::text('directorioSerie',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del directorio', 'onkeyup' => 'replaceSpacesWithUnderLine(this.value, this.id)', 'id' => 'directorioSerie'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('descripcionSerie', 'Descripción', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-book"></i>
                    </span>
                    {!!Form::text('descripcionSerie',null,['class'=>'form-control','placeholder'=>'Ingrese la descripción'])!!}
                </div>
            </div>
        </div>
    </fieldset>


    @if(isset($serie))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'serie')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'serie')"])!!}
    @endif

    {!!Form::close()!!}

    <script>
        serierelacion = '<?php echo isset($serie) ? $serie->Serie_idSerie : "";?>';

        if(serierelacion != '' && serierelacion == 1)
        {
            $("#directorioSerie").attr('readonly', true);
        }
    </script>

@stop