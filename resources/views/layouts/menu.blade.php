<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="image/x-icon" rel="icon" href="{!!('storage/Elyon.ico')!!}">

        <!-- Bootstrap -->
        {!!Html::style('bookstores/bootstrap/css/bootstrap.min.css'); !!}
        {!!Html::style('bookstores/bootstrap/css/bootstrap-theme.min.css'); !!}
        {!!Html::style('css/menu.css'); !!}
        {!!Html::style('css/modal.css'); !!}
        {!!Html::style('css/verticaltab.css'); !!}

        <!-- Choosen -->
        {!!Html::style('bookstores/choosen/chosen.css'); !!}

        <!-- Fontawesome -->
        {!!Html::style('bookstores/font-awesome/css/all.css'); !!}

        <!-- Fileinput -->
        {!!Html::style('bookstores/fileinput/css/fileinput.css'); !!}

        <!-- Dropzone -->
        {!!Html::style('bookstores/dropzone/dist/min/dropzone.min.css'); !!}
        {!!Html::style('bookstores/dropzone/css/dropzone.css'); !!}

        <!-- IziToast -->
        {!!Html::style('bookstores/izitoast/dist/css/iziToast.min.css'); !!}

        {!!Html::style('css/general.css'); !!}

        <!-- jQuery -->
        {!!Html::script('bookstores/jquery/jquery.min.js'); !!}

        <!-- Choosen -->
        {!!Html::script('bookstores/choosen/chosen.jquery.js'); !!}
        {!!Html::script('bookstores/choosen/docsupport/prism.js'); !!}

        {!!Html::script('js/general.js'); !!} 

        <!-- Bootstrap -->
        {!!Html::script('bookstores/bootstrap/js/bootstrap.min.js'); !!}

        <!-- Fileinput -->
        {!!Html::script('bookstores/fileinput/js/fileinput.js'); !!}
        {!!Html::script('bookstores/fileinput/js/fileinput_locale_es.js'); !!}

        <!-- Dropzone -->
        {!!Html::script('bookstores/dropzone/js/dropzone.js'); !!}

        <!-- IziToast -->
        {!!Html::script('bookstores/izitoast/dist/js/iziToast.min.js'); !!}    

        <!-- Chart -->
        {!!Html::script('bookstores/chart/chart.js'); !!}    

        @yield('clases')
        <title>Elyon</title>
    </head>

    <body>
        <nav class="navbar navbar-fixed-top navbar-default">
            <input type="hidden" id="token" value="{{csrf_token()}}"/>    
            <div class="navbar-header">
                <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<?php echo 'http://'.$_SERVER["HTTP_HOST"].'/index'?>" class="navbar-brand">Elyon</a>
            </div>

        
            <div id="navbarCollapse" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">

                    <?php 
                        $paquete = DB::table('userscompaniarol AS U')
                                    ->leftJoin('rol AS R', 'U.Rol_idRol', '=', 'R.idRol')
                                    ->leftJoin('rolopcion AS RO', 'U.Rol_idRol', '=', 'RO.Rol_idRol')
                                    ->leftJoin('opcion AS O', 'RO.Opcion_idOpcion', '=', 'O.idOpcion')
                                    ->leftJoin('paquete AS P', 'O.Paquete_idPaquete', '=', 'P.idPaquete')
                                    ->select('idPaquete', 'nombrePaquete', 'iconoPaquete')
                                    ->where('U.Users_id', '=', \Session::get('idUsuario'))
                                    ->where('U.Compania_idCompania', '=', \Session::get('idCompania'))
                                    ->groupBy('P.idPaquete')
                                    ->orderBy('P.ordenPaquete')
                                    ->get();

                        foreach ($paquete as $idP => $datosP) 
                        {
                            echo '
                                <li class="dropdown">
                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="fa fa-'.$datosP->iconoPaquete.'"></span> '.$datosP->nombrePaquete.' <b class="caret"></b></a>
                                    <ul role="menu" class="dropdown-menu">';

                                    $opcion = DB::table('userscompaniarol AS U')
                                            ->leftJoin('rol AS R', 'U.Rol_idRol', '=', 'R.idRol')
                                            ->leftJoin('rolopcion AS RO', 'U.Rol_idRol', '=', 'RO.Rol_idRol')
                                            ->leftJoin('opcion AS O', 'RO.Opcion_idOpcion', '=', 'O.idOpcion')
                                            ->leftJoin('paquete AS P', 'O.Paquete_idPaquete', '=', 'P.idPaquete')
                                            ->select('idOpcion', 'nombreOpcion', 'rutaOpcion')
                                            ->where('U.Users_id', '=', \Session::get('idUsuario'))
                                            ->where('U.Compania_idCompania', '=', \Session::get('idCompania'))
                                            ->where('O.Paquete_idPaquete', '=', $datosP->idPaquete)
                                            ->groupBy('O.idOpcion')
                                            ->orderBy('O.ordenOpcion')
                                            ->get();

                                            foreach ($opcion as $idO => $datosO) 
                                            {
                                                echo '<li><a href="http://'.$_SERVER["HTTP_HOST"].'/'.$datosO->rutaOpcion.'">'.$datosO->nombreOpcion.'</a></li>';
                                            }
                            echo    '</ul>
                                </li>';
                        }

                        $compania = DB::table('userscompaniarol')
                                    ->leftJoin('compania', 'userscompaniarol.Compania_idCompania', '=', 'compania.idCompania')
                                    ->select('idCompania', 'nombreCompania')
                                    ->where('Users_id', '=', \Session::get('idUsuario'))
                                    ->get();

                            echo '
                                <li class="dropdown">
                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="fa fa-university"></span> Compañías<b class="caret"></b></a>
                                    <ul role="menu" class="dropdown-menu">';

                                    foreach ($compania as $idC => $datosC) 
                                    {
                                        echo '<li><a style="cursor:pointer" onclick="recargarSesionCompania('.$datosC->idCompania.')">'.$datosC->nombreCompania.'</a></li>';
                                    }
                                    
                            echo    '</ul>
                                </li>';
                        
                    ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a><?php echo \Session::get('nombreCompania'). '- ' .\Session::get('nombreUsuario');?></a></li>
                    <li><a href="<?php echo 'http://'.$_SERVER["HTTP_HOST"].'/cerrarsesion'?>"><span class="glyphicon glyphicon-off"></span> Salir</a></li>
                </ul>
            </div>
        </nav>


        <div id="contenedor-cuerpo" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @yield('content') 
        </div>

        <div id="footer">
            <p>© 2019 Copyright - Os Document</p>
        </div>

    </body>
</html>

<script>
	function recargarSesionCompania(idCompania)
	{
		var token = document.getElementById('token').value;

		$.ajax({
			headers: {'X-CSRF-TOKEN': token},
			dataType: "json",
			data: {'idCompania': idCompania},
			url:   'http://'+location.host+'/actualizarVariablesSession',
			type:  'post',
			beforeSend: function(){
				abrirDistractor();
			},
			success: function(respuesta){
                mensajeAlerta("sesion", "¡Bien!", respuesta, "green", "fa fa-circle");
                setTimeout(function(){
                    location.replace("http://"+location.host+"/index")
                },3000)
			},
			error:    function(xhr,err){ 
                cerrarDistractor();
                mensajeAlerta("sesion", "Error", "No se ha podido cambiar de compañía", "red", "fa fa-times", "10000");
			}
		});
	}
</script>