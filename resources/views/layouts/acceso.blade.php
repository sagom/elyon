<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="image/x-icon" rel="icon" href="{!!('storage/Elyon.ico')!!}">
            {!!Html::style('bookstores/bootstrap/css/bootstrap.min.css'); !!}
            {!!Html::style('bookstores/bootstrap/css/bootstrap-theme.min.css'); !!}
            {!!Html::style('css/login.css'); !!}
            {!!Html::script('bookstores/jquery/jquery.min.js'); !!}
            {!!Html::script('bookstores/bootstrap/js/bootstrap.min.js'); !!}

        <title>Elyon</title>
    </head>
    <body id='body'>

        @yield('content')

    </body>
</html>
