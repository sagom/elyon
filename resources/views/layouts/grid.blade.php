@extends('layouts.menu')
    @section('clases')
    
        <!-- DataTables -->
        {!!Html::script('bookstores/datatables/media/js/jquery.dataTables.js'); !!}
        {!!Html::script('bookstores/datatables/media/js/jquery.dataTables.min.js'); !!}
        {!!Html::script('bookstores/datatables/extensions/Buttons/js/dataTables.buttons.min.js'); !!}
        {!!Html::script('bookstores/datatables/extensions/Responsive/js/dataTables.responsive.min.js'); !!}
        {!!Html::script('bookstores/datatables/extensions/Responsive/js/responsive.bootstrap.min.js'); !!}
        {!!Html::script('bookstores/datatables/media/js/dataTables.bootstrap.min.js'); !!}
        {!!Html::style('bookstores/datatables/media/css/dataTables.bootstrap.min.css'); !!}
        <LINK REL=StyleSheet HREF="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css" TYPE="text/css" MEDIA=screen>
        {!!Html::script('js/grid.js'); !!}
        {!!Html::script('js/gridselect.js'); !!}
        {!!Html::script('bookstores/chart/chart.js'); !!}    
    @stop