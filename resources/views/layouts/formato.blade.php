<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="image/x-icon" rel="icon" href="{!!('storage/Elyon.png')!!}">

        <!-- Bootstrap -->
        {!!Html::style('bookstores/bootstrap/css/bootstrap.min.css'); !!}
        {!!Html::style('bookstores/bootstrap/css/bootstrap-theme.min.css'); !!}

        <!-- {!!Html::style('css/general.css'); !!} -->

        <!-- jQuery -->
        {!!Html::script('bookstores/jquery/jquery.min.js'); !!}

        {!!Html::script('js/general.js'); !!} 

        <!-- Bootstrap -->
        {!!Html::script('bookstores/bootstrap/js/bootstrap.min.js'); !!}

        <!-- Chart -->
        {!!Html::script('bookstores/chart/chart.js'); !!}    

        @yield('clases')
    </head>

    <body>

        <div id="contenedor-cuerpo" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @yield('content') 
        </div>

    </body>
</html>