<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="image/x-icon" rel="icon" href="{!!('storage/Elyon.ico')!!}">

        <!-- Bootstrap -->
        {!!Html::style('bookstores/bootstrap/css/bootstrap.min.css'); !!}
        {!!Html::style('bookstores/bootstrap/css/bootstrap-theme.min.css'); !!}
        {!!Html::style('css/menu.css'); !!}
        {!!Html::style('css/modal.css'); !!}

        <!-- Choosen -->
        {!!Html::style('bookstores/choosen/chosen.css'); !!}

        <!-- Fontawesome -->
        {!!Html::style('bookstores/font-awesome/css/all.css'); !!}

        <!-- Fileinput -->
        {!!Html::style('bookstores/fileinput/css/fileinput.css'); !!}

        <!-- Dropzone -->
        {!!Html::style('bookstores/dropzone/dist/min/dropzone.min.css'); !!}
        {!!Html::style('bookstores/dropzone/css/dropzone.css'); !!}

        <!-- IziToast -->
        {!!Html::style('bookstores/izitoast/dist/css/iziToast.min.css'); !!}

        {!!Html::style('css/general.css'); !!}

        <!-- jQuery -->
        {!!Html::script('bookstores/jquery/jquery.min.js'); !!}

        <!-- Choosen -->
        {!!Html::script('bookstores/choosen/chosen.jquery.js'); !!}
        {!!Html::script('bookstores/choosen/docsupport/prism.js'); !!}

        {!!Html::script('js/general.js'); !!} 

        <!-- Bootstrap -->
        {!!Html::script('bookstores/bootstrap/js/bootstrap.min.js'); !!}

        <!-- Fileinput -->
        {!!Html::script('bookstores/fileinput/js/fileinput.js'); !!}
        {!!Html::script('bookstores/fileinput/js/fileinput_locale_es.js'); !!}

        <!-- Dropzone -->
        {!!Html::script('bookstores/dropzone/js/dropzone.js'); !!}

        <!-- IziToast -->
        {!!Html::script('bookstores/izitoast/dist/js/iziToast.min.js'); !!}    

        <!-- DataTables -->
        {!!Html::script('bookstores/datatables/media/js/jquery.dataTables.js'); !!}
        {!!Html::script('bookstores/datatables/media/js/jquery.dataTables.min.js'); !!}
        {!!Html::script('bookstores/datatables/extensions/Buttons/js/dataTables.buttons.min.js'); !!}
        {!!Html::script('bookstores/datatables/extensions/Responsive/js/dataTables.responsive.min.js'); !!}
        {!!Html::script('bookstores/datatables/extensions/Responsive/js/responsive.bootstrap.min.js'); !!}
        {!!Html::script('bookstores/datatables/media/js/dataTables.bootstrap.min.js'); !!}
        {!!Html::style('bookstores/datatables/media/css/dataTables.bootstrap.min.css'); !!}
        <LINK REL=StyleSheet HREF="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css" TYPE="text/css" MEDIA=screen>
        {!!Html::script('js/grid.js'); !!}
        {!!Html::script('js/gridselect.js'); !!}

        @yield('clases')
        <title>Elyon</title>
    </head>

    <body>

        <div id="contenedor" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @yield('content') 
        </div>

    </body>
</html>