@extends('layouts.vista')

@section('content')
    @include('alerts.request')

    @if(isset($empleado))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($empleado,['route'=>['empleado.destroy',$empleado->idEmpleado],'method'=>'DELETE', 'id' => 'form-empleado'])!!}
		@else
			{!!Form::model($empleado,['route'=>['empleado.update',$empleado->idEmpleado],'method'=>'PUT', 'id' => 'form-empleado'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'empleado.store','method'=>'POST', 'id' => 'form-empleado'])!!}
    @endif

    <fieldset>
        <legend>Información de Empleados</legend>
        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('documentoEmpleado', 'Identificación', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-id-card"></i>
                    </span>
                    {!!Form::hidden('idEmpleado', null, array('id' => 'idEmpleado')) !!}
                    {!!Form::text('documentoEmpleado',null,['class'=>'form-control','placeholder'=>'Ingrese el número de identificación'])!!}
                </div>
            </div>
        </div>


        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('nombreCompletoEmpleado', 'Nombre', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::text('nombreCompletoEmpleado',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre completo'])!!}
                </div>
            </div>
        </div>


        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('estadoEmpleado', 'Estado', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-question-circle"></i>
                    </span>
                    {!!Form::select('estadoEmpleado',['Activo'=>'Activo','Inactivo'=>'Inactivo'],null,['class' => 'form-control','placeholder'=>'- Seleccione el estado del empleado -', 'onchange' => 'activarOpciones(this.value)'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('CentroCosto_idCentroCosto', 'Centro de costo', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-cube"></i>
                    </span>
                    {!!Form::select('CentroCosto_idCentroCosto',$centrocosto, (isset($empleado) ? $empleado->CentroCosto_idCentroCosto : null),["class" => "select form-control", "placeholder" =>"- Seleccione el centro de costo -"])!!}
                </div>
            </div>
        </div>

    </fieldset>
    

    @if(isset($empleado))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'empleado')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'empleado')"])!!}
    @endif 
     
    {!!Form::close()!!}

@stop