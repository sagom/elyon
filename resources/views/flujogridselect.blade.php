@extends('layouts.modal') 
@section('content')

<script type="text/javascript">
    $(document).ready( function () 
    {
        var idRadicado = '<?php echo (isset($_GET["idRadicado"]) ? $_GET["idRadicado"] : 0);?>';

        configurarGridSelect('flujoselect',"{!! URL::to ('/flujodataselect?idRadicado="+idRadicado+"')!!}");
    }); 
</script>

<?php
    $visible = '';

    $idRadicado = isset($_GET['idRadicado']) ? $_GET['idRadicado'] : '';

    if(isset($datos[0])) 
    {
        $dato = get_object_vars($datos[0]);
        if ($dato['adicionarRolOpcion'] == 1) 
            $visible = 'inline-block;'; 
        else
            $visible = 'none;';
    }
    else
        $visible = 'none;';
?>

<legend>Flujo</legend>
<table id="flujoselect" class="table table-striped table-bordered nowrap" style="width:100%">
    <thead class="btn-success">
        <tr>
            <th>ID</th>
            <th>Código</th>
            <th>Nombre</th>
        </tr>
    </thead>
    <tfoot class="btn-default active">
        <tr>
            <th>ID</th>
            <th>Código</th>
            <th>Nombre</th>
        </tr>
    </tfoot>
</table>
@stop