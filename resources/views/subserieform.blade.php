@extends('layouts.vista')

@section('content')
    @include('alerts.request')

    @if(isset($subserie))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($subserie,['route'=>['subserie.destroy',$subserie->idSubSerie],'method'=>'DELETE', 'id' => 'form-subserie'])!!}
		@else
			{!!Form::model($subserie,['route'=>['subserie.update',$subserie->idSubSerie],'method'=>'PUT', 'id' => 'form-subserie'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'subserie.store','method'=>'POST', 'id' => 'form-subserie'])!!}
    @endif

    <fieldset>
        <legend>Información de SubSerie</legend>
        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('codigoSubSerie', 'Código', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!!Form::hidden('idSubSerie', null, array('id' => 'idSubSerie')) !!}
                    {!!Form::text('codigoSubSerie',null,['class'=>'form-control','placeholder'=>'Ingrese el código'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('nombreSubSerie', 'Nombre', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::text('nombreSubSerie',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('directorioSubSerie', 'Directorio', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-folder-open"></i>
                    </span>
                    {!!Form::text('directorioSubSerie',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del directorio', 'onkeyup' => 'replaceSpacesWithUnderLine(this.value, this.id)', 'id' => 'directorioSubSerie'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('descripcionSubSerie', 'Descripción', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-book"></i>
                    </span>
                    {!!Form::text('descripcionSubSerie',null,['class'=>'form-control','placeholder'=>'Ingrese la descripción'])!!}
                </div>
            </div>
        </div>
    </fieldset>


    @if(isset($subserie))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'subserie')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'subserie')"])!!}
    @endif

    {!!Form::close()!!}

    <script>
        subserierelacion = '<?php echo isset($subserie) ? $subserie->SubSerie_idSubSerie : "";?>';

        if(subserierelacion != '' && subserierelacion == 1)
        {
            $("#directorioSubSerie").attr('readonly', true);
        }
    </script>
@stop