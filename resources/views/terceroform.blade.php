@extends('layouts.vista')

@section('content')
    {!!Html::script('js/tercero.js'); !!} 
    @include('alerts.request')

    @if(isset($tercero))
		@if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
			{!!Form::model($tercero,['route'=>['tercero.destroy',$tercero->idTercero],'method'=>'DELETE', 'id' => 'form-tercero'])!!}
		@else
			{!!Form::model($tercero,['route'=>['tercero.update',$tercero->idTercero],'method'=>'PUT', 'id' => 'form-tercero'])!!}
		@endif
	@else
		{!!Form::open(['route'=>'tercero.store','method'=>'POST', 'id' => 'form-tercero'])!!}
    @endif

    <fieldset>
        <legend>Información de Terceros</legend>
        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('documentoTercero', 'Identificación', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-id-card"></i>
                    </span>
                    {!!Form::hidden('idTercero', null, array('id' => 'idTercero')) !!}
                    {!!Form::text('documentoTercero',null,['class'=>'form-control','placeholder'=>'Ingrese el número de identificación'])!!}
                </div>
            </div>
        </div>


        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('nombreCompletoTercero', 'Nombre', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    {!!Form::text('nombreCompletoTercero',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre completo'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('codigoTercero', 'Código', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-barcode"></i>
                    </span>
                    {!!Form::text('codigoTercero',null,['class'=>'form-control','placeholder'=>'Ingrese el código del tercero'])!!}
                </div>
            </div>
        </div>

        <div class="div-responsive-doce div-agrupador">
        {!!Form::label('tipoTercero', 'Tipo', array('class' => 'label-responsive-dos')) !!}
        <div class="div-input-responsive">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-window-restore"></i>
                </span>
                {!!Form::select('tipoTercero',['Estudiante'=>'Estudiante','Empleado'=>'Empleado','Cliente'=>'Cliente','Proveedor'=>'Proveedor'],null,['class' => 'form-control','placeholder'=>'- Seleccione el tipo de tercero -'])!!}
            </div>
        </div>
    </div>

        <div class="div-responsive-doce div-agrupador">
            {!!Form::label('estadoTercero', 'Estado', array('class' => 'label-responsive-dos')) !!}
            <div class="div-input-responsive">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fas fa-question-circle"></i>
                    </span>
                    {!!Form::select('estadoTercero',['Activo'=>'Activo','Egresado'=>'Egresado','Graduado'=>'Graduado','En proceso de admision'=>'En proceso de admisión','Modalidad silla vacia'=>'Modalidad silla vacía','Movilidad terminada'=>'Movilidad terminada','Admitido no matriculado'=>'Admitido no matriculado','No admitido'=>'No admitido','Retirado'=>'Retirado','Inactivo'=>'Inactivo'],null,['class' => 'form-control','placeholder'=>'- Seleccione el estado del tercero -', 'onchange' => 'activarOpciones(this.value)'])!!}
                </div>
            </div>
        </div>

    </fieldset>
    

    @if(isset($tercero))
        @if(isset($_GET['accion']) and $_GET['accion'] == 'destroy')
   			{!!Form::submit('Eliminar',["class"=>"btn btn-danger"])!!}
  		@else
   			{!!Form::submit('Modificar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'tercero')"])!!}
  		@endif
 	@else
  		{!!Form::submit('Adicionar',["class"=>"btn btn-success", "onclick"=>"validarFormulario(event,'tercero')"])!!}
    @endif 
     
    {!!Form::close()!!}

@stop