<?php

    $radicadointerno = DB::table('radicadointerno')
    ->leftJoin('publicacion', 'radicadointerno.idRadicadoInterno', '=', 'publicacion.Radicado_idRadicadoInterno')
    ->leftJoin('users AS ur', 'radicadointerno.UsersRemitente_idRadicadoInterno', '=', 'ur.id')
    ->leftJoin('users AS ud', 'radicadointerno.UsersDestinatario_idRadicadoInterno', '=', 'ud.id')
    ->leftJoin('dependencia AS dr', 'ur.Dependencia_idDependencia', '=', 'dr.idDependencia')
    ->leftJoin('dependencia AS dd', 'ud.Dependencia_idDependencia', '=', 'dd.idDependencia')
    ->select(DB::raw('radicadointerno.*, Radicado_idRadicadoInterno, ur.name as nombreRemitente, ud.name as nombreDestinatario, dr.nombreDependencia as nombreDependenciaRemitente, dd.nombreDependencia as nombreDependenciaDestinataria'))
    ->where('radicadointerno.Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();
    $row = array();

    foreach ($radicadointerno as $key => $value) 
    {  
        $row[$key][] =  ($value->Radicado_idRadicadoInterno != null ? '<a href="radicadointerno/'.$value->idRadicadoInterno.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;' : '').
                        '<a onclick="imprimirRadicado('.$value->idRadicadoInterno.')">'.
                            '<span class="glyphicon glyphicon-print"></span>'.
                        '</a>&nbsp;'.
                        ($value->Radicado_idRadicadoInterno == null ? '<a onclick="cargarPublicacion('.$value->idRadicadoInterno.')">'.
                            '<span class="glyphicon glyphicon-cloud-upload"></span>'.
                        '</a>&nbsp;' : '').
                        ($value->Radicado_idRadicadoInterno != null ? '<a onclick="cargarImagenPublicacion('.$value->idRadicadoInterno.')">'.
                            '<span class="glyphicon glyphicon-picture"></span>'.
                        '</a>&nbsp;' : '');
        $row[$key][] = $value->idRadicadoInterno;
        $row[$key][] = "I".$value->numeroRadicadoInterno;
        $row[$key][] = $value->fechaRadicadoInterno;
        $row[$key][] = $value->nombreRemitente;
        $row[$key][] = $value->nombreDependenciaRemitente;
        $row[$key][] = $value->asuntoRadicadoInterno;
        $row[$key][] = $value->fechaDocumentoRadicadoInterno;
        $row[$key][] = $value->nombreDestinatario;
        $row[$key][] = $value->nombreDependenciaDestinataria;
        $row[$key][] = $value->observacionRadicadoInterno;
                
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>