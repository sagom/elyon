<?php

    $opcion = \App\Opcion::All();
    $opcion = DB::table('opcion')
    ->leftJoin('paquete', 'opcion.Paquete_idPaquete', '=', 'paquete.idPaquete')
    ->select(DB::raw('opcion.*, nombrePaquete'))
    ->get();

    $row = array();

    foreach ($opcion as $key => $value) 
    {  
        $row[$key][] = '<a href="opcion/'.$value->idOpcion.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="opcion/'.$value->idOpcion.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>';
        $row[$key][] = $value->idOpcion;
        $row[$key][] = $value->ordenOpcion;
        $row[$key][] = $value->nombreOpcion;
        $row[$key][] = $value->rutaOpcion;
        $row[$key][] = $value->nombrePaquete; 
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>