<?php

    // $tercero = \App\Tercero::All();
    $tercero = DB::table('tercero')
    ->leftJoin('compania', 'tercero.Compania_idCompania', '=', 'compania.idCompania')
    ->select(DB::raw('tercero.*, nombreCompania'))
    ->where('Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();
    $row = array();

    foreach ($tercero as $key => $value) 
    {  
        $row[$key][] = '<a href="tercero/'.$value->idTercero.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="tercero/'.$value->idTercero.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>';
        $row[$key][] = $value->idTercero;
        $row[$key][] = $value->documentoTercero;
        $row[$key][] = $value->nombreCompletoTercero;
        $row[$key][] = $value->codigoTercero;
        $row[$key][] = $value->tipoTercero;
        $row[$key][] = $value->estadoTercero;
    }

    $output['aaData'] = $row;
    echo json_encode($output);