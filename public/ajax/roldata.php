<?php

    // $rol = \App\Rol::All();
    $rol = DB::table('rol')
    ->leftJoin('compania', 'rol.Compania_idCompania', '=', 'compania.idCompania')
    ->select(DB::raw('rol.*, nombreCompania'))
    ->where('Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();
    $row = array();

    foreach ($rol as $key => $value) 
    {  
        $row[$key][] = '<a href="rol/'.$value->idRol.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="rol/'.$value->idRol.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>';
        $row[$key][] = $value->idRol;
        $row[$key][] = $value->codigoRol;
        $row[$key][] = $value->nombreRol; 
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>