<?php

    // $centrocosto = \App\CentroCosto::All();
    $centrocosto = DB::table('centrocosto')
    ->leftJoin('compania', 'centrocosto.Compania_idCompania', '=', 'compania.idCompania')
    ->select(DB::raw('centrocosto.*, nombreCompania'))
    ->where('Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();
    $row = array();

    foreach ($centrocosto as $key => $value) 
    {  
        $row[$key][] = '<a href="centrocosto/'.$value->idCentroCosto.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="centrocosto/'.$value->idCentroCosto.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>';
        $row[$key][] = $value->codigoCentroCosto;
        $row[$key][] = $value->nombreCentroCosto;
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>