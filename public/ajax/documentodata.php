<?php

    // $documento = \App\Documento::All();
    $documento = DB::table('documento')
    ->leftJoin('compania', 'documento.Compania_idCompania', '=', 'compania.idCompania')
    ->select(DB::raw('documento.*, nombreCompania'))
    ->where('Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();
    $row = array();

    foreach ($documento as $key => $value) 
    {  
        $row[$key][] = '<a href="documento/'.$value->idDocumento.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="documento/'.$value->idDocumento.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>';
        $row[$key][] = $value->idDocumento;
        $row[$key][] = $value->codigoDocumento;
        $row[$key][] = $value->nombreDocumento;
        $row[$key][] = $value->directorioDocumento;
        $row[$key][] = $value->versionDocumento; 
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>