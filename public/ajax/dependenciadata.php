<?php

    // $dependencia = \App\Dependencia::All();
    $dependencia = DB::table('dependencia')
    ->leftJoin('compania', 'dependencia.Compania_idCompania', '=', 'compania.idCompania')
    ->select(DB::raw('dependencia.*, nombreCompania'))
    ->where('Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();

    $row = array();

    foreach ($dependencia as $key => $value) 
    {  
        $row[$key][] = '<a href="dependencia/'.$value->idDependencia.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="dependencia/'.$value->idDependencia.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>';
        $row[$key][] = $value->idDependencia;
        $row[$key][] = $value->codigoDependencia;
        $row[$key][] = $value->nombreDependencia;
        $row[$key][] = $value->directorioDependencia; 
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>