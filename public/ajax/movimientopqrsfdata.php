<?php

    $movimientopqrsf = DB::table('movimientopqrsf')
    ->leftJoin('users', 'movimientopqrsf.Users_idMovimientoPQRSF', '=', 'users.id')
    ->leftJoin('compania', 'movimientopqrsf.Compania_idCompania', '=', 'compania.idCompania')
    ->select(DB::raw('movimientopqrsf.*, name'))
    ->where('Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();
    $row = array();

    foreach ($movimientopqrsf as $key => $value) 
    {  
        $row[$key][] = ($value->correoRespuestaMovimientoPQRSF == 0 ? 
                        '<a href="movimientopqrsf/'.$value->idMovimientoPQRSF.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="movimientopqrsf/'.$value->idMovimientoPQRSF.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>' : '');
        $row[$key][] = $value->idMovimientoPQRSF;
        $row[$key][] = $value->nombreSolicitanteMovimientoPQRSF;
        $row[$key][] = $value->fechaSolicitudMovimientoPQRSF;
        $row[$key][] = $value->correoSolicitanteMovimientoPQRSF;
        $row[$key][] = $value->tipoMovimientoPQRSF;
        $row[$key][] = $value->mensajeMovimientoPQRSF;
        $row[$key][] = $value->respuestaMovimientoPQRSF;
        $row[$key][] = $value->fechaRespuestaMovimientoPQRSF;
        $row[$key][] = $value->name;
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>