<?php

    // $subserie = \App\SubSerie::All();
    $subserie = DB::table('subserie')
    ->leftJoin('compania', 'subserie.Compania_idCompania', '=', 'compania.idCompania')
    ->select(DB::raw('subserie.*, nombreCompania'))
    ->where('Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();
    $row = array();

    foreach ($subserie as $key => $value) 
    {  
        $row[$key][] = '<a href="subserie/'.$value->idSubSerie.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="subserie/'.$value->idSubSerie.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>';
        $row[$key][] = $value->idSubSerie;
        $row[$key][] = $value->codigoSubSerie;
        $row[$key][] = $value->nombreSubSerie;
        $row[$key][] = $value->directorioSubSerie;
        $row[$key][] = $value->descripcionSubSerie;
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>