<?php
    
    $flujo = DB::table('flujo')
    ->leftJoin('compania', 'flujo.Compania_idCompania', '=', 'compania.idCompania')
    ->select(DB::raw('flujo.*, nombreCompania'))
    ->where('Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();

    $idRadicado = isset($_GET['idRadicado']) ? $_GET['idRadicado'] : '';
    
    $row = array();

    foreach ($flujo as $key => $value) 
    {  
        $row[$key][] = $value->idFlujo;
        $row[$key][] = $value->codigoFlujo;
        $row[$key][] = $value->nombreFlujo; 
        $row[$key][] = $idRadicado; 
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>