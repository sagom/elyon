<?php

    // $serie = \App\Serie::All();
    $serie = DB::table('serie')
    ->leftJoin('compania', 'serie.Compania_idCompania', '=', 'compania.idCompania')
    ->select(DB::raw('serie.*, nombreCompania'))
    ->where('Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();
    $row = array();

    foreach ($serie as $key => $value) 
    {  
        $row[$key][] = '<a href="serie/'.$value->idSerie.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="serie/'.$value->idSerie.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>';
        $row[$key][] = $value->idSerie;
        $row[$key][] = $value->codigoSerie;
        $row[$key][] = $value->nombreSerie;
        $row[$key][] = $value->directorioSerie;
        $row[$key][] = $value->descripcionSerie;
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>