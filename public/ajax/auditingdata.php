<?php

    $auditing = DB::table('audits as ad')
    ->leftJoin('users as u', 'ad.user_id', '=', 'u.id')
    ->select(DB::raw("REPLACE (auditable_type, 'App', '') AS Modulo,
                    name AS Usuario,
                    CASE 
                        WHEN EVENT = 'updated' THEN 'Actualizar' 
                        WHEN EVENT = 'created' THEN 'Crear' 
                        WHEN EVENT = 'deleted' THEN 'Eliminar'
                    END AS 'Evento',
                    auditable_id AS IdModulo,
                    old_values AS ValorAnterior,
                    new_values AS ValorNuevo,
                    ad.created_at as FechaTransaccion"))
    // ->where('Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();
    $row = array();

    foreach ($auditing as $key => $value) 
    {  
        $row[$key][] = $value->IdModulo;
        $row[$key][] = $value->Modulo;
        $row[$key][] = $value->Usuario;
        $row[$key][] = $value->Evento;
        $row[$key][] = str_replace('', '', str_replace('', '', str_replace('{"', '', $value->ValorAnterior)));
        $row[$key][] = $value->ValorNuevo;
        $row[$key][] = $value->FechaTransaccion;
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>