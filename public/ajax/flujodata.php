<?php

    $flujo = DB::table('flujo')
    ->leftJoin('compania', 'flujo.Compania_idCompania', '=', 'compania.idCompania')
    ->select(DB::raw('flujo.*, nombreCompania'))
    ->where('Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();
    $row = array();

    foreach ($flujo as $key => $value) 
    {  
        $row[$key][] = '<a href="flujo/'.$value->idFlujo.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="flujo/'.$value->idFlujo.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>';
        $row[$key][] = $value->idFlujo;
        $row[$key][] = $value->codigoFlujo;
        $row[$key][] = $value->nombreFlujo; 
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>