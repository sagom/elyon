<?php

    // $clasificaciontarea = \App\ClasificacionTarea::All();
    $clasificaciontarea = DB::table('clasificaciontarea')
    ->where('Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();

    $row = array();

    foreach ($clasificaciontarea as $key => $value) 
    {  
        $row[$key][] = '<a href="clasificaciontarea/'.$value->idClasificacionTarea.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="clasificaciontarea/'.$value->idClasificacionTarea.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>';
        $row[$key][] = $value->idClasificacionTarea;
        $row[$key][] = $value->codigoClasificacionTarea;
        $row[$key][] = $value->nombreClasificacionTarea;
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>