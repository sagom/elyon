<?php

    $paquete = \App\Paquete::All();
    $row = array();

    foreach ($paquete as $key => $value) 
    {  
        $row[$key][] = '<a href="paquete/'.$value->idPaquete.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="paquete/'.$value->idPaquete.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>';
        $row[$key][] = $value->idPaquete;
        $row[$key][] = $value->ordenPaquete;
        $row[$key][] = $value->iconoPaquete;
        $row[$key][] = $value->nombrePaquete; 
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>