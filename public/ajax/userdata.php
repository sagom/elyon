<?php

    // $user = \App\User::All();
    $user = DB::table('users')
    ->leftJoin('userscompaniarol', 'users.id', '=', 'userscompaniarol.Users_id')
    ->leftJoin('rol', 'userscompaniarol.Rol_idRol', '=', 'rol.idRol')
    ->leftJoin('compania', 'userscompaniarol.Compania_idCompania', '=', 'compania.idCompania')
    ->select(DB::raw('users.*, nombreRol, nombreCompania'))
    ->where('idCompania', '=', \Session::get('idCompania'))
    ->get();

    $row = array();

    foreach ($user as $key => $value) 
    {  
        $row[$key][] = '<a href="user/'.$value->id.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="user/'.$value->id.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>';
        $row[$key][] = $value->id;
        $row[$key][] = $value->user;
        $row[$key][] = $value->name;
        $row[$key][] = $value->email;
        $row[$key][] = $value->nombreRol;
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>