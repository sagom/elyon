<?php

    $tipo = (isset($_GET['tipo']) ? $_GET['tipo'] : '');

    $tarea = DB::table('tarea')
    ->leftJoin('users AS us', 'tarea.Users_idSolicitante', '=', 'us.id')
    ->leftJoin('users AS ur', 'tarea.Users_idResponsable', '=', 'ur.id')
    ->leftJoin('clasificaciontarea', 'tarea.ClasificacionTarea_idClasificacionTarea', '=', 'clasificaciontarea.idClasificacionTarea')
    ->select(DB::raw('tarea.*, us.name as nombreSolicitanteTarea, ur.name as nombreResponsableTarea, nombreClasificacionTarea'))
    ->where('tarea.Compania_idCompania', '=', \Session::get('idCompania'))
    ->where(function($sql) {
        $sql->where('Users_idSolicitante', '=', \Session::get('idUsuario'))
          ->orWhere('Users_idResponsable', '=', \Session::get('idUsuario'));
        });

    if($tipo != '')
    {
        $tarea->whereRaw("estadoTarea = '$tipo'");
    }
    $tarea = $tarea->get();

    $row = array();

    foreach ($tarea as $key => $value) 
    {  
        $row[$key][] = '<a href="tarea/'.$value->idTarea.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="tarea/'.$value->idTarea.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>';
        $row[$key][] = $value->idTarea;
        $row[$key][] = $value->numeroTarea;
        $row[$key][] = $value->fechaElaboracionTarea;
        $row[$key][] = $value->asuntoTarea;
        $row[$key][] = $value->prioridadTarea;
        $row[$key][] = $value->nombreResponsableTarea;
        $row[$key][] = $value->nombreClasificacionTarea;
        $row[$key][] = $value->estadoTarea;
        $row[$key][] = $value->fechaVencimientoTarea;
        $row[$key][] = $value->nombreSolicitanteTarea;
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>