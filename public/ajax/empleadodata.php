<?php

    // $empleado = \App\Empleado::All();
    $empleado = DB::table('empleado')
    ->leftJoin('compania', 'empleado.Compania_idCompania', '=', 'compania.idCompania')
    ->leftJoin('centrocosto', 'empleado.CentroCosto_idCentroCosto', '=', 'centrocosto.idCentroCosto')
    ->select(DB::raw('empleado.*, nombreCompania, nombreCentroCosto'))
    ->where('empleado.Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();
    $row = array();

    foreach ($empleado as $key => $value) 
    {  
        $row[$key][] = '<a href="empleado/'.$value->idEmpleado.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="empleado/'.$value->idEmpleado.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>';
        $row[$key][] = $value->idEmpleado;
        $row[$key][] = $value->documentoEmpleado;
        $row[$key][] = $value->nombreCompletoEmpleado;
        $row[$key][] = $value->estadoEmpleado;
        $row[$key][] = $value->nombreCentroCosto;
    }

    $output['aaData'] = $row;
    echo json_encode($output);