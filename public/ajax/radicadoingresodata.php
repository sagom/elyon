<?php

    $radicadoingreso = DB::table('radicadoingreso')
    ->leftJoin('publicacion', 'radicadoingreso.idRadicadoIngreso', '=', 'publicacion.Radicado_idRadicadoIngreso')
    ->leftJoin('users AS ur', 'radicadoingreso.Users_idRadicadoIngreso', '=', 'ur.id')
    ->leftJoin('dependencia', 'radicadoingreso.Dependencia_idRadicadoIngreso', '=', 'dependencia.idDependencia')
    ->leftJoin('compania', 'radicadoingreso.Compania_idCompania', '=', 'compania.idCompania')
    ->select(DB::raw('radicadoingreso.*, Radicado_idRadicadoIngreso, ur.name, nombreDependencia, nombreCompania'))
    ->where('radicadoingreso.Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();
    $row = array();

    foreach ($radicadoingreso as $key => $value) 
    {  
        $row[$key][] = ($value->Radicado_idRadicadoIngreso != null ? '<a href="radicadoingreso/'.$value->idRadicadoIngreso.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;' : '').
                        '<a onclick="imprimirRadicado('.$value->idRadicadoIngreso.')">'.
                            '<span class="glyphicon glyphicon-print"></span>'.
                        '</a>&nbsp;'.
                        ($value->impresionRadicadoIngreso == 1 ?'<a onclick="cargarPublicacion('.$value->idRadicadoIngreso.')">'.
                            '<span class="glyphicon glyphicon-cloud-upload"></span>'.
                        '</a>&nbsp;' : '').
                        ($value->Radicado_idRadicadoIngreso != 'null' ? '<a onclick="cargarImagenPublicacion('.$value->idRadicadoIngreso.')">'.
                            '<span class="glyphicon glyphicon-picture"></span>'.
                        '</a>&nbsp;' : '').
                        ($value->name != '' ? '<a onclick="seleccionarFlujo('.$value->idRadicadoIngreso.')">'.
                            '<span class="glyphicon glyphicon-check"></span>'.
                        '</a>&nbsp;' : '');
        $row[$key][] = $value->idRadicadoIngreso;
        $row[$key][] = "R".$value->numeroRadicadoIngreso;
        $row[$key][] = $value->fechaRadicadoIngreso;
        $row[$key][] = $value->tipoRadicadoIngreso;
        $row[$key][] = $value->nombreRemitenteRadicadoIngreso;
        $row[$key][] = $value->identificacionRemitenteRadicadoIngreso;
        $row[$key][] = $value->asuntoRadicadoIngreso;
        $row[$key][] = $value->fechaDocumentoRadicadoIngreso;
        $row[$key][] = $value->name;
        $row[$key][] = $value->nombreDependencia;
        $row[$key][] = $value->nombreCompania;
        $row[$key][] = $value->observacionRadicadoIngreso;
                
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>