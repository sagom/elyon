<?php

    // $metadato = \App\Metadato::All();
    $metadato = DB::table('metadato')
    ->leftJoin('compania', 'metadato.Compania_idCompania', '=', 'compania.idCompania')
    ->select(DB::raw('metadato.*, nombreCompania'))
    ->where('Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();
    $row = array();

    foreach ($metadato as $key => $value) 
    {  
        $row[$key][] = '<a href="metadato/'.$value->idMetadato.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;'.
                        '<a href="metadato/'.$value->idMetadato.'/edit?accion=destroy">'.
                            '<span class="glyphicon glyphicon-trash"></span>'.
                        '</a>';
        $row[$key][] = $value->idMetadato;
        $row[$key][] = $value->codigoMetadato;
        $row[$key][] = $value->nombreMetadato;
        $row[$key][] = $value->tipoMetadato;
        $row[$key][] = $value->opcionMetadato;
        $row[$key][] = $value->longitudMetadato; 
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>