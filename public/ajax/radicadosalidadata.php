<?php

    $radicadosalida = DB::table('radicadosalida')
    ->leftJoin('publicacion', 'radicadosalida.idRadicadoSalida', '=', 'publicacion.Radicado_idRadicadoSalida')
    ->leftJoin('users AS ur', 'radicadosalida.Users_idRadicadoSalida', '=', 'ur.id')
    ->leftJoin('dependencia', 'radicadosalida.Dependencia_idRadicadoSalida', '=', 'dependencia.idDependencia')
    ->leftJoin('compania', 'radicadosalida.Compania_idCompania', '=', 'compania.idCompania')
    ->select(DB::raw('radicadosalida.*, Radicado_idRadicadoSalida, ur.name, nombreDependencia, nombreCompania'))
    ->where('radicadosalida.Compania_idCompania', '=', \Session::get('idCompania'))
    ->get();
    $row = array();

    foreach ($radicadosalida as $key => $value) 
    {  
        $row[$key][] = ($value->Radicado_idRadicadoSalida != null ? '<a href="radicadosalida/'.$value->idRadicadoSalida.'/edit">'.
                            '<span class="glyphicon glyphicon-pencil"></span>'.
                        '</a>&nbsp;' : '').
                        '<a onclick="imprimirRadicado('.$value->idRadicadoSalida.')">'.
                            '<span class="glyphicon glyphicon-print"></span>'.
                        '</a>&nbsp;'.
                        ($value->impresionRadicadoSalida == 1 ?'<a onclick="cargarPublicacion('.$value->idRadicadoSalida.')">'.
                            '<span class="glyphicon glyphicon-cloud-upload"></span>'.
                        '</a>&nbsp;' : '').
                        ($value->Radicado_idRadicadoSalida != null ? '<a onclick="cargarImagenPublicacion('.$value->idRadicadoSalida.')">'.
                            '<span class="glyphicon glyphicon-picture"></span>'.
                        '</a>&nbsp;' : '').
                        ($value->name != '' ? '<a onclick="seleccionarFlujo('.$value->idRadicadoSalida.')">'.
                            '<span class="glyphicon glyphicon-check"></span>'.
                        '</a>&nbsp;' : '');
        $row[$key][] = $value->idRadicadoSalida;
        $row[$key][] = "E".$value->numeroRadicadoSalida;
        $row[$key][] = $value->fechaRadicadoSalida;
        $row[$key][] = $value->nombreDestinatarioRadicadoSalida;
        $row[$key][] = $value->identificacionDestinatarioRadicadoSalida;
        $row[$key][] = $value->asuntoRadicadoSalida;
        $row[$key][] = $value->fechaDocumentoRadicadoSalida;
        $row[$key][] = $value->name;
        $row[$key][] = $value->nombreDependencia;
        $row[$key][] = $value->nombreCompania;
        $row[$key][] = $value->observacionRadicadoSalida;
                
    }

    $output['aaData'] = $row;
    echo json_encode($output);
?>