$(document).ready(function(){
    if($("#idRadicadoInterno").val() > 0)
    {
        $('#Users_idRadicadoSalida option:not(:selected)').attr('disabled',true);
        $('#Dependencia_idRadicadoSalida option:not(:selected)').attr('disabled',true);
    }
});

function abrirModalInicial()
{
    $('#modalRadicado').css('display','block');
}

function cerrarModal()
{
    $('#modalRadicado').css('display','none');
}

function guardarDatosIniciales()
{
    dependencia = $("#Dependencia_idRadicadoSalida").val();
    asunto = $("#asuntoRadicadoSalida").val();
    nombre = $("#nombreDestinatarioRadicadoSalida").val();
    identificacion = $("#identificacionDestinatarioRadicadoSalida").val();

    if(dependencia == "")    
    {
        mensajeAlerta("Erroridentificación", "Aviso", "El origen es obligatorio", "red", "fas fa-times-circle", "6000", "topCenter");
        return;
    }

    if(asunto == "")
    {
        mensajeAlerta("Errorasunto", "Aviso", "El asunto es obligatorio", "red", "fas fa-times-circle", "6000", "topCenter");
        return;
    }

    if(nombre == "")
    {
        mensajeAlerta("ErrorDestinatario", "Aviso", "El Destinatario es obligatoria", "red", "fas fa-times-circle", "6000", "topCenter");
        return;
    }

    // if(identificacion == "")    
    // {
    //     mensajeAlerta("Erroridentificación", "Aviso", "La identificacion es obligatoria", "red", "fas fa-times-circle", "6000", "topCenter");
    //     return;
    // }

    var token = document.getElementById('token').value;
    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        dataType: "json",
        data: {'asunto': asunto, 'nombre': nombre, 'identificacion': identificacion, 'dependencia': dependencia},
        url:   'http://'+location.host+'/guardarRadicadoSalida',
        type:  'post',
        beforeSend: function(){
            abrirDistractor();
            $('#modalRadicado').css('display','none');
        },
        success: function(respuesta){
            cerrarDistractor();
            $('#modalRadicado').css('display','block');
            mensajeAlerta("error", "Aviso", respuesta, "green", "fas fa-info-circle", "6000", "topCenter");
            location.reload();
        },
        error:    function(xhr,err){ 
            mensajeAlerta("error", "Aviso", "No se ha podido guardar el radicado", "red", "fas fa-times-circle", "6000", "topCenter");
            $('#modalRadicado').css('display','block');
        }
    });
}

function imprimirRadicado(idRadicado)
{
    window.open('http://'+location.host+'/imprimirRadicadoSalida/'+idRadicado,'_blank','width=400px, height=400px, scrollbars=yes');
    location.reload();
}

function cargarPublicacion(idRadicado)
{
    $("#modalPublicacion").modal();
    $('#divPublicacion').load('/publicacionce?idRadicadoSalida='+idRadicado);
}

function cargarImagenPublicacion(idRadicadoSalida)
{
    var token = document.getElementById('token').value;
    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        dataType: "json",
        data: {'idRadicadoSalida': idRadicadoSalida},
        url:   'http://'+location.host+'/cargarImagenRadicadoSalida',
        type:  'post',
        beforeSend: function(){
            abrirDistractor();
        },
        success: function(respuesta){
            cerrarDistractor();
            window.open('http://'+location.host+'/'+respuesta[0]['rutaImagenPublicacionVersion']);
        },
        error:    function(xhr,err){ 
            cerrarDistractor();
            alert("Error");
        }
    });
}

function seleccionarFlujo(idRadicado)
{
    $("#modalFlujo").modal();
    $('#divFlujo').load('/flujoselect?idRadicado='+idRadicado);
}

function adicionarRegistros(idTabla, datos)
{
    adicionarFlujo = confirm("¿Desea iniciar este flujo?")
    if(adicionarFlujo) 
    {
        idRadicado = datos[0][3];
        idFlujo = datos[0][0];
        
        var token = document.getElementById('token').value;
        $.ajax({
            headers: {'X-CSRF-TOKEN': token},
            dataType: "json",
            data: {'idRadicado': idRadicado, 'idFlujo': idFlujo},
            url:   'http://'+location.host+'/guardarFlujoRadicadoSalida',
            type:  'post',
            beforeSend: function(){
                abrirDistractor();
            },
            success: function(respuesta){
                cerrarDistractor();
                if(respuesta[0])
                {
                    mensajeAlerta("respuestaRadicadoFlujo", "Aviso", respuesta[1], "green", "fas fa-info-circle", "6000", "topCenter");
                    $("#modalFlujo").modal('hide');
                    tarea = confirm("¿Desea ir al panel de tareas?")
                    if(tarea)
                    {
                        location.replace("http://"+location.host+"/tareas"); 
                    }
                }
                else
                {
                    mensajeAlerta("respuestaRadicadoFlujo", "Aviso", respuesta[1], "orange", "fas fa-times-circle", "6000", "topCenter");
                }
            },
            error:    function(xhr,err){ 
                cerrarDistractor();
                alert("Error");
            }
        });
    }
}