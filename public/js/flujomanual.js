
function cargarVistaPreviaVersion(file, idDocumento) {
  console.log(idDocumento);
  if (idDocumento == '') {
    mensajeAlerta("guardar", "Cuidado", "El documento es obligatorio para realizar la indexación", "orange", "fa fa-times", "10000");
    return;
  }
  url = 'http://' + location.host + '/storage/temporal/' + file['name'];

  var iframe = $("#iframeImagen_" + idDocumento);
  iframe.attr('src', '');
  if (iframe.length) {
    iframe.attr('src', url);
    $("#botonIndexacion_" + idDocumento).html('<button type="button" class="btn btn-success fa fa-info-circle" onclick="indexarDocumento(\'' + file['name'] + '\', \'' + idDocumento + '\')"> Indexar' + file['name'] + '</button>');
  }
}

function indexarDocumento(filename, idDocumento) {
  console.log(idDocumento, filename);
  $("#rutaImagenPublicacionVersion").val(filename);
  $('#modalIndexacion').css('display', 'block');

  var token = document.getElementById('token').value;

  $.ajax({
    headers: { 'X-CSRF-TOKEN': token },
    dataType: "json",
    data: { 'idDocumento': idDocumento },
    url: 'http://' + location.host + '/armarMetadatos',
    type: 'post',
    beforeSend: function () {
      abrirDistractor();
    },
    success: function (respuesta) {
      cerrarDistractor();
      $("#divIndexacion").html(respuesta);
    },
    error: function (xhr, err) {
      cerrarDistractor();
      mensajeAlerta("indexacion", "Error", "Ha ocurrido un problema armando los campos para la indexación", "red", "fa fa-times", "10000");
    }
  });
}

function consultarDatosMetadatos(value) {
  var campos = $("#camposDocumentoMetadato").val();
  var tablaDocumento = $("#tablaDocumento").val();
  var condicion = $("#condicionDocumentoMetadato").val();
  var idDocumento = $("#Documento_idDocumento").val();

  var token = document.getElementById('token').value;
  $.ajax({
    headers: { 'X-CSRF-TOKEN': token },
    dataType: "json",
    data: { 'campos': campos, 'value': value, 'idDocumento': idDocumento, 'tablaDocumento': tablaDocumento, 'condicion': condicion },
    url: 'http://' + location.host + '/consultarDatosMetadatos',
    type: 'post',
    beforeSend: function () {
      abrirDistractor();
    },
    success: function (respuesta) {
      cerrarDistractor();
      if (respuesta == '') {
        mensajeAlerta("datos", "Error", "No se han encontrado datos.", "red", "fa fa-times", "10000");
        // $('#divIndexacion').find('input:text, textarea').val('');
      }
      else {
        for (i = 0; i < respuesta.length; i++) {
          $("#" + respuesta[i]["campo"]).val(respuesta[i]["valor"]);
        }
      }
    },
    error: function (xhr, err) {
      cerrarDistractor();
      mensajeAlerta("guardar", "Error", "Ha ocurrido un problema al consultar los metadatos", "red", "fa fa-times", "10000");
    }
  });
}

function cerrarModal() {
  var modal = document.getElementById('modalIndexacion');
  modal.style.display = "none";
}

function guardarDatos(event, idForm) {

  if ($("#nombreFlujoManual").val() == '') {
    return mensajeAlerta("guardar", "Validar", "El nombre del flujo es obligatorio", "orange", "fa fa-times", "10000");
  }

  if ($("#Dependencia_idDependencia").val() == '') {
    return mensajeAlerta("guardar", "Validar", "La dependencia es obligatoria", "orange", "fa fa-times", "10000");
  }

  if ($("#Serie_idSerie").val() == '') {
    return mensajeAlerta("guardar", "Validar", "La serie es obligatoria", "orange", "fa fa-times", "10000");
  }

  if ($("#SubSerie_idSubSerie").val() == '') {
    return mensajeAlerta("guardar", "Validar", "La sub serie es obligatoria", "orange", "fa fa-times", "10000");
  }

  if ($("#Documento_idDocumento").val() == '') {
    return mensajeAlerta("guardar", "Validar", "El documento es obligatorio", "orange", "fa fa-times", "10000");
  }

  if ($("#Flujo_idFlujo").val() == '') {
    return mensajeAlerta("guardar", "Validar", "El flujo es obligatorio", "orange", "fa fa-times", "10000");
  }

  var formId = '#form-' + idForm;
  var token = document.getElementById('token').value;
  console.log($(formId).serialize());
  $.ajax({
    async: true,
    headers: { 'X-CSRF-TOKEN': token },
    url: $(formId).attr('action'),
    type: $(formId).attr('method'),
    data: $(formId).serialize(),
    dataType: 'html',
    beforeSend: function () {
      abrirDistractor();
    },
    success: function (result) {
      cerrarDistractor();
      mensajeAlerta("guardar", "¡Bien!", result, "green", "fa fa-exclamation-circle", "10000");
      location.replace("http://" + location.host + "/tareas");
    },
    error: function () {
      cerrarDistractor();
      mensajeAlerta("guardar", "Error", "Ha ocurrido un problema guardando el documento", "red", "fa fa-times", "10000");
    }
  });
};