$(document).ready(function(){ 

    companiarol = new Atributos('companiarol','contenedorcomaniaprol','companiarol_');

    companiarol.altura = '35px';
    companiarol.campoid = 'idUsersCompaniaRol';
    companiarol.campoEliminacion = 'eliminarCompaniaRol';

    companiarol.campos   = ['idUsersCompaniaRol', 'Compania_idCompania','Rol_idRol'];
    companiarol.etiqueta = ['input','select','select'];
    companiarol.tipo     = ['hidden','',''];
    companiarol.estilo   = ['','width:400px;','width:400px;'];
    companiarol.clase    = ['','',''];
    companiarol.opciones = ['', valorcompania, ''];
    companiarol.funciones = ['', buscarrol, ''];
    companiarol.sololectura = [false,false,false];
    
    for(var j=0, k = companiaroles.length; j < k; j++)
    {
      companiarol.agregarCampos(JSON.stringify(companiaroles[j]),'L');
      consultarRol(companiaroles[j]['Compania_idCompania'], 'Compania_idCompania'+j, companiaroles[j]['Rol_idRol']);
    }

});

function consultarRol(idCompania, registro, idSelect = '')
{
    reg = registro.replace('Compania_idCompania', '');

    if(idCompania == '')
    {
        mensajeAlerta("errorCompania", "Error", "Debe seleccionar una compañía", "red", "fa fa-times", "10000");
        $("#Rol_idRol"+reg).empty();
        return;
    }

    var token = document.getElementById('token').value;

    $.ajax({
            headers: {'X-CSRF-TOKEN': token},
            dataType: "json",
            data: {'idCompania': idCompania},
            url:   'http://'+location.host+'/consultarRol',
            type:  'post',
            async: false,
            beforeSend: function(){
                abrirDistractor();
            },
            success: function(respuesta){
                cerrarDistractor();

                if(respuesta.length == 0)
                {
                    mensajeAlerta("roles", "Atención", "No se han encontrado roles en esta compañía", "red", "fa fa-times", "10000");
                    $("#Rol_idRol"+reg).empty();
                    return;
                }
                    
                var select = document.getElementById('Rol_idRol'+reg);
                    
                select.options.length = 0;
                var option = '';

                option = document.createElement('option');
                option.value = null;
                option.text = 'Seleccione...';
                select.appendChild(option);

                for (var i = 0; i < respuesta.length ; i++)
                {
                    option = document.createElement('option');
                    option.value = respuesta[i]["idRol"];
                    option.text = respuesta[i]["nombreRol"];
                    option.selected = (idSelect ==  respuesta[i]["idRol"] ? true : false);
                    select.appendChild(option);
                }

            },
            error:    function(xhr,err){ 
                cerrarDistractor();
                mensajeAlerta("indexacion", "Error", "Ha ocurrido un problema buscando los roles de esta compañía", "red", "fa fa-times", "10000");
                $("#Rol_idRol"+reg).empty();
            }
        });
}
