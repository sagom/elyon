$(document).ready( function () {
   
    if($("#idRadicadoIngreso").val() > 0)
    {
        $('#tipoRadicadoIngreso option:not(:selected)').attr('disabled',true);
    }
});

function abrirModalInicial()
{
    $('#modalRadicado').css('display','block');
}

function cerrarModal()
{
    $('#modalRadicado').css('display','none');
}

function guardarDatosIniciales()
{
    tipo = $("#tipoRadicadoIngreso").val();
    asunto = $("#asuntoRadicadoIngreso").val();
    remitente = $("#nombreRemitenteRadicadoIngreso").val();
    identificacion = $("#identificacionRemitenteRadicadoIngreso").val();
    dependencia = $("#Dependencia_idRadicadoIngreso").val();

    if(tipo == '')
    {
        mensajeAlerta("Errortipo", "Aviso", "El tipo es obligatorio", "red", "fas fa-times-circle", "6000", "topCenter");
        return;
    }

    if(asunto == '')
    {
        mensajeAlerta("Errorasunto", "Aviso", "El asunto es obligatorio", "red", "fas fa-times-circle", "6000", "topCenter");
        return;
    }

    if(remitente == '')
    {
        mensajeAlerta("Errornombre", "Aviso", "El remitente es obligatorio", "red", "fas fa-times-circle", "6000", "topCenter");
        return;
    }

    // if(identificacion == '')
    // {
    //     mensajeAlerta("Erroridentificacion", "Aviso", "El identificación es obligatorio", "red", "fas fa-times-circle", "6000", "topCenter");
    //     return;
    // }

    if(dependencia == '')
    {
        mensajeAlerta("Errordependencia", "Aviso", "El destino es obligatorio", "red", "fas fa-times-circle", "6000", "topCenter");
        return;
    }

    var token = document.getElementById('token').value;
    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        dataType: "json",
        data: {'tipo': tipo, 'asunto': asunto, 'remitente': remitente, 'identificacion': identificacion, 'dependencia': dependencia},
        url:   'http://'+location.host+'/guardarRadicadoIngreso',
        type:  'post',
        beforeSend: function(){
            abrirDistractor();
            $('#modalRadicado').css('display','none');
        },
        success: function(respuesta){
            cerrarDistractor();
            $('#modalRadicado').css('display','block');
            mensajeAlerta("error", "Aviso", respuesta, "green", "fas fa-info-circle", "6000", "topCenter");
            location.reload();
        },
        error:    function(xhr,err){ 
            mensajeAlerta("error", "Aviso", "No se ha podido guardar el radicado", "red", "fas fa-times-circle", "6000", "topCenter");
            $('#modalRadicado').css('display','block');
        }
    });
}

function imprimirRadicado(idRadicado)
{
    window.open('http://'+location.host+'/imprimirRadicadoIngreso/'+idRadicado,'_blank','width=400px, height=400px, scrollbars=yes');
    location.reload();
}

function cargarPublicacion(idRadicado)
{
    $("#modalPublicacion").modal();
    $('#divPublicacion').load('/publicacionce?idRadicadoIngreso='+idRadicado);
}

function cargarImagenPublicacion(idRadicadoIngreso)
{
    var token = document.getElementById('token').value;
    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        dataType: "json",
        data: {'idRadicadoIngreso': idRadicadoIngreso},
        url:   'http://'+location.host+'/cargarImagenRadicadoIngreso',
        type:  'post',
        beforeSend: function(){
            abrirDistractor();
        },
        success: function(respuesta){
            cerrarDistractor();
            window.open('http://'+location.host+'/'+respuesta[0]['rutaImagenPublicacionVersion']);
        },
        error:    function(xhr,err){ 
            cerrarDistractor();
            alert("Error");
        }
    });
}

function seleccionarFlujo(idRadicado)
{
    $("#modalFlujo").modal();
    $('#divFlujo').load('/flujoselect?idRadicado='+idRadicado);
}

function adicionarRegistros(idTabla, datos)
{
    adicionarFlujo = confirm("¿Desea iniciar este flujo?")
    if(adicionarFlujo) 
    {
        idRadicado = datos[0][3];
        idFlujo = datos[0][0];
        
        var token = document.getElementById('token').value;
        $.ajax({
            headers: {'X-CSRF-TOKEN': token},
            dataType: "json",
            data: {'idRadicado': idRadicado, 'idFlujo': idFlujo},
            url:   'http://'+location.host+'/guardarFlujoRadicadoIngreso',
            type:  'post',
            beforeSend: function(){
                abrirDistractor();
            },
            success: function(respuesta){
                cerrarDistractor();
                if(respuesta[0])
                {
                    mensajeAlerta("respuestaRadicadoFlujo", "Aviso", respuesta[1], "green", "fas fa-info-circle", "6000", "topCenter");
                    $("#modalFlujo").modal('hide');
                    tarea = confirm("¿Desea ir al panel de tareas?")
                    if(tarea)
                    {
                        location.replace("http://"+location.host+"/tareas"); 
                    }
                }
                else
                {
                    mensajeAlerta("respuestaRadicadoFlujo", "Aviso", respuesta[1], "orange", "fas fa-times-circle", "6000", "topCenter");
                }
            },
            error:    function(xhr,err){ 
                cerrarDistractor();
                alert("Error");
            }
        });
    }
}