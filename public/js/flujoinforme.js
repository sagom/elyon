function consultarFlujo(tipo) {
    estadoFlujo = $("#estadoFlujo").val();
    flujo = $("#Flujo_idFlujo").val();
    fechaFlujoInicio = $("#fechaFlujoInicio").val();
    fechaFlujoFin = $("#fechaFlujoFin").val();
    estadoFlujoComentario = $("#estadoFlujoComentario").val();
    condicion = '';

    if ((fechaFlujoInicio != '' && fechaFlujoFin == '') || fechaFlujoInicio == '' && fechaFlujoFin != '') {
        mensajeAlerta("fechas", "Error", "Debe llenar ambas fechas", "red", "fa fa-times", "10000");
        return;
    }

    if (fechaFlujoInicio != '' && fechaFlujoFin != '')
        condicion = condicion + ((condicion != '' && fechaFlujoInicio != '') ? ' and ' : '') + 'DATE_FORMAT(fechaFlujoTareaRadicadoComentario, "%Y-%m-%d") >= "' + fechaFlujoInicio + '" and DATE_FORMAT(fechaFlujoTareaRadicadoComentario, "%Y-%m-%d") <= "' + fechaFlujoFin + '"';

    if (estadoFlujoComentario != '')
        condicion = condicion + ((condicion != '' && estadoFlujoComentario != '') ? ' and ' : '') + 'estadoFlujoTareaRadicadoComentario = "' + estadoFlujoComentario + '"';

    if (estadoFlujo != '')
        condicion = condicion + ((condicion != '' && estadoFlujo != '') ? ' and ' : '') + 'estadoFlujoTareaRadicado = "' + estadoFlujo + '"';

    if (flujo != '')
        condicion = condicion + ((condicion != '' && flujo != '') ? ' and ' : '') + 'Flujo_idFlujo = "' + flujo + '"';


    window.open('generarflujoinforme?condicion=' + condicion + '&tipo=' + tipo, '_blank', 'width=2500px, height=700px, scrollbars=yes');
}