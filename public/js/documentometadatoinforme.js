function consultarDocumentoMetadato(tipo) {
    documento = $("#Documento_idDocumento").val();
    fechaInicioPublicacion = $("#fechaInicioPublicacion").val();
    fechaFinPublicacion = $("#fechaFinPublicacion").val();
    condicion = '';

    if ((fechaInicioPublicacion != '' && fechaFinPublicacion == '') || fechaInicioPublicacion == '' && fechaFinPublicacion != '') {
        mensajeAlerta("fechas", "Error", "Debe llenar ambas fechas", "red", "fa fa-times", "10000");
        return;
    }

    if (documento == '') {
        mensajeAlerta("fechas", "Error", "Debe seleccionar un documento", "red", "fa fa-times", "10000");
        return;
    }

    if (fechaInicioPublicacion != '' && fechaFinPublicacion != '')
        condicion = condicion + ((condicion != '' && fechaInicioPublicacion != '') ? ' and ' : '') + 'DATE_FORMAT(fechaPublicacionVersion, "%Y-%m-%d") >= "' + fechaInicioPublicacion + '" and DATE_FORMAT(fechaPublicacionVersion, "%Y-%m-%d") <= "' + fechaFinPublicacion + '"';

    if (documento != '')
        condicion = condicion + ((condicion != '' && documento != '') ? ' and ' : '') + 'publicacion.Documento_idDocumento = "' + documento + '"';


    window.open('generardocumentometadatoinforme?condicion=' + condicion + '&tipo=' + tipo + '&documento=' + documento, '_blank', 'width=2500px, height=700px, scrollbars=yes');
}