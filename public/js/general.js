$(document).ready(function () {
    $(".div-responsive-doce").addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12');
    $(".div-responsive-once").addClass('col-lg-11 col-md-11 col-sm-11 col-xs-11');
    $(".div-responsive-diez").addClass('col-lg-10 col-md-10 col-sm-10 col-xs-10');
    $(".div-responsive-nueve").addClass('col-lg-9 col-md-9 col-sm-9 col-xs-9');
    $(".div-responsive-ocho").addClass('col-lg-8 col-md-8 col-sm-8 col-xs-8');
    $(".div-responsive-siete").addClass('col-lg-7 col-md-7 col-sm-7 col-xs-7');
    $(".div-responsive-seis").addClass('col-lg-6 col-md-6 col-sm-6 col-xs-6');
    $(".div-responsive-cinco").addClass('col-lg-5 col-md-5 col-sm-5 col-xs-5');
    $(".div-responsive-cuatro").addClass('col-lg-4 col-md-4 col-sm-4 col-xs-4');
    $(".div-responsive-tres").addClass('col-lg-3 col-md-3 col-sm-3 col-xs-3');
    $(".div-responsive-dos").addClass('col-lg-2 col-md-2 col-sm-2 col-xs-2');
    $(".div-responsive-uno").addClass('col-lg-1 col-md-1 col-sm-1 col-xs-1');
    $(".div-input-responsive").addClass('col-lg-8 col-md-8 col-sm-8');
    $(".label-responsive-dos").addClass('col-lg-2 col-md-2 col-sm-2');
    $(".label-responsive-tres").addClass('col-lg-3 col-md-3 col-sm-3');
    $(".label-responsive-cuatro").addClass('col-lg-4 col-md-4 col-sm-4');

    $('input').attr('autocomplete', 'off');

    $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});

var Titulos = function (titnombreObjeto, titnombreContenedor, titnombreDiv) {

    this.nombre = titnombreObjeto;
    this.contenedor = titnombreContenedor;
    this.contenido = titnombreDiv;
    this.contador = 0;
    this.texto = new Array();
    this.estilo = new Array();
    this.clase = new Array();
};

Titulos.prototype.agregarTitulos = function (grupo, nombreGrupo) {

    // cada que adicione titulos del detalle, los creamos dentro del div detalles
    // y el nombre de cada div, va a set el nombre del contenedor + id del grupo de preguntas
    // este mismo div, nos va a servir para adicionar los div de titulos y los div de datos
    var espacio = document.getElementById('detalle');
    var divpadre = document.createElement('div');
    divpadre.id = this.contenedor + grupo;
    divpadre.setAttribute("class", 'row show-grid');
    divpadre.setAttribute("width", '100%');
    espacio.appendChild(divpadre);

    var espacio = document.getElementById(this.contenedor + grupo);

    var div = document.createElement('div');
    div.id = this.contenido + this.contador;
    div.setAttribute("width", '100%');

    var label = document.createElement('div');
    label.setAttribute("class", this.clase[0]);
    label.setAttribute("style", "width: 1120px;");
    label.innerHTML = nombreGrupo;
    div.appendChild(label);

    for (var i = 0, e = this.texto.length; i < e; i++) {

        var label = document.createElement('div');
        label.setAttribute("class", this.clase[i]);
        label.setAttribute("style", this.estilo[i]);
        label.innerHTML = this.texto[i];
        div.appendChild(label);
    }

    espacio.appendChild(div);
    this.contador++;
}


var Atributos = function (nombreObjeto, nombreContenedor, nombreDiv) {
    this.altura = '35px;';
    this.campoid = '';
    this.campoEliminacion = '';
    this.botonEliminacion = true;

    this.nombre = nombreObjeto;
    this.contenedor = nombreContenedor;
    this.contenido = nombreDiv;
    this.contador = 0;
    this.campos = new Array();
    this.etiqueta = new Array();
    this.tipo = new Array();
    this.estilo = new Array();
    this.clase = new Array();
    this.sololectura = new Array();
    this.completar = new Array();
    this.etiqueta = new Array();
    this.opciones = new Array();
    this.funciones = new Array();
    this.nombreOpcion = new Array();
    this.valorOpcion = new Array();
    this.eventoclick = new Array();
    this.obligatorio = new Array();


};

Atributos.prototype.agregarCampos = function (datos, tipo) {

    var valor;
    if (tipo == 'A')
        valor = datos;
    else
        valor = $.parseJSON(datos);

    var espacio = document.getElementById(this.contenedor);


    var div = document.createElement('div');
    div.id = this.contenido + this.contador;
    div.setAttribute("class", "col-sm-12");
    div.setAttribute("style", "height:" + this.altura + "margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px;");

    // si esta habilitado el parametro de eliminacion de registros del detalle, adicionamos la caneca
    if (this.botonEliminacion) {
        var img = document.createElement('i');
        var caneca = document.createElement('div');
        caneca.id = 'eliminarRegistro' + this.contador;
        caneca.setAttribute('onclick', this.nombre + '.borrarCampos(\'' + div.id + '\',\'' + this.campoEliminacion + '\',\'' + this.campoid + this.contador + '\')');
        caneca.setAttribute("class", "col-md-1");
        caneca.setAttribute("style", "width:40px; height:35px; cursor:pointer; display:inline-block");
        img.setAttribute("class", "glyphicon glyphicon-trash");

        caneca.appendChild(img);
        div.appendChild(caneca);
    }


    for (var i = 0, e = this.campos.length; i < e; i++) {
        if (this.etiqueta[i] == 'input') {
            var input = document.createElement('input');
            input.type = this.tipo[i];
            input.id = this.campos[i] + this.contador;
            input.name = this.campos[i] + '[]';

            input.value = (typeof (valor[(tipo == 'A' ? i : this.campos[i])]) !== "undefined" ? valor[(tipo == 'A' ? i : this.campos[i])] : '');
            input.setAttribute("class", this.clase[i]);
            input.setAttribute("style", this.estilo[i]);
            input.readOnly = this.sololectura[i];
            input.autocomplete = this.completar[i];
            input.setAttribute("onblur", "quitarCaracterEspecial(this.value)");
            if (typeof (this.funciones[i]) !== "undefined") {
                for (var h = 0, c = this.funciones[i].length; h < c; h += 2) {
                    input.setAttribute(this.funciones[i][h], this.funciones[i][h + 1]);
                }
            }

            if (this.obligatorio[i] === true)
                input.setAttribute("required", "required");

            div.appendChild(input);

            // este codigo es despues de que se cree espacio (div contenedor)
            //  $('#fotoInspeccionDetalle0').fileinput({
            //   language: 'es',
            //   uploadUrl: '#',
            //   allowedFileExtensions : ['jpg', 'png','gif'],
            //    initialPreview:
            //     [
            //         '<img style="width:60px; height:60px;" src="imagenes/accidente/firmaaccidente_1.png">'
            //     ],
            //   dropZoneTitle: 'Seleccione la imagen',
            //   removeLabel: '',
            //   uploadLabel: '',
            //   browseLabel: '',
            //   uploadClass: "",
            //   uploadLabel: "",
            //   uploadIcon: "",
            // });
        }
        if (this.etiqueta[i] == 'file') {
            var input = document.createElement('input');
            input.type = 'file';
            input.id = this.campos[i] + this.contador;
            input.name = this.campos[i] + '[]';

            input.filename = '';
            input.setAttribute("class", this.clase[i]);
            input.setAttribute("style", this.estilo[i]);
            input.readOnly = this.sololectura[i];
            if (typeof (this.funciones[i]) !== "undefined") {
                for (var h = 0, c = this.funciones[i].length; h < c; h += 2) {
                    input.setAttribute(this.funciones[i][h], this.funciones[i][h + 1]);
                }
            }

            div.appendChild(input);



        } else if (this.etiqueta[i] == 'textarea') {
            var input = document.createElement('textarea');
            input.id = this.campos[i] + this.contador;
            input.name = this.campos[i] + '[]';

            input.value = valor[(tipo == 'A' ? i : this.campos[i])];
            input.setAttribute("class", this.clase[i]);
            input.setAttribute("style", this.estilo[i]);
            input.setAttribute("onblur", "quitarCaracterEspecial(this.value)");
            if (this.sololectura[i] === true)
                input.setAttribute("readOnly", "readOnly");

            div.appendChild(input);
        } else if (this.etiqueta[i] == 'select') {
            var select = document.createElement('select');
            var option = '';
            select.id = this.campos[i] + this.contador;
            select.name = this.campos[i] + '[]';
            select.setAttribute("style", this.estilo[i]);
            select.setAttribute("class", this.clase[i]);

            if (this.tipo[i] === 'multiple') {
                select.setAttribute("multiple", 'multiple');
            }

            if (typeof (this.funciones[i]) !== "undefined") {
                for (var h = 0, c = this.funciones[i].length; h < c; h += 2) {
                    select.setAttribute(this.funciones[i][h], this.funciones[i][h + 1]);
                }
            }

            if (this.obligatorio[i] === true)
                select.setAttribute("required", "required");

            option = document.createElement('option');
            option.value = '';
            option.text = 'Seleccione...';
            select.appendChild(option);

            for (var j = 0, k = this.opciones[i].length; j < k; j += 2) {
                for (var p = 0, l = this.opciones[i][j].length; p < l; p++) {
                    option = document.createElement('option');
                    option.value = this.opciones[i][j][p];
                    option.text = this.opciones[i][j + 1][p];

                    option.selected = (valor[(tipo == 'A' ? i : this.campos[i])] == this.opciones[i][j][p] ? true : false);
                    select.appendChild(option);
                }
            }

            div.appendChild(select);


        } else if (this.etiqueta[i] == 'checkbox') {
            var divCheck = document.createElement('div');
            divCheck.setAttribute('class', this.clase[i]);
            divCheck.setAttribute('style', this.estilo[i]);

            var inputHidden = document.createElement('input');
            inputHidden.type = 'hidden';
            inputHidden.id = this.campos[i] + this.contador;
            inputHidden.name = this.campos[i] + '[]';
            inputHidden.value = valor[(tipo == 'A' ? i : this.campos[i])];

            divCheck.appendChild(inputHidden);

            var input = document.createElement('input');
            input.type = this.tipo[i];
            input.setAttribute('style', this.estilo[i]);
            input.id = this.campos[i] + 'C' + this.contador;
            input.name = this.campos[i] + 'C' + '[]';
            input.checked = (valor[(tipo == 'A' ? i : this.campos[i])] == 1 ? true : false);


            var func = this.nombre + '.cambiarCheckbox("' + this.campos[i] + '",' + this.contador + ');';
            if (typeof (this.funciones[i]) !== "undefined") {
                for (var h = 0, c = this.funciones[i].length; h < c; h += 2) {
                    // alert(this.funciones[i][h] );
                    // if(this.funciones[i][h] == 'onclick')
                    //     this.funciones[i][h+1] = this.funciones[i][h+1] + ';' + this.nombre+'.cambiarCheckbox("'+this.campos[i]+'",'+this.contador+')'
                    // alert(this.funciones[i][h+1]);
                    func = func + this.funciones[i][h + 1];
                    //input.setAttribute(this.funciones[i][h], this.funciones[i][h+1] );
                }
            }

            input.setAttribute("onclick", func);

            divCheck.appendChild(input);

            div.appendChild(divCheck);

        } else if (this.etiqueta[i] == 'firma') {
            // conlos campos de firma creamos
            // un img para mostrar la firma en base64 y desde la vista
            // tambien debe crear un input hidden para
            // guardar el dato base64 para que el controlador lo guarde
            var firma = document.createElement('img');
            firma.id = this.campos[i] + this.contador;
            firma.src = (typeof (valor[(tipo == 'A' ? i : this.campos[i])]) !== "undefined" ? valor[(tipo == 'A' ? i : this.campos[i])] : '');
            firma.setAttribute("class", this.clase[i]);
            firma.setAttribute("style", this.estilo[i]);
            firma.setAttribute("onclick", "mostrarFirma(" + this.contador + ")");
            if (typeof (this.funciones[i]) !== "undefined") {
                for (var h = 0, c = this.funciones[i].length; h < c; h += 2) {
                    firma.setAttribute(this.funciones[i][h], this.funciones[i][h + 1]);
                }
            }
            div.appendChild(firma);
        } else if (this.etiqueta[i] == 'imagen') {
            // conlos campos de imagen creamos
            // un img para mostrarla  en base64
            var imagen = document.createElement('img');
            imagen.id = this.campos[i] + this.contador;
            imagen.src = (typeof (valor[(tipo == 'A' ? i : this.campos[i])]) !== "undefined" ? 'http://' + location.host + '/imagenes/' + valor[(tipo == 'A' ? i : this.campos[i])] : '');
            // ruta = imagen.src;
            // src = ruta.substring(ruta.length-4);
            // alert(src);
            // if (src == '.pdf');
            // {
            //     imagen.src = 'http://'+location.host+'/images/iconosgenerales/file.png';
            // }
            imagen.setAttribute("placeholder", 'Vista previa de la imagen');
            imagen.setAttribute("class", this.clase[i]);
            imagen.setAttribute("style", this.estilo[i]);
            imagen.setAttribute("onclick", "mostrarImagen('" + 'http://' + location.host + '/imagenes/' + valor[this.campos[i]] + "')");
            if (typeof (this.funciones[i]) !== "undefined") {
                for (var h = 0, c = this.funciones[i].length; h < c; h += 2) {
                    imagen.setAttribute(this.funciones[i][h], this.funciones[i][h + 1]);
                }
            }
            div.appendChild(imagen);
        } else if (this.etiqueta[i] == 'button') {
            var button = document.createElement('button');
            button.type = this.tipo[i];
            button.id = this.campos[i] + this.contador;
            button.name = this.campos[i] + '[]';

            button.value = (typeof (valor[(tipo == 'A' ? i : this.campos[i])]) !== "undefined" ? valor[(tipo == 'A' ? i : this.campos[i])] : '');
            button.setAttribute("class", this.clase[i]);
            button.setAttribute("style", this.estilo[i]);
            if (typeof (this.funciones[i]) !== "undefined") {
                for (var h = 0, c = this.funciones[i].length; h < c; h += 2) {
                    button.setAttribute(this.funciones[i][h], this.funciones[i][h + 1]);
                }
            }

            div.appendChild(button);
        }


    }


    //if(tipo == 'A')
    //  espacio.prepend(div);
    //else
    espacio.appendChild(div);

    this.contador++;

    // var config = {
    //     '.chosen-select': {},
    //     '.chosen-select-deselect': { allow_single_deselect: true },
    //     '.chosen-select-no-single': { disable_search_threshold: 10 },
    //     '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
    //     '.chosen-select-width': { width: "95%" }
    // }
    // for (var selector in config) {
    //     $(selector).chosen(config[selector]);
    // }

}

Atributos.prototype.borrarCampos = function (elemento, campoEliminacion, campoid) {

    if ($("#" + campoid) && ($("#" + campoid).val() == '' || $("#" + campoid).val() == 0 || $("#" + campoid).val() == null)) {
        $("#" + elemento).remove();
    } else {
        if (campoEliminacion && $("#" + campoEliminacion) && $("#" + campoid))
            $("#" + campoEliminacion).val($("#" + campoEliminacion).val() + $("#" + campoid).val() + ',');
        // aux = elemento.parentNode;
        // alert(aux);
        // if(aux );
        $("#" + elemento).remove();
    }

}

Atributos.prototype.borrarTodosCampos = function () {

    for (var posborrar = 0; posborrar < this.contador; posborrar++) {

        this.borrarCampos(this.contenido + posborrar, this.campoEliminacion, this.campoid + posborrar);
    }
    this.contador = 0;
}

Atributos.prototype.cambiarCheckbox = function (campo, registro) {
    document.getElementById(campo + registro).value = document.getElementById(campo + "C" + registro).checked ? 1 : 0;
}

function quitarCaracterEspecial(str) {
    // recibe el texto completo en str
    // Se valida el string que esta recibiendo para que elimine el ENTER y lo deje vacio
    str.replace(/\r\n/g, "");
    // Lo que quede restante en la variable la va a limpiar en la segunda, ya sea tabulaciones
    str.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, '');
    // 1. lo convierte todo a minúsculas y lo guarda en lower
    var lower = str.toLowerCase();
    // 2. lo convierte todo a mayúsculas y lo guarda en upper
    var upper = str.toUpperCase();
    var res = "";
    // 3. con el for recorre el texto original letra por letra
    for (var i = 0; i < lower.length; ++i) {
        // si esa letra en minúscula no es igual a la misma letra en mayúscula lower[i] != upper[i]
        // OR
        // 5. SI es un espacio en blanco o un vacio
        // OR
        // 6. si es un numero de 0 a 9
        // 7. Si es una de las anteriores gurda ese carácter en la variable RES
        // por ultimo devuelve RES
        if (lower[i] != upper[i] || lower[i].trim() === '' || (lower[i].trim() >= 0 && lower[i].trim() <= 9))
            res += str[i];
    }
    return res;
}

function mensajeAlerta(id, titulo, mensaje, color, icono, time = 60000, position = 'topRight') {
    iziToast.show({
        id: id,
        title: titulo,
        titleColor: 'red',
        titleSize: '14',
        message: mensaje,
        theme: 'light', // dark
        color: color, // blue, red, green, yellow
        icon: icono,
        layout: 2,
        balloon: false,
        close: true,
        closeOnEscape: true,
        closeOnClick: true,
        rtl: false,
        position: position, // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
        timeout: 0,
        drag: true,
        overlay: false,
        overlayClose: false,
        overlayColor: 'rgba(0, 0, 0, 0.6)',
        transitionIn: 'fadeInLeft',
        transitionOut: 'fadeOut',
        transitionInMobile: 'fadeInUp',
        transitionOutMobile: 'fadeOutDown',
        timeout: time,
        buttons: {},
        inputs: {}
    });
}

function validarFormulario(event, formulario, tipo = 'vista', modal = '') {

    $("#msj").html('');

    route = $('#form-' + formulario).attr('action');
    token = $("#token").val();
    data = $('#form-' + formulario).serialize();

    sw = true;

    $.ajax({
        async: false,
        url: route,
        headers: { 'X-CSRF-TOKEN': token },
        type: 'POST',
        dataType: 'json',
        data: data,

        beforeSend: function (data) {
            abrirDistractor();
        },

        error: function (respuesta) {
            if (typeof respuesta.responseJSON !== 'undefined') {
                // Concatenamos un mensaje con listas para mostrarle al usuario los errores de validación
                var mensaje = 'Por favor verifique los siguientes valores <br><ul>';
                $.each(respuesta.responseJSON, function (index, value) {
                    mensaje += '<li>' + value + '</li>';
                });
                mensaje += '</ul>';

                // ubicamos el formulario al proncipio para que el usuario vea las validaciones
                document.body.scrollTop = 0; //  Safari
                document.documentElement.scrollTop = 0; //  Chrome, Firefox, IE y Opera

                // ubicamos el mensaje en el div de errores
                $("#msj").html(mensaje);
                $("#msj-error").fadeIn();

                // cambiamos el Switch para indicar que existen errores
                sw = false;
            }

        }

    });

    // si la validación devuelve errores detenemos la accion del formulario para  que no intente guardar
    if (sw == false) {
        cerrarDistractor();
        event.preventDefault();
    } else {
        cerrarDistractor();
        if (tipo == 'modal') {
            window.parent.$('#' + modal).modal('hide');
            window.parent.location.reload();
        }
    }
}

function cerrarDistractor() {
    $("#WindowLoad").remove();
}

function abrirDistractor(mensaje) {
    //eliminamos si existe un div ya bloqueando
    cerrarDistractor();

    //si no enviamos mensaje se pondra este por defecto
    if (mensaje === undefined)
        mensaje = "";

    //centrar imagen gif
    height = 20;//El div del titulo, para que se vea mas arriba (H)
    var ancho = 0;
    var alto = 0;

    //obtenemos el ancho y alto de la ventana de nuestro navegador, compatible con todos los navegadores
    if (window.innerWidth == undefined)
        ancho = window.screen.width;
    else
        ancho = window.innerWidth;

    if (window.innerHeight == undefined)
        alto = window.screen.height;
    else
        alto = window.innerHeight;

    //operación necesaria para centrar el div que muestra el mensaje
    var heightdivsito = alto / 2 - parseInt(height) / 2;//Se utiliza en el margen superior, para centrar

    //imagen que aparece mientras nuestro div es mostrado y da apariencia de cargando
    imgCentro = "<div style='text-align:center;height:" + alto + "px;'><div  style='color:#000;margin-top:" + heightdivsito + "px; font-size:20px;font-weight:bold'>" + mensaje + "</div><img  src='storage/load.gif'></div>";

    //creamos el div que bloquea grande------------------------------------------
    div = document.createElement("div");
    div.id = "WindowLoad"
    div.style.width = ancho + "px";
    div.style.height = alto + "px";
    $("body").append(div);

    //creamos un input text para que el foco se plasme en este y el usuario no pueda escribir en nada de atras
    input = document.createElement("input");
    input.id = "focusInput";
    input.type = "text"

    //asignamos el div que bloquea
    $("#WindowLoad").append(input);

    //asignamos el foco y ocultamos el input text
    $("#focusInput").focus();
    $("#focusInput").hide();

    //centramos el div del texto
    $("#WindowLoad").html(imgCentro);
}

function replaceSpacesWithUnderLine(str, campo) {
    newStr = str.replace(' ', '_');
    $("#" + campo).val(newStr);
}