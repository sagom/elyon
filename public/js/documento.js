$(document).ready(function () {

  metadato = new Atributos('metadato', 'contenedormetadatos', 'metadato_');

  metadato.altura = '35px';
  metadato.campoid = 'idDocumentoMetadato';
  metadato.campoEliminacion = 'eliminarDocumentoMetadato';

  metadato.campos = ['idDocumentoMetadato', 'ordenDocumentoMetadato', 'Metadato_idMetadato', 'campoTablaDocumentoMetadato', 'indiceDocumentoMetadato'];
  metadato.etiqueta = ['input', 'input', 'select', 'select', 'checkbox'];
  metadato.tipo = ['hidden', 'number', '', '', 'checkbox'];
  metadato.estilo = ['', 'width:100px;', 'width:270px;', 'width:270px;', 'width:40px; display:inline-block;'];
  metadato.clase = ['', '', '', '', ''];
  metadato.opciones = ['', '', valormetadato, '', ''];
  metadato.sololectura = [false, false, false, false, false];

  for (var j = 0, k = metadatos.length; j < k; j++) {
    metadato.agregarCampos(JSON.stringify(metadatos[j]), 'L');
    campoTablaDocumentoMetadato = (metadatos[j]['campoTablaDocumentoMetadato']);
    llenarCampos($("#tablaDocumento").val(), campoTablaDocumentoMetadato, j);
  }

  docrol = new Atributos('docrol', 'contenedordocrol', 'docrol_');

  docrol.altura = '35px';
  docrol.campoid = 'idDocumentoRol';
  docrol.campoEliminacion = 'eliminarDocRol';

  docrol.campos = ['idDocumentoRol', 'Rol_idRol'];
  docrol.etiqueta = ['input', 'select'];
  docrol.tipo = ['hidden', ''];
  docrol.estilo = ['', 'width:600px;'];
  docrol.clase = ['', ''];
  docrol.opciones = ['', valorrol];
  docrol.sololectura = [false, false];

  for (var j = 0, k = docroles.length; j < k; j++) {
    docrol.agregarCampos(JSON.stringify(docroles[j]), 'L');
  }

});

function consultarCamposTablaDocumento(tabla) {
  var token = document.getElementById('token').value;

  $.ajax({
    headers: { 'X-CSRF-TOKEN': token },
    dataType: "json",
    data: { 'tabla': tabla },
    url: 'http://' + location.host + '/consultarCamposTablaDocumento',
    type: 'post',
    beforeSend: function () {
      abrirDistractor();
    },
    success: function (respuesta) {
      cerrarDistractor();
      var valores = new Array();
      var nombres = new Array();

      for (var i = 0; i < respuesta.length; i++) {
        valores[i] = respuesta[i]["Campo"];
        nombres[i] = respuesta[i]["Comentario"];
      }

      respuesta = [valores, nombres];
      metadato.opciones[3] = respuesta;
    },
    error: function (xhr, err) {
      cerrarDistractor();
      mensajeAlerta("campostabla", "Error", "Ha ocurrido un problema conectándose a la base de datos para la consulta de los campos", "red", "fa fa-times", "10000");
    }
  });
}

function llenarCampos(tabla, campoTablaDocumentoMetadato, reg) {
  var token = document.getElementById('token').value;
  $.ajax({
    headers: { 'X-CSRF-TOKEN': token },
    dataType: "json",
    data: { 'tabla': tabla },
    url: 'http://' + location.host + '/consultarCamposTablaDocumento',
    type: 'post',
    beforeSend: function () {
      abrirDistractor();
    },
    success: function (respuesta) {
      cerrarDistractor();
      var tablas = respuesta;

      var select = document.getElementById('campoTablaDocumentoMetadato' + reg);
      select.options.length = 0;
      var option = '';

      option = document.createElement('option');
      option.value = null;
      option.text = 'Seleccione...';
      select.appendChild(option);

      for (var i = 0; i < tablas.length; i++) {
        option = document.createElement('option');
        option.value = tablas[i]["Campo"];
        option.text = tablas[i]["Comentario"];
        option.selected = (campoTablaDocumentoMetadato == tablas[i]["Campo"] ? true : false);
        select.appendChild(option);
      }

    },
    error: function (xhr, err) {
      // alert("No se ha podido conectar a la base de datos");
    }
  });
}