function asignarIdDependenciaSerie(idDependencia, idSerie) {
    $("#Dependencia_idDependencia").val(idDependencia);
    $("#Serie_idSerie").val(idSerie);
    $("#SubSerie_idSubSerie").val('');
}

function asignarIdSubSerie(idSubSerie) {
    $("#SubSerie_idSubSerie").val(idSubSerie);
    $("#Documento_idDocumento").val('');
}

function indexarDocumento(filename, idDocumento) {
    $("#rutaImagenPublicacionVersion").val(filename);
    $('#modalIndexacion').css('display', 'block');

    var token = document.getElementById('token').value;

    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        dataType: "json",
        data: { 'idDocumento': idDocumento },
        url: 'http://' + location.host + '/armarMetadatos',
        type: 'post',
        beforeSend: function () {
            abrirDistractor();
        },
        success: function (respuesta) {
            cerrarDistractor();
            $("#divIndexacion").html(respuesta);
        },
        error: function (xhr, err) {
            cerrarDistractor();
            mensajeAlerta("indexacion", "Error", "Ha ocurrido un problema armando los campos para la indexación", "red", "fa fa-times", "10000");
        }
    });
}

function cargarVistaPrevia(file, idDocumento, idDrop) {
    // fileName = file['name'].replace(' ', '_');
    fileName = file['name'].replace(/ +/g, '_');
    fileName = fileName.replace('(', '_');
    fileName = fileName.replace(')', '_');
    url = 'http://' + location.host + '/storage/temporal/' + fileName;
    $("#Documento_idDocumento").val(idDocumento);

    var iframe = $("#iframeImagen_" + idDrop);
    iframe.attr('src', '');
    if (iframe.length) {
        iframe.attr('src', url);
        $("#botonIndexacion" + idDrop).html('<button type="button" class="btn btn-success fa fa-info-circle" onclick="indexarDocumento(\'' + fileName + '\', \'' + idDocumento + '\')"> Indexar documento: ' + fileName + '</button>');
    }
}

function cargarVistaPreviaVersion(file, idPublicacion) {
    // fileName = file['name'].replace(' ', '_');
    fileName = file['name'].replace(/ +/g, '_');
    fileName = fileName.replace('(', '_');
    fileName = fileName.replace(')', '_');
    url = 'http://' + location.host + '/storage/temporal/' + fileName;

    var iframe = $("#iframeImagen_" + idPublicacion);
    iframe.attr('src', '');
    if (iframe.length) {
        iframe.attr('src', url);
        $("#botonIndexacion_" + idPublicacion).html('<input type="radio" name="ubicacionPublicacionVersion" value="Arriba" checked="checked"> Arriba <input type="radio" name="ubicacionPublicacionVersion" value="Abajo"> Abajo<br><button type="button" class="btn btn-success fa fa-info-circle" onclick="cargarArchivoPublicacionVersion(\'' + fileName + '\', \'' + idPublicacion + '\')"> Cargar versión documento: ' + fileName + '</button>');
    }
}

function cargarArchivoPublicacionVersion(filename, idPublicacion) {
    var ubicacionPublicacionVersion = $('input[name=ubicacionPublicacionVersion]:checked').val();

    var token = document.getElementById('token').value;
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        dataType: "json",
        data: { 'filename': filename, 'idPublicacion': idPublicacion, 'ubicacionPublicacionVersion': ubicacionPublicacionVersion },
        url: 'http://' + location.host + '/cargarArchivoPublicacionVersion',
        type: 'post',
        beforeSend: function () {
            abrirDistractor();
        },
        success: function (respuesta) {
            mensajeAlerta("guardar", "¡Bien!", "Versión del documento cargada correctamente", "green", "fa fa-exclamation-circle", "10000");
            cerrarDistractor();
            window.opener.actualizarVistaPrevia(idPublicacion);
            setTimeout(function () {
                window.close();
            }, 5000);

        },
        error: function (xhr, err) {
            cerrarDistractor();
            mensajeAlerta("guardar", "Error", "Ha ocurrido un problema al cargar la nueva versión del documento, su archivo PDF no es compatible con el sistema.", "red", "fa fa-times", "10000");
        }
    });
}

function cerrarModal() {
    var modal = document.getElementById('modalIndexacion');
    modal.style.display = "none";
}

function consultarDatosMetadatos(value) {
    var campos = $("#camposDocumentoMetadato").val();
    var tablaDocumento = $("#tablaDocumento").val();
    var condicion = $("#condicionDocumentoMetadato").val();
    var idDocumento = $("#Documento_idDocumento").val();

    var token = document.getElementById('token').value;
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        dataType: "json",
        data: { 'campos': campos, 'value': value, 'idDocumento': idDocumento, 'tablaDocumento': tablaDocumento, 'condicion': condicion },
        url: 'http://' + location.host + '/consultarDatosMetadatos',
        type: 'post',
        beforeSend: function () {
            abrirDistractor();
        },
        success: function (respuesta) {
            cerrarDistractor();
            if (respuesta == '') {
                mensajeAlerta("datos", "Error", "No se han encontrado datos.", "red", "fa fa-times", "10000");
                // $('#divIndexacion').find('input:text, textarea').val('');
            }
            else {
                for (i = 0; i < respuesta.length; i++) {
                    $("#" + respuesta[i]["campo"]).val(respuesta[i]["valor"]);
                }
            }
        },
        error: function (xhr, err) {
            cerrarDistractor();
            mensajeAlerta("guardar", "Error", "Ha ocurrido un problema al consultar los metadatos", "red", "fa fa-times", "10000");
        }
    });
}

function guardarDatos(event, idForm) {
    var formId = '#form-' + idForm;
    var token = document.getElementById('token').value;
    $.ajax({
        async: true,
        headers: { 'X-CSRF-TOKEN': token },
        url: $(formId).attr('action'),
        type: $(formId).attr('method'),
        data: $(formId).serialize(),
        dataType: 'html',
        beforeSend: function () {
            abrirDistractor();
        },
        success: function (result) {
            $(formId)[0].reset();
            cerrarDistractor();
            mensajeAlerta("guardar", "¡Bien!", result, "green", "fa fa-exclamation-circle", "10000");
            $('#modalIndexacion').css('display', 'none');
            if ($("#Radicado_idRadicadoIngreso").val() != '' || $("#Radicado_idRadicadoSalida").val() != '' || $("#Radicado_idRadicadoInterno").val() != '') {
                window.parent.location.reload();
            }
        },
        error: function () {
            cerrarDistractor();
            mensajeAlerta("guardar", "Error", "Ha ocurrido un problema guardando el documento", "red", "fa fa-times", "10000");
        }
    });
};