$(document).ready(function(){ 

    estructura = new Atributos('estructura','contenedorestructura','estructura_');

    estructura.altura = '35px';
    estructura.campoid = 'idEstructura';
    estructura.campoEliminacion = 'eliminarEstructura';

    estructura.campos   = ['idEstructura', 'Dependencia_idDependencia','Serie_idSerie','SubSerie_idSubSerie','Documento_idDocumento'];
    estructura.etiqueta = ['input','select','select','select','select'];
    estructura.tipo     = ['hidden','','','',''];
    estructura.estilo   = ['','width:300px;','width:300px;','width:300px;','width:300px;'];
    estructura.clase    = ['','','','','','',''];
    estructura.opciones = ['', valordependencia, valorserie, valorsubserie, valordocumento];
    estructura.sololectura = [false,false,false,false,false];
    
    for(var j=0, k = estructuras.length; j < k; j++)
    {
      estructura.agregarCampos(JSON.stringify(estructuras[j]),'L');
    }

});