$(document).ready(function(){ 

    opcion = new Atributos('opcion','contenedoropciones','opcion_');

    opcion.altura = '35px';
    opcion.campoid = 'idRolOpcion';
    opcion.campoEliminacion = 'eliminarOpcion';

    opcion.campos   = ['idRolOpcion', 'Opcion_idOpcion','adicionarRolOpcion','modificarRolOpcion','eliminarRolOpcion','imprimirRolOpcion','aprobarRolOpcion'];
    opcion.etiqueta = ['input','select','checkbox','checkbox','checkbox','checkbox','checkbox'];
    opcion.tipo     = ['hidden','','checkbox','checkbox','checkbox','checkbox','checkbox'];
    opcion.estilo   = ['','width:300px;','width:100px; display:inline-block;','width:100px; display:inline-block;','width:100px; display:inline-block;','width:100px; display:inline-block;','width:100px; display:inline-block;'];
    opcion.clase    = ['','','','','','',''];
    opcion.opciones = ['', valoropcion, '', '', '', '', ''];
    opcion.sololectura = [false,false,false,false,false,false];
    
    for(var j=0, k = opciones.length; j < k; j++)
    {
      opcion.agregarCampos(JSON.stringify(opciones[j]),'L');
    }

});

function abrirModalOpcion()
{
    abrirDistractor();
}