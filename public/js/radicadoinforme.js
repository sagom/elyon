function consultarRadicado(tipo)
{
    tipoRadicado = $("#tipoRadicado").val();
    dependencia = $("#Dependencia_idDependencia").val();
    fechaRadicadoInicio = $("#fechaRadicadoInicio").val();
    fechaRadicadoFin = $("#fechaRadicadoFin").val();
    condicion = '';

    if(tipoRadicado == '')
    {
        mensajeAlerta("tipo", "Error", "Debe seleccionar un tipo de radicado", "red", "fa fa-times", "10000");
        return;
    }

    if ((fechaRadicadoInicio != '' && fechaRadicadoFin == '') || fechaRadicadoInicio == '' && fechaRadicadoFin != '') 
    {
        mensajeAlerta("fechas", "Error", "Debe llenar ambas fechas", "red", "fa fa-times", "10000");
        return;
    }

    if(tipoRadicado == 'Correspondencia enviada')
    {
        modeloRadicado = 'salida';

        if (fechaRadicadoInicio != '' && fechaRadicadoFin != '')
            condicion = condicion + ((condicion !='' && fechaRadicadoInicio !='') ? ' and ' : '') + 'DATE_FORMAT(fechaRadicadoSalida, "%Y-%m-%d") >= "'+fechaRadicadoInicio+'" and DATE_FORMAT(fechaRadicadoSalida, "%Y-%m-%d") <= "'+fechaRadicadoFin+'"';

        if (dependencia != '') 
            condicion = condicion + ((condicion !='' && dependencia != '') ? ' and ' : '') + 'Dependencia_idRadicadoSalida = "'+dependencia+'"';
    }
    else
    {
        modeloRadicado = 'ingreso';

        if (tipoRadicado != '')
            condicion = condicion + 'tipoRadicadoIngreso = "'+tipoRadicado+'"';

        if (dependencia != '') 
            condicion = condicion + ((condicion !='' && dependencia != '') ? ' and ' : '') + 'Dependencia_idRadicadoIngreso = "'+dependencia+'"';

        if (fechaRadicadoInicio != '' && fechaRadicadoFin != '')
            condicion = condicion + ((condicion !='' && fechaRadicadoInicio !='') ? ' and ' : '') + 'DATE_FORMAT(fechaRadicadoIngreso, "%Y-%m-%d") >= "'+fechaRadicadoInicio+'" and DATE_FORMAT(fechaRadicadoIngreso, "%Y-%m-%d") <= "'+fechaRadicadoFin+'"';
    }
    
    window.open('generarradicadoinforme?condicion='+condicion+'&modelo='+modeloRadicado+'&tipo='+tipo,'_blank','width=2500px, height=700px, scrollbars=yes');
}