$(document).ready(function () {

    $.fn.extend({
        treed: function (o) {

            var openedClass = 'glyphicon-minus-sign';
            var closedClass = 'glyphicon-plus-sign';

            if (typeof o != 'undefined') {
                if (typeof o.openedClass != 'undefined') {
                    openedClass = o.openedClass;
                }
                if (typeof o.closedClass != 'undefined') {
                    closedClass = o.closedClass;
                }
            };

            //initialize each of the top levels
            var tree = $(this);
            tree.addClass("tree");
            tree.find('li').has("ul").each(function () {
                var branch = $(this); //li with children ul
                branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
                branch.addClass('branch');
                branch.on('click', function (e) {
                    if (this == e.target) {
                        var icon = $(this).children('i:first');
                        icon.toggleClass(openedClass + " " + closedClass);
                        $(this).children().children().toggle();
                    }
                })
                branch.children().children().toggle();
            });
            //fire event from the dynamically added icon
            tree.find('.branch .indicator').each(function () {
                $(this).on('click', function () {
                    $(this).closest('li').click();
                });
            });
            //fire event to open branch if the li contains an anchor instead of text
            tree.find('.branch>a').each(function () {
                $(this).on('click', function (e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
            //fire event to open branch if the li contains a button instead of text
            tree.find('.branch>button').each(function () {
                $(this).on('click', function (e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
        }
    });

    idsDependencia = $('#idsDependencia').val();
    if (idsDependencia != undefined) {
        arrayidsDependencia = idsDependencia.split(',');
        strIdsDependencia = '';

        for (let i = 0; i < arrayidsDependencia.length; i++) {
            strIdsDependencia += '#' + arrayidsDependencia[i] + ', ';
        }

        strIdsDependencia = strIdsDependencia.substring(0, strIdsDependencia.length - 2);

        $(strIdsDependencia).treed({ openedClass: 'glyphicon-folder-open', closedClass: 'glyphicon-folder-close' });
    }

    dependencia = new Atributos('dependencia', 'contenedordependencia', 'dependencia_');

    dependencia.altura = '35px';
    dependencia.campoid = '';
    dependencia.campoEliminacion = '';
    dependencia.campos = ['Dependencia_idDependencia'];
    dependencia.etiqueta = ['select'];
    dependencia.tipo = ['',];
    dependencia.estilo = ['width: 700px;height:35px;'];
    dependencia.clase = ['chosen-select'];
    dependencia.sololectura = [true];
    dependencia.opciones = [datodependencia];

    documento = new Atributos('documento', 'contenedordocumento', 'documento_');

    documento.altura = '35px';
    documento.campoid = '';
    documento.campoEliminacion = '';
    documento.campos = ['Documento_idDocumento'];
    documento.etiqueta = ['select'];
    documento.tipo = [''];
    documento.estilo = ['width: 700px;height:35px;'];
    documento.clase = ['chosen-select'];
    documento.opciones = [datodocumento];
    documento.sololectura = [true];

    metadato = new Atributos('metadato', 'contenedormetadato', 'metadato_');

    metadato.campos = ['parentesisIniciometadato', 'campometadato', 'operadormetadato', 'valormetadato', 'parentesisFinmetadato', 'conectormetadato'];
    metadato.etiqueta = ['select', 'select', 'select', 'input', 'select', 'select'];
    metadato.tipo = ['', '', '', 'text', '', ''];
    metadato.opciones = [parentesisAbre, datometadato, operador, '', parentesisCierra, conector];
    metadato.estilo = ['width: 120px;height:35px;', 'width: 300px;height:35px;', 'width: 190px;height:35px;', 'width: 220px;height:35px;', 'width: 130px;height:35px;', 'width: 130px;height:35px;'];
    metadato.clase = ['', '', '', '', '', ''];
    metadato.completar = ['off', 'off', 'off', 'off', 'off', 'off'];
    metadato.sololectura = [false, false, false, false, false, false];

});

function ejecutarConsulta() {
    $("#condicionMetadato").val('');
    datos = '';

    for (i = 0; i < metadato.contador; i++) {
        campo = $("#campometadato" + i).val();
        operador = $("#operadormetadato" + i).val();
        valor = $("#valormetadato" + i).val();

        if (campo == '' || typeof campo == 'undefined') {
            alert('Verifique que el campo esté diligenciado');
            return;
        }

        if (operador == '' || typeof operador == 'undefined') {
            alert('Verifique que el operador esté diligenciado');
            return;
        }

        if (valor == '' || typeof valor == 'undefined') {
            alert('Verifique que el valor esté diligenciado');
            return;
        }

        valor = (operador == 'like') ? '|**' + valor + '**|' : '|' + valor + '|';

        if ($("#operadormetadato" + i)) {
            datos +=
                $("#parentesisIniciometadato" + i).val() +
                'valorPublicacionDocumentoMetadato ' +
                operador + ' ' +
                valor + ' ' +
                $("#parentesisFinmetadato" + i).val() + '' +
                $("#conectormetadato" + i).val() + ' ';
        }
    }

    condicion = document.getElementById("condicionMetadato").value += datos.substring(0, datos.length - 1);

    idDocumento = '';
    idDependencia = '';

    for (var i = 0; i < documento.contador; i++) {
        if ($("#Documento_idDocumento" + i) && typeof $("#Documento_idDocumento" + i).val() != 'undefined') {
            idDocumento += $("#Documento_idDocumento" + i).val() + ',';
        }
    }

    for (var i = 0; i < dependencia.contador; i++) {
        if ($("#Dependencia_idDependencia" + i) && typeof $("#Dependencia_idDependencia" + i).val() != 'undefined') {
            idDependencia += $("#Dependencia_idDependencia" + i).val() + ',';
        }
    }

    if (idDependencia != '')
        condicion = condicion + '&idDependencia=' + idDependencia.substring(0, idDependencia.length - 1);

    if (idDocumento != '')
        condicion = condicion + '&idDocumento=' + idDocumento.substring(0, idDocumento.length - 1);


    if (condicion != '') {
        window.open('http://' + location.host + '/consultapublicacion?condicion=' + condicion);
    }
    else {
        alert("Debe ingresar al menos una condición.");
    }
}

function limpiarCondicion() {
    $("#contenedormetadato").empty();
    metadato.contador = 0;
}

function asignarValoresMaestros(id, tipo) {
    switch (tipo) {
        case 'D':
            $("#Dependencia_idDependencia").val(id);
            break;
        case 'S':
            $("#Serie_idSerie").val(id);
            break;
        case 'SS':
            $("#SubSerie_idSubSerie").val(id);
            break;
        case 'Dc':
            $("#Documento_idDocumento").val(id);
            consultarDocumentosEstructura();
            break;
    }
}

function consultarDocumentosEstructura() {
    idDependencia = $("#Dependencia_idDependencia").val();
    idSerie = $("#Serie_idSerie").val();
    idSubSerie = $("#SubSerie_idSubSerie").val();
    idDocumento = $("#Documento_idDocumento").val();
    condGeneral = ($("#condGeneral").val() == '') ? '|' : $("#condGeneral").val();

    url = 'http://' + location.host + '/consultapublicaciondocumento/' + idDependencia + '/' + idSerie + '/' + idSubSerie + '/' + idDocumento + '/' + condGeneral;

    var iframe = $("#iframeDocumento");
    iframe.attr('src', '');
    if (iframe.length) {
        iframe.attr('src', url);
    }
}

function cerrarModal(tipo) {
    var modal = document.getElementById(tipo);
    modal.style.display = "none";
}

function imprimirPublicacion(archivo) {
    doc = window.open('http://' + location.host + '/' + archivo);
    doc.print();
    doc.close;
}

function abrirModalCorreo() {
    $("#modalCorreo").css('display', 'block');
}

function enviarCorreo() {
    rutaAdjunto = $("#rutaImagenPublicacionVersion").val();
    direccion = $("#direccionCorreo").val();
    asunto = $("#asuntoCorreo").val();
    mensaje = $("#mensajeCorreo").val();

    var token = $('#token').val();
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        dataType: "json",
        data: { 'rutaAdjunto': rutaAdjunto, 'direccion': direccion, 'asunto': asunto, 'mensaje': mensaje },
        url: 'http://' + location.host + '/enviarCorreoPublicacion',
        type: 'post',
        beforeSend: function () {
            $('#modalCorreo').css('display', 'none');
            $('#modalConsulta').css('display', 'none');
            abrirDistractor();
        },
        success: function (respuesta) {
            cerrarDistractor();
            mensajeAlerta("guardar", "¡Bien!", respuesta, "green", "fa fa-info-circle", "10000");
            $('#modalConsulta').css('display', 'block');
        },
        error: function (xhr, err) {
            cerrarDistractor();
            $('#modalConsulta').css('display', 'block');
            $('#modalCorreo').css('display', 'block');
            mensajeAlerta("guardar", "Error", "Ha ocurrido un problema eliminando la publicación", "red", "fa fa-times", "10000");
        }
    });
}

function eliminarPublicacion(publicacion) {
    var borrar = confirm("¿Realmente desea eliminarlo?");
    if (borrar) {
        var token = $('#token').val();
        $.ajax({
            headers: { 'X-CSRF-TOKEN': token },
            dataType: "json",
            data: { publicacion: publicacion },
            url: 'http://' + location.host + '/eliminarPublicacion/' + publicacion,
            type: 'get',
            beforeSend: function () {
                abrirDistractor();
            },
            success: function (respuesta) {
                cerrarDistractor();
                mensajeAlerta("guardar", "¡Bien!", respuesta, "green", "fa fa-info-circle", "10000");
                $('#modalConsulta').css('display', 'none');
                consultarDocumentosEstructura();
            },
            error: function (xhr, err) {
                cerrarDistractor();
                mensajeAlerta("guardar", "Error", "Ha ocurrido un problema eliminando la publicación", "red", "fa fa-times", "10000");
            }
        });
    }
}

function cargarVersionPublicacion(publicacion) {
    window.open("http://" + location.host + "/publicacionversion/" + publicacion, '_blank');
}

function actualizarVistaPrevia(idPublicacion) {
    var token = $('#token').val();
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        dataType: "json",
        data: { idPublicacion: idPublicacion },
        url: 'http://' + location.host + '/consultarRutaVistaPrevia',
        type: 'post',
        beforeSend: function () {
            abrirDistractor();
        },
        success: function (respuesta) {
            var iframe = window.parent.$("#iframeImagen");
            iframe.attr('src', '');
            if (iframe.length) {
                iframe.attr('src', respuesta[0]['rutaImagenPublicacionVersion']);
            }
        },
        error: function (xhr, err) {
            cerrarDistractor();
            mensajeAlerta("guardar", "Error", "Ha ocurrido un problema actualizando la vista previa", "red", "fa fa-times", "10000");
        }
    });
}