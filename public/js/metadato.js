function activarOpciones(tipo)
{
    if(tipo == '' || tipo == 'Texto' || tipo == 'Numero' || tipo == 'Fecha' || tipo == 'Hora' || tipo == 'Editor')
    {
        $("#opcionMetadato").attr('readonly', true);
        $("#opcionMetadato").val('');
    }
    else
    {
        $("#opcionMetadato").attr('readonly', false);
        $("#opcionMetadato").val('');
    }
}