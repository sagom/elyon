$(document).ready(function(){
    if($("#idRadicadoInterno").val() > 0)
        buscarDependenciaUsuario($("#UsersDestinatario_idRadicadoInterno").val());
});

function abrirModalInicial()
{
    $('#modalRadicado').css('display','block');
}

function cerrarModal()
{
    $('#modalRadicado').css('display','none');
}

function buscarDependenciaUsuario(idUsuario)
{
    if(idUsuario == '')
    {
        mensajeAlerta("ErrorRemitente", "Aviso", "El destinatario es obligatorio", "red", "fas fa-times-circle", "6000", "topCenter");
        $("#nombreDependenciaDestinatario").val('');
        return;
    }
    
    var token = document.getElementById('token').value;
    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        dataType: "json",
        data: {'idUsuario': idUsuario},
        url:   'http://'+location.host+'/buscarDependenciaUsuario',
        type:  'post',
        beforeSend: function(){
            abrirDistractor();
        },
        success: function(respuesta){
            cerrarDistractor();
            $("#nombreDependenciaDestinatario").val(respuesta[0]['nombreDependencia']);
        },
        error:    function(xhr,err){ 
            cerrarDistractor();
            alert("Error");
        }
    });
}

function guardarDatosIniciales()
{
    asunto = $("#asuntoRadicadoInterno").val();
    destinatario = $("#UsersDestinatario_idRadicadoInterno").val();

    if(asunto == "")    
    {
        mensajeAlerta("ErrorAsunto", "Aviso", "El asunto es obligatorio", "red", "fas fa-times-circle", "6000", "topCenter");
        return;
    }

    if(destinatario == "")
    {
        mensajeAlerta("ErrorRemitente", "Aviso", "El destinatario es obligatorio", "red", "fas fa-times-circle", "6000", "topCenter");
        return;
    }
    
    var token = document.getElementById('token').value;
    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        dataType: "json",
        data: {'asunto': asunto, 'destinatario': destinatario},
        url:   'http://'+location.host+'/guardarRadicadoInterno',
        type:  'post',
        beforeSend: function(){
            abrirDistractor();
            $('#modalRadicado').css('display','none');
        },
        success: function(respuesta){
            cerrarDistractor();
            $('#modalRadicado').css('display','block');
            mensajeAlerta("error", "Aviso", respuesta, "green", "fas fa-info-circle", "6000", "topCenter");
            location.reload();
        },
        error:    function(xhr,err){ 
            mensajeAlerta("error", "Aviso", "No se ha podido guardar el radicado", "red", "fas fa-times-circle", "6000", "topCenter");
            $('#modalRadicado').css('display','block');
        }
    });
}

function imprimirRadicado(idRadicado)
{
    window.open('http://'+location.host+'/imprimirRadicadoInterno/'+idRadicado,'_blank','width=2500px, height=700px,scrollbars=no');
    location.reload();
}

function cargarPublicacion(idRadicado)
{
    $("#modalPublicacion").modal();
    $('#divPublicacion').load('/publicacionce?idRadicadoInterno='+idRadicado);
}

function cargarImagenPublicacion(idRadicadoInterno)
{
    var token = document.getElementById('token').value;
    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        dataType: "json",
        data: {'idRadicadoInterno': idRadicadoInterno},
        url:   'http://'+location.host+'/cargarImagenRadicadoInterno',
        type:  'post',
        beforeSend: function(){
            abrirDistractor();
        },
        success: function(respuesta){
            cerrarDistractor();
            window.open('http://'+location.host+'/'+respuesta[0]['rutaImagenPublicacionVersion']);
        },
        error:    function(xhr,err){ 
            cerrarDistractor();
            alert("Error");
        }
    });
}