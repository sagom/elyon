$(document).ready(function(){ 

    retencion = new Atributos('retencion','contenedorretencion','retencion_');

    retencion.altura = '35px';
    retencion.campoid = 'idRetencion';
    retencion.campoEliminacion = 'eliminarRetencion';

    retencion.campos   = ['idRetencion', 'Dependencia_idDependencia','Serie_idSerie','SubSerie_idSubSerie','Documento_idDocumento', 'tiempoGestionRetencion', 'tiempoCentralRetencion', 'soporteRetencion', 'disposicionFinalRetencion', 'procedimientoRetencion', 'estadoRetencion'];
    retencion.etiqueta = ['input','select','select','select','select', 'input', 'input', 'select', 'select', 'input', 'select'];
    retencion.tipo     = ['hidden','','','','', 'number', 'number', '', '', 'text', ''];
    retencion.estilo   = ['','width:300px;','width:300px;','width:300px;','width:300px;','width:100px;','width:100px;','width:200px;','width:200px;','width:400px;','width:200px;'];
    retencion.clase    = ['','','','','','','','','','','','',''];
    retencion.opciones = ['', valordependencia, valorserie, valorsubserie, valordocumento, '', '', soporte, disposicion, '', estado];
    retencion.sololectura = [false,false,false,false,false,false,false,false,false,false,false];
    
    for(var j=0, k = retenciones.length; j < k; j++)
    {
      retencion.agregarCampos(JSON.stringify(retenciones[j]),'L');
    }

});