$(document).ready( function () {
    
    if($("#idMovimientoPQRSF").val() > 0)
    {
        $('#tipoMovimientoPQRSF option:not(:selected)').attr('disabled',true);
    }
});

function ejecutarRespuesta()
{
    $("#divrespuesta").css('display', 'block');
    $("#divfecharespuesta").css('display', 'block');
    $("#divusuariorespuesta").css('display', 'block');
    $("#divadjuntorespuesta").css('display', 'block');

    $("#nombreSolicitanteMovimientoPQRSF").attr("readonly",true);
    $("#telefonoSolicitanteMovimientoPQRSF").attr("readonly",true);
    $("#movilSolicitanteMovimientoPQRSF").attr("readonly",true);
    $("#correoSolicitanteMovimientoPQRSF").attr("readonly",true);
    $("#mensajeMovimientoPQRSF").attr("readonly",true);
}