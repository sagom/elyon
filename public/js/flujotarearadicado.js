$(document).ready(function () {
    encuesta = new Atributos('encuesta', 'contenedorencuestas', 'encuesta_');

    encuesta.altura = '35px';
    encuesta.botonEliminacion = false;

    encuesta.campos = ['preguntaEncuestaFlujoTareaComentario', 'respuestaEncuestaFlujoTareaComentario', 'observacionEncuestaFlujoTareaComentario'];
    encuesta.etiqueta = ['input', 'select', 'textarea'];
    encuesta.tipo = ['text', '', ''];
    encuesta.estilo = ['width:400px;display:inline-block', 'width:200px;display:inline-block', 'width:400px;display:inline-block'];
    encuesta.clase = ['', '', ''];
    encuesta.opciones = ['', respuestas, ''];
    encuesta.sololectura = [true, false, false];
});

function cargarVersionPublicacion() {
    window.open("http://" + location.host + "/publicacionversion/" + $("#idPublicacion").val(), '_blank');
}

function actualizarVistaPrevia() {
    var token = $('#token').val();
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        dataType: "json",
        data: { idPublicacion: $("#idPublicacion").val() },
        url: 'http://' + location.host + '/consultarRutaVistaPrevia',
        type: 'post',
        beforeSend: function () {
            abrirDistractor();
        },
        success: function (respuesta) {
            var iframe = window.parent.$("#iframeImagenRadicado");
            iframe.attr('src', '');
            if (iframe.length) {
                iframe.attr('src', respuesta[0]['rutaImagenPublicacionVersion']);
            }
        },
        error: function (xhr, err) {
            cerrarDistractor();
            mensajeAlerta("guardar", "Error", "Ha ocurrido un problema actualizando la vista previa", "red", "fa fa-times", "10000");
        }
    });
}

function modalEncuesta(registro) {
    encuesta.borrarTodosCampos();
    $("#modalEncuesta").css('display', 'block');
    encuestas = document.getElementById("encuestaFlujoTarea").value;
    encuestasLlenas = document.getElementById("respuestaFlujoTareaRadicadoComentario").value;

    if (encuestasLlenas) {
        JSON.parse(encuestasLlenas).map(function (obj) {
            var objTransform = {
                preguntaEncuestaFlujoTareaComentario: obj.preguntaEncuestaFlujoTareaComentario,
                respuestaEncuestaFlujoTareaComentario: obj.respuestaEncuestaFlujoTareaComentario,
                observacionEncuestaFlujoTareaComentario: obj.observacionEncuestaFlujoTareaComentario
            };
            encuesta.agregarCampos(JSON.stringify(objTransform), 'L');
        });
    } else {
        JSON.parse(encuestas).map(function (obj) {
            var objTransform = {
                preguntaEncuestaFlujoTareaComentario: obj.encuestaFlujoTarea,
                respuestaEncuestaFlujoTareaComentario: '',
                observacionEncuestaFlujoTareaComentario: ''
            };
            encuesta.agregarCampos(JSON.stringify(objTransform), 'L');
        });
    }
}

function cerrarModalEncuesta() {
    $("#modalEncuesta").css('display', 'none');
}

function setDataJsonFlujoTarea() {
    var div = document.getElementById("contenedorencuestas");
    var inputs = div.getElementsByTagName("input");
    var select = div.getElementsByTagName("select");
    var textareas = div.getElementsByTagName("textarea");

    var rechazar = false;
    var lleno = true;
    var json = [];
    for (var i = 0; i < inputs.length; i++) {
        if (select[i]?.value == 'No') {
            rechazar = true;
        }

        if (select[i]?.value == '') {
            lleno = false;
        }

        json.push({
            preguntaEncuestaFlujoTareaComentario: inputs[i].value,
            respuestaEncuestaFlujoTareaComentario: select[i]?.value ?? '',
            observacionEncuestaFlujoTareaComentario: textareas[i].value,
        });
    }

    if (!lleno) {
        return alert('Debe llenar todas las respuestas');
    }

    if (rechazar) {
        $("#aceptarFlujoRadicado").prop('disabled', true);
    } else {
        $("#aceptarFlujoRadicado").prop('disabled', false);
    }

    $("#respuestaFlujoTareaRadicadoComentario").val(JSON.stringify(json));
    $("#rechazarFlujoRadicado").val(rechazar);

    $("#modalEncuesta").css('display', 'none');

}