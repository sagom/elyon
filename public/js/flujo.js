$(document).ready(function () {

  tarea = new Atributos('tarea', 'contenedortareas', 'tarea_');

  tarea.altura = '35px';
  tarea.campoid = 'idFlujoTarea';
  tarea.campoEliminacion = 'eliminarFlujoTarea';

  tarea.campos = ['idFlujoTarea', 'buttonFlujoTarea', 'nombreFlujoTarea', 'Users_idFlujoTarea', 'permiteAnularFlujoTarea', 'encuestaFlujoTarea'];
  tarea.etiqueta = ['input', 'button', 'input', 'select', 'checkbox', 'input'];
  tarea.tipo = ['hidden', 'button', 'text', '', 'checkbox', 'hidden'];
  tarea.estilo = ['', 'width:40px', 'width:400px;', 'width:250px;', 'width:100px; display:inline-block', ''];
  tarea.clase = ['', 'fa fa-edit', '', '', '', ''];
  tarea.opciones = ['', '', '', usuario, '', ''];
  tarea.funciones = ['', abrirModalEncuesta, '', '', '', ''];
  tarea.sololectura = [false, false, false, false, false, true];

  for (var j = 0, k = tareas.length; j < k; j++) {
    console.log(tareas[j], 'oeoeoe');
    var dataEncuestaFlujoTarea = tareas[j]?.encuestaFlujoTarea || [];
    var json = [];
    for (var i = 0; i < dataEncuestaFlujoTarea.length; i++) {
      json.push({ encuestaFlujoTarea: dataEncuestaFlujoTarea[i].encuestaFlujoTarea });
    };
    console.log(json);

    tarea.agregarCampos(JSON.stringify(tareas[j]), 'L');
    console.log(json, JSON.stringify(json));
    $("#encuestaFlujoTarea" + j).val(JSON.stringify(json));
  }

  encuesta = new Atributos('encuesta', 'contenedorencuestas', 'encuesta_');

  encuesta.altura = '35px';
  encuesta.campoEliminacion = 'eliminarFlujoTarea';

  encuesta.campos = ['dataEncuestaFlujoTarea'];
  encuesta.etiqueta = ['input'];
  encuesta.tipo = ['text'];
  encuesta.estilo = ['width:700px;'];
  encuesta.clase = [''];
  encuesta.sololectura = [false];
});

function ejecutarTarea(idFlujoTareaRadicado, tipoRadicado, idFlujo, tipoEjecucion, idRadicado) {
  $("#modalTarea").css('display', 'block');
  $('#divTarea').load('/ejecutartarea/' + idFlujoTareaRadicado + '/' + tipoRadicado + '/' + idFlujo + '/' + tipoEjecucion + '/' + idRadicado);
}

function cerrarModal() {
  $("#modalTarea").css('display', 'none');
}

function modalEncuesta(registro) {
  encuesta.borrarTodosCampos();
  var reg = registro.replace("buttonFlujoTarea", "");
  var dataEncuestaFlujoTarea = tareas[reg]?.encuestaFlujoTarea || [];
  $("#registroTareaFlujo").val(reg);
  $("#modalEncuesta").css('display', 'block');

  for (var i = 0; i < dataEncuestaFlujoTarea.length; i++) {
    var obj = { dataEncuestaFlujoTarea: dataEncuestaFlujoTarea[i].encuestaFlujoTarea };
    encuesta.agregarCampos(JSON.stringify(obj), 'L');
  };
}

function cerrarModalEncuesta() {
  $("#registroTareaFlujo").val('');
  $("#modalEncuesta").css('display', 'none');
}

function setDataJsonFlujoTarea() {
  var div = document.getElementById("contenedorencuestas");
  var inputs = div.getElementsByTagName("input");

  var json = [];
  for (var i = 0; i < inputs.length; i++) {
    json.push({ encuestaFlujoTarea: inputs[i].value });
  }

  $("#encuestaFlujoTarea" + document.getElementById("registroTareaFlujo").value).val(JSON.stringify(json));

  $("#modalEncuesta").css('display', 'none');

}

function filterList(idBuscador, idLista) {
  input = document.getElementById(`${idBuscador}`).value.toLowerCase();
  var lis = document.querySelectorAll(`#${idLista} li`);
  lis.forEach(function (li) {
    var textoLi = li.textContent.toLowerCase();
    if (textoLi.includes(input)) {
      li.style.display = 'block';
    } else {
      li.style.display = 'none';
    }
  });
}