function buscarClasificacionDetalle(idClasificacion, valorClasificacionDetalle = '')
{
    var token = document.getElementById('token').value;
    $.ajax({
            headers: {'X-CSRF-TOKEN': token},
            dataType: "json",
            data: {'idClasificacion': idClasificacion},
            url:   'http://'+location.host+'/buscarClasificacionDetalle',
            type:  'post',
            beforeSend: function(){
              abrirDistractor();
            },
            success: function(respuesta){
                cerrarDistractor();

                var select = document.getElementById('ClasificacionTareaDetalle_idClasificacionTareaDetalle');
                select.options.length = 0;
                var option = '';

                option = document.createElement('option');
                option.text = '- Seleccione la sub clasificación -';
                select.appendChild(option);

                for (var i = 0; i < respuesta.length ; i++)
                {
                    option = document.createElement('option');
                    option.value = respuesta[i]["idClasificacionTareaDetalle"];
                    option.text = respuesta[i]["nombreClasificacionTareaDetalle"];
                    option.selected = (valorClasificacionDetalle ==  respuesta[i]["idClasificacionTareaDetalle"] ? true : false);
                    select.appendChild(option);                    
                }

            },
            error:  function(xhr,err){ 
                cerrarDistractor();
                mensajeAlerta("error", "Error", "Ha ocurrido un error cargando las sub clasificaciones", "green", "fa fa-exclamation-circle", "10000");
            }
    });
}

function buscarUsuarioResponsable(idDependenciaResponsable, valorDependencia = '')
{
    var token = document.getElementById('token').value;
    $.ajax({
            headers: {'X-CSRF-TOKEN': token},
            dataType: "json",
            data: {'idDependenciaResponsable': idDependenciaResponsable},
            url:   'http://'+location.host+'/buscarUsuarioResponsable',
            type:  'post',
            beforeSend: function(){
              abrirDistractor();
            },
            success: function(respuesta){
                cerrarDistractor();

                var select = document.getElementById('Users_idResponsable');
                select.options.length = 0;
                var option = '';

                option = document.createElement('option');
                option.value = '';
                option.text = '- Seleccione el responsable -';
                select.appendChild(option);

                for (var i = 0; i < respuesta.length ; i++)
                {
                    option = document.createElement('option');
                    option.value = respuesta[i]["id"];
                    option.text = respuesta[i]["name"];
                    option.selected = (valorDependencia ==  respuesta[i]["id"] ? true : false);
                    select.appendChild(option);                    
                }

            },
            error:  function(xhr,err){ 
                cerrarDistractor();
                mensajeAlerta("error", "Error", "Ha ocurrido un error cargando las sub clasificaciones", "green", "fa fa-exclamation-circle", "10000");
            }
    });
}

function cambiarEstado(tipo = '')
{
    location.href= 'http://'+location.host+"/tarea?tipo="+tipo;
}

function mostrarDashboard()
{
    window.open('tareadashboard','dashboard','width=5000,height=5000,scrollbars=yes, status=0, toolbar=0, location=0, menubar=0, directories=0');
}